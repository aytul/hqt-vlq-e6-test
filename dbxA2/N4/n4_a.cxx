#include "n4_a.h"
#include "TParameter.h"
#include "WIndices.C"
#include <TRandom.h>

int N4dbxA::plotVariables(int sel) {
 dbxA::plotVariables (sel);
  int retval=0;
  const Double_t small=1e-4;
  char tmp[128], idxn[128];
  sprintf(tmp,"c4-%s",cname);
  sprintf(idxn,"inv_mass-%s",cname);

  // I define my canvases here
  TCanvas *c1 = new TCanvas("inv_mass_plots", "c1", 150, 10, 650, 500);
  c1->SetFillColor(0); c1->SetBorderMode(0); c1->Divide(2,2,small,small);
  c1->Draw();

  TCanvas *c2 = new TCanvas("aux_plots", "c2", 10, 10, 450, 500);
  c2->SetFillColor(0); c2->SetBorderMode(0); c2->Divide(2,2,small,small);
  c2->Draw();

  TCanvas* c3 = new TCanvas("angle_plots", "c3", 10, 10, 250, 500);
  c3->SetFillColor(0); c3->SetBorderMode(0); c3->Divide(1,3,small,small);
  c3->Draw();


//------------------CANVAS 1----------------
// invariant mass  plots
//
     c1->cd(1);
     gPad->SetRightMargin(small);
     vinvm1->SetLineColor(4);
     vinvm2->SetLineColor(2);
     vinvms->Draw();
     vinvm1->Draw("same");
     vinvm2->Draw("same");
// Delta inv mass
     c1->cd(2);
     gPad->SetRightMargin(small);
     jinvm->Draw();
//  Multiplicities
    gPad->SetRightMargin(small);
    c1->cd(3);
    mmult->SetLineColor(2);
    mmult->Draw();
    jmult->Draw("same");
// Efficiencies    
    c1->cd(4);
    gPad->SetRightMargin(small);
    eff->Draw();
//------------------CANVAS 2----------------
// PT leptons
     c2->cd(1);
     gPad->SetRightMargin(small);
     mupt->SetLineColor(2);
     mupt->Draw();

// PT jets
     c2->cd(2);
     gPad->SetRightMargin(small);
     jetpt->SetLineColor(4);
     jetpt->Draw();
     ptmss->SetLineColor(1);
     ptmss->Draw("same");
// boson invm
     c2->cd(3);
     gPad->SetRightMargin(small);
//     gPad->SetGridx();
//     gPad->SetLogy();
     binvm->SetLineColor(2);
    jjinvm->Draw();
     binvm->Draw("same");
// the beast in the s channel
     c2->cd(4);
     hinvm->SetLineColor(4);
     hinvm->Draw();
//------------------CANVAS 3----------------
// eta-phi leptons
     c3->cd(1);
     gPad->SetRightMargin(small);
     lepeta->SetLineColor(2);
     lepeta->Draw();
     lepphi->Draw("same");
// eta-phi jets
     c3->cd(2);
     gPad->SetRightMargin(small);
     jeteta->SetLineColor(2);
     jeteta->Draw();
     jetphi->Draw("same");
// eta-phi b-jets
     c3->cd(3);
     gPad->SetRightMargin(small);
     veta->SetLineColor(2);
     veta->Draw();
     vphi->Draw("same");
 
/////////  SAVE /////////////////
  char aaa[64];
  sprintf (aaa, "%s-1.png",cname); c1->SaveAs(aaa);
  sprintf (aaa, "%s-2.png",cname); c2->SaveAs(aaa);
  sprintf (aaa, "%s-3.png",cname); c3->SaveAs(aaa);
  sprintf (aaa, "%s-1.eps",cname); c1->SaveAs(aaa);
  sprintf (aaa, "%s-2.eps",cname); c2->SaveAs(aaa);
  sprintf (aaa, "%s-3.eps",cname); c3->SaveAs(aaa);

 return retval;  
}

int N4dbxA:: readAnalysisParams() {
  int retval=0;
  TString CardName=cname;
          CardName+="-card.txt";

   minwidthw  = ReadCard(CardName,"minwidthw" ,20.);
   minwidthz  = ReadCard(CardName,"minwidthz" ,25.);
   maxetconem = ReadCard(CardName,"maxetconem",6.0);
   minptm     = ReadCard(CardName,"minptm",15.);
   maxetam    = ReadCard(CardName,"maxetam",2.5);
   minptj     = ReadCard(CardName,"minptj", 15.);
   maxetaj    = ReadCard(CardName,"maxetaj",2.5);
   minmassmm  = ReadCard(CardName,"minmassmm", 5.);
   mindrmm    = ReadCard(CardName,"mindrmm",2.0);
   maxmet     = ReadCard(CardName,"maxmet", 30.);
   mindrjm    = ReadCard(CardName,"mindrjm",0.4);
   maxresn4m  = ReadCard(CardName,"maxresn4m", 0.25);
   n4peak     = ReadCard(CardName,"n4peak" , 0.);
   minwidthn4 = ReadCard(CardName,"minwidthn4", 1e4);
   chargeveto= Int_t(ReadCard(CardName,"chargeveto",0));
   minnjetsw = Int_t(ReadCard(CardName,"minnjetsw",2));
// Minimum number of jets to reconstruct the hadronic Ws from: 
   if ( minnjetsw!=1 && minnjetsw!=2 ) minnjetsw = 2;

   TRGe = ReadCard(CardName,"TRGe",1);
   TRGm = ReadCard(CardName,"TRGm",1);

	
	////// PUT ANALYSIS PARAMETERS INTO .ROOT //////////////
	
	TParameter<double> *minwidthw_tmp=new TParameter<double> ("minwidthw", minwidthw);
	TParameter<double> *minwidthz_tmp=new TParameter<double> ("minwidthz", minwidthz);
	TParameter<double> *maxetconem_tmp=new TParameter<double> ("maxetconem", maxetconem);
	TParameter<double> *minptm_tmp=new TParameter<double> ("minptm", minptm);
	TParameter<double> *maxetam_tmp=new TParameter<double> ("maxetam", maxetam);
	TParameter<double> *minptj_tmp=new TParameter<double> ("minptj", minptj);
	TParameter<double> *maxetaj_tmp=new TParameter<double> ("maxetaj", maxetaj);
	TParameter<double> *minmassmm_tmp=new TParameter<double> ("minmassmm", minmassmm);
	TParameter<double> *mindrmm_tmp=new TParameter<double> ("mindrmm", mindrmm);
	TParameter<double> *maxmet_tmp=new TParameter<double> ("maxmet", maxmet);
	TParameter<double> *mindrjm_tmp=new TParameter<double> ("mindrjm", mindrjm);
	TParameter<double> *maxresn4m_tmp=new TParameter<double> ("maxresn4m", maxresn4m);
	TParameter<double> *n4peak_tmp=new TParameter<double> ("n4peak", n4peak);
	TParameter<double> *minwidthn4_tmp=new TParameter<double> ("minwidthn4", minwidthn4);
	TParameter<double> *chargeveto_tmp=new TParameter<double> ("chargeveto", chargeveto);
	TParameter<double> *minnjetsw_tmp=new TParameter<double> ("minnjetsw", minnjetsw);
	//TParameter<double> *mqhxmin_tmp=new TParameter<double> ("mqhxmin", mqhxmin);
	//TParameter<double> *mqhxmax_tmp=new TParameter<double> ("mqhxmax", mqhxmax);
	//TParameter<double> *maxMuPtCone_tmp=new TParameter<double> ("maxMuPtCone", maxMuPtCone);
	//TParameter<double> *maxMuEtCone_tmp=new TParameter<double> ("maxMuEtCone", maxMuEtCone);
	//TParameter<double> *maxElPtCone_tmp=new TParameter<double> ("maxElPtCone", maxElPtCone);
	//TParameter<double> *maxElEtCone_tmp=new TParameter<double> ("maxElEtCone", maxElEtCone);
	TParameter<double> *TRGe_tmp=new TParameter<double> ("TRGe", TRGe);
	TParameter<double> *TRGm_tmp=new TParameter<double> ("TRGm", TRGm);
	
	minwidthw_tmp->Write("minpte");
    minwidthz_tmp->Write("minptm");
    maxetconem_tmp->Write("maxetae");
    minptm_tmp->Write("maxetam");
	maxetam_tmp->Write("minmet");
	minptj_tmp->Write("minptj");
	maxetaj_tmp->Write("maxetaj");
	minmassmm_tmp->Write("mindrjm");
	mindrmm_tmp->Write("mindrje");
	maxmet_tmp->Write("minptj1");
	mindrjm_tmp->Write("minptj2");
	maxresn4m_tmp->Write("minEj2");
	n4peak_tmp->Write("minetaj2");
	minwidthn4_tmp->Write("mindrjj");
	chargeveto_tmp->Write("minetajj");
	minnjetsw_tmp->Write("maxdeltam");
	//mqhxmin_tmp->Write("mqhxmin");
	//mqhxmax_tmp->Write("mqhxmax");
	//maxMuPtCone_tmp->Write("maxMuPtCone");
	//maxMuEtCone_tmp->Write("maxMuEtCone");
	//maxElPtCone_tmp->Write("maxElPtCone");
	//maxElEtCone_tmp->Write("maxElEtCone");
	TRGe_tmp->Write("TRGe");
	TRGm_tmp->Write("TRGm");

  return retval;
}

int N4dbxA:: printEfficiencies() {
  int retval=0;
  PrintEfficiencies(eff);
  return retval;
}

int N4dbxA:: initGRL() {
  int retval=0;
  grl_cut=true;
  return retval;
}

int N4dbxA:: bookAdditionalHistos() {
//additional histograms are defined here
 hmmult   = new TH1F("hmmult" ,"Muon multiplicity",20,-0.5,19.5);
 hmptnu4     = new TH1F("hmptnu4" ,"Muon P_{T}",100,0.,50.);
 hmetanu4    = new TH1F("hmetanu4" ,"Muon #eta",100,-5.,5.);
 hmphinu4    = new TH1F("hmphinu4" ,"#phi of muons",100,-5.,5.);

  hjmult = new TH1F("hjmult" ,"Jet multiplicity",20,-0.5,60.5);
  hjptnu4   = new TH1F("hjptnu4" ,"Jet P_{T}",80,0.,400.);
  hjetanu4  = new TH1F("hjetanu4" ,"Jet #eta",100,-5.,5.);
  hjphinu4  = new TH1F("hjphinu4" ,"#phi of jets",100,-5.,5.);
  hjmnu4    = new TH1F("hjmnu4" ,"Mass of jets",80, 0.,20.);

   vinvms = new TH1F("vinvms","Invariant Mass for #nu",100,0.,500.);
   vinvm1 = new TH1F("vinvm1","Invariant Mass for #nu_{1}",100,0.,500.);
   vinvm2 = new TH1F("vinvm2","Invariant Mass for #nu_{2}",100,0.,500.);
  vinvm2D = new TH2F("vinvm2D","#nu_{1} vs. #nu_{2} invariant Mass",80,0.,400.,80,0.,400.);
   dvinvm = new TH1F("dvinvm","#Delta(Invariant Mass for #nu_{4})",100,0.,500.);
    vmres = new TH1F("vmres" ,"#DeltaM/<M> for #nu_{4}",100,0,2.);
   
 hvsvinvm = new TH2F("hvsvinvm","H vs. #nu_{2} invariant Mass",112,100.,1220.,80,0.,400.);
    hinvm = new TH1F("hinvm" ,"Invariant Mass for H",112,100.,1220.);
    jinvm = new TH1F("jinvm" ,"Invariant Mass for J",60,0.,300.);
   jjinvm = new TH1F("jjinvm","Invariant Mass for jj",80,0.,400.);
    binvm = new TH1F("binvm" ,"Invariant Mass for #mu#mu (Z/b)",80,0.,400.);
    zinvm = new TH1F("zinvm" ,"Invariant Mass for #mu^{+}#mu^{-}",80,0.,400.);
    //
      y12 = new TH1F("y12",  "#sqrt(y_{12})",40, 0, 2.0); 
   lepeta = new TH1F("lepeta" ,"eta of leptons",100,-5.,5.);
   lepphi = new TH1F("lepphi" ,"phi of leptons",100,-5.,5.);
  lepchrg = new TH1F("lepchrg","Charge_{lep1}xCharge_{lep2}",3,-1.5,1.5);
    jetpt = new TH1F("jetpt" ,"Jet P_{T}",80,0.,400.);
   jeteta = new TH1F("jeteta" ,"Jet #eta",100,-5.,5.);
   jetphi = new TH1F("jetphi" ,"#phi of jets",100,-5.,5.);
  jetpthi = new TH1F("jetpthi" ,"P_{T} of hardest jet",80,0.,400.);
 jetetahi = new TH1F("jetetahi","#eta of hardest jet",100,-5.,5.);
     veta = new TH1F("veta" ,"#eta of #nu_{4}",100,-5.,5.);
     vphi = new TH1F("vphi" ,"#phi of #nu_{4}",100,-5.,5.);
     mupt = new TH1F("mupt"  ,"Muon P_{T}",125,0.,250.);
   muptlo = new TH1F("muptlo","Muon P_{T} for 2^{nd} hardest #mu",125,0.,250.);
   mupthi = new TH1F("mupthi","Muon P_{T} for hardest #mu",125,0.,250.);
   mminvm = new TH1F("mminvm","Invariant Mass for #mu#mu",80,0.,400.);
   mmdelr = new TH1F("mmdelr","#DeltaR(#mu_{1}#mu_{2})",60,0,6);
   mumudr = new TH1F("mumudr","#DeltaR(#mu_{1}#mu_{2})",60,0,6);
 mumudphi = new TH1F("mumudphi","|#Delta#phi(#mu_{1}#mu_{2}|)",32,0,3.2);
    ptmss = new TH1F("ptmss" ,"Missing P_{T}",50,0.,250.);
  ptmssig = new TH1F("ptmssig","Missing P_{T} Significance",50,0.,10.);
ptmssVsMt = new TH2F("ptmssVsMt","Missing P_T vs. M_T",100,0,250,40,0.01,160.01);
ptmssVsMv = new TH2F("ptmssVsMv","Missing P_T vs. M_v",100,0,250,40,0.01,160.01);
 ptmuVsMt = new TH2F("ptmuVsMt","#mu P_{T} vs. M_{T}",80,0,400,40,0.01,160.01);
 ptmuVsMv = new TH2F("ptmuVsMv","#mu P_{T} vs. M_{v}",40,0,400,40,0.01,160.01);
ptmssVsSET=new TH2F("ptmssVsSET","Miss. P_{T} Sig. vs. #SigmaE_{T}",50,0,10.,80,100,900);
    jmult = new TH1F("jmult" ,"Jet multiplicity",20,-0.5,19.5);
   bjmult = new TH1F("bjmult","b-tagged Jet multiplicity",10,-0.5,9.5);
   elmult = new TH1F("elmult","Electron multiplicity after cuts",5,-0.5,4.5);
    mmult = new TH1F("mmult" ,"Muon multiplicity",20,-0.5,19.5);
  mu1Wcos = new TH2F("mu1Wcos" ,"Cos #theta_{W#mu1}",50,-1,1,50,-1,1);
  mu2Wcos = new TH2F("mu2Wcos" ,"Cos #theta_{W#mu2}",50,-1,1,50,-1,1);
    dvphi = new TH1F("dvphi" ,"#Delta#phi_{#nu4 cands}",35,-3.5,3.5);
   jet2mu = new TH2F("jet2mu" , "#DeltaR from 1st #mu to closest jet vs. jet P_{T}",
                    60, 0, 6.0, 100, 0, 200.);
  jet2mu2 = new TH2F("jet2mu2", "#DeltaR from 2nd #mu to closest jet vs. jet P_{T}",
                    60, 0, 6.0, 100, 0, 200.);
  sjet2mu = new TH2F("sjet2mu", "#DeltaR from 1st #mu to closest non-W jet vs. jet P_{T}",
                    60, 0, 6.0, 100, 0, 200.);
    jmudr = new TH2F("jmudr", "#DeltaR(muons to closest jet of P_{T}>20 GeV)",
                    40, 0, 4.0, 40, 0, 4.0);

    hsv0p_mass  = new TH1F("hsv0p_mass" , "sv0p_mass", 50, 0., 5.);
    hsv0p_n2t   = new TH1F("hsv0p_n2t"  , "sv0p_n2t",  40, 0.,20.);
    hsv0p_svok  = new TH1F("hsv0p_svok" , "sv0p_svok", 50, 0., 1.);
    hsv0p_ntrk  = new TH1F("hsv0p_ntrk" , "sv0p_ntrk", 20, 0., 10.);
    hsv0p_ntrkv = new TH1F("hsv0p_ntrkv", "sv0p_ntrkv",20, 0., 10.);
    hsv0p_ntrkj = new TH1F("hsv0p_ntrkj", "sv0p_ntrkj",20, -1, 1.);
    hsv0p_efrc  = new TH1F("hsv0p_efrc" , "sv0p_efrc" ,50, 0., 1.);

    hmuet= new TH1F ("hmuet", "mu etcone", 50, 0, 25);
    hmupt= new TH1F ("hmupt", "mu ptcone", 50, 0, 25);

    hjet_vtxfrac0b= new TH1F("hjet_vtxfrac0j","4 jets vtx frac - 0b", 60, -1.5, 1.5);
    hjet_vtxfrac1b= new TH1F("hjet_vtxfrac1j","4 jets vtx frac - 1b", 60, -1.5, 1.5);

  vinvms->SetXTitle("<m_{W#mu}> (GeV)");
  vinvm1->SetXTitle("m_{W#mu, 1} (GeV)");
  vinvm2->SetXTitle("m_{W#mu, 2} (GeV)");
 vinvm2D->SetXTitle("m_{W#mu, 1} (GeV)");
 vinvm2D->SetYTitle("m_{W#mu, 2} (GeV)");
hvsvinvm->SetXTitle("m_{H} (GeV)");  hvsvinvm->SetYTitle("m_{W#mu, 2} (GeV)");
 dvinvm->SetXTitle("#Deltam_{#nu} (GeV)");
  vmres->SetXTitle("#Deltam_{#nu} / <m_{#nu}>");
  hinvm->SetXTitle("m_{W#muW#mu} (GeV)");
  jinvm->SetXTitle("m_{j} (GeV)");
 jjinvm->SetXTitle("m_{jj} (GeV)");
   mupt->SetXTitle("P_{T}^{#mu} (GeV)");
 muptlo->SetXTitle("P_{T}^{#mu} (GeV)");
 mupthi->SetXTitle("P_{T}^{#mu} (GeV)");
  jetpt->SetXTitle("P_{T}^{jet} (GeV)");
  ptmss->SetXTitle("P_{T}^{miss} (GeV)");
  ptmssVsMv->SetXTitle("P_{T}^{miss} (GeV)");
  ptmssVsMv->SetYTitle("m^{visible} (GeV)");
  ptmssVsMt->SetXTitle("P_{T}^{miss} (GeV)");
  ptmssVsMt->SetYTitle("m^{transverse} (GeV)");
 jet2mu->SetYTitle("GeV");
jet2mu2->SetYTitle("GeV");
sjet2mu->SetYTitle("GeV");
 jmudr->SetXTitle("#DeltaR(1^{st} #mu to closest jet)");
 jmudr->SetYTitle("#DeltaR(2^{nd} #mu to closest jet)");
 dvphi->SetXTitle("#Delta#phi_{#nu4 cands}");

  eff= new TH1F("eff","selection efficiencies",15,0.5,15.5);
 int retval=0;
  return retval;
}

/////////////////////////
int N4dbxA::makeAnalysis(vector<dbxMuon> muons, vector<dbxElectron> electrons,
                               vector<dbxJet> jets, TVector2 met, evt_data anevt) {

  int retval=0;
  int cur_cut=1;

  eff->GetXaxis()->SetBinLabel(cur_cut,"all");
  eff->Fill(cur_cut, anevt.mcevt_weight);
  cur_cut++;


// force no GRL for MC events
  if ((TRGm>1) || (TRGe>1)) { grl_cut=false; }

// add the goodrunsList xml for this analysis
  eff->GetXaxis()->SetBinLabel(cur_cut,"GRL");
  if ( grl_cut) {
    bool pass = isGoodRun(anevt.run_no,anevt.lumiblk_no);
    if ( !pass ) return cur_cut;
  }
  eff->Fill(cur_cut, anevt.mcevt_weight);
  cur_cut++;

  bool trgpass=false;
  if ( TRGm>0 ) { if (anevt.TRG_m)  trgpass=true; }

  eff->GetXaxis()->SetBinLabel(cur_cut,TString("TRiGger "));
  if (!trgpass) return cur_cut;
    eff->Fill(cur_cut, anevt.mcevt_weight);
    cur_cut++;

// Fill kinematics of muon&jets before any cut
  hmmult->Fill(muons.size(), anevt.mcevt_weight);
  for (UInt_t i=0; i<muons.size(); i++) {
     hmptnu4->Fill(fabs(muons.at(i).lv().Pt()), anevt.mcevt_weight);
    hmetanu4->Fill(muons.at(i).lv().Eta(),anevt.mcevt_weight);
    hmphinu4->Fill(muons.at(i).lv().Phi(), anevt.mcevt_weight);
  } 

  hjmult->Fill(jets.size(), anevt.mcevt_weight);
  for (UInt_t i=0; i<jets.size(); i++) {
     hjptnu4->Fill(fabs(jets.at(i).lv().Pt()), anevt.mcevt_weight);
    hjetanu4->Fill(jets.at(i).lv().Eta(), anevt.mcevt_weight);
    hjphinu4->Fill(jets.at(i).lv().Phi(), anevt.mcevt_weight);
      hjmnu4->Fill(jets.at(i).lv().M(),  anevt.mcevt_weight);
  }

// clean wrt the primary vertex number of tracks
  eff->GetXaxis()->SetBinLabel(cur_cut,"Vtx Trks>4");
  if (anevt.vxp_maxtrk_no<5) return cur_cut;
    eff->Fill(cur_cut, anevt.mcevt_weight);
    cur_cut++;

// clean wrt bad jets
  eff->GetXaxis()->SetBinLabel(cur_cut,"bad jets>0");
  if (anevt.badjet) return cur_cut;
    eff->Fill(cur_cut, anevt.mcevt_weight);
    cur_cut++;

  vector<dbxElectron> loose_eles;
  vector<dbxMuon> muonsAfterInitialCuts;
  muonsAfterInitialCuts.clear();
  for (UInt_t i=0; i<muons.size(); i++) {
     bool mu_jet_overlap=false;
     TLorentzVector mu4p = muons.at(i).lv();

     for (UInt_t j=0; j<jets.size(); j++) {
       TLorentzVector j4p=jets.at(j).lv();
// Don't consider jets below GeV for overlap removal (20GeV)
       if ( j4p.Pt() < 20 ) continue;
       double deta= fabs(mu4p.Eta() - jets.at(j).EmEta() );
       double dphi= fabs(mu4p.Phi() - jets.at(j).EmPhi() );
       if (dphi > M_PI) dphi=2*M_PI-dphi;
       double dR=sqrt((dphi*dphi)+(deta*deta));
       if(dR < 0.4) { mu_jet_overlap=true; break;}
     }
// muons in the event             
     if (   (muons.at(i).EtCone() < maxMuEtCone) 
         && (muons.at(i).PtCone() < maxMuPtCone) 
         && (fabs(mu4p.Pt())  > minptm)
         && (fabs(mu4p.Eta()) < maxetam)
         && (!mu_jet_overlap)
        )  { 
        muonsAfterInitialCuts.push_back(muons.at(i));
     }
   }  // end of muon loop


// loose electrons
  for (UInt_t i=0; i<electrons.size(); i++) {
    TLorentzVector el4p = electrons[i].lv();
    if (  (fabs(el4p.Pt())  > minpte)
       && (fabs(el4p.Eta()) < maxetae)
       && ((fabs(el4p.Eta())>1.52) || (fabs(el4p.Eta())<1.37))
       ) {
         loose_eles.push_back(electrons.at(i) );
       }
  }         

// Fill the jets vector with jets of pt > jptcut && |eta| < jetacut
//  and count jets that are b-tagged
  Int_t nbjet=0;

  vector<dbxJet> jetsAfterInitialCuts;
  jetsAfterInitialCuts.clear();   
  Float_t maxjetpt = 0, maxjeteta = -9.9;

// jets to be used in QCD calculation
   for (UInt_t i=0; i<jets.size(); i++) {
      TLorentzVector jet4p = jets.at(i).lv();
      if ( maxjetpt < jet4p.Pt() ) {
         maxjetpt = jet4p.Pt();
         maxjeteta = jet4p.Eta();
      }

      bool e_jet_overlap=false;
      for (UInt_t j=0; j<loose_eles.size(); j++) {
         double deta= fabs(loose_eles.at(j).TrkEta() - jets.at(i).EmEta() );
         double dphi= fabs(loose_eles.at(j).TrkPhi() - jets.at(i).EmPhi() );
         if (dphi > M_PI) dphi=2*M_PI-dphi;
         double dR=sqrt((dphi*dphi)+(deta*deta));
         if(dR < 0.2) e_jet_overlap=true;
      }

      if (  (fabs(jet4p.Pt())  > minptj)
          && (fabs(jets.at(i).EmEta()) < maxetaj)
          && (jet4p.E() > 0)
          && (!e_jet_overlap)
         )  {
            jetsAfterInitialCuts.push_back(jets.at(i));
      }
   }


// b-jet selection
   int nbj=0;
   for (UInt_t i=0; i<jetsAfterInitialCuts.size(); i++) {
     if ((jetsAfterInitialCuts.at(i).Flavor()>5.85) ) nbj++;
   }

// cut on the # of muons
  eff->GetXaxis()->SetBinLabel(cur_cut,"2 muons pt");
  if (muonsAfterInitialCuts.size() < 2) return retval;
    eff->Fill(cur_cut, anevt.mcevt_weight);
    cur_cut++;


// count the # of jets
   jmult->Fill(jetsAfterInitialCuts.size(), anevt.mcevt_weight);
   bjmult->Fill(nbjet, anevt.mcevt_weight);
   jetpthi->Fill(maxjetpt, anevt.mcevt_weight); 
   jetetahi->Fill(maxjeteta, anevt.mcevt_weight);

// cut on the # of jets 
  eff->GetXaxis()->SetBinLabel(cur_cut,"4 jets pt,eta");
  if (jetsAfterInitialCuts.size() < 4) return retval;
    eff->Fill(cur_cut,anevt.mcevt_weight);
    cur_cut++;

// Missing Et cut
  ptmss->Fill(met.Mod());
  eff->GetXaxis()->SetBinLabel(cur_cut,"MET");
  if ( met.Mod() > maxmet ) return retval;
     eff->Fill(cur_cut, anevt.mcevt_weight);
     cur_cut++;

// TRIGGER Match 
  bool trgmatch = false;
  if ( TRGm>0 ) trgmatch=( (muonsAfterInitialCuts.at(0).TopMuInTrigger()) || (muonsAfterInitialCuts.at(1).TopMuInTrigger()) );
  eff->GetXaxis()->SetBinLabel(cur_cut,"Trig. match");
  if ( !trgmatch ) return retval;
     eff->Fill(cur_cut, anevt.mcevt_weight);
     cur_cut++;


// ............ do NOT remove .....
  dbxA::makeAnalysis (muonsAfterInitialCuts, electrons, jetsAfterInitialCuts, met, anevt);
// ............ do NOT remove .....


// Construct Z
  TLorentzVector theMuon1 = muonsAfterInitialCuts.at(0).lv();
  TLorentzVector theMuon2 = muonsAfterInitialCuts.at(1).lv();
  TLorentzVector Zmm= theMuon1 + theMuon2;
  mupthi->Fill(theMuon1.Pt());
  muptlo->Fill(theMuon2.Pt());
  mupt->Fill( theMuon1.Pt() ) ;   mupt->Fill( theMuon2.Pt() );
  lepeta->Fill( theMuon1.Eta() ); lepeta->Fill( theMuon2.Eta() );
  lepphi->Fill( theMuon1.Phi() ); lepphi->Fill( theMuon2.Phi() );
  lepchrg->Fill( muonsAfterInitialCuts.at(0).q()*muonsAfterInitialCuts.at(1).q() );

  mminvm->Fill( Zmm.M() );
  if ( fabs(Zmm.M()-m_mz) > minwidthz ) mmdelr->Fill( (theMuon1).DeltaR(theMuon2) );

  
// Jet Related plots here
  for (UInt_t j=0; j<jetsAfterInitialCuts.size(); j++) {
     hsv0p_mass->Fill(jetsAfterInitialCuts.at(j).Sv0mass(), anevt.mcevt_weight);
     hsv0p_n2t->Fill(jetsAfterInitialCuts.at(j).Sv0nt2(), anevt.mcevt_weight);
     hsv0p_svok->Fill(jetsAfterInitialCuts.at(j).Sv0svok(), anevt.mcevt_weight);
     hsv0p_ntrk->Fill(jetsAfterInitialCuts.at(j).Sv0ntrk(), anevt.mcevt_weight);
     hsv0p_ntrkv->Fill(jetsAfterInitialCuts.at(j).Sv0ntrkv(), anevt.mcevt_weight);
     hsv0p_ntrkj->Fill(jetsAfterInitialCuts.at(j).Sv0ntrkj(), anevt.mcevt_weight);
     hsv0p_efrc->Fill(jetsAfterInitialCuts.at(j).Sv0efrc(), anevt.mcevt_weight);
  
     TLorentzVector tlvJets=jetsAfterInitialCuts.at(j).lv();
     jinvm->Fill ( tlvJets.M() );
     jetpt->Fill ( tlvJets.Pt()  );
     jeteta->Fill( tlvJets.Eta() );
     jetphi->Fill( tlvJets.Phi() );
  }
  
// Reconstruct hadronic Ws
  vector<TLorentzVector> tlvW1;
  vector<TLorentzVector> tlvW2;
  vector<Double_t> y121;
  vector<Double_t> y122;   
  tlvW1.clear();  tlvW2.clear();   
  y121.clear(); y122.clear();   
  TLorentzVector Wcand1, Wcand2; 
  Double_t y1, y2;
  vector<WIndices> WIndx;
  WIndx.clear();

  for (UInt_t i1=0; i1<(jetsAfterInitialCuts.size()-3); i1++)  {
     for (UInt_t j1=i1+1; j1<jetsAfterInitialCuts.size(); j1++)  {

        for (UInt_t i2=i1+1; i2<(jetsAfterInitialCuts.size()-1); i2++) {
           for (UInt_t j2=i2+1; j2<jetsAfterInitialCuts.size(); j2++)  {

                 TLorentzVector jeti1=jetsAfterInitialCuts.at(i1).lv();
                 TLorentzVector jetj1=jetsAfterInitialCuts.at(j1).lv();
                 TLorentzVector jeti2=jetsAfterInitialCuts.at(i2).lv();
                 TLorentzVector jetj2=jetsAfterInitialCuts.at(j2).lv();
                 if ( i2!=j1 && j2!=j1 ) {                          
                    Wcand1 = jeti1 + jetj1;
                    Wcand2 = jeti2 + jetj2;
                    tlvW1.push_back(Wcand1);
                    tlvW2.push_back(Wcand2);
                    y1 = TMath::Min( jeti1.Perp(jetj1.Vect()),
                                     jetj1.Perp(jeti1.Vect()) ) / Wcand1.Pt() ;
                    y2 = TMath::Min( jeti2.Perp(jetj2.Vect()),
                                     jetj2.Perp(jeti2.Vect()) ) / Wcand2.Pt() ;
                    y121.push_back(y1);
                    y122.push_back(y2);
                         
                    WIndices w(i1, j1, i2, j2);
                    WIndx.push_back(w);
                         
                 } // not to have same jets in W1 and W2                 
           } //j2
        } //i2
     } // j1
  } // i1
 

  // One W or both W from one jet, 
  if ( minnjetsw == 1 ) {  
     for (UInt_t i1=0;  i1 < jetsAfterInitialCuts.size(); i1++)  {
        for (UInt_t i2=0; i2< jetsAfterInitialCuts.size(); i2++)  {
           for (UInt_t j2 = 0; j2 < jetsAfterInitialCuts.size(); j2++) {
              if ( (i1 != i2) && (i1 != j2) && (i2 != j2) && (i2 < j2)) {
                 TLorentzVector jetsi1=jetsAfterInitialCuts.at(i1).lv();
                 TLorentzVector jetsi2=jetsAfterInitialCuts.at(i2).lv();
                 TLorentzVector jetsj2=jetsAfterInitialCuts.at(j2).lv();
                 // one W from single jet
                 Wcand1 = jetsi1;
                 Wcand2 = jetsi2+jetsj2;
                 tlvW1.push_back(Wcand1); y121.push_back(-1);
                 tlvW2.push_back(Wcand2); y122.push_back(-1);
                 WIndices w(i1, -1, i2, j2);
                 WIndx.push_back(w);
                 // both W from single jet
                 if ( ((i1==2 && i2==0 && j2==1)) || ((i1==1 && i2==0)) || (i1==0) ) {
                    tlvW1.push_back(jetsi2); y121.push_back(-1);
                    tlvW2.push_back(jetsj2); y121.push_back(-1);
                    WIndices ww(i2, -1, j2, -1);
                    WIndx.push_back(ww);
                 } 
              }
           }
        }
     }      
  }

  // select the case with the best 2 Ws.
  Float_t sum_diff = 0;
  Float_t max_diff = 99999999.999;    
  UInt_t sel=0;
  for (UInt_t i=0; i<tlvW1.size(); i++) {
     sum_diff = pow(tlvW1.at(i).M()-m_mw, 2) + pow(tlvW2.at(i).M()-m_mw, 2);
     if ( max_diff > sum_diff ) {
         max_diff=sum_diff;
         sel=i;
     }
  }
  jjinvm->Fill( tlvW1.at(sel).M(), anevt.mcevt_weight );
  jjinvm->Fill( tlvW2.at(sel).M(), anevt.mcevt_weight );
  
// print indices
  vector<Int_t> wres = (WIndx.at(sel)).getWIndices();
  for ( UInt_t i=0; i < wres.size(); i++) { 
     if ( wres.at(i) < 0 )  std::cout << "W reconstructed from a single jet : " << i <<  std::endl;
  }    
  if (nbjet==0) {
     hjet_vtxfrac0b->Fill(jetsAfterInitialCuts.at(wres.at(0)).JetVtxF());
     hjet_vtxfrac0b->Fill(jetsAfterInitialCuts.at(wres.at(1)).JetVtxF());
     hjet_vtxfrac0b->Fill(jetsAfterInitialCuts.at(wres.at(2)).JetVtxF());
     hjet_vtxfrac0b->Fill(jetsAfterInitialCuts.at(wres.at(3)).JetVtxF());
  } else {
     hjet_vtxfrac1b->Fill(jetsAfterInitialCuts.at(wres.at(0)).JetVtxF());
     hjet_vtxfrac1b->Fill(jetsAfterInitialCuts.at(wres.at(1)).JetVtxF());
     hjet_vtxfrac1b->Fill(jetsAfterInitialCuts.at(wres.at(2)).JetVtxF());
     hjet_vtxfrac1b->Fill(jetsAfterInitialCuts.at(wres.at(3)).JetVtxF());
  }
  
  eff->GetXaxis()->SetBinLabel(cur_cut,"Wjj");
  if ( (fabs(tlvW1.at(sel).M()-m_mw) > minwidthw) || (fabs(tlvW2.at(sel).M()-m_mw)> minwidthw) ) return retval;
    eff->Fill(cur_cut, anevt.mcevt_weight);
    cur_cut++;
  
  y12->Fill(y121.at(sel)); y12->Fill(y122.at(sel));
  
// Closest jet (in Delta R) to the first muon.
  double dRjet2mu = 99, ptjet2mu = 0;
  for (UInt_t i=0; i<jetsAfterInitialCuts.size(); i++) {
    double dR = theMuon1.DeltaR( jetsAfterInitialCuts.at(i).lv() );
    if ( dR < dRjet2mu ) { dRjet2mu = dR; ptjet2mu = jetsAfterInitialCuts.at(i).lv().Pt(); }
  }
  jet2mu->Fill( dRjet2mu, ptjet2mu, anevt.mcevt_weight );

  double dRjet2mu2 = 99;
  ptjet2mu = 0; // Now the same for the second muon
  for (UInt_t i=0; i<jetsAfterInitialCuts.size(); i++) {
    double dR = theMuon2.DeltaR( jetsAfterInitialCuts.at(i).lv() );
    if ( dR < dRjet2mu2 ) { dRjet2mu2 = dR; ptjet2mu =jetsAfterInitialCuts.at(i).lv().Pt(); }
  }
  jet2mu2->Fill( dRjet2mu2, ptjet2mu, anevt.mcevt_weight );

// dR between the muons and their closest jets among jets of P_T>20
  dRjet2mu = 99, dRjet2mu2 = 99;
  for (UInt_t i=0; i<jetsAfterInitialCuts.size(); i++) {
    if ( jetsAfterInitialCuts.at(i).lv().Pt() > 20. ) {
       double dR = theMuon1.DeltaR( jetsAfterInitialCuts.at(i).lv() );  if ( dR < dRjet2mu  ) dRjet2mu = dR;
       dR = theMuon1.DeltaR( jetsAfterInitialCuts.at(i).lv() );  if ( dR < dRjet2mu2 ) dRjet2mu2 = dR;
    }
  }   
  jmudr->Fill( dRjet2mu, dRjet2mu2, anevt.mcevt_weight );

  
  eff->GetXaxis()->SetBinLabel(cur_cut,"dR_muj");
  if ( dRjet2mu < mindrjm || dRjet2mu2 < mindrjm ) return retval; 
     eff->Fill(cur_cut, anevt.mcevt_weight);
     cur_cut++;

// b-tag veto    
  eff->GetXaxis()->SetBinLabel(cur_cut,"veto bjt");
  if (nbjet > 0) return retval;
     eff->Fill(cur_cut, anevt.mcevt_weight);
     cur_cut++;

  // Angles between Ws and muons
  mu1Wcos->Fill( cos(theMuon1.Angle(tlvW1.at(sel).Vect())) ,
                 cos(theMuon1.Angle(tlvW2.at(sel).Vect())) );
  mu2Wcos->Fill( cos(theMuon2.Angle(tlvW1.at(sel).Vect())) , 
                 cos(theMuon2.Angle(tlvW2.at(sel).Vect())) );

// Muon charge requirement  
  eff->GetXaxis()->SetBinLabel(cur_cut,"muC*muC");
  if (muonsAfterInitialCuts.at(0).q()*muonsAfterInitialCuts.at(1).q() == chargeveto ) return retval;
     eff->Fill(cur_cut, anevt.mcevt_weight);
     cur_cut++;

// mu-mu invariant mass (b-quark or Z mass) cut
#define __allsignmumu__ true
  binvm->Fill( Zmm.M() );
  eff->GetXaxis()->SetBinLabel(cur_cut,"m_mumu");
  if ( __allsignmumu__ || (muonsAfterInitialCuts.at(0).q() != muonsAfterInitialCuts.at(1).q() )  ) {
    zinvm->Fill( Zmm.M() );
    if ( Zmm.M() < minmassmm ) return retval;
    if ( fabs(Zmm.M()-m_mz) < minwidthz ) return retval;
  }
  eff->Fill(cur_cut, anevt.mcevt_weight);
  cur_cut++;

// mu-mu delta R cut
  mumudr->Fill( theMuon1.DeltaR(theMuon2), anevt.mcevt_weight );
  mumudphi->Fill( fabs( theMuon1.DeltaPhi(theMuon2) ), anevt.mcevt_weight );
  eff->GetXaxis()->SetBinLabel(cur_cut,"dR_mumu");
  if ( __allsignmumu__ || muonsAfterInitialCuts.at(0).q() != muonsAfterInitialCuts.at(1).q() ) {
    if ( theMuon1.DeltaR(theMuon2) < mindrmm ) return retval;
  }  
  eff->Fill(cur_cut);
  cur_cut++;

// trigger requirement 
/*
  bool trgpass=false;
  if ( TRGe>0 ) { if (anevt.TRG_e[TRGe-1])  trgpass=true; } // TRG=2: MC      TRG=1: data
  if ( TRGm>0 ) { if (anevt.TRG_m[TRGm-1])  trgpass=true; }


  eff->GetXaxis()->SetBinLabel(cur_cut,TString("TRiGger "));
  if (!trgpass) return cur_cut;
    eff->Fill(cur_cut, anevt.mcevt_weight);
    cur_cut++;
*/

// select the best V4 candidate by comparing the 2 possibilities
  TLorentzVector  V4rec[2][2];

  V4rec[0][0]= ( tlvW1.at(sel)+theMuon1 );
  V4rec[0][1]= ( tlvW2.at(sel)+theMuon2 );
  V4rec[1][0]= ( tlvW1.at(sel)+theMuon2 );
  V4rec[1][1]= ( tlvW2.at(sel)+theMuon1 );

#if 1
  UInt_t reco=0;
  if ( fabs( V4rec[1][0].M() - V4rec[1][1].M() ) < 
       fabs( V4rec[0][0].M() - V4rec[0][1].M() ) )  reco=1;
#else
  TRandom3 rdnmgen( (UInt_t) tlvJets.at(0).Pt() );
  reco = (int)(rdnmgen.Rndm()*2);
#endif
  vinvm1->Fill( V4rec[reco][0].M() ); 
  vinvm2->Fill( V4rec[reco][1].M() );
  dvinvm->Fill( fabs(V4rec[reco][0].M() - V4rec[reco][1].M()) );
  vinvm2D->Fill( V4rec[reco][0].M() , V4rec[reco][1].M() );

// Delta M / Mean M cut
  double resm = 2 * fabs(V4rec[reco][0].M() - V4rec[reco][1].M()) /
                    fabs(V4rec[reco][0].M() + V4rec[reco][1].M()) ;
  vmres->Fill( resm );
  eff->GetXaxis()->SetBinLabel(cur_cut,"delM/<M>");
  if ( resm > maxresn4m ) return retval;
    eff->Fill(cur_cut);
    cur_cut++;


// Fill the information about the v4 candidates
#if 0  
  vinvms->Fill( V4rec[reco][0].M() ); vinvms->Fill( V4rec[reco][1].M() );
  vinvms->SetXTitle("m_{W#mu} (GeV)");
#else
  vinvms->Fill( 0.5 *( V4rec[reco][0].M() + V4rec[reco][1].M() ) );
#endif

  veta->Fill( V4rec[reco][0].Eta() );
  veta->Fill( V4rec[reco][1].Eta() );
  vphi->Fill( V4rec[reco][0].Phi() );
  vphi->Fill( V4rec[reco][1].Phi() );

// 2D sliding window cut    
  eff->GetXaxis()->SetBinLabel(cur_cut,"2D-v4v4");
  if ( fabs(V4rec[reco][0].M()-n4peak) > minwidthn4 || fabs(V4rec[reco][1].M()-n4peak) > minwidthn4 ) return retval;
    eff->Fill(cur_cut);
    cur_cut++;

//  std::cout << "Nu4 FOUND!!!! v4_1  = " << V4rec[reco][0].M() << " v4_2 = " << V4rec[reco][1].M() << std::endl;  
//  std::cout << "event number =" << EventNumber << " run number = " << RunNumber
//            << "  lb number = " <<  lbn << std::endl;

// Transverse angle between the two candidates
  dvphi->Fill( V4rec[reco][0].DeltaPhi( V4rec[reco][1] ) );

// Bookkeep the v4 masses for correlation studies later
//  v4invms.addData( V4rec[reco][0].M(), V4rec[reco][1].M() );

// reconstruct finally the beast in the s channel
  TLorentzVector hrec=V4rec[reco][0]+V4rec[reco][1];
  // std::cout << "Invariant mass of di-v4 = " << hrec.M() << std::endl;
  hinvm->Fill( hrec.M() );
  hvsvinvm->Fill( hrec.M() , V4rec[reco][1].M() );

  return retval;

}

