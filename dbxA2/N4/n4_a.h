#ifndef N4_A_H
#define N4_A_H

#include "dbx_a.h"
#include "ReadCard.h"
#include "LeptonicWReconstructor.h"
#include <iostream>
#include "analysis_core.h"


class N4dbxA : public dbxA {
  public: 
      N4dbxA(char *aname) : dbxA ( aname)
         {
         sprintf (cname,"%s",aname); // keep the current analysis name in the class variable
         int r=dbxA::setDir(cname);  // make the relevant root directory
         if (r)  std::cout <<"Root Directory Set Failure in:"<<cname<<std::endl;
         //grl_cut=false;
         }

      int initGRL();
      int readAnalysisParams();
      int plotVariables(int sel);
      int printEfficiencies();
      int bookAdditionalHistos();
      int makeAnalysis(vector<dbxMuon> muons, vector<dbxElectron> electrons, 
                               vector<dbxJet> jets, TVector2 met, evt_data anevt); 
      int saveHistos() {
        int r = dbxA::saveHistos();
        return r;
      }

   private:
      bool grl_cut;
      char cname[CHMAX];
      TH1F *hmmult, *hmptnu4, *hmetanu4, *hmphinu4;
      TH1F *hjmult, *hjptnu4, *hjetanu4, *hjphinu4, *hjmnu4;
      TH1F *vinvms, *vinvm1, *vinvm2, *dvinvm, *vmres;
      TH1F *hinvm, *jinvm, *jjinvm, *binvm, *zinvm, *eff;
      TH2F *vinvm2D, *hvsvinvm;
      TH1F *y12, *lepeta, *lepphi, *lepchrg, *jeteta, *jetphi, *veta, *vphi;
      TH1F *jetpthi, *jetetahi, *mminvm, *mmdelr;
      TH1F *mupt, *muptlo, *mupthi, *mumudr, *mumudphi, *jetpt, *ptmss, *ptmssig;
      TH2F *ptmssVsMt, *ptmssVsMv, *ptmssVsSET, *ptmuVsMt, *ptmuVsMv;
      TH1F *jmult, *bjmult, *elmult, *mmult, *dvphi;
      TH2F *jet2mu, *jet2mu2, *sjet2mu, *jmudr, *mu1Wcos, *mu2Wcos;
     
      TH1F *hsv0p_mass, *hsv0p_n2t, *hsv0p_svok, *hsv0p_ntrk;
      TH1F *hsv0p_ntrkv, *hsv0p_ntrkj,*hsv0p_efrc;
      TH1F *hjet_vtxfrac1b;
      TH1F *hjet_vtxfrac0b;
      TH1F *hmuet, *hmupt, *hjpt_lead;


      Double_t minwidthw, minwidthz, minptm, minmassmm, maxetam, mindrmm, maxetconem, minptj;
      Double_t maxetaj, maxmet, mindrjm, maxresn4m, n4peak, minwidthn4, jmasscut ;
      Int_t chargeveto, minnjetsw;  
      int TRGe, TRGm;
   
};
#endif
