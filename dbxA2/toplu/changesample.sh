#!/bin/bash

#1se sherpa 2yse alpgen
rm xsections.txt sample-FF_1.txt
type=$1

if [ $type == 1 ]; then
ln -s xsections.txt_sherpa xsections.txt
ln -s sample-FF_1.txt_sherpa sample-FF_1.txt
fi

if [ $type == 2 ]; then
ln -s xsections.txt_alpgen xsections.txt
ln -s sample-FF_1.txt_alpgen sample-FF_1.txt
fi

ls -lstr sample-FF_1.txt
ls -lstr xsections.txt

