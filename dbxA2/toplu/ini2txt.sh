#!/bin/bash

inpf=${1}-card.ini
outf=${1}-card.txt


cat $inpf | grep -v "cmd "| grep -v "def " | grep -v "histo " > $outf
cat $inpf | grep "def " | awk 'BEGIN{i=1} { print "def" i++,"=", substr($0, index($0,$2)) }' >> $outf
cat $inpf | grep "cmd " | awk 'BEGIN{i=1} { print "cmd" i++,"=", substr($0, index($0,$2)) }' >> $outf

outf=${1}-histos.txt
ocnum=`grep -o FillHistos  $inpf | wc -l|awk '{print $1}' `

#grep "histo " $inpf| awk "/FillHistos/ {flag++; next}; {print}; " | awk 'BEGIN{i=1} { print "histo" i++,"=", substr($0, index($0,$2)) }' 
#grep "histo " $inpf| awk 'BEGIN{i=1} /FillHistos/ {flag++; next}; { print "histo" i++,"=", substr($0, index($0,$2)) }' 


cat $inpf| awk  -F'[{]' 'BEGIN{i=1} /FillHistos/ {flag++; next};  { if (flag>=1) { 
if ($2 != "") 
   print "histo" i++,"=", $1,flag ", {"$2   
else if ($1 ~ /Basics/ ) print "histo" i++,"=",  "histo   \"Basics,",flag,"," ,flag,"\"" 
}
}' > pippo.tmp

cat pippo.tmp | awk '{print $1,$2,substr($0, index($0,$4)) }' > outf
rm -f  pippo.tmp
