# analysis parameters card for FF wjwj semileptonic
# format is "variable = value"
# ########################################
# MIND THE SPACES BEFORE AND AFTER THE = sign
# ########################################
minpte  = 25.0  # min pt of electrons
minptm2011  = 20.0  # min pt of muons
minptm2012  = 25.0  # min pt of muons
maxz0e = 2           #max z0 wrt PV for electrons   //we are not using now
maxz0m = 2           #max z0 wrt PV for muons
jetVtxf2011 = 0.75  #jet vertex fraction of 2011
jetVtxf2012 = 0.50  #jet vertex fraction of 2012
maxetae = 2.47   # max pseudorapidity of electrons
maxetam = 2.5   # max pseudorapidity of muons
minptj  = 30   # min pt of jets
maxetaj = 2.5   # max pseudorapidity of jets
mindrjm = 0.4   # min deltaR between jets and muons
mindrje = 0.1   # min deltaR between jets and electrons
minptj1 = 60.   # min pt of jet1 120=3s 160 is ok for 600
minptj2 = 10.   # min pt of jet2
minEj2 = 1.      # min E of jet2
minetaj2 = 0.0  # min pseudorapidity of jet2
mindrjj = 0.1   # min radial distance between any two jets
minetajj = 0.1  # same as above but for eta
HFtype = 3      # 1isBBorCC 2isC 3isLightFlavor, DONT_CHANGE
maxdeltam = 100 # maximum mass difference between 2 candidates
mqhxmin = 50    # for the final HQ mass histo, x-axis min
mqhxmax = 850 # for the final HQ mass histo, x-axis max
maxMuPtCone = 2.5 # Max muon Pt
maxMuEtCone = 4 # Max muon Et
maxElPtCone = 4 # Max electron Pt
maxElEtCone = 3.5 # Max electron Pt
minmetm = 20.0  # min MET muon
minmete = 35.0  # min MET electron
minmwtmetm = 60.0  # min MWT+MET muon
minmwtmete = 25.0  # min MWT+MET electron
# ########## Serhat's E6 2l+jets parameters #########
MW = 80.6
MZ = 90.8
SGMW = 7.38
SGMZ = 1.5
SGHQ = 24.0
SGHQBAR = 15.0
MINPTE0 = 0
MINPTE1 = 0
MINPTMO = 0
MINPTM1 = 0
MINNJETS = 4
MINPTJ0 = 0
MINPTJ1 = 0
MINPTELES = 0
MINWMASS = -100000
MAXWMASS = 1000000
MINPTPR1CHI2 = 0
MINDRWPR1CHI2 = 0
MINPTPR2CHI2 = 0
MAXZMASS = 10000000
MINZMASS = -100000
MINDRZPR2CHI2 = 0
MINPTZREC = 0
MINPTWREC = 0
MINDELTAM = -100000
QCDRUN = 0
TRGe = 0 # electron Trigger Type: 0=dont trigger, 1=1st trigger (data) 2=2nd trigger (MC)
TRGm = 2 #     muon Trigger Type: 0=dont trigger, 1=1st trigger (data) 2=2nd trigger (MC)
