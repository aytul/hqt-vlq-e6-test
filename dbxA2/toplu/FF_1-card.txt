# analysis parameters card for FF wjwj semileptonic
# format is "variable = value"
# ########################################
# MIND THE SPACES BEFORE AND AFTER THE = sign
# ########################################
# bit coded integer for systematics selection
# bci = sum( 2^(systematics_index-1) ) ==> to enable jer & msf = 2^1 + 2^2 = 6      
# 01_jes 02_jer 03_mureco 04_esf 05_jespileup 06_jesbjet 07_jetreco 08_caliBjet 09_mptsmear 10_escaler 11_mutrig 12_jescomU 13_jescomD 14_esfrc 
# 15_esfid 16_esftr
#for binary usage,start with 0b-----and card reads from not from left to right start from the right for the 1st systematic                       
systematics_bci = 0b0000000010000000 #256 # 0000 0000 0000 0000
iBoson = 0      # Boson in hadronic channel, 0:W 1:Z 2:h 
wBoson = 0.015   # Boson width in hadronic channel in chi2
minpte  = 25.0  # min pt of electrons
minptm2011  = 20.0  # min pt of muons 
minptm2012  = 25.0  # min pt of muons 
maxz0e = 2           #max z0 wrt PV for electrons   //we are not using now
maxz0m = 2           #max z0 wrt PV for muons
jetVtxf2011 = 0.75  #jet vertex fraction of 2011
jetVtxf2012 = 0.50  #jet vertex fraction of 2012
maxetae = 2.47   # max pseudorapidity of electrons
maxetam = 2.5   # max pseudorapidity of muons
minptj  = 25.0   # min pt of jets
maxetaj = 2.5   # max pseudorapidity of jets
mindrjm = 0.4   # min deltaR between jets and muons
mindrje = 0.1   # min deltaR between jets and electrons
minptj1 = 60.   # min pt of jet1 120=3s 160 is ok for 600
minptj2 = 10.   # min pt of jet2
minEj2 = 1.      # min E of jet2
minetaj2 = 0.0  # min pseudorapidity of jet2
mindrjj = 0.1   # min radial distance between any two jets
minetajj = 0.1  # same as above but for eta
HFtype = 3      # 1isBBorCC 2isC 3isLightFlavor, DONT_CHANGE
maxdeltam = 100 # maximum mass difference between 2 candidates 
mqhxmin = 50    # for the final HQ mass histo, x-axis min
mqhxmax = 850 # for the final HQ mass histo, x-axis max
maxMuPtCone = 2.5 # Max muon Pt 
maxMuEtCone = 4 # Max muon Et
maxElPtCone = 4 # Max electron Pt
maxElEtCone = 3.5 # Max electron Pt
minmetm = 20.0  # min MET muon
minmete = 30.0  # min MET electron
minmwtmetm = 60.0  # min MWT+MET muon
minmwtmete = 30.0  # min MWT electron
TRGm = 2 #     muon Trigger Type: 0=dont trigger, 1=1st trigger (data) 2=2nd trigger (MC)
TRGe = 0 # electron Trigger Type: 0=dont trigger, 1=1st trigger (data) 2=2nd trigger (MC)
