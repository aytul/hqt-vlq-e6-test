#!/bin/bash
uselight=1
#to run on server uselight should be 0

#select to run electrons OR muons
#use 2 letter keyword: ee OR mm
TRGtype=ee
#TRGtype=mm

ecm="13TeV"
# options are 7tev or 8tev or ?tev

run_no_req=1
#DO NOT CHANGE THIS -1
#if you want to give single runnumber,  write ./run_proof.sh runnumber
if [ $# -ge 1 ] ; then
 echo "got run number request": $1
 run_no_req=$1
fi

# put a 1 to turn on shape systematics, otherwise 0.
shape=0;

relv=`cat RELEASE`
T="$(date +%s)"
qcd=0
############################choose your dataset and for loop#####################

Data_m="VLQ_20170216_data"
Data_e="VLQ_20170216_data"

#mcList="VVSherpa"
#mcList="Zjets_Sherpa221"
mcList="ttbar"
#Test="dataPeriodB_electrons"

 declare -a arr

 fileid=0
 mc_with_systs=" "
 for mcf in $mcList ; do

    if [ $shape -gt 0 ]; then
     newmcf=`ls -d /Volumes/2TB/${mcf}_*/|cut -f4 -d'/'| grep -v nominal `
    else
     newmcf=`ls -d /Volumes/2TB/${mcf}_nominal*/|cut -f4 -d'/' `
    fi
     mc_with_systs=$mc_with_systs" "$newmcf
     for ffile in $newmcf ; do
#       echo $ffile $mcf
       arr[${fileid}]=$mcf
       fileid=$((fileid+1))
     done
 done
 mcListProg=$mcList
 mcList=$mc_with_systs

 
 fileid=0
# for mcf in $Test ; do 
for mcf in $mcList ; do
#for mcf in $Data_e ; do 
#for mcf in $Data_m ; do 

#add below the run options, mind the spaces. TRG is set automatically. example -FF 2 -Skeleton 1
#opt=" -E62 1 -S 1 "
opt=" -E62 1 " #SPACE before the quotation marks

   if [[ "$opt" =~ "-S 1" && $shape -gt 0 ]]; then
    echo "Cannot work with both shape systematics and SF systematics! quitting... "
    exit 1
   fi

###if [[ $mcf == *"data"* ]] ; then
###   lpath=`echo $mcf | cut -f2 -d'_'`
##### fix the TRG in the datacard   
###   ../analysis_core/fixTRG.sh 1 $opt
### else
###   lpath="mc"
# fix the TRG in the datacard   
###   ../analysis_core/fixTRG.sh 2 $opt
### fi

  opt+="-Q ${qcd} "
  echo working on $mcf from ${arr[${mcf}]} "shape systs:" $shape "  with option " $opt "  at:" $ecm
###  if [ $shape -gt 0 ]; then
   runnolist=`../analysis_core/getres.sh ${arr[${fileid}]} listrunnos| grep -v number | cut -f3 -d' '`
   fileid=$((fileid+1))
###  else
###   runnolist=`../analysis_core/getres.sh ${mcf} listrunnos| grep -v number | cut -f3 -d' '`
###  fi

  echo  "Will run: "
  for runno in $runnolist; do
   echo -n $runno " "
  done
  echo " "

  if [ $run_no_req -ne -1 ]; then
   runnolist=$run_no_req
  fi

  for runno in $runnolist; do
     ls -1 /Volumes/2TB/$mcf/user.nikiforo.${runno}.*lvl0/*lvl0_${TRGtype}.root > ${runno}.txt  
##     ls -1 /media/data1/2017_a/systs/$mcf/user.nikiforo.${runno}.*lvl0/*lvl0_${TRGtype}.root > ${runno}.txt  

    if [ $USER == "atb" ]; then
      sed -i -e 's#^#root://pc-atb-srv-01/#'  ${runno}.txt
    fi

    i=1
    while [ `wc -l ${runno}.txt|awk '{print $1}'` -gt 400 ]; do 
      echo "Too many files to process, working on them in 400 chunks"  
      head -n 400 ${runno}.txt > ${runno}-$i.txt
      diff --suppress-common-lines ${runno}-$i.txt ${runno}.txt  | awk '{print $2}' > temp.txt
      mv ${runno}-$i.txt ${runno}.txt
      root -b -q -x runSelector.C"(\"${runno}\",$uselight,\"$opt\")" 
      mv ${runno}.root ${runno}-$i.root
      i=$(( $i + 1 ))
      mv temp.txt ${runno}.txt
    done

     root -b -q -x runSelector.C"(\"${runno}\",$uselight,\"$opt\")" 
    # alti satir yukarida bunun aynisi var ? -- evet normal.
    ##################root -b -q -x runSelector.C"(\"${runno}\",$uselight,\"$opt\")"  
    if [ $i -gt 1 ]; then
      mv ${runno}.root ${runno}-$i.root
      hadd  ${runno}.root ${runno}-*.root
     rm -f ${runno}-*.root
    fi
# rename the output histogram to something relevant to systematics name
  if [ $shape -gt 0 ]; then
   mv  ${runno}.root  ${runno}_$mcf.root
  fi

 killall -9 proofserv.exe
#end of loop over run numbers
  done

# over samples
done

T="$(($(date +%s)-T))"
echo "Total time in seconds: ${T}"
