#!/bin/bash
# compare.C format (int qm=350, float lumi=35, float skf=1.0, int pset=1, int disc=0)
# pset = 3: 1-bjet, 4: 0-bjet 8: untag
# disc = 2: HT, 1: mQ hadronic, 3: HTvsMQ FD, 4:mQ both, 5:mQ average


T="$(date +%s)"

cht=`cat ANA_DEFS | grep -v SUM | grep CHANNEL| grep -v "^#" |cut -f3 -d' '`
mdl=`cat ANA_DEFS | grep MODEL| grep -v "^#" |cut -f3 -d' '`
inj=`cat ANA_DEFS | grep INJECTION| grep -v "^#" |cut -f3 -d' '`
lumi=`cat ANA_DEFS | grep INTLUMI| grep -v "^#" |cut -f3 -d' '`
limitter=`cat ANA_DEFS | grep LIMITTER| grep -v "^#" |cut -f3 -d' '`
dosyst=`cat ANA_DEFS | grep SYSTERR| grep -v "^#" |cut -f3 -d' '`
sumchannels=`cat ANA_DEFS | grep SUMCHANNELS | grep -v "^#" |cut -f3 -d' '`
other_ch="xxxx"
if [ $# -ge 1 ]; then
 if [ $1 == "forced" ]; then
  other_ch="forced"
 fi
fi 

#rm -f Limit.txt
touch Limit.txt

if [ $cht == "1" ] ; then
 chtname="ELE"
else
 chtname="MUON"
fi
if [ $sumchannels == "1" ]; then
 chtname="ELEMU"
fi

for pset in 3 ; do

for disc in 1 ; do

  for ERR in 0 ; do
   cp ANA_DEFS ANA_DEFS_wk
   grep -v STATERR ANA_DEFS > ana_defs_tmp
   mv ana_defs_tmp ANA_DEFS
   if [ $ERR -eq 0 ] ; then
    echo "STATERR = 0" >> ANA_DEFS
   fi
   if [ $ERR -eq 1 ] ; then
    echo "STATERR = 1" >> ANA_DEFS
   fi
   if [ $ERR -eq 2 ] ; then
    echo "STATERR = 2" >> ANA_DEFS
   fi

   for mq  in 600 1000 1200 1400 1600 ; do
#   for mq  in 1600; do
   echo $sumchannels "  " $other_ch

# in case of sumchannels, if forced or not present, create the sibg file, and exit
    if [ $sumchannels == "1" ]; then
     if [ $other_ch == "forced" ] || [ $other_ch == "missing" ] ; then
#  a quick rename to the other channel
      cp ANA_DEFS ANA_DEFS_temporary
      cat ANA_DEFS | grep -v 'CHANNEL =' > ANA_DEFS_inv
# invert : if electrons put muons
      if [ $cht == "1" ]; then
         echo 'CHANNEL = 0 // 0:muons  1:electrons' >> ANA_DEFS_inv
      else
         echo 'CHANNEL = 1 // 0:muons  1:electrons' >> ANA_DEFS_inv
      fi
      cp ANA_DEFS_inv ANA_DEFS
# if no syst, we run to create the usual sibg file and rename
      if [ $dosyst -eq 0 ]; then
           root -l -q -x compare.C\($mq,-1,1,${pset},-${disc},0\)
      else
#here we will work with the systematics, we will create N files.
         echo  looping over systematics, take a copy of ANA_DEFS, modify and run
         cp ANA_DEFS ANA_DEFS_B4SYST
 	mdl=`cat ANA_DEFS | grep MODEL| grep -v "#" |cut -f3 -d' '`         
	systFile=systematics_$mdl
	for systmodel in `cat $systFile` ; do
            cat ANA_DEFS_B4SYST | grep -v "DIRECTORY" > ANA_DEFS
            echo 'DIRECTORY = ' $systmodel >> ANA_DEFS
            echo root -l -q -x compare.C\($mq,-1,1,${pset},-${disc},0\)
         done
# move the datacard back
         cp ANA_DEFS_B4SYST ANA_DEFS
      fi
# end of systematics or not.
      mv ANA_DEFS_temporary ANA_DEFS 
     fi
    fi
#sumchannels if ended

# the normal action : NO CHANNEL MERGEING
# if no syst, we run to create the usual sibg file 
     if [ $dosyst -eq 0 ]; then
         root -l -q -x compare.C\($mq,-1,1,\"${pset}\",${disc},0\) 
     else
#here we will work with the systematics, we will create N files and rename.
         echo  looping over systematics, take a copy of ANA_DEFS, modify and run
         cp ANA_DEFS ANA_DEFS_B4SYST
	 mdl=`cat ANA_DEFS | grep MODEL| grep -v "^#" |cut -f3 -d' '|head -1`         
	 systFile=systematics_$mdl
	 for systmodel in `cat $systFile` ; do
            cat ANA_DEFS_B4SYST | grep -v "DIRECTORY" > ANA_DEFS
            echo "DIRECTORY = " ${systmodel}  >> ANA_DEFS
            root -l -q -x compare.C\($mq,-1,1,\"${pset}\",-${disc},0\)
            fn=`ls -1tr  *_*.root|tail -1`
            mv $fn ${systmodel}_${fn}
         done
         atit=`head -1 $systFile`
         #hadd -f pippo.root ${atit}_*${mq}*.root #SFsyst
         hadd -f pippo.root *${mdl}_*${mq}*.root
         #fn=`ls ${atit}_sibg*${mq}*root | cut -f3- -d'_'` #SFsyst
         fn=`ls *${mdl}_sibg*${mq}*root | cut -f3- -d'_'`
         echo "new file "$fn
         #mv pippo.root $fn
         mv pippo.root sibg_ELE_${mq}_${pset}_${disc}.root
         #rm -f ${atit}_*.root #SFsyst
         rm -f ${mdl}_*.root
# move the datacard back
         cp ANA_DEFS_B4SYST ANA_DEFS
# this time execute to get the limits, without making a new sibg file
        echo root -l -q -x compare.C\($mq,-1,1,\"${pset}\",${disc},0,0\)
      fi
# end of systematics or not.
   done
#end of mq loop

cp  Limit.txt Limit_${lumi}_${pset}_${disc}_${chtname}_${mdl}_${inj}_${limitter}.txt
mv ANA_DEFS_wk ANA_DEFS
  done
#end of error loop

##root.exe -q -x ExclusionPlot.C\(${lumi},0\)
##mv OutputPlots/NoEr_Limits.eps OutputPlots/NoEr_Limits_${chtname}_Stat_${pset}_${disc}_${mdl}.eps
##mv OutputPlots/Stat_Limits.eps OutputPlots/Stat_Limits_${chtname}_Stat_${pset}_${disc}_${mdl}.eps
##mv OutputPlots/Syst_Limits.eps OutputPlots/Syst_Limits_${chtname}_Stat_${pset}_${disc}_${mdl}.eps
##mv OutputPlots/Disc_Limits.eps OutputPlots/Disc_Limits_${chtname}_Stat_${pset}_${disc}_${mdl}.eps
##mv  OutputPlots/CLb_Limits.eps  OutputPlots/CLb_Limits_${chtname}_Stat_${pset}_${disc}_${mdl}.eps
##rm Limit.txt; touch Limit.txt
  
done
#end of discriminator loop
done
#end of pset loop

T="$(($(date +%s)-T))"                                      
echo "Total time in seconds: ${T}"                          
printf "total time : %02d:%02d:%02d:%02d\n" "$((T/86400))"  

