#!/bin/bash
# use this file within the directory that includes log results of mclimit xml version. 
# it extracts scale factors from log files and creates ALimit.txt file which is appropriate format for ExclusionPlot.C


for mQ in 400 500 600 700 800; do
fname=$1

sf=`cat ${mQ}$1.txt | grep Summary | cut -f5 -d' '`
sf2m=`cat ${mQ}$1.txt  | grep Summary | cut -f6 -d' '`
sf1m=`cat ${mQ}$1.txt | grep Summary | cut -f7 -d' '`
sf2p=`cat ${mQ}$1.txt | grep Summary | cut -f8 -d' '`
sf1p=`cat ${mQ}$1.txt | grep Summary | cut -f9 -d' '`

echo -en '\n'>> ALimit.txt
echo '#Limitter is TLimit   toyMCevts=30000' >>ALimit.txt
echo -en '\n'>> ALimit.txt
echo mQ ${mQ} uB _ELE_ Syst CLs sf $sf2m LLR_ExpH0M2 1.00000 cls1k 1.00000 clb1k 1.00000 >> ALimit.txt
echo mQ ${mQ} uB _ELE_ Syst CLs sf $sf1m LLR_ExpH0M1 1.00000 cls1k 1.00000 clb1k 1.00000 >> ALimit.txt
echo mQ ${mQ} uB _ELE_ Syst CLs sf $sf LLR_ExpH0Med 1.00000 cls1k 1.00000 clb1k 1.00000 >> ALimit.txt
echo mQ ${mQ} uB _ELE_ Syst CLs sf $sf1p LLR_ExpH0P1 1.00000 cls1k 1.00000 clb1k 1.00000 >> ALimit.txt
echo mQ ${mQ} uB _ELE_ Syst CLs sf $sf2p LLR_ExpH0P2 1.00000 cls1k 1.00000 clb1k 1.00000 >> ALimit.txt

done

