//herhangi bişeyi std::stringe çevirmek için
#include <string>

template <typename T> std::string tostr(const T& t) {
   std::ostringstream os; os<<t; return os.str();
 }
