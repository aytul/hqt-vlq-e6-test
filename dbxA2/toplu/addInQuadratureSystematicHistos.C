void addInQuadratureSystematicHistograms(TH1F* modhisto0, TH1F* modhisto1,
			    TString sampleName, TString sysSuffix,
			    unsigned nS2m, int*systNos2merge) {
  int nbins = modhisto0->GetNbinsX();
  for (int ib=0;ib<=nbins;++ib) {

    float binc = modhisto0->GetBinContent(ib);
    float highest = binc; //nominal value
    float lowest = binc; //nominal value
    float largestErr = modhisto0->GetBinError(ib);
    float distancesquared = 0;
    float distanceUp = 0;
    float distanceDown = 0;
    float distancesquaredUp = 0;
    float distancesquaredDown = 0;

    // it would be more efficient to loop over histos as the outside loop
    //   and nest the loop over the bins; but this is more similar to the
    //   other code we have.
    for (unsigned int i=0; i<nS2m; ++i) { // loop over systematics
      for (unsigned int is=0; is<2; ++is) { // loop over ups and downs
        TString histoname;
        histoname.Form("%s%s%d_%d", sampleName.Data(), sysSuffix.Data(),systNos2merge[i], is);
//        cout << "working on:"<<histoname<<"\n";
        TObject *myobj = gROOT->FindObject(histoname);
        float nbc=0;
        if ( myobj == 0 ) {
          cout << "Could not find " << histoname << " in the file. IGNORING!!!" << endl;
        } else {
          nbc = ((TH1F*)myobj)->GetBinContent(ib); // the current systematics bin
          if (nbc> binc) {distancesquaredUp   += TMath::Power(fabs(nbc-binc), 2 );}
          if (nbc< binc) {distancesquaredDown += TMath::Power(fabs(nbc-binc), 2 );}
        }
      } // end of loop over ups and downs
// now can add the exceptions 
// sample_1_syst_E62_1_44_model167_0 and sample_1_syst_E62_1_44_model170_0
/* --- enable this area to allow modelling systematics
      histoname.Form("%s%sE62_1_44_model%d_%d", sampleName.Data(), sysSuffix.Data(),167, is);
      cout << "working on:"<<histoname<<"\n";
     {
      TObject *myobj = gROOT->FindObject(histoname);
      float nbc=0;
      if ( myobj == 0 ) {
        cout << "Could not find " << histoname << " in the file. IGNORING!!!" << endl;
      } else {
        nbc = ((TH1F*)myobj)->GetBinContent(ib); // the current systematics bin
        if (nbc> binc) {distancesquaredUp   += TMath::Power(fabs(nbc-binc), 2 );}
        if (nbc< binc) {distancesquaredDown += TMath::Power(fabs(nbc-binc), 2 );}
      }
     }

      histoname.Form("%s%sE62_1_44_model%d_%d", sampleName.Data(), sysSuffix.Data(),170, is);
      cout << "working on:"<<histoname<<"\n";
      TObject *myobj = gROOT->FindObject(histoname);
      float nbc=0;
      if ( myobj == 0 ) {
        cout << "Could not find " << histoname << " in the file. IGNORING!!!" << endl;
      } else {
        nbc = ((TH1F*)myobj)->GetBinContent(ib); // the current systematics bin
        if (nbc> binc) {distancesquaredUp   += TMath::Power(fabs(nbc-binc), 2 );}
        if (nbc< binc) {distancesquaredDown += TMath::Power(fabs(nbc-binc), 2 );}
      }
*/
    } // end of loop over systematics
    distanceUp = sqrt(distancesquaredUp);
    distanceDown = sqrt(distancesquaredDown);
    cout << " at the bin number " << ib <<" :" << "nominal bin content: " << binc << "\t  Up: " << distanceUp << "\t   Down:" <<  distanceDown << endl;
    highest = binc + distanceUp;
    lowest = binc - distanceDown;
    modhisto0->SetBinContent(ib, highest); 
    modhisto1->SetBinContent(ib, lowest);

  } // end of loop over all bins
}// end of function



// If readUpdown is true, we will attempt to read and separately handle up and down histos
// If createUpdown is true, the output will surely contain to sets of histos, with the final suffix
//    chosen as 0 and 1 automatically, overriding your preferred updownSuffix
// If createUpdown and readUpdown are both false, we will attempt to create only one set of histograms
//    whose title will end in the updownsuffix that you choose.
// updownsuffix variable is used only in determining the title of your merged output histogram
void checkAndAddSystematicHistograms(TFile *outfile,
                                TString sampleName,
                                TString sysSuffix,
                                unsigned int nS2m,
                                int *systNos2merge,
                                int newSystId,
                                bool createUpdown = false,
				                bool readUpdown = false,
                                TString updownSuffix = "_0") {

  TString newSystName = sampleName+sysSuffix+TString::Format("%d",newSystId);
  cout << "I will attempt to create " << newSystName << " histogram(s)" << endl;

  // Start by checking whether the nominal histogram exists
  TH1F *nominal = (TH1F*)(gROOT->FindObject(sampleName));
  TH1F *modhisto_0 = 0;  // |
  TH1F *modhisto_1 = 0;  // up and down systematics
  if (!nominal) {
    cout << "Exiting! Something is wrong, can't find base histogram" << endl;
    return; }
  else {
    if (createUpdown) updownSuffix = "_0";  // if updown is true, override the user's preference
    modhisto_0 = (TH1F*)nominal->Clone(newSystName+updownSuffix); // set modhisto_0 to nominal
    if (createUpdown) modhisto_1 = (TH1F*)nominal->Clone(newSystName+"_1"); } //set modhisto_1 to nominal

  const int nbinsn = nominal->GetNbinsX();

  // check that the requested systematics are already in the file, and call the function to make the envelope.
  for (unsigned int is=0; is<1+(readUpdown); ++is) {
    for (unsigned int i=0; i<nS2m; ++i) {
      TString histoname;
      histoname.Form("%s%s%d_%d", sampleName.Data(), sysSuffix.Data(), systNos2merge[i], is);
      TObject *myobj = gROOT->FindObject(histoname);
      if ( myobj == 0 ) {
	cout << "Could not find " << histoname << " in the file. IGNORING..." << endl;
        continue;
      }
      if ( ! myobj->InheritsFrom("TH1F") ) {
	cout << "Exiting! " << histoname << " is not a TH1F object." << endl;
	return; }
      TH1F *syshist = (TH1F*)myobj;
      if ( nbinsn != syshist->GetNbinsX() ) {
	cout << "Exiting! " << histoname << " has incorrect number of bins." << endl;
	return; }
    } // end of loop over systematics histograms
   } // end of loop over ups and downs
  cout << "Checked that all the needed input histograms are present." << endl;

  if ( createUpdown && readUpdown )
    addInQuadratureSystematicHistograms(modhisto_0, modhisto_1, sampleName, sysSuffix,
			   nS2m, systNos2merge);
  delete nominal;
}

void copyToNewFile(TFile *infile, TFile *outfile, bool verbose=true) {
    
    int noOfBrowsables = infile->GetListOfKeys()->GetEntries();
    for (int k=0; k<noOfBrowsables; k++) {
        TString objname(infile->GetListOfKeys()->At(k)->GetName());
        infile->cd();
        TObject *myobj = gROOT->FindObject(objname);
        if ( myobj == 0 ) continue;
        if ( verbose ) cout << objname << "\t" << myobj->GetName() << endl;
        TObject *cloneobj = myobj->Clone(objname);
        delete myobj;
        outfile->cd();
        cloneobj->Write();
    }
    
}

void addInQuadratureSystematicHistos(TString inputfile="sibg_ELE_1000_3_3_allsys") {

    TString lepton=gSystem->GetFromPipe(TString::Format("echo %s |  awk -F\"_\" '{print $2}' ", inputfile.Data())); //get the lepton kind from the input file
    int PSET=gSystem->GetFromPipe(TString::Format("echo %s |  awk -F\"_\" '{print $4}' ", inputfile.Data())).Atoi(); //get the PSET from the input file
    int disc=gSystem->GetFromPipe(TString::Format("echo %s |  awk -F\"_\" '{print $4}' ", inputfile.Data())).Atoi(); // get the discriminant of the specific PSET from the input file
    cout << lepton << PSET << disc << endl;

    TString variable = gSystem->GetFromPipe(TString::Format("grep  \"\\s%s%i\\s\" compare.dat |  awk -F\"=\" '{print $2}' |  awk -F\",\" '{print $%i}'", lepton.Data(), PSET, disc)); // find the name of the discriminant from the compare.dat file.
    cout << variable << endl;
    
    
    TFile *_file0 = TFile::Open(inputfile+".root");
    if (!_file0) { cout << "Cannot open input file, exiting!" << endl; return; }
    TFile *outfile = TFile::Open(inputfile+"_mod.root", "RECREATE", "", 9);
    cout << "Start copying already existing objects into the new file..." << endl;
    copyToNewFile(_file0, outfile, false);
    cout << "Copy complete" << endl;
    _file0->Close();

    // Z+JETS SAMPLE SYST MERGING

    const unsigned int nS2m = 73; // number of systematics to merge
    int systNos2merge[nS2m];
    for (unsigned int i=0; i<nS2m; ++i) { systNos2merge[i]=1+i; }
    checkAndAddSystematicHistograms( outfile, "sample_1", "_syst_", nS2m, systNos2merge, 200, true, true);
    checkAndAddSystematicHistograms( outfile, "sample_2", "_syst_", nS2m, systNos2merge, 200, true, true);
    checkAndAddSystematicHistograms( outfile, "sample_3", "_syst_", nS2m, systNos2merge, 200, true, true);
    checkAndAddSystematicHistograms( outfile, "sample_4", "_syst_", nS2m, systNos2merge, 200, true, true);
    checkAndAddSystematicHistograms( outfile, "sample_5", "_syst_", nS2m, systNos2merge, 200, true, true);


    outfile->cd();
    TH1F *norma   =  (TH1F*)(gROOT->FindObject("sample_1")) ;
          norma->Add((TH1F*)(gROOT->FindObject("sample_2")));  
          norma->Add((TH1F*)(gROOT->FindObject("sample_3")));  
          norma->Add((TH1F*)(gROOT->FindObject("sample_4")));  
          norma->Add((TH1F*)(gROOT->FindObject("sample_5")));  

    TH1F *totalup =  (TH1F*)(gROOT->FindObject("sample_1_syst_200_0")) ;
     if ( totalup == 0 ) { cout << "Could not find total up\n"; return ; }
          totalup->Add((TH1F*)(gROOT->FindObject("sample_2_syst_200_0")));  
          totalup->Add((TH1F*)(gROOT->FindObject("sample_3_syst_200_0")));  
          totalup->Add((TH1F*)(gROOT->FindObject("sample_4_syst_200_0")));  
          totalup->Add((TH1F*)(gROOT->FindObject("sample_5_syst_200_0")));  

    TH1F *totaldn  =   (TH1F*)(gROOT->FindObject("sample_1_syst_200_1")) ;
          totaldn->Add((TH1F*)(gROOT->FindObject("sample_2_syst_200_1")));  
          totaldn->Add((TH1F*)(gROOT->FindObject("sample_3_syst_200_1")));  
          totaldn->Add((TH1F*)(gROOT->FindObject("sample_4_syst_200_1")));  
          totaldn->Add((TH1F*)(gROOT->FindObject("sample_5_syst_200_1")));
    
    TH1F *total=(TH1F *)totalup->Clone(variable.Data());
    int binNumber = totalup->GetNbinsX();
    for (int ibin=0;ibin<=binNumber;++ibin) {
        float avg = 0;
        float avgerr = 0;
        avg = (totalup->GetBinContent(ibin)+totaldn->GetBinContent(ibin))/2;
        avgerr = totalup->GetBinContent(ibin)-avg;
        avg = (totalup->GetBinContent(ibin)+totaldn->GetBinContent(ibin))/2;
        total->SetBinContent(ibin,avg);
        total->SetBinError(ibin,avgerr);
    }

    TFile *outfile2 = TFile::Open(inputfile+"_band.root", "RECREATE", "", 9);
    TDirectory *topdir = outfile2->mkdir("E62_1");
    topdir->cd();

    TH1F *eff   = new TH1F("eff","selection efficiencies ",30,0.5,30.5);
    for (int ij=0; ij<30; ij++) eff->SetBinContent(ij, 1.0);
    eff->Write();
    total->Write();
    norma->Write("norma");
    totalup->Write();
    totaldn->Write();
    outfile2->Write();
    outfile2->Close();
 
    outfile->Write();
    outfile->Close();
    cout << "Done." << endl;

}
