#!/bin/bash

pdir=$1
pfile=${pdir}-histos.txt 
hfile=histoOut-${pdir}.root

plotlist=`cat $pfile |cut -f1 -d','|cut -f2 -d'"'`

usersel="aabbcc"

while [ 1 ]; do
	echo This is the histogram list.
	for plotn in $plotlist; do
   		echo $plotn
	done
	echo "Enter a histo to draw or enter QUIT to quit."
	read usersel
        if [ $usersel == "QUIT" ]; then
          exit 
        fi
	root -l -x simplePlot.C\(\"$pdir\",\"$usersel\",\"$hfile\"\)
done
