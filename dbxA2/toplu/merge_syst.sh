#!/bin/bash

runlist=`ls -1 *SINGLE*|cut -f1 -d'_'|sort| uniq` 
singlelist=`ls -1 *SINGLE*|cut -f1 -d'.'` 

for afile in $singlelist ; do
 cp ${afile}.root  ${afile}down.root
done

for runno in $runlist ; do
 filelist=`ls -t1 ${runno}_*.root`
 systid=21
 localcount=0
 for afile in $filelist ; do
  systname=`echo $afile | cut -f 3- -d'_'| cut -f 1 -d'.'`
  echo $systname $localcount $systid
  if [ $(($localcount % 2)) -eq 0 ]; then
       systid=$(($systid + 1))
  fi
  localcount=$(($localcount +1))
  echo root.exe -q -x 'ren.C("'${runno}'","'${systname}'" , "'${systid}'")'
###  root.exe -q -x 'ren.C("'${runno}'","'${systname}'" , "'${systid}'")'
## if [ $(($localcount % 2)) -eq 0 ]; then
##   exit 1
## fi
 done
done
