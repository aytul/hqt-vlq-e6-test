#include <vector>
#include <string>

#define __ROOT6__

void testSelector(TString runname_tmp="107700", int lite=0, TString opts=" -E62 1 -S 0 ")
{

cout << "got:"<<runname_tmp<<endl;

TString workdir(gSystem->pwd());
gInterpreter->AddIncludePath(workdir+"/../analysis_core");
gInterpreter->AddIncludePath(workdir+"/../FF");
gInterpreter->AddIncludePath(workdir+"/../challenge");
gInterpreter->AddIncludePath(workdir+"/../E61");
gInterpreter->AddIncludePath(workdir+"/../E62");
gInterpreter->AddIncludePath(workdir+"/../N4");
gInterpreter->AddIncludePath(workdir+"/../BP");
gInterpreter->AddIncludePath(workdir+"/../Skeleton");
gInterpreter->AddIncludePath(workdir+"/../Dump");
gInterpreter->AddIncludePath(workdir+"/../DiLepChal");

#ifdef __ROOT6__
gROOT->LoadMacro("../analysis_core/ReadCard.cpp+");
#else
R__LOAD_LIBRARY(../analysis_core/dbxParticle.cpp+)
#endif

gSystem->Load("libdbxParticle_cpp.so");
gSystem->Load("libdbx_electron_h.so");
gSystem->Load("libdbx_jet_h.so");
gSystem->Load("libdbx_muon_h.so");
gSystem->Load("libdbx_photon_h.so");
gSystem->Load("grl_C.so");
gSystem->Load("libdbxCut_h.so");
gSystem->Load("libDBXNtuple_cpp.so");


gInterpreter->GenerateDictionary("vector< dbxElectron>","dbx_electron.h;vector");
gInterpreter->GenerateDictionary("vector< dbxMuon>","dbx_muon.h;vector");
gInterpreter->GenerateDictionary("vector< dbxJet>","dbx_jet.h;vector");


TProof *proof;
if (lite) {
    proof = TProof::Open("");
//    proof->SetParallel(3); //for using only few cores, we reserve the rest for other tasks
} else {
  //proof = TProof::Open("gokhans-macbook-pro.local");  //gokhan's laptop for debugging
    proof = TProof::Open("aga");
    if (!proof) { cout << "Encountered problem with proof server. Exiting" << endl; return; }

}

// prepare the packages to be uploaded
cout << "Making the cards package"<<endl;
  gSystem->Exec("mkdir cards; mkdir cards/PROOF-INF");
  //gSystem->Exec("cat ../analysis_core/SETUP.C | grep -v Load | grep -v Exec > cards/PROOF-INF/SETUP.C");
  gSystem->Exec("cp *-*.txt cards");
  gSystem->Exec("echo \"Int_t SETUP(){return 0;}\" > cards/PROOF-INF/SETUP.C");
  gSystem->Exec("tar czhf cards.par cards");
  gSystem->Exec("rm -rf cards");
  cout << "cards prepared.\n";

cout << "Making the libs package"<<endl;
  gSystem->Exec("mkdir libs");
  gSystem->Exec("mkdir libs/PROOF-INF");
  gSystem->Exec("cp *.so* libs");
  gSystem->Exec("cp ../analysis_core/* libs");
  gSystem->Exec("cat ../analysis_core/SETUP.C | grep -v BAT | grep -v KLFitter > libs/PROOF-INF/SETUP.C");
  gSystem->Exec("tar czhf libs.par libs");
  gSystem->Exec("rm -rf libs");
  cout << "libs prepared\n";



if (!lite) {
  gSystem->Exec("tar czhf analysis_core.par  ../analysis_core");
  gSystem->Exec("tar czhf E61.par  ../E61");
  gSystem->Exec("tar czhf E62.par  ../E62");
  gSystem->Exec("tar czhf FF.par  ../FF");
  gSystem->Exec("tar czhf Dump.par  ../Dump");
  gSystem->Exec("tar czhf N4.par  ../N4");
  gSystem->Exec("tar czhf BP.par  ../BP");
  gSystem->Exec("tar czhf challenge.par  ../challenge");
  gSystem->Exec("tar czhf DiLepChal.par  ../DiLepChal");
  gSystem->Exec("tar czhf Skeleton.par  ../Skeleton");
}


proof->ClearCache(); // if we clear the cache, dbxAna will be recompiled.

cout << "uploading packages\n";

#ifdef __ROOT6__
#define PROPT TProof::EUploadPackageOpt::kRemoveOld
#else
#define PROPT 1
#endif


//proof->ClearPackage("cards");
proof->UploadPackage("cards", PROPT ); 
if (lite) proof->EnablePackage("cards",1); 
    else  proof->EnablePackage("cards");
/*
   proof->EnablePackage("cards");
*/

cout << "cards OK."<<endl;
proof->UploadPackage("libs", PROPT ); proof->EnablePackage("libs");
cout << "libs OK."<<endl;

 
if (!lite) {
  proof->UploadPackage("analysis_core", PROPT ); proof->EnablePackage("analysis_core");
  cout << "analysis_core OK."<<endl;
  proof->UploadPackage("E61", PROPT ); proof->EnablePackage("E61");
  cout << "E61 OK."<<endl;
  proof->UploadPackage("E62", PROPT ); proof->EnablePackage("E62");
  cout << "E62 OK."<<endl;
  proof->UploadPackage("FF", PROPT ); proof->EnablePackage("FF");
  cout << "FF OK."<<endl;
  proof->UploadPackage("N4", PROPT ); proof->EnablePackage("N4");
  cout << "N4 OK."<<endl;
  proof->UploadPackage("BP", PROPT ); proof->EnablePackage("BP");
  cout << "BP OK."<<endl;
  proof->UploadPackage("Dump", PROPT ); proof->EnablePackage("Dump");
  cout << "Dump OK."<<endl;
  proof->UploadPackage("Skeleton", PROPT ); proof->EnablePackage("Skeleton");
  cout << "Skeleton OK."<<endl;
  proof->UploadPackage("challenge", PROPT ); proof->EnablePackage("challenge");
  cout << "challenge OK."<<endl;
  proof->UploadPackage("DiLepChal", PROPT ); proof->EnablePackage("DiLepChal");
  cout << "DiLepChal OK."<<endl;

  cout << "All packages uploaded and enabled."<<endl;
  proof->AddDynamicPath("libs");
  proof->AddIncludePath("/tmp/proof/hepuser/packages/analysis_core/", kTRUE);
  proof->AddIncludePath("/tmp/proof/hepuser/packages/E61/", kTRUE);
  proof->AddIncludePath("/tmp/proof/hepuser/packages/E62/", kTRUE);
  proof->AddIncludePath("/tmp/proof/hepuser/packages/FF/", kTRUE);
  proof->AddIncludePath("/tmp/proof/hepuser/packages/N4/", kTRUE);
  proof->AddIncludePath("/tmp/proof/hepuser/packages/BP/", kTRUE);
  proof->AddIncludePath("/tmp/proof/hepuser/packages/Dump/", kTRUE);
  proof->AddIncludePath("/tmp/proof/hepuser/packages/Skeleton/", kTRUE);
  proof->AddIncludePath("/tmp/proof/hepuser/packages/challenge/", kTRUE);
  proof->AddIncludePath("/tmp/proof/hepuser/packages/DiLepChal/", kTRUE);
}
proof->ShowPackages(); // for debugging
#define __SIMPLE_TEST__
#ifdef __SIMPLE_TEST__
TDSet *aset;
aset = new TDSet("TTree","nt_tree");
if (lite) {

//for (int idx=1; idx<10; ++idx)
//        aset->Add(TString::Format("/Volumes/2TB/ttbar_nominal/user.nikiforo.410501.13TeV_ttbar.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_nonallhad.DAOD_EXOT4.2017.02.17_bkg_output.root_nominal_lvl0/user.nikiforo.11442762._00000%d.output-nominal-lvl0_ee.root",idx));
//for (int idx=10; idx<18; ++idx)
//        aset->Add(TString::Format("/Volumes/2TB/ttbar_nominal/user.nikiforo.410501.13TeV_ttbar.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_nonallhad.DAOD_EXOT4.2017.02.17_bkg_output.root_nominal_lvl0/user.nikiforo.11442762._0000%d.output-nominal-lvl0_ee.root",idx));
//aset->Add("/Volumes/2TB/signalLVL0_JET_21NP_JET_EffectiveNP_2__1up/user.istin.308963.E6_13TeV.CompHepPythia8EvtGen_A14NNPDF23LO_HbbarZlljj.EXOT4.e6253_a766_a821_r7676_p2949.v1_output.root_JET_21NP_JET_EffectiveNP_2__1up_lvl0/user.istin.12210180._000001.output-JET_21NP_JET_EffectiveNP_2__1up-lvl0_ee.root");
//aset->Add("/Volumes/2TB/signalLVL0_JET_21NP_JET_EffectiveNP_2__1up/user.istin.308963.E6_13TeV.CompHepPythia8EvtGen_A14NNPDF23LO_HbbarZlljj.EXOT4.e6253_a766_a821_r7676_p2949.v1_output.root_JET_21NP_JET_EffectiveNP_2__1up_lvl0/user.istin.12210180._000002.output-JET_21NP_JET_EffectiveNP_2__1up-lvl0_ee.root");
//aset->Add("/Volumes/2TB/signalLVL0_MET_SoftTrk_ScaleUp/user.istin.307510.E6_13TeV.CompHepPythia8EvtGen_A14_NNPDF23LO_HbbarZlljj.EXOT4.e5726_s2726_r7772_r7676_p2949.v1_output.root_MET_SoftTrk_ScaleUp_lvl0/user.istin.12301532._000001.output-MET_SoftTrk_ScaleUp-lvl0_ee.root");
//aset->Add("/Volumes/2TB/signalLVL0_MET_SoftTrk_ScaleUp/user.istin.307510.E6_13TeV.CompHepPythia8EvtGen_A14_NNPDF23LO_HbbarZlljj.EXOT4.e5726_s2726_r7772_r7676_p2949.v1_output.root_MET_SoftTrk_ScaleUp_lvl0/user.istin.12301532._000002.output-MET_SoftTrk_ScaleUp-lvl0_ee.root");
//aset->Add("/Volumes/2TB/signal_lvl0/user.istin.307510.E6_13TeV.CompHepPythia8EvtGen_A14_NNPDF23LO_HbbarZlljj.EXOT4.e5726_s2726_r7772_r7676_p2949.v1_output.root_nominal_lvl0/user.istin.12301532._000001.output-nominal-lvl0_ee.root");
//aset->Add("/Volumes/2TB/signal_lvl0/user.istin.308963.E6_13TeV.CompHepPythia8EvtGen_A14NNPDF23LO_HbbarZlljj.EXOT4.e6253_a766_a821_r7676_p2949.v1_output.root_nominal_lvl0/user.istin.12210180._000001.output-nominal-lvl0_ee.root");
//aset->Add("/Volumes/atb3TB/LVL0-SYS-AUG17-B/Zjets_Sherpa221_JET_21NP_JET_EffectiveNP_3__1up_ee/user.nikiforo.364118.13TeV_Zjets_Sherpa221.Zee_MAXHTPTV70_140_CFilterBVeto.DAOD_EXOT4.2017.03.02_bkg_output.root_JET_21NP_JET_EffectiveNP_3__1up_lvl0/user.nikiforo.10867863._000006.output-JET_21NP_JET_EffectiveNP_3__1up-lvl0_ee.root");
//aset->Add("/Volumes/atb3TB/LVL0-SYS-AUG17-B/Zjets_Sherpa221_nominal_ee/user.nikiforo.364123.13TeV_Zjets_Sherpa221.Zee_MAXHTPTV280_500_CVetoBVeto.DAOD_EXOT4.2017.03.02_bkg_output.root_nominal_lvl0/user.nikiforo.10867884._000001.output-nominal-lvl0_ee.root");
//aset->Add("/Volumes/2TB/signaLVL0_JET_21NP_JET_PunchThrough_MC15__1down/user.istin.307510.E6_13TeV.CompHepPythia8EvtGen_A14_NNPDF23LO_HbbarZlljj.EXOT4.e5726_s2726_r7772_r7676_p2949.v1_output.root_JET_21NP_JET_PunchThrough_MC15__1down_lvl0/user.istin.12301532._000001.output-JET_21NP_JET_PunchThrough_MC15__1down-lvl0_ee.root");

//aset->Add("/Volumes/2TB/ttbar_EG_RESOLUTION_ALL__1down/user.nikiforo.410501.13TeV_ttbar.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_nonallhad.DAOD_EXOT4.2017.02.17_bkg_output.root_EG_RESOLUTION_ALL__1down_lvl0/user.nikiforo.11442762._000001.output-EG_RESOLUTION_ALL__1down-lvl0_ee.root");

//aset->Add("/Volumes/atb3TB/BKG-MODELL-LVL0/ttbar_VV/ttbar_nominal/user.nikiforo.410501.ttbar.ttbar_hdamp258p75_nonallhad.EXOT4.e5458_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root_nominal_lvl0/user.nikiforo.11699360._000001.output-nominal-lvl0_ee.root"); 

/*aset->Add("/Volumes/atb3TB/BKG-MODELL-LVL0/ttbar_VV/ttbar_nominal/user.nikiforo.410501.ttbar.ttbar_hdamp258p75_nonallhad.EXOT4.e5458_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root_nominal_lvl0/user.nikiforo.11699360._000022.output-nominal-lvl0_ee.root");
aset->Add("/Volumes/atb3TB/BKG-MODELL-LVL0/ttbar_VV/ttbar_nominal/user.nikiforo.410501.ttbar.ttbar_hdamp258p75_nonallhad.EXOT4.e5458_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root_nominal_lvl0/user.nikiforo.11699360._000023.output-nominal-lvl0_ee.root");
aset->Add("/Volumes/atb3TB/BKG-MODELL-LVL0/ttbar_VV/ttbar_nominal/user.nikiforo.410501.ttbar.ttbar_hdamp258p75_nonallhad.EXOT4.e5458_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root_nominal_lvl0/user.nikiforo.11699360._000021.output-nominal-lvl0_ee.root");
aset->Add("/Volumes/atb3TB/BKG-MODELL-LVL0/ttbar_VV/ttbar_nominal/user.nikiforo.410501.ttbar.ttbar_hdamp258p75_nonallhad.EXOT4.e5458_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root_nominal_lvl0/user.nikiforo.11699360._000020.output-nominal-lvl0_ee.root");
aset->Add("/Volumes/atb3TB/BKG-MODELL-LVL0/ttbar_VV/ttbar_nominal/user.nikiforo.410501.ttbar.ttbar_hdamp258p75_nonallhad.EXOT4.e5458_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root_nominal_lvl0/user.nikiforo.11699360._000019.output-nominal-lvl0_ee.root");
aset->Add("/Volumes/atb3TB/BKG-MODELL-LVL0/ttbar_VV/ttbar_nominal/user.nikiforo.410501.ttbar.ttbar_hdamp258p75_nonallhad.EXOT4.e5458_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root_nominal_lvl0/user.nikiforo.11699360._000018.output-nominal-lvl0_ee.root");
aset->Add("/Volumes/atb3TB/BKG-MODELL-LVL0/ttbar_VV/ttbar_nominal/user.nikiforo.410501.ttbar.ttbar_hdamp258p75_nonallhad.EXOT4.e5458_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root_nominal_lvl0/user.nikiforo.11699360._000017.output-nominal-lvl0_ee.root");
aset->Add("/Volumes/atb3TB/BKG-MODELL-LVL0/ttbar_VV/ttbar_nominal/user.nikiforo.410501.ttbar.ttbar_hdamp258p75_nonallhad.EXOT4.e5458_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root_nominal_lvl0/user.nikiforo.11699360._000016.output-nominal-lvl0_ee.root");
aset->Add("/Volumes/atb3TB/BKG-MODELL-LVL0/ttbar_VV/ttbar_nominal/user.nikiforo.410501.ttbar.ttbar_hdamp258p75_nonallhad.EXOT4.e5458_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root_nominal_lvl0/user.nikiforo.11699360._000015.output-nominal-lvl0_ee.root");
aset->Add("/Volumes/atb3TB/BKG-MODELL-LVL0/ttbar_VV/ttbar_nominal/user.nikiforo.410501.ttbar.ttbar_hdamp258p75_nonallhad.EXOT4.e5458_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root_nominal_lvl0/user.nikiforo.11699360._000014.output-nominal-lvl0_ee.root");
aset->Add("/Volumes/atb3TB/BKG-MODELL-LVL0/ttbar_VV/ttbar_nominal/user.nikiforo.410501.ttbar.ttbar_hdamp258p75_nonallhad.EXOT4.e5458_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root_nominal_lvl0/user.nikiforo.11699360._000013.output-nominal-lvl0_ee.root");
aset->Add("/Volumes/atb3TB/BKG-MODELL-LVL0/ttbar_VV/ttbar_nominal/user.nikiforo.410501.ttbar.ttbar_hdamp258p75_nonallhad.EXOT4.e5458_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root_nominal_lvl0/user.nikiforo.11699360._000012.output-nominal-lvl0_ee.root");
aset->Add("/Volumes/atb3TB/BKG-MODELL-LVL0/ttbar_VV/ttbar_nominal/user.nikiforo.410501.ttbar.ttbar_hdamp258p75_nonallhad.EXOT4.e5458_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root_nominal_lvl0/user.nikiforo.11699360._000011.output-nominal-lvl0_ee.root");
aset->Add("/Volumes/atb3TB/BKG-MODELL-LVL0/ttbar_VV/ttbar_nominal/user.nikiforo.410501.ttbar.ttbar_hdamp258p75_nonallhad.EXOT4.e5458_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root_nominal_lvl0/user.nikiforo.11699360._000010.output-nominal-lvl0_ee.root");
aset->Add("/Volumes/atb3TB/BKG-MODELL-LVL0/ttbar_VV/ttbar_nominal/user.nikiforo.410501.ttbar.ttbar_hdamp258p75_nonallhad.EXOT4.e5458_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root_nominal_lvl0/user.nikiforo.11699360._000005.output-nominal-lvl0_ee.root");
aset->Add("/Volumes/atb3TB/BKG-MODELL-LVL0/ttbar_VV/ttbar_nominal/user.nikiforo.410501.ttbar.ttbar_hdamp258p75_nonallhad.EXOT4.e5458_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root_nominal_lvl0/user.nikiforo.11699360._000004.output-nominal-lvl0_ee.root");
aset->Add("/Volumes/atb3TB/BKG-MODELL-LVL0/ttbar_VV/ttbar_nominal/user.nikiforo.410501.ttbar.ttbar_hdamp258p75_nonallhad.EXOT4.e5458_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root_nominal_lvl0/user.nikiforo.11699360._000001.output-nominal-lvl0_ee.root");
aset->Add("/Volumes/atb3TB/BKG-MODELL-LVL0/ttbar_VV/ttbar_nominal/user.nikiforo.410501.ttbar.ttbar_hdamp258p75_nonallhad.EXOT4.e5458_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root_nominal_lvl0/user.nikiforo.11699360._000002.output-nominal-lvl0_ee.root");
aset->Add("/Volumes/atb3TB/BKG-MODELL-LVL0/ttbar_VV/ttbar_nominal/user.nikiforo.410501.ttbar.ttbar_hdamp258p75_nonallhad.EXOT4.e5458_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root_nominal_lvl0/user.nikiforo.11699360._000003.output-nominal-lvl0_ee.root");
*/

}else{
 //  aset->Add("root://10.0.1.2//Users/ngu/project/dbx_work/analyses/toplu/lvl0-1.root");
   //aset->Add("root://pc-atb-srv-01//media/data1/2017/signal/user.istin.11370185._000001.output-lvl0.root");
   //aset->Add("root://pc-atb-srv-01//media/data1/2017_a/ttbar/user.nikiforo.410501.13TeV_ttbar.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_nonallhad.DAOD_EXOT4.2017.02.17_bkg_output.root_lvl0/user.nikiforo.11442762._000001.output-lvl0_ee.root");
//for (int idx=1; idx<10; ++idx)
  //      aset->Add(TString::Format("root://pc-atb-srv-01//media/data1/2017_a/ttbar/user.nikiforo.410501.13TeV_ttbar.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_nonallhad.DAOD_EXOT4.2017.02.17_bkg_output.root_lvl0/user.nikiforo.11442762._00000%d.output-lvl0_ee.root",idx));
    //for (int idx=10; idx<41; ++idx)
 //       aset->Add(TString::Format("root://pc-atb-srv-01//media/data1/2017_a/ttbar/user.nikiforo.410501.13TeV_ttbar.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_nonallhad.DAOD_EXOT4.2017.02.17_bkg_output.root_lvl0/user.nikiforo.11442762._0000%d.output-lvl0_ee.root",idx));
	for(int i = 1; i < 10; ++i) aset->Add(TString::Format("root://10.0.0.1//home/hepuser/user.istin.364122.Zjets_Sherpa221.Zee_MAXHTPTV140_280_BFilter_nominal_0N2_XYZ.root.tgz/user.nikiforo.11699390._00000%d.output-nominal-lvl0_ee.root",i));

	for(int i = 10; i < 41; ++i) aset->Add(TString::Format("root://10.0.0.1//home/hepuser/user.istin.364122.Zjets_Sherpa221.Zee_MAXHTPTV140_280_BFilter_nominal_0N2_XYZ.root.tgz/user.nikiforo.11699390._0000%d.output-nominal-lvl0_ee.root",i));

}
Long64_t nevt=-1; // a positive integer is the event number or -1 for all events
aset->Process("dbxAna.C+",opts,nevt);
//aset->Process(dbxAnaSel,opts,nevt);

//---------------not simple test.
#else
//------------------------------
//gProof->SetLogLevel(2, TProofDebug::kPacketizer | TProofDebug::kGlobal);  // this is big long debug statement
//aset->Add("lvl0-3.root");  
//aset->GetListOfElements()->Print();
// we should get the cmd line parameters and pass these to below

Long64_t nevt=-1; // a positive integer is the event number
//opts=" -FF 1 -S 1"; // use the cmd line

//vector <TString> runnumbers;
//                 runnumbers.push_back("107700");
//                 runnumbers.push_back("107701");
//                 runnumbers.push_back("107702");
//                 runnumbers.push_back("107703");
//                 runnumbers.push_back("107704");
//                 runnumbers.push_back("107705");

//for (UInt_t itr=0; itr<runnumbers.size(); itr++) {
  //  TString runname_tmp=runnumbers.at(itr);
    TString  runfile=runname_tmp;
            runfile+=".txt";
    TString  outcmd="mv histoOut-tot.root ";
             outcmd+=runname_tmp;
             outcmd+=".root";
    TFileCollection *fc = new TFileCollection("a", "a", runfile);
    proof->Process(fc,"dbxAna.C+",opts,nevt);
    gSystem->Exec(outcmd);
//}
#endif

}
