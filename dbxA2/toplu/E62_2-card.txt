# analysis parameters card for FF wjwj semileptonic
# format is "variable = value"
# PLEASE PAY ATTENTION TO SPACE BEFORE AND AFTER = SIGN

#----------------------------------1st systematic is at the lowest bit on the RHS.
systematics_bci = 0b11111111111111111
#                      ^last-m   ^last-e


minpte  = 30.0  # min pt of electrons old 25
minptm  = 30.0  # min pt of muons 
maxz0e = 2           #max z0 wrt PV for electrons   //we are not using now
maxz0m = 2           #max z0 wrt PV for muons
jetVtxf2011 = 0.75  #jet vertex fraction of 2011
jetVtxf2012 = 0.50  #jet vertex fraction of 2012

maxetae = 2.47   # max pseudorapidity of electrons  TO BE OPTIMIZED
maxetam = 2.5  # max pseudorapidity of muons TO BE OPTIMIZED

minmete = 60   # min Met of Elec
minmetmu = 60  # min Met of Muon
minptj  = 25.0   # min pt of jets
maxetaj = 2.50   # max pseudorapidity of jets
mindrjm = 0.4   # min deltaR between jets and muons
mindrje = 0.2   # min deltaR between jets and electrons
mqhxmin = 50    # for the final HQ mass histo, x-axis min
mqhxmax = 850 # for the final HQ mass histo, x-axis max
maxMuPtCone = 2.5 # Max muon Pt for 2011
maxMuEtCone = 4.0 # Max muon Et for 2011

# E6 Analysis Parameters
maxDeltaZmassEl = 10 # maximum Z mass spread of Elec
maxDeltaZmassMu = 10 # maximum Z mass spread of Muon old 15
HerrorM = 15 # Higgs Chisquare sigma Value of Muon
DHerrorM = 40 # De from Higgs Chisquare sigma Value of Muon
DZerrorM = 30 # De from Z Chisquare sigma Value of Muon
HerrorE = 12 # Higgs Chisquare sigma Value of Elec
DHerrorE = 45 # De from Higgs Chisquare sigma Value of Elec
DZerrorE = 25 # De from Z Chisquare sigma Value of Elec
MaximumChi = 15 # Max Chisqure value
MinRDist = 1 # Minimum R Dist between Jets
MinPJetPtE = 100 # Minimum Pt of at least one of the Propmt Jets for Electron
MinPJetPtM = 100 # Minimum Pt of at least one of the Propmt Jets for Muon
MinSubPJetPtE = 65 # Minimum Pt of at least one of the Propmt Jets for Electron
MinSubPJetPtM = 65 # Minimum Pt of at least one of the Propmt Jets for Muon

TRGm = 2 #     muon Trigger Type: 0=dont trigger, 1=1st trigger (data) 2=2nd trigger (MC)
TRGe = 0 # electron Trigger Type: 0=dont trigger, 1=1st trigger (data) 2=2nd trigger (MC)
###### DEFINITIONS

def1 = "mLL : { LEP_1 LEP_0 }m"
def2 = "qLL : { LEP_1 LEP_0 }q"
def3 = "Zreco : LEP_1 LEP_0 "
def4 = "dR(LL,J0) : { Zreco , JET_0 }R "

########These are the preselection cuts. cut0 is reserved to count all events without weight. testTable
cut1  = "ALL "
cut2  = "nLEP >= 1 "
cut3  = "nLEP >= 2 "
cut4  = "qLL == 0 " #same as qLEP.qLEP but more generic
cut5  = "LEPsf " # this to say include LEPTON ScaleFactor at this point
cut6  = "mLL <= 400 " #same as m2LEP, but more generic: { JET_0 LEP_0 LEP_1 }m [] 900 1100
cut7  = "FillHistos "
cut8  = "nLEP == 2 "
cut9  = "nJET >= 2 "
cut10 = "FillHistos "
cut11 = "nBJET >= 2 "
cut12 = "mLL ][ 50 81 OR mLL > 101 " #look at the defs...
cut13 = " dR(LL,J0) <= 2.0 OR dR(LL,J0) >= 2.8 "
cut14 = "SumHTJET < 1150 "
cut15 = "nJET >= 4 " # 4jets
cut16 = "FillHistos " # like that



########
########REGULAR
#cut1  = "ALL "
#cut2  = "nLEP >= 1 "
#cut3  = "nLEP >= 2 "
#cut4  = "nLEP == 2 "
#cut5  = "qLL == 0 "
#cut6  = "LEPsf "
#cut7  = "FillHistos "
#cut8  = "nJET >= 2 "
#cut9 = "FillHistos "
#cut10 = "nJET >= 4 "
#cut11 = "FillHistos "
#cut12 = "mLL > 15 "
#cut13 = "mLL ][  81 101 "
cut14 = "{ JET_0 }Pt > 100 "
########ttbarCRT9
#cut1  = "ALL "
#cut2  = "nLEP >= 1 "
#cut3  = "nLEP >= 2 "
#cut4  = "qLL == 0 "
#cut5  = "LEPsf "  ## this to say include LEPTON ScaleFactor at this point
#cut6  = "mLL <= 400 "
#cut7  = "FillHistos "
#cut8  = "nLEP == 2 "
#cut9 = "nJET >= 2 "
#cut10 = "FillHistos "
#cut11 = "nBJET >= 2 "
#cut12 = "mLL ][ 50 81 OR mLL > 101 " #using [] notation
#cut13 = "{ Zreco }Pt < 600 "
#cut14 = "MET < 200 "
#cut15 = "R2LEPJ0 <= 2.0 OR R2LEPJ0 >= 2.8 "
#cut16 = "nJET >= 4 "
#cut17 = "FillHistos "
########zjetsCRT9
#cut1  = "ALL "
#cut2  = "nLEP >= 1 "
#cut3  = "nLEP >= 2 "
#cut4  = "qLL == 0 "
#cut5  = "LEPsf "  ## this to say include LEPTON ScaleFactor at this point
#cut6  = "mLL <= 400 "
#cut7  = "FillHistos "
#cut8  = "nLEP == 2 "
#cut9 = "nJET >= 2 "
#cut10 = "FillHistos "
#cut11 = "nBJET >= 2 "
#cut12 = "mLL ][  81 101  "
#cut13 = "SumHTJET < 1150 "
#cut14 = "nJET >= 4 " # 4jets
#cut15 = "FillHistos " # like that
########ttbarCRT3
#cut1  = "ALL "
#cut2  = "nLEP == 2 "
#cut3  = "LEPsf "  ## this to say include LEPTON ScaleFactor at this point
#cut4  = "nBJET >= 2 "
#cut5  = "FillHistos "
#cut6  = "mLL ][ 50 81 OR mLL > 101 "
#cut7  = "FillHistos "
#cut8  = "{ Zreco }Pt < 600 "
#cut9 = "nJET >= 4 "
#cut10 = "FillHistos "
########zjetsCRT3
#cut1  = "ALL "
#cut2  = "nLEP == 2 "
#cut3  = "LEPsf "  ## this to say include LEPTON ScaleFactor at this point
#cut4  = "nBJET >= 2 "
#cut5  = "FillHistos "
#cut6  = "mLL ][  81 101 "
#cut7  = "FillHistos "
#cut8 = "{ Zreco }Pt < 250 "
#cut9 = "nJET >= 4 "
#cut10 = "FillHistos "


