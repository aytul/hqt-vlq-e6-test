//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Mar 14 10:31:47 2012 by ROOT version 5.32/00
//////////////////////////////////////////////////////////

#ifndef dbxAna_h
#define dbxAna_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include "TProofOutputFile.h"
#include <TSelector.h>

// Header file for the classes stored in the TTree if any.
#include <vector>
// analysis related headers
#include "DBXNtuple.h"
#include "ff_a.h"
//#include "challenge.h"
#include "DiLepChal.h"
#include "e6_a1.h"
#include "e6_a2.h"
#include "n4_a.h"
#include "bp_a.h"
#include "skeleton_a.h"
#include "dump_a.h"
#include "analysis_core.h"
#include "AnalysisController.h"
#include "AtlMin.h"
#include "VLLMin.h"


// Fixed size dimensions of array or collections stored in the TTree if any.

class dbxAna : public TSelector {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   DBXNtuple      *event ;   // DBX analysis ntuple
   TBranch        *b_event; 
   TProofOutputFile *fProofFile; //optimized proof file merger BS
   TH1F           *hdmd; //dandik histogram
   AtlMin      *atlmina; // belki de mini ntuple okuruz
   VLLMin      *vllmina; // belki de atlas vll mini ntuple okuruz:)

     char        dtype;

   analy_struct aselect;
// temporary ------- until we find a better sltn
// make space for the multiple analysis
   FFdbxA  **FFanalyses;
   E61dbxA **E61analyses;
   E62dbxA **E62analyses;
   N4dbxA  **N4analyses;
   Challenge **MCHanalyses ;
   DiLepChal **DCHanalyses ;
   SkeletondbxA **Skeletonanalyses;
   DumpdbxA **Dumpanalyses;
   int extra_analysis_count;
   vector<string> analynames;
   AnalysisController *aCtrl;
    vector <dbxMuon> mu0; vector<dbxElectron> el0; vector<dbxJet> je0; vector<dbxLJet> lje0; vector<dbxTruth> tru0; TVector2 me0; evt_data ev0;

   dbxAna(TTree * /*tree*/ =0) : fChain(0), fProofFile(0) { hdmd=0; }
   virtual ~dbxAna() { }
   virtual Int_t   Version() const { return 2; }
   virtual void    Begin(TTree *tree);
   virtual void    SlaveBegin(TTree *tree);
   virtual void    Init(TTree *tree);
   virtual Bool_t  Notify();
   virtual Bool_t  Process(Long64_t entry);
   virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { if (dtype=='l') { 
                                                                  return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; 
                                                                } else {
                                                                  return atlmina->GetEntry(entry);
                                                                } else if (dtype=='v') {
                                                                    return vllmina->GetEntry(entry);
                                                                }
   }
   virtual void    SetOption(const char *option) { fOption = option; }
   virtual void    SetObject(TObject *obj) { fObject = obj; }
   virtual void    SetInputList(TList *input) { fInput = input; }
   virtual TList  *GetOutputList() const { return fOutput; }
   virtual void    SlaveTerminate();
   virtual void    Terminate();

   ClassDef(dbxAna,0);
};

#endif

#ifdef dbxAna_cxx
void dbxAna::Init(TTree *tree)
{
 if (dtype=='m'){
   atlmina=new AtlMin("XXX",(TChain*)tree, 2016);

 } else if (dtype=='v'){
     vllmina=new VLLMin("/physics/nominal",(TChain*)tree, 2016);
     
 } else {
   // Initialize the object pointer
   event = new DBXNtuple();

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fChain->SetBranchAddress("dbxAsave", &event, &b_event);
  }
}

Bool_t dbxAna::Notify()
{
   return kTRUE;
}

#endif // #ifdef dbxAna_cxx
