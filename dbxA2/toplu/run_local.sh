#!/bin/bash


##tsf=`which ts`
##if [ ${#tsf} -lt 2 ]; then
## echo this script relies on TaskSpooler, please install it from http://vicerveza.homeunix.net/~viric/soft/ts/
## exit -1
##fi


if [ $USER == "ngu" ]; then
  ncores=2;
  fpath=../../data/rel17/u4
  fpath=/Users/ngu/project/dbx_work/data/rel17/det-e
  fpath=/Volumes/250GB/rel17-data
else
  ncores=4;
  fpath=/home/sahinsoy/Desktop/Data/challenge/u4

fi


flist=`ls $fpath/NTUP_T*`

mcflist="
       $fpath/NTUP_TOPBOOST.662279._000001.root.1 
       $fpath/NTUP_TOPBOOST.662279._000002.root.1 
       $fpath/NTUP_TOPBOOST.662279._000003.root.1 
       $fpath/NTUP_TOPBOOST.662279._000004.root.1 
       $fpath/NTUP_TOPBOOST.662279._000005.root.1 
       $fpath/NTUP_TOPBOOST.662279._000006.root.1 
       $fpath/NTUP_TOPBOOST.662279._000007.root.1 
       $fpath/NTUP_TOPBOOST.662279._000008.root.1
      "

ccore=1;
job=1;
no_free_cores=true;
rm -rf job*.pid 
rm -rf job*.out 

for fname in $flist ; do
    if [ $ccore -le $ncores ]; then
#        ./root_analysisd3pd $fname -MCH 1 -UN _$job > job${job}.out &
        ./root_analysisd3pd $fname -D 1 -UN _$job > job${job}.out &
#        echo submitting ${job}
        PID=$!  
        echo $PID > job${job}.pid
        job=$(( $job + 1 ))
        ccore=$(( $ccore + 1 ))
        if [ $ccore -gt $ncores ] ; then 
          no_free_cores=true
        fi
    fi
sleep 1
# here we sleep until a core is freed.
    echo core busy status: $no_free_cores   ccore $ccore
    while [ $no_free_cores == true ]; do 
        if [ $ccore -le $ncores ]; then
          echo there is at least 1 free slot
          no_free_cores=false
        else 
#          echo checking for free cores, sleeping
          for pfile in job*.pid ; do
           p2check=`cat $pfile`; echo -n checking $p2check
           APPCHK=$(ps ux | grep -v grep| awk '{print $1, $2}'| grep $p2check )
           if [ ${#APPCHK} -gt 0 ]; then
             echo " process is running, will wait"
           else
            echo " process is finished"
            rm -f $pfile
            ccore=$(( $ccore - 1 ))
           fi
          done
          echo
          sleep 5
        fi 
          
    done

done

# all jobs submitted, now wait for the finish
still_running=true;

while [ $still_running == true ]; do

pfl=`ls job*.pid`;
if [ ${#pfl} -gt 1 ]; then
   for pfile in job*.pid ; do
      p2check=`cat $pfile`; echo checking $p2check
      APPCHK=$(ps ux | grep -v grep| awk '{print $1, $2}'| grep $p2check )
      if [ ${#APPCHK} -eq 0 ]; then
         rm -f $pfile
      fi
   done
   sleep 5
else
   still_running=false
fi
done

#adding them all:
#rm -f histoOut-MCH_1.root
#hadd histoOut-MCH_1.root histoOut-MCH_1_*.root
#if [ -a histoOut-MCH_1.root ]; then
# rm -f  histoOut-MCH_1_*.root
#fi

#adding them all:
rm -f histoOut-Dump_1.root
hadd histoOut-Dump_1.root histoOut-MCH_1_*.root
if [ -a histoOut-Dump_1.root ]; then
 rm -f  histoOut-Dump_1_*.root
fi



echo -n "All ev = "          ; cat job*.out| grep  "          all w" | awk '{ SUM+=$10}; END {print SUM}' 
echo -n "TRiGger = "         ;  cat job*.out| grep  "        TRiGger" | awk '{ SUM+=$9}; END {print SUM}' 
echo -n "OQ==2 = "           ; cat job*.out| grep  "          OQ==2" | awk '{ SUM+=$9}; END {print SUM}' 
echo -n "Vtx Trks>=5 = "     ;  cat job*.out| grep  "    Vtx Trks>=5" | awk '{ SUM+=$10}; END {print SUM}' 
echo -n ">=1 Lepton = "      ;  cat job*.out| grep  "     >=1 Lepton" | awk '{ SUM+=$10}; END {print SUM}' 
echo -n "=1e/m pT>25/20 =  " ;  cat job*.out| grep  " =1e/m pT>25/20" | awk '{ SUM+=$10}; END {print SUM}' 
echo -n "e OR m =  "         ;  cat job*.out| grep  "         e OR m" | awk '{ SUM+=$11}; END {print SUM}' 
echo -n "TRiGger match = "   ;  cat job*.out| grep  "  TRiGger match" | awk '{ SUM+=$10}; END {print SUM}' 
echo -n "e-mu overlap = "    ;  cat job*.out| grep  "   e-mu overlap" | awk '{ SUM+=$10}; END {print SUM}' 
echo -n "bad goodJets>0 =  " ;  cat job*.out| grep  " bad goodJets>0" | awk '{ SUM+=$10}; END {print SUM}' 
echo -n "# Iso-goodJets>=2 = "  ;  cat job*.out| grep  " # Iso-goodJets>=2" | awk '{ SUM+=$10}; END {print SUM}' 
echo -n "# Iso-goodJets>=3 =  " ;  cat job*.out| grep  " # Iso-goodJets>=3" | awk '{ SUM+=$10}; END {print SUM}'
echo -n "# Iso-goodJets>=4 =  " ;  cat job*.out| grep  " # Iso-goodJets>=4" | awk '{ SUM+=$10}; END {print SUM}'
echo -n "MET m/e >20/35 =  " ;  cat job*.out| grep  " MET m/e >20/35" | awk '{ SUM+=$11}; END {print SUM}' 
echo -n "MWT m/e >60/25 = "  ;  cat job*.out| grep  " MWT m/e >60/25" | awk '{ SUM+=$11}; END {print SUM}' 
echo -n "b-tag veto = "      ;  cat job*.out| grep  "     b-tag veto =" | awk '{ SUM+=$10}; END {print SUM}'

