#!/bin/bash

# 8-9 - 1,2,5
# 3-6 - 1,2,5
# 4-7 - 1,2,5
#and the x10 s

lim=3
#limdir=results_muon_tlimit
limdir=results_muon_mclimit
rm -rf limits.out

set1=(3 4 8 30 40 80)
set2=(6 7 9 60 70 90)

for i in 0 1 2 3 4 5; do
 for k in 1 2 5 ; do
  fn=${set1[i]}"vs"${set2[i]}_$k.tex
  echo "\begin{centering}" > $fn
  echo "\begin{tabular}{c|c|c|c|c|c|c}" >> $fn
  echo '\hline' >> $fn
  echo 'Method & Exp -2 & Exp -1 & Exp & Obs & Exp +1 & Exp +2 \\' >> $fn
  echo '\hline' >> $fn
  echo '\hline' >> $fn
  echo -n '$\chi^2$ & ' >> $fn
  cp ${limdir}/Limit_1034.5_${set1[i]}_${k}_MUON_FF_1_0_${lim}.txt Limit.txt
  root.exe -l -q -x ExclusionPlot.C | grep -v Processing | grep -v Welcome | grep -v "^$"| cut -f2 -d':'  | awk '{ printf $0 "&"}'  | cut -f-6 -d'&' >> $fn
  echo ' \\' >> $fn

  echo '\hline' >> $fn
  echo -n 'KLF & ' >> $fn
  cp ${limdir}/Limit_1034.5_${set2[i]}_${k}_MUON_FF_1_0_${lim}.txt Limit.txt
  root.exe -l -q -x ExclusionPlot.C | grep -v Processing | grep -v Welcome | grep -v "^$"| cut -f2 -d':'  | awk '{ printf $0 "&"}'  | cut -f-6 -d'&' >> $fn
  echo ' \\' >> $fn


  echo '\hline' >> $fn
  echo "\end{tabular}" >> $fn
  echo "\end{centering}" >> $fn
 done
done

