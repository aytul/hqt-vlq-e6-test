
#include <numeric>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>
#include <iterator>
#include <cmath>
#include <cctype> // for toupper
#include <vector>

#include "TH1.h"
#include "TParameter.h"
#include "TFile.h"
#include "TNtuple.h"

#include "model.h"
#include "neyman.h"
#include "fit.h"

float poi_range=10.0;
int npe_exp = 10000;


TNtuple *make_ntuple(model *m)
{
  TNtuple *nt;
  char ntform[500] = "imu:true:nev:fit:ipe:lh_best:lh_mu:lambda_mu:q_mu";

  for (int z=0;z<m->nsamples;z++)
    strcat(ntform,Form(":scale%d_true:scale%d_mu:scale%d_fit",z+1,z+1,z+1));

  for (int z=0;z<m->nsyst;z++)
    strcat(ntform,Form(":shape%d_true:shape%d_mu:shape%d_fit",z+1,z+1,z+1));
  
  std::cout << "Ntuple string is " << ntform << std::endl;
  
  nt = new TNtuple("neyman","",ntform);
  return nt;
}

double q_mu(model *mymodel, TH1F *toy_data, double mu)
{
  
  // get best fit mu and nu
  double mu_hat;
  nuis_params nu_hat;
  nu_hat.init(mymodel->nsamples,mymodel->nsyst);
  double lh_best = fit_histo( toy_data , mymodel, mu_hat, nu_hat,false);
  if (mu_hat<0)
    {
      mu_hat = 0;
      // fix mu to 0, maximize the nps, recalc lhood	    
      nu_hat.init(mymodel->nsamples,mymodel->nsyst);
      lh_best = fit_histo( toy_data , mymodel, mu_hat, nu_hat,false, mu_hat);
    }

  // get best fit nu at this mu
  nuis_params nu_hat_hat;
  nu_hat_hat.init(mymodel->nsamples,mymodel->nsyst);
  double lh_mu   = fit_histo( toy_data , mymodel, mu, nu_hat_hat, false,mu);
  
  double lambda_mu = lh_mu / lh_best;

  double res = 0;
  if (mu_hat > mu)
    res = 0;
  else
    {
      if (lambda_mu>0)
	res = - 2 * log (lambda_mu );
      else
	{
	  std::cout << "Error lambda_mu shouldbe > 0 " << lambda_mu << " lh mu " << lh_mu << " lh best " << lh_best << std::endl;
	  std::cout << " ===== recreate lh mu ====== " << std::endl;
	  fit_histo( toy_data , mymodel, mu, nu_hat_hat, true,mu);
	  std::cout << " ===== recreate lh best ====== " << std::endl;
	  fit_histo( toy_data , mymodel, mu_hat, nu_hat,false);
	}
    }
  return res;
}

int main(int argc, char *argv[])
{

  if (argc <4)
    {
      std::cout << "Usage: ./pcl <bg+data file> Nbackgrounds Nsyst <sig file>" << std::endl;
      exit(0);
    }
  // inputs:  sig file, bg file, nsamp, nsyst
  char fname_bg[100];
  strcpy(fname_bg,argv[1]);
  int nsamples = atoi(argv[2]);
  int nsyst = atoi(argv[3]);
  char fname_sig[100];
  strcpy(fname_sig,argv[4]);
  
  std::cout << " Bg file = " << fname_bg << std::endl;
  std::cout << " Nsamples = " << nsamples << std::endl;

  std::cout << " Nsystem. = " << nsyst << std::endl;
  std::cout << " Sig file = " << fname_sig << std::endl;

  // build the model
  model *mymodel = new model( fname_sig, fname_bg, nsamples, nsyst, poi_range);

  // open an output file
  TString foutname;
  foutname.Form("pvalue_bg-%s_samp_%d_sys_%d_sig-%s",fname_bg,nsamples,nsyst,fname_sig);
  foutname.ReplaceAll('/','_');
  std::cout << " Output file = " << foutname << std::endl;
  TFile *tf = new TFile(foutname,"recreate");

  // define an ntuple that tracks fitter performance etc
  TNtuple *nt = make_ntuple(mymodel);
// get nu for mu=0 in data
  double mu;
  nuis_params nu_mu0_obs;
  nu_mu0_obs.init( mymodel->nsamples,mymodel->nsyst);
  fit_histo( mymodel->obs , mymodel, mu, nu_mu0_obs, 0.0 );

  /// expected test stat
  TH1F *thtstat = new TH1F("thtstat","",1000,-0.1,10.0);
  std::vector<double> ts_back;

  std::cout << "ooo Generated pseudo-data at POI=0 (no signal) to get SM distribution ooo " << std::endl;

  TNtuple *tnte = new TNtuple("perf","","ipe:nev:lim");
  for (int ipe=0; ipe < npe_exp; ipe++)
    {
      if (ipe%1000==0) std::cout << " PE " << ipe << " / " << npe_exp << std::endl;

      TH1F *toy_data =  mymodel->gen_exp(0, nu_mu0_obs );
      mymodel->randomize_priors();
 
      double qmu = q_mu(mymodel,toy_data,1.0);

      ts_back.push_back(qmu);
      thtstat->Fill(qmu);
    }
  sort(ts_back.begin(),ts_back.end());
  mymodel->reset_priors();

  std::cout << " (5,16,50,84,95) threshold are (" 
	    << ts_back[ ((int) ts_back.size()*0.05 ) ] << ", "
	    << ts_back[ ((int) ts_back.size()*0.16 ) ] << ", "
	    << ts_back[ ((int) ts_back.size()*0.50 ) ] << ", "
	    << ts_back[ ((int) ts_back.size()*0.94 ) ] << ", "
	    << ts_back[ ((int) ts_back.size()*0.95 ) ] << ") " << std::endl;


  double qmu_obs = q_mu(mymodel,mymodel->obs,1.0);
  std::cout << " Observed q_mu is " << qmu_obs << std::endl;
  for (int i=0;i<ts_back.size();i++)
    {
      if (qmu_obs < ts_back[i])
	{
	  
	  std::cout << " p-value is " << i << " / " << ts_back.size() << " = " << (i*1.0)/ts_back.size() << std::endl;
	  break;
	}
    }
  tf->cd();
  thtstat->Write();
  TParameter<double> *qo = new TParameter<double>("obs",qmu_obs);
  qo->Write();
  tf->Close();
}

