#include "TH1.h"
#include <vector>
#include "model.h"

double fit_histo(TH1F *data, model *m, double &poi, nuis_params &nuis, bool verbose=false,double poi_fix = -1.0);

