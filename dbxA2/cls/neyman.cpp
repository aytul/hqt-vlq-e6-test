#include "neyman.h"
#include "fit.h"
#include <cmath>
#include <iostream>

neyman::neyman(model *m, double range, int ns, int npe, TNtuple *nt)
{
  poi_upper = range;
  nsteps = ns;
  npes = npe;
  thismodel = m;
  
  bool verbose=true;

  for (int imu =0; imu<nsteps;imu++)
    {

      double delta = (imu*1.0)/(nsteps-1);
      double mu = pow(delta,2)*poi_upper;
      q_sb_mu_dist[imu] = new TH1F(Form("q_sb_mu_%d",imu),Form("mu=%1.3f",mu),10000,-1.0,100.0);
      q_b_mu_dist[imu]  = new TH1F(Form("q_b_mu_%d",imu),Form("mu=%1.3f",mu),10000,-1.0,100.0);

      if (verbose) std::cout << "=== neyman imu = " << imu << " mu = " << mu << " === " << std::endl;

      // get nu for this mu in data
      nuis_params nu_hat_hat_obs;
      nu_hat_hat_obs.init(thismodel->nsamples,thismodel->nsyst);
      if (verbose) std::cout << " Calling fit to find observed nu " << std::endl;
      fit_histo( m->obs , m, mu, nu_hat_hat_obs, true,mu );
      
      for (int ipe=0;ipe<npes;ipe++)
	{
	  //	  std::cout << "  == neyman toy mc = " << ipe << " == " << std::endl;
	  TH1F *toy_data =  m->gen_exp(mu, nu_hat_hat_obs );
	  
	  ///// randomly vary the constraints
	   m->randomize_priors();

	  // get best fit mu and nu
	  double mu_hat;
	  nuis_params nu_hat;
	  nu_hat.init(thismodel->nsamples,thismodel->nsyst);
	  double lh_best = fit_histo( toy_data , m, mu_hat, nu_hat,false);
	  if (mu_hat<0)
	    {
	      mu_hat = 0;
	      // fix mu to 0, maximize the nps, recalc lhood	    
	      nu_hat.init(thismodel->nsamples,thismodel->nsyst);
	      lh_best = fit_histo( toy_data , m, mu_hat, nu_hat,false, mu_hat);
	    }

	  // get best fit nu at this mu
	  nuis_params nu_hat_hat;
	  nu_hat_hat.init(thismodel->nsamples,thismodel->nsyst);
	  double lh_mu   = fit_histo( toy_data , m, mu, nu_hat_hat, false,mu);
	  
	  double lambda_mu = lh_mu / lh_best;


	  double q_mu = 0;
	  if (mu_hat > mu)
	    q_mu = 0;
	  else
	    {
	      if (lambda_mu>0)
		q_mu = - 2 * log (lambda_mu );
	      else
		{
		  std::cout << "Error lambda_mu shouldbe > 0 " << lambda_mu << " lh mu " << lh_mu << " lh best " << lh_best << std::endl;
		  std::cout << " ===== recreate lh mu ====== " << std::endl;
		  fit_histo( toy_data , m, mu, nu_hat_hat, true,mu);
		  std::cout << " ===== recreate lh best ====== " << std::endl;
		  fit_histo( toy_data , m, mu_hat, nu_hat,false);
		}
	    }
	  q_sb_mu_dist[imu]->Fill(q_mu);

	  int id=0;
	  float ntdata[100];
	  ntdata[id++] = imu;
	  ntdata[id++] = mu;
	  ntdata[id++] = toy_data->Integral();
	  ntdata[id++] = mu_hat;
	  ntdata[id++] = ipe;
	  ntdata[id++] = lh_best;
	  ntdata[id++] = lh_mu;
	  ntdata[id++] = lambda_mu;
	  ntdata[id++] = q_mu;
	  for (int z=0;z<m->nsamples;z++)
	    {
	      ntdata[id++] = nu_hat_hat_obs.overall[z];
	      ntdata[id++] = nu_hat_hat.overall[z];
	      ntdata[id++] = nu_hat.overall[z];
	    }
	  for (int z=0;z<m->nsyst;z++)
	    {
	      ntdata[id++] = nu_hat_hat_obs.shape[z];
	      ntdata[id++] = nu_hat_hat.shape[z];
	      ntdata[id++] = nu_hat.shape[z];
	    }
	  if (ipe%100==0) 
	    std::cout << "  PE " << ipe << " true = " << mu << " fit = " << mu_hat << " lh_best = " << lh_best << " lh_mu = " << lh_mu << " lambda_mu = " << lambda_mu << " q_mu = " << q_mu << std::endl;
	  //	  std::cout << " ntuple = " << nt << std::endl;
	  nt->Fill(ntdata);

	}


      //// bg-only
      for (int ipe=0;ipe<npes;ipe++)
	{
	  //	  std::cout << "  == neyman toy mc = " << ipe << " == " << std::endl;
	  TH1F *toy_data =  m->gen_exp(0, nu_hat_hat_obs );
	  
	  ///// randomly vary the constraints
	   m->randomize_priors();

	  // get best fit mu and nu
	  double mu_hat;
	  nuis_params nu_hat;
	  nu_hat.init(thismodel->nsamples,thismodel->nsyst);
	  double lh_best = fit_histo( toy_data , m, mu_hat, nu_hat,false);
	  if (mu_hat<0)
	    {
	      mu_hat = 0;
	      // fix mu to 0, maximize the nps, recalc lhood	    
	      nu_hat.init(thismodel->nsamples,thismodel->nsyst);
	      lh_best = fit_histo( toy_data , m, mu_hat, nu_hat,false, mu_hat);
	    }

	  // get best fit nu at this mu
	  nuis_params nu_hat_hat;
	  nu_hat_hat.init(thismodel->nsamples,thismodel->nsyst);
	  double lh_mu   = fit_histo( toy_data , m, mu, nu_hat_hat, false,mu);
	  
	  double lambda_mu = lh_mu / lh_best;


	  double q_mu = 0;
	  if (mu_hat > mu)
	    q_mu = 0;
	  else
	    {
	      if (lambda_mu>0)
		q_mu = - 2 * log (lambda_mu );
	      else
		{
		  std::cout << "Error lambda_mu shouldbe > 0 " << lambda_mu << " lh mu " << lh_mu << " lh best " << lh_best << std::endl;
		  std::cout << " ===== recreate lh mu ====== " << std::endl;
		  fit_histo( toy_data , m, mu, nu_hat_hat, true,mu);
		  std::cout << " ===== recreate lh best ====== " << std::endl;
		  fit_histo( toy_data , m, mu_hat, nu_hat,false);
		}
	    }
	  q_b_mu_dist[imu]->Fill(q_mu);

	  int id=0;
	  float ntdata[100];
	  ntdata[id++] = imu;
	  ntdata[id++] = mu;
	  ntdata[id++] = toy_data->Integral();
	  ntdata[id++] = mu_hat;
	  ntdata[id++] = ipe;
	  ntdata[id++] = lh_best;
	  ntdata[id++] = lh_mu;
	  ntdata[id++] = lambda_mu;
	  ntdata[id++] = q_mu;
	  for (int z=0;z<m->nsamples;z++)
	    {
	      ntdata[id++] = nu_hat_hat_obs.overall[z];
	      ntdata[id++] = nu_hat_hat.overall[z];
	      ntdata[id++] = nu_hat.overall[z];
	    }
	  for (int z=0;z<m->nsyst;z++)
	    {
	      ntdata[id++] = nu_hat_hat_obs.shape[z];
	      ntdata[id++] = nu_hat_hat.shape[z];
	      ntdata[id++] = nu_hat.shape[z];
	    }
	  if (ipe%100==0) 
	    std::cout << "  PE " << ipe << " true = " << mu << " fit = " << mu_hat << " lh_best = " << lh_best << " lh_mu = " << lh_mu << " lambda_mu = " << lambda_mu << " q_mu = " << q_mu << std::endl;
	  //	  std::cout << " ntuple = " << nt << std::endl;
	  nt->Fill(ntdata);

	}
      


      _mu[imu] = mu;
      std::cout << " neynman imu= " << imu << " true mu = " << mu << " mean q_mu(s+b) = " << q_sb_mu_dist[imu]->GetMean()  << " mean q_mu(b) = " << q_b_mu_dist[imu]->GetMean()  << std::endl;
    }
  m->reset_priors();
}

neyman::~neyman()
{
}

double neyman::p(double q_mu, TH1F *q_mu_dist)
{
  int ibin = q_mu_dist->GetXaxis()->FindBin(q_mu);
  float total = q_mu_dist->GetEntries();
  float frac = q_mu_dist->Integral(0,ibin);
  std::cout << " p(" << q_mu << " = " << frac << " / " << total << " = " << frac/total << " bin " << ibin << " / " << q_mu_dist->GetNbinsX() << std::endl;
  return frac/total;
}

double neyman::p_b(double q_mu,float mu,int &im0,int &im1)
{
  std::cout << " p_sb called with imus " << im0 << ", " << im1 << " range " << _mu[im0] << ", " << _mu[im1] << std::endl;
  while (_mu[im0] > mu && im0>0)
    {
      im0--;
      im1--;
      std::cout << " adjusting to " << im0 << ", " << im1 << " range " << _mu[im0] << ", " << _mu[im1] << std::endl;
    }

  double p0 = p(q_mu,q_b_mu_dist[im0]);
  double p1 = p(q_mu,q_b_mu_dist[im1]);
 
  std::cout << "s+b p0 = " << p0 << " at " << _mu[im0] << " p1 = " << p1 << " at " << _mu[im1] << " --> " << (p0 + (p1-p0)*( mu - _mu[im0])/(_mu[im1]-_mu[im0])) << std::endl;
  return p0 + (p1-p0)*( mu - _mu[im0])/(_mu[im1]-_mu[im0]);

}

double neyman::p_sb(double q_mu,float mu,int &im0,int &im1)
{
  std::cout << " p_sb called with imus " << im0 << ", " << im1 << " range " << _mu[im0] << ", " << _mu[im1] << std::endl;
  while (_mu[im0] > mu && im0>0)
    {
      im0--;
      im1--;
      std::cout << " adjusting to " << im0 << ", " << im1 << " range " << _mu[im0] << ", " << _mu[im1] << std::endl;
    }

  double p0 = p(q_mu,q_sb_mu_dist[im0]);
  double p1 = p(q_mu,q_sb_mu_dist[im1]);
 
  std::cout << "b p0 = " << p0 << " at " << _mu[im0] << " p1 = " << p1 << " at " << _mu[im1] << " --> " << (p0 + (p1-p0)*( mu - _mu[im0])/(_mu[im1]-_mu[im0])) << std::endl;
  return p0 + (p1-p0)*( mu - _mu[im0])/(_mu[im1]-_mu[im0]);

}

double neyman::get_limits (TH1F *hexp, bool verbose)
{
  std::cout << "======== ======== ======== ======== ========" <<std::endl;
  std::cout << "get_limits(" << hexp->Integral() << ") called " << std::endl;
  // get best fit mu and nu
  double mu_hat;
  nuis_params nu_hat;
  nu_hat.init(thismodel->nsamples,thismodel->nsyst);
  double lh_best = fit_histo( hexp , thismodel, mu_hat, nu_hat,true);
  if (mu_hat<0) 
    {
      mu_hat = 0;
      // fix mu to 0, maximize the nps, recalc lhood
      nu_hat.init(thismodel->nsamples,thismodel->nsyst);	      
      lh_best = fit_histo( hexp , thismodel, mu_hat, nu_hat,true, mu_hat );
    }
  std::cout << " fit poi = " << mu_hat << " lh_best = " << lh_best << std::endl;
  int im0=nsteps-2;
  int im1=nsteps-1;
  for (double mu = poi_upper; mu > 1e-5 ; mu *= 0.98)
    {
      nuis_params nu_hat_hat;
      nu_hat_hat.init(thismodel->nsamples,thismodel->nsyst);
      double lh_mu = fit_histo( hexp , thismodel, mu, nu_hat_hat,true,mu);

      double lambda_mu = lh_mu / lh_best;
      double q_mu = 0;
      if (mu_hat > mu)
	q_mu = 0;
      else
	q_mu = - 2 * log (lambda_mu );

      double pb = 1.0-p_b(q_mu,mu,im0,im1);
      double psb = 1.0-p_sb(q_mu,mu,im0,im1);
      double cls = psb/(1.0-pb);
      std::cout << "  Scan mu = " << mu << " lh_mu = " << lh_mu << " q_mu = " << q_mu << " psb = " << psb << " pb = " << pb << " cls = " << psb/(1.0-pb) << std::endl;
      
      if (cls > 0.05)
	{
	  std::cout << " --> limit is " << mu << std::endl;
	  return mu;
	}

      // speed up
      if (cls < 0.001)
	mu = mu *0.75;

    }
  return 0.0;
  std::cout << " This should never happen.  Panic!" << std::endl;
  exit(-1);
  return -1.0;  
}
