
#ifndef MODELH
#define MODELH
#include "TH1.h"
#include <cmath>
#include "TRandom3.h"

const int max_samples=25;
const int max_shapes=10;

struct syst_res
{
  TH1F *h_samp[max_samples];
  TH1F *hsig;
};

struct nuis_params
{
  double overall[max_samples];
  double shape[max_shapes];
  void set(double *par, int nsamp,int nsyst);
  void fill(double *par, int nsamp,int nsyst);
  void print(int nsamp,int nsyst);
  void init(int nsamp,int nsyst);
};


struct syst_info_samp
{
  TH1F *h_samp;
  TH1F **h_samp_p;
  TH1F **h_samp_n;
};

class model
{
  TRandom3 R;
  syst_info_samp *signal;
  syst_info_samp *samples;

  bool checkbins();
  void distort_hist(TH1F *h_samp,TH1F **h_samp_n,TH1F **h_samp_p, double *sfluc, int nsyst, TH1F *res);
public:

  double uncert[max_samples];
  double prior[max_samples];
  double shape_prior[max_shapes];

  void print();

  syst_res *new_syst(nuis_params &nuis);
  // fixed memory for results, to avoid so much Cloning
  syst_res *sys_res;
  TH1F *hexp;
  
  void reset_priors();
  void randomize_priors();
  
  double poi_upper;
  int nsyst;
  int nsamples;
  TH1F *obs;
  model( char sigfile[1000], char bgfile[1000], int nsamp, int nsyst, double poi_up );
  ~model();
  TH1F * gen_exp( double poi, nuis_params &nuis);
  double log_lhood ( TH1F *data, double poi, nuis_params &nu, bool verbose=false);
  double lhood( TH1F *data, double poi, nuis_params &nu, bool verbose) {return exp( log_lhood ( data, poi, nu, verbose ) ); }
};

#endif MODELH
