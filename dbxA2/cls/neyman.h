#ifndef NEYMANH
#define NEYMANH

#include "model.h"
#include "TGraph.h"
#include "TNtuple.h"

class neyman
{
public:
  double poi_upper;
  double _mu[1000];

  // holds the distribution of test statistics
  TH1F *q_sb_mu_dist[1000];
  TH1F *q_b_mu_dist[1000];
  double p(double q_mu,TH1F *q_mu_dist);

  double p_b(double q_mu,float mu, int &im0,int &im1);
  double p_sb(double q_mu,float mu, int &im0,int &im1);

  int nsteps;
  int npes;
  model *thismodel;
  neyman ( model *m, double range, int ns, int npe, TNtuple *nt=0);
  ~neyman();
  double get_limits( TH1F *hexp , bool verbose=false);

};

#endif NEYMANH
