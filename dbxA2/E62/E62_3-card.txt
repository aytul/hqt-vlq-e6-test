# analysis parameters card for FF wjwj semileptonic
# format is "variable = value"
minpte  = 25.0  # min pt of electrons 25
minptm2011  = 20.0  # min pt of muons
minptm2012  = 25.0  # min pt of muons
maxz0e=2           #max z0 wrt PV for electrons   //we are not using now
maxz0m=2           #max z0 wrt PV for muons
jetVtxf2011=0.75;  #jet vertex fraction of 2011
jetVtxf2012=0.50;  #jet vertex fraction of 2012
maxetae = 2.47   # max pseudorapidity of electrons
maxetam = 2.5   # max pseudorapidity of muons
minmete = 60   # min Met of Elec
minmetmu = 60  # min Met of Muon
minptj  = 25.0   # min pt of jets
maxetaj = 2.50   # max pseudorapidity of jets
mindrjm = 0.4   # min deltaR between jets and muons
mindrje = 0.2   # min deltaR between jets and electrons
mqhxmin = 50    # for the final HQ mass histo, x-axis min
mqhxmax = 850 # for the final HQ mass histo, x-axis max
maxMuPtCone = 2.5 # Max muon Pt for 2011
maxMuEtCone = 4.0 # Max muon Et for 2011
TRGe = 2 # electron Trigger Type: 0=dont trigger, 1=1st trigger (data) 2=2nd trigger (MC) etc
TRGm = 0 #     muon Trigger Type: 0=dont trigger, 1=1st trigger (data) 2=2nd trigger (MC) etc
# E6 Analysis Parameters
maxDeltaZmassEl = 10 # maximum Z mass spread of Elec
maxDeltaZmassMu = 15 # maximum Z mass spread of Muon
HerrorM = 10 # Higgs Chisquare sigma Value of Muon
DHerrorM = 30 # De from Higgs Chisquare sigma Value of Muon
DZerrorM = 27 # De from Z Chisquare sigma Value of Muon
HerrorE = 10 # Higgs Chisquare sigma Value of Elec
DHerrorE = 40 # De from Higgs Chisquare sigma Value of Elec
DZerrorE = 17 # De from Z Chisquare sigma Value of Elec
MaximumChi = 25 # Max Chisqure value
MinRDist = 1.3 # Minimum R Dist between Jets
MinPJetPtE = 270 # Minimum Pt of at least one of the Propmt Jets for Electron
MinPJetPtM = 270 # Minimum Pt of at least one of the Propmt Jets for Muon
MinSubPJetPtE = 180 # Minimum Pt of at least one of the Propmt Jets for Electron
MinSubPJetPtM = 180 # Minimum Pt of at least one of the Propmt Jets for Muon

