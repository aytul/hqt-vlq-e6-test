#include "TRandom3.h"
#include "TParameter.h"

#ifdef VLQLIGHT
#include "VLQLight/e6_a2.h"
#include "VLQLight/analysis_core.h"
#include "VLQLight/dbx_a.h"

#else
#include "e6_a2.h"
#include "analysis_core.h"
#include "dbx_a.h"
#endif

//#define __VERBOSE3__

//#define _CLV_

#ifdef _CLV_
#define DEBUG(a) std::cout<<a
#else
#define DEBUG(a)
#endif



void find_idxtype_TObeused( dbxCut *acut, vector <int> *found_idx_vecs, vector <int> *found_type_vecs, vector <int> *found_idx_origs,  vector <int> *ret_i, vector <int> *ret_t ){
#ifdef _CLV_
    DEBUG( "--previous finder:  ");
    DEBUG( acut->getName()<<" Found idx: "); for (int qq=0; qq<found_idx_vecs->size(); qq++) DEBUG(found_idx_vecs->at(qq)<<" "); DEBUG("\t");
    DEBUG("found type<>origs:"); for (int qq=0; qq<found_type_vecs->size(); qq++) DEBUG(found_type_vecs->at(qq)<<"<>"<< found_idx_origs->at(qq)<<" "); DEBUG("\n");
    DEBUG(" searchable types in this cut: ");
    for (int qq=0; qq<acut->getSearchableType(); qq++) DEBUG( acut->getSearchableType(qq)<<" "); DEBUG("  ");
    DEBUG("ALL idxs in this cut: "); for (int qq=0; qq<acut->getParticleIndex(); qq++) DEBUG(acut->getParticleIndex(qq)<<" "); DEBUG("\t");
#endif
    
    if (found_idx_vecs->size() == 0) {cerr << "Found idx vector empty!!\n"; exit (23);}
    vector <int> alreadyused; // store already used found idx
    vector<int>::iterator it;
    vector<int> local_part_idxs;
    
    for (int ipd=0; ipd< acut->getParticleIndex(); ipd++)  // this cut needs // loop over indices remove 999
        if ( acut->getParticleIndex(ipd) < 0) local_part_idxs.push_back(acut->getParticleIndex(ipd));
    
    for (int ipd=0; ipd< acut->getSearchableType(); ipd++)  // this cut needs // loop over indices MUST
        for (unsigned int itk=0; itk<found_idx_vecs->size(); itk++){ // read from found list.
            //             DEBUG (local_part_idxs[ipd]<<" -vs- "<< found_idx_vecs->at(itk) << "\n");
            if ( found_type_vecs->at(itk) == acut->getSearchableType(ipd)  // types match
                && ( local_part_idxs[ipd]  < 0 )                             // searchable type.
                && found_idx_origs->at(itk) == local_part_idxs[ipd]          // idx match
                ) {
                DEBUG(" useI:"<<found_idx_vecs->at(itk)<<"\n");
                it=find(alreadyused.begin(), alreadyused.end(), itk);
                if ( it == alreadyused.end() ) { // not previously used
                    ret_i->push_back(found_idx_vecs->at(itk));
                    ret_t->push_back(found_type_vecs->at(itk));
                    alreadyused.push_back(itk);
                }
            }
        }// loop over all previously found idx
    for (int ipd=0; ipd< ret_i->size(); ipd++){
        DEBUG(" ridx:"<<ret_i->at(ipd)<< "  rtyp:"<<ret_t->at(ipd)<<"\n");
    }
    DEBUG(": previous finder--\n");
    return ;
}

int E62dbxA::setQCD() {
	int retval=0;
	QCD=true;
	std::cout << "E62 QCD on"<<std::endl;
	dbxA::ChangeDir(cname); // go back to zulu = output file
	return retval;
}
bool E62dbxA::ChiSquare(vector<dbxJet> goodJets, vector<dbxLJet> goodLJets, dbxParticle ZBos,
                        double MinLeadJetPt, double MinSubJetPt, double Herror, double DHerror, double DZerror,
                        dbxParticle  *&Hbos, dbxParticle  *&DfH, dbxParticle *& DfZ, unsigned int SJet[],
                        double & eventchi, double & Hchi, double & Dchi, int ForceBtag=0)
{
// bu ne bicim function AMQ, program yazaydiniz!  // NGU.
// bu kodu yazanin !#!@$@#$@#$@#$!@#!$@#$@#$@##!@#@$%$%&%&$%%@#$#!@#$#%

	eventchi=999999;
	Hchi=1000;
	Dchi=1000;
	delete Hbos;
	delete DfZ;
	delete DfH;
	Hbos=NULL;
	DfH=NULL;
	DfZ=NULL;

    
	for(unsigned int jet1=0;jet1<(goodJets.size()-1);jet1++)
	{
		for(unsigned int jet2=jet1+1;jet2<goodJets.size();jet2++)
		{

             if(ForceBtag==1 && goodJets.at(jet1).isbtagged_77() == 0 && goodJets.at(jet2).isbtagged_77() == 0) continue; //
             if(ForceBtag==2 && ( goodJets.at(jet1).isbtagged_77() == 0  || goodJets.at(jet2).isbtagged_77() == 0)) continue;//
             dbxParticle HRecon = goodJets.at(jet1) + goodJets.at(jet2);
		     double chisqH = ((HRecon.lv().M()-126)*(HRecon.lv().M()-126)/(Herror*Herror));

		     if (chisqH>eventchi )
		     {
			 HRecon.~dbxParticle();
			 continue;
		     }

		     for(unsigned int jet3=0; (jet3<(goodJets.size()));jet3++)
			 {
				if(jet3==jet2 || jet3==jet1) continue;
				for(unsigned int jet4=0; (jet4<(goodJets.size()));jet4++)
				{
					if(jet4==jet2 || jet4==jet1 || jet4==jet3) continue;

					if((goodJets.at(jet4).lv().Pt()<MinLeadJetPt || goodJets.at(jet3).lv().Pt()<MinSubJetPt )&& (goodJets.at(jet3).lv().Pt()<MinLeadJetPt || goodJets.at(jet4).lv().Pt()<MinSubJetPt )) continue;
					if(goodJets.at(jet3).lv().DeltaR(HRecon.lv())<MinRDist) continue;
					if(goodJets.at(jet4).lv().DeltaR(ZBos.lv())<MinRDist) continue;

					dbxParticle DeHiggs = HRecon + goodJets.at(jet3);
					dbxParticle DeZ= ZBos + goodJets.at(jet4);
					double chisqD = (DeHiggs.lv().M()-DeZ.lv().M())*(DeHiggs.lv().M()-DeZ.lv().M())/(DHerror*DHerror + DZerror*DZerror);

					if ((chisqD+chisqH)>eventchi ) {
						DeHiggs.~dbxParticle();
						DeZ.~dbxParticle();
						continue;
					}
					eventchi=chisqD+chisqH;
					Hchi = chisqH;
					Dchi = chisqD;
					SJet[0]=jet1;
					SJet[1]=jet2;
					SJet[2]=jet3;
					SJet[3]=jet4;
					delete Hbos;
					delete DfZ;
					delete DfH;
					Hbos = new dbxParticle(HRecon);
					DfZ = new dbxParticle(DeZ);
					DfH = new dbxParticle(DeHiggs);
				}
			}
			HRecon.~dbxParticle();
		}//loop over jet2
	}//loop over jet1

	if (Hbos == NULL) return false;
	else{
			HiggsRDist[ForceBtag]->Fill(goodJets.at(SJet[2]).lv().DeltaR(Hbos->lv()));
			ZRDist[ForceBtag]->Fill(goodJets.at(SJet[3]).lv().DeltaR(ZBos.lv()));
			HiggsAllJetMass[ForceBtag]->Fill(goodJets.at(SJet[0]).lv().M());
			HiggsAllJetMass[ForceBtag]->Fill(goodJets.at(SJet[1]).lv().M());
			if( (goodJets.at(SJet[1]).lv().M()-125) > (goodJets.at(SJet[0]).lv().M()-125) )
			{
				HiggsLeadJetMass[ForceBtag]->Fill(goodJets.at(SJet[0]).lv().M());
				HiggsSubJetMass[ForceBtag]->Fill(goodJets.at(SJet[1]).lv().M());
			}
			else
			{
				HiggsLeadJetMass[ForceBtag]->Fill(goodJets.at(SJet[1]).lv().M());
				HiggsSubJetMass[ForceBtag]->Fill(goodJets.at(SJet[0]).lv().M());
			}

		return true;
	}
}


int E62dbxA::indexNoSelfParentParticle(int particle_index, vector<vector<int> > *mc_parent_index, vector<int> *mc_pdgId){

	if(mc_parent_index->at(particle_index).size()==0) return -1;
	const int PDG = fabs(mc_pdgId->at(particle_index));
	int parentindex = mc_parent_index->at(particle_index).at(0);
	int parentpdg = abs(mc_pdgId->at(parentindex ));
	while(parentpdg==PDG){
		if(mc_parent_index->at(parentindex).size()==0) return parentindex;
		parentindex = mc_parent_index->at(parentindex).at(0);
		parentpdg = fabs(mc_pdgId->at(parentindex ));
	}
	return parentindex;
}
int E62dbxA::TrueLep(int id, vector<vector<int> > *mc_parent_index, vector<int> *mc_pdgId, vector<int> *mc_status)
{
	int truLep=0;
	for(int ii=0; ii<mc_pdgId->size(); ii++){   // Loop over all truth particles
		int pdg=abs(mc_pdgId->at(ii));
		int status=mc_status->at(ii);
		if(pdg!=id || status!=1) continue; // Check for final stat e or mu
		int parentindex=indexNoSelfParentParticle( ii,mc_parent_index, mc_pdgId);
		int parentpdg = parentindex>=0 ? abs(mc_pdgId->at(parentindex )) : 0;

		if (parentpdg==24 || parentpdg==23) truLep++; // Count l from W->lnu or Z->ll
		else if(parentpdg==15){
			parentindex=indexNoSelfParentParticle(parentindex,mc_parent_index, mc_pdgId);
			parentpdg = parentindex>=0 ? fabs(mc_pdgId->at(parentindex )) : 0;
			if (parentpdg==24 || parentpdg==23) truLep++; // Count l from Z,W->taunu->lnunu
		}

	}
	return truLep;
}
double E62dbxA::deltaPhi(double phi1, double phi2) {
	double dPhi=std::fabs(phi1-phi2);
	if (dPhi>M_PI) dPhi=2*M_PI-dPhi;
	return dPhi;
}
bool E62dbxA::isCosmicEvent(vector<dbxMuon> muons){
	bool isCosmicEvents=false;
	for (int i=0;i<(int)muons.size();i++) {
		for (int j=0;j<(int)muons.size();j++) {
			if (muons.at(i).d0()*muons.at(j).d0()>0) continue;
			if (fabs(muons.at(i).d0())<0.5 || fabs(muons.at(j).d0())<0.5) continue;
			//-delta(phi)>3.10
			if (deltaPhi(muons.at(i).lv().Phi(), muons.at(j).lv().Phi())<3.10) continue;
			isCosmicEvents=true;
			break;
		}
		if (isCosmicEvents) break;
	}
	return isCosmicEvents;
}
int E62dbxA::bookAdditionalHistos(){
	int retval=0;
        dbxA::ChangeDir(cname);
        int kk=1;
        std::string subdelimiter = ",";
        char HCardName[64];
        size_t apos = 0; 

// read histo defitions from file 
        sprintf(HCardName,"%s-histos.txt",getDataCardPrefix().c_str() );
        vector <pair <int, vector<dbxCut*> > > aa;
        vector <dbxCut*> aajunk;
                         aa.push_back(make_pair(-1, aajunk) ); 
        for (int dummy=0; dummy<9; dummy++) { histos_order.push_back( aa ); b_histos[dummy]=-1;}
        
        while (1){
                  TString basedef="histo";
                          basedef+=kk++; 
                  string def0 = ReadCardString(HCardName,basedef,"XXX");
                  if (def0=="XXX") break;
//                  cout << "++++++++=> histo id:"<<kk-1<<") "<<def0<<endl;
                  if ((apos=def0.find(subdelimiter)) != std::string::npos )  {  
                         std::string subtoken0, subtoken1;
                         int hnbin, horder;
                         float hxmin, hxmax;
                         string hargument;

                         subtoken0 = def0.substr(0, apos); // histo name
                         def0.erase(0, apos + subdelimiter.length());
                         apos=def0.find(subdelimiter);     // histo title
                         subtoken1 = def0.substr(0, apos); // histo title
                         def0.erase(0, apos + subdelimiter.length());
                         apos=def0.find(subdelimiter);     // histo nbins
                         hnbin=atoi(def0.substr(0, apos).data());

                         if (subtoken0.find("Basics") != std::string::npos  ) {
                          cout << "\nBasics Histos @:"<<hnbin<<" using id:"<< atoi(subtoken1.data() ) << "  ";
                          b_histos[hnbin]=atoi(subtoken1.data() );
                         } else {

                            def0.erase(0, apos + subdelimiter.length());
                            apos=def0.find(subdelimiter);            //histo xmin
                            hxmin=atof(def0.substr(0, apos).data()); //histo xmin
                            def0.erase(0, apos + subdelimiter.length());
                            apos=def0.find(subdelimiter);            //histo xmax
                            hxmax=atof(def0.substr(0, apos).data()); //histo xmax
                            def0.erase(0, apos + subdelimiter.length());
                            apos=def0.find(subdelimiter);            //histo order
                            horder=atoi(def0.substr(0, apos).data());//histo order
                            def0.erase(0, apos + subdelimiter.length());
                            apos=def0.find(subdelimiter);     // histo argument
                            hargument = def0.substr(0, apos); // histo argument
                        
                            std::cout <<"\nHISTO ---:  "<<subtoken0<<" name:"<<subtoken1<< "."<< " nbin:"<<hnbin<<" ["<<hxmin<<","<<hxmax<<"]" << " @:"<<horder<<" w/"<<hargument << " ";
                            a_histos[kk-1]=new TH1F(subtoken0.data(), subtoken1.data(), hnbin, hxmin, hxmax);

                            std::vector<dbxCut*> acutlist;
                            acutlist.reserve(10);
                            e62cutlist.cutTokenizer(hargument, &acutlist); //e62cutlist is not a real list, name change and also just a local variable TODO
                            
                            histos_order.at(horder-1).push_back( make_pair (kk-1, acutlist) );
                         }

                  } else{
                  cout << "This histo is problematic. STOP\n"; exit (3);
                  }
        }




	HiggsMass[0] = new TH1F("HiggsMass", "Higgs Mass (GeV)", 140, 60 , 200); //this was 100 before BG
	HiggsMass[1] = new TH1F("HiggsMassB", "Higgs Mass (GeV)", 140, 60 , 200);
	HiggsMass[2] = new TH1F("HiggsMassBB", "Higgs Mass (GeV)", 140, 60 , 200);
    
        HiggsPT[0] = new TH1F("HiggsPT","H PT (GeV)", 120, 0, 1200);//A.A
        HiggsPT[1] = new TH1F("HiggsPTB","H PT (GeV)", 120, 0, 1200);//A.A
        HiggsPT[2] = new TH1F("HiggsPTBB","H PT (GeV)", 120, 0, 1200);//A.A

        HiggsETA[0] = new TH1F("HiggsETA","H Eta ", 25, -2.5, 2.5);//A.A
        HiggsETA[1] = new TH1F("HiggsETAB","H Eta ", 25, -2.5, 2.5);//A.A
        HiggsETA[2] = new TH1F("HiggsETABB","H Eta ", 25, -2.5, 2.5);//A.A
        
        HiggsPHI[0] = new TH1F("HiggsPHI","H Phi ", 32, -3.2, 3.2);//A.A
        HiggsPHI[1] = new TH1F("HiggsPHIB","H Phi ", 32, -3.2, 3.2);//A.A
        HiggsPHI[2] = new TH1F("HiggsPHIBB","H Phi ", 32, -3.2, 3.2);//A.A

	ZMass[0]= new TH1F("ZMass","Z Mass (GeV)", 60, 60, 120);
	ZMass[1]= new TH1F("ZMassB","Z Mass (GeV)", 60, 60, 120);
	ZMass[2]= new TH1F("ZMassBB","Z Mass (GeV)", 60, 60, 120);

	ZPT[0] = new TH1F("ZPT","Z PT (GeV)", 360, 0, 1800);//A.A
	ZPT[1] = new TH1F("ZPTB","Z PT (GeV)", 360, 0, 1800);//A.A
	ZPT[2] = new TH1F("ZPTBB","Z PT (GeV)", 360, 0, 1800);//A.A
        ZETA[0] = new TH1F("ZETA"  ,"Z Eta ", 50, -5.0, 5.0);
        ZETA[1] = new TH1F("ZETAB" ,"Z Eta ", 50, -5.0, 5.0);
        ZETA[2] = new TH1F("ZETABB","Z Eta ", 50, -5.0, 5.0);
        ZPHI[0] = new TH1F("ZPHI","Z Phi ",  36, -3.6, 3.6);//A.A change im range *berare
        ZPHI[1] = new TH1F("ZPHIB","Z Phi ",  36, -3.6, 3.6);//A.A  change in range *berare
        ZPHI[2] = new TH1F("ZPHIBB","Z Phi ",  36, -3.6, 3.6);//A.A change in range
/*
	ZPTcon[0] = new TH1F("ZPTcon0","Z PT after preselection cuts GeV", 360, 0, 1800);//berare //done.
        ZPTcon[1] = new TH1F("ZPTcon1","Z PT preselection & 2j GeV", 360, 0, 1800);//berare //done.
        ZPTcon[2] = new TH1F("ZPTcon2","Z PT preselections & 2j & 4j GeV", 360, 0, 1800);//berare //done.
        ZETAcon[0] = new TH1F("ZETAcon0","Z ETA after preselection cuts GeV", 50, -5.0, 5.0);//berare //done.
        ZETAcon[1] = new TH1F("ZETAcon1","Z ETA preselections & 2j GeV "       , 50, -5.0, 5.0);//berare //done.
        ZETAcon[2] = new TH1F("ZETAcon2","Z ETA preselections & 2j & 4j GeV",     50, -5.0, 5.0);//berare  //done.
	ZPHIcon[0] = new TH1F("ZPHIcon0","Z PHI after preselection cuts GeV ",  36, -3.6, 3.6);//berare //done.
	ZPHIcon[1] = new TH1F("ZPHIcon1","Z PHI preselection & 2j GeV ",  36, -3.6, 3.6);//berare //done.
	ZPHIcon[2] = new TH1F("ZPHIcon2","Z PHI preselections & 2j & 4j GeV",  36, -3.6, 3.6);//berare //done.
*/



	DeZMass[0]= new TH1F("DeZMass",   "De Mass From Z Channel (GeV)", 200, 0, 2000);
	DeZMass[1]= new TH1F("DeZMassB",  "De Mass From Z Channel (GeV)", 200, 0, 2000);
	DeZMass[2]= new TH1F("DeZMassBB", "De Mass From Z Channel (GeV)", 200, 0, 2000);
    
        DeZPT[0]= new TH1F("DeZPT",   "De PT From Z Channel (GeV)", 200, 0, 2000);//A.A
        DeZPT[1]= new TH1F("DeZPTB",  "De PT From Z Channel (GeV)", 200, 0, 2000);//A.A
        DeZPT[2]= new TH1F("DeZPTBB", "De PT From Z Channel (GeV)", 200, 0, 2000);//A.A

        DeZETA[0]= new TH1F("DeZETA",   "De ETA From Z Channel ", 25, -2.5, 2.5);//A.A
        DeZETA[1]= new TH1F("DeZETAB",  "De ETA From Z Channel ", 25, -2.5, 2.5);//A.A
        DeZETA[2]= new TH1F("DeZETABB", "De ETA From Z Channel ", 25, -2.5, 2.5);//A.A

        DeZPHI[0]= new TH1F("DeZPHI",   "De PHI From Z Channel ", 32, -3.2, 3.2);//A.A
        DeZPHI[1]= new TH1F("DeZPHIB",  "De PHI From Z Channel ", 32, -3.2, 3.2);//A.A        
        DeZPHI[2]= new TH1F("DeZPHIBB", "De PHI From Z Channel ", 32, -3.2, 3.2);//A.A

	DeHMass[0]= new TH1F("DeHMass",   "De Mass From Higgs Channel (GeV)", 200, 0, 2000);
	DeHMass[1]= new TH1F("DeHMassB",  "De Mass From Higgs Channel (GeV)", 200, 0, 2000);
	DeHMass[2]= new TH1F("DeHMassBB", "De Mass From Higgs Channel (GeV)", 200, 0, 2000);
    
        DeHPT[0]= new TH1F("DeHPT",   "De PT From H Channel (GeV)", 200, 0, 2000);//A.A
        DeHPT[1]= new TH1F("DeHPTBB", "De PT From H Channel (GeV)", 200, 0, 2000);//A.A
        DeHPT[2]= new TH1F("DeHPTBB", "De PT From H Channel (GeV)", 200, 0, 2000);//A.A
        
        DeHETA[0]= new TH1F("DeHETA",   "De ETA From H Channel ", 25, -2.5, 2.5);//A.A
        DeHETA[1]= new TH1F("DeHETAB",  "De ETA From H Channel ", 25, -2.5, 2.5);//A.A
        DeHETA[2]= new TH1F("DeHETABB", "De ETA From H Channel ", 25, -2.5, 2.5);//A.A
        
        DeHPHI[0]= new TH1F("DeHPHI",   "De PHI From H Channel ", 32, -3.2, 3.2);//A.A
        DeHPHI[1]= new TH1F("DeHPHIB",  "De PHI From H Channel ", 32, -3.2, 3.2);//A.A
        DeHPHI[2]= new TH1F("DeHPHIBB", "De PHI From H Channel ", 32, -3.2, 3.2);//A.A

	DeAMass[0]= new TH1F("DeAMass",   "Average De Mass (GeV)", 250, 0, 2500);
	DeAMass[1]= new TH1F("DeAMassB",  "Average De Mass (GeV)", 250, 0, 2500);
	DeAMass[2]= new TH1F("DeAMassBB", "Average De Mass (GeV)", 250, 0, 2500);

	EventChi[0]= new TH1F("EventChi",   "Total Chisquare", 200, 0, 200);
	EventChi[1]= new TH1F("EventChiB",  "Total Chisquare", 200, 0, 200);
	EventChi[2]= new TH1F("EventChiBB", "Total Chisquare", 200, 0, 200);
        EventChiBF[0]= new TH1F("EventChiBF",   "Total Chisquare", 200, 0, 200);
        EventChiBF[1]= new TH1F("EventChiBFB",  "Total Chisquare", 200, 0, 200);
        EventChiBF[2]= new TH1F("EventChiBFBB", "Total Chisquare", 200, 0, 200);



	HiggsChi[0]= new TH1F("HiggsChi",   "Higgs Calc Chisquare", 200, 0, 200);
	HiggsChi[1]= new TH1F("HiggsChiB",  "Higgs Calc Chisquare", 200, 0, 200);
	HiggsChi[2]= new TH1F("HiggsChiBB", "Higgs Calc Chisquare", 200, 0, 200);


	DeChi[0]= new TH1F("DeChi", "De Calc Chisquare", 200, 0, 200);
	DeChi[1]= new TH1F("DeChiB", "De Calc Chisquare", 200, 0, 200);
	DeChi[2]= new TH1F("DeChiBB", "De Calc Chisquare", 200, 0, 200);

	NHFlavorHist[0]= new TH1F("NHFlavorHist", "Non Higgs Jets Flavors", 20, -0.1, 1.1);
	NHFlavorHist[1]= new TH1F("NHFlavorHistB", "Non Higgs Jets Flavors", 20, -0.1, 1.1);
	NHFlavorHist[2]= new TH1F("NHFlavorHistBB", "Non Higgs Jets Flavors", 20, -0.1, 1.1);

	HFlavorHist[0]= new TH1F("HFlavorHist", "Higgs Jets Flavors", 20, -0.1, 1.1);
	HFlavorHist[1] = new TH1F("HFlavorHistB", "Higgs Jets Flavors", 20, -0.1, 1.1);
	HFlavorHist[2] = new TH1F("HFlavorHistBB", "Higgs Jets Flavors", 20, -0.1, 1.1);

	METE[0]= new TH1F("METE",  "Missing Et", 200, 0, 400);
	METE[1]= new TH1F("METEB", "Missing Et", 200, 0, 400);
	METE[2]= new TH1F("METEB", "Missing Et", 200, 0, 400);

	METEvsZpt[0]= new TH2F("METEvsZpt",  "METE vs Zpt Graph",200,0,400,200,400);
	METEvsZpt[1]= new TH2F("METEvsZptB", "METE vs Zpt Graph B tagged",200,0,400,200,400);
	METEvsZpt[2]= new TH2F("METEvsZptB", "METE vs Zpt Graph B tagged",200,0,400,200,400);

	METEvsHm[0]= new TH2F("METEvsHm",   "METE vs Higgs Mass",200,0,400,100,200);
	METEvsHm[1]= new TH2F("METEvsHmB",  "METE vs Higgs Mass B tagged",200,0,400,100,200);
	METEvsHm[2]= new TH2F("METEvsHmBB", "METE vs Higgs Mass B tagged",200,0,400,100,200);

	hBtagMultJ_higgs[0] = new TH1F ("hBtagMultJ_higgs"  ,"B Tag Multiplicity in Higgs Jets",4,-0.5,3.5);
	hBtagMultJ_higgs[1] = new TH1F ("hBtagMultJ_higgsB"  ,"B Tag Multiplicity in Higgs Jets",4,-0.5,3.5);
	hBtagMultJ_higgs[2] = new TH1F ("hBtagMultJ_higgsBB"  ,"B Tag Multiplicity in Higgs Jets",4,-0.5,3.5);

	HiggsRDist[0] = new TH1F ("HiggsRDist", "HiggsRDist", 200, 0,20);
	HiggsRDist[1] = new TH1F ("HiggsRDistB", "HiggsRDist", 200, 0,20);
	HiggsRDist[2] = new TH1F ("HiggsRDistBB", "HiggsRDist", 200, 0,20);

	ZRDist[0] = new TH1F ("ZRDist", "ZRDist", 200, 0,20);
	ZRDist[1] = new TH1F ("ZRDistB", "ZRDist", 200, 0,20);
	ZRDist[2] = new TH1F ("ZRDistBB", "ZRDist", 200, 0,20);

	METEvsZpt[2]= new TH2F("METEvsZptB", "METE vs Zpt Graph B tagged",200,0,400,200,400);

	METEvsHm[0]= new TH2F("METEvsHm", "METE vs Higgs Mass",200,0,400,100,200);
	METEvsHm[1]= new TH2F("METEvsHmB", "METE vs Higgs Mass B tagged",200,0,400,100,200);
	METEvsHm[2]= new TH2F("METEvsHmBB", "METE vs Higgs Mass B tagged",200,0,400,100,200);

	hBtagMultJ_higgs[0] = new TH1F ("hBtagMultJ_higgs"  ,"B Tag Multiplicity in Higgs Jets",4,-0.5,3.5);
	hBtagMultJ_higgs[1] = new TH1F ("hBtagMultJ_higgsB"  ,"B Tag Multiplicity in Higgs Jets",4,-0.5,3.5);
	hBtagMultJ_higgs[2] = new TH1F ("hBtagMultJ_higgsBB"  ,"B Tag Multiplicity in Higgs Jets",4,-0.5,3.5);

	HiggsRDist[0] = new TH1F ("HiggsRDist", "HiggsRDist", 200, 0,20);
	HiggsRDist[1] = new TH1F ("HiggsRDistB", "HiggsRDist", 200, 0,20);
	HiggsRDist[2] = new TH1F ("HiggsRDistBB", "HiggsRDist", 200, 0,20);

	ZRDist[0] = new TH1F ("ZRDist", "ZRDist", 200, 0,20);
	ZRDist[1] = new TH1F ("ZRDistB", "ZRDist", 200, 0,20);
	ZRDist[2] = new TH1F ("ZRDistBB", "ZRDist", 200, 0,20);

	hphij[0] = new TH1F ("hphij", "#phi j", 32, -3.2, 3.2);
	hphij[1] = new TH1F ("hphijB", "#phi j", 32, -3.2, 3.2);
	hphij[2] = new TH1F ("hphijBB", "#phi j", 32, -3.2, 3.2);

	hphij_err[0] = new TH1F ("hphij_err", "#phi j", 32, -3.2, 3.2);
	hphij_err[1] = new TH1F ("hphij_errB", "#phi j", 32, -3.2, 3.2);
	hphij_err[2] = new TH1F ("hphij_errBB", "#phi j", 32, -3.2, 3.2);

	hphiv[0] = new TH1F ("hphiv", "#phi v", 96, -9.6, 9.6);
	hphiv[1] = new TH1F ("hphivB", "#phi v", 96, -9.6, 9.6);
	hphiv[2] = new TH1F ("hphivBB", "#phi v", 96, -9.6, 9.6);

	hphiv_err[0] = new TH1F ("hphiv_err", "#phi v", 96, -9.6, 9.6);
        hphiv_err[1] = new TH1F ("hphiv_errB", "#phi v", 96, -9.6, 9.6);
	hphiv_err[2] = new TH1F ("hphiv_errBB", "#phi v", 96, -9.6, 9.6);

	hLeadMu[0] = new TH1F ("hLeadMu","Leading Muon PT",360,0,1800);
	hLeadMu[1] = new TH1F ("hLeadMuB","Leading Muon PT",360,0,1800);
	hLeadMu[2] = new TH1F ("hLeadMuBB","Leading Muon PT",360,0,1800);

	hLeadEl[0] = new TH1F ("hLeadEl","Leading El PT",240,0,1200);
	hLeadEl[1] = new TH1F ("hLeadElB","Leading El PT",240,0,1200);
	hLeadEl[2] = new TH1F ("hLeadElBB","Leading El PT",240,0,1200);

	hjpt_lead[0] = new TH1F("jpt_lead", "pt of Leading jet ", 360, 0.0, 1800.0);
	hjpt_lead[1] = new TH1F("jpt_leadB", "pt of Leading jet ", 360, 0.0, 1800.0);
	hjpt_lead[2] = new TH1F("jpt_leadBB", "pt of Leading jet ", 360, 0.0, 1800.0);

	hjpt_lead_err[0] = new TH1F("jpt_lead_err", "pt of qcd muons ", 360, 0.0, 1800.0);
	hjpt_lead_err[1] = new TH1F("jpt_lead_errB", "pt of qcd muons ", 360, 0.0, 1800.0);
	hjpt_lead_err[2] = new TH1F("jpt_lead_errBB", "pt of qcd muons ", 360, 0.0, 1800.0);

	hjpt_sublead[0] = new TH1F("jpt_sublead", "pt of Subleading jet ", 360, 0.0, 1800.0);
	hjpt_sublead[1] = new TH1F("jpt_subleadB", "pt of Subleading jet ", 360, 0.0, 1800.0);
	hjpt_sublead[2] = new TH1F("jpt_subleadBB", "pt of Subleading jet ", 360, 0.0, 1800.0);

	hjpt_sublead_err[0]= new TH1F("jpt_sublead_err", "pt of qcd muons ", 360, 0.0, 1800.0);
	hjpt_sublead_err[1]= new TH1F("jpt_sublead_errB", "pt of qcd muons ", 360, 0.0, 1800.0);
	hjpt_sublead_err[2]= new TH1F("jpt_sublead_errBB", "pt of qcd muons ", 360, 0.0, 1800.0);

	hjmultip[0]= new TH1F ("hjmultip", "jet multiplicity", 16, -0.5, 15.5);
	hjmultip[1]= new TH1F ("hjmultipB", "jet multiplicity", 16, -0.5, 15.5);
	hjmultip[2]= new TH1F ("hjmultipBB", "jet multiplicity", 16, -0.5, 15.5);

	hjmultip_err[0]= new TH1F ("hjmultip_err", "jet multiplicity err", 16, -0.5, 15.5);
	hjmultip_err[1]= new TH1F ("hjmultip_errB", "jet multiplicity err", 16, -0.5, 15.5);
	hjmultip_err[2]= new TH1F ("hjmultip_errBB", "jet multiplicity err", 16, -0.5, 15.5);

	hmultJ[0]= new TH1F ("hmultJ"  ,"Jet Multiplicity",15,-0.5,14.5);
	hmultJ[1]= new TH1F ("hmultJB"  ,"Jet Multiplicity",15,-0.5,14.5);
	hmultJ[2]= new TH1F ("hmultJBB"  ,"Jet Multiplicity",15,-0.5,14.5);


	hmultJ_err[0]= new TH1F ("hmultJ_err"  ,"Jet Multiplicity err",15,-0.5,14.5);
	hmultJ_err[1]= new TH1F ("hmultJ_errB"  ,"Jet Multiplicity err",15,-0.5,14.5);
	hmultJ_err[2]= new TH1F ("hmultJ_errBB"  ,"Jet Multiplicity err",15,-0.5,14.5);

	BTaggedJets[0] = new TH1F ("BTaggedJets", "Number of BTagged Jets in each event", 7, 0,7);
	BTaggedJets[1] = new TH1F ("BTaggedJetsB", "Number of BTagged Jets in each event", 7, 0,7);
	BTaggedJets[2] = new TH1F ("BTaggedJetsBB", "Number of BTagged Jets in each event", 7, 0,7);

	HiggsLeadJetMass[0] = new TH1F ("HiggsLeadJetMass", "Higgs Lead Jet Mass", 160, -400.0, 400.0);
	HiggsLeadJetMass[1] = new TH1F ("HiggsLeadJetMassB", "Higgs Lead Jet Mass", 160, -400.0, 400.0);
	HiggsLeadJetMass[2] = new TH1F ("HiggsLeadJetMassBB", "Higgs Lead Jet Mass", 160, -400.0, 400.0);

	HiggsSubJetMass[0] = new TH1F ("HiggsSubJetMass", "Higgs Sub Jet Mass", 80, 0.0, 400.0);
	HiggsSubJetMass[1] = new TH1F ("HiggsSubJetMassB", "Higgs Sub Jet Mass", 80, 0.0, 400.0);
	HiggsSubJetMass[2] = new TH1F ("HiggsSubJetMassBB", "Higgs Sub Jet Mass", 80, 0.0, 400.0);

	HiggsAllJetMass[0] = new TH1F ("HiggsAllJetMass", "Higgs All Jet Mass", 160, -400.0, 400.0);
	HiggsAllJetMass[1] = new TH1F ("HiggsAllJetMassB", "Higgs All Jet Mass", 160, -400.0, 400.0);
	HiggsAllJetMass[2] = new TH1F ("HiggsAllJetMassBB", "Higgs All Jet Mass", 160, -400.0, 400.0);



	hPileUp[0] = new TH1F ("hPileUp", "hPileUp", 80, -4, 4);
	hPileUpErr[0] = new TH1F ("hPileUpErr", "hPileUpErr", 80, -4, 4);
	hZVtx[0] = new TH1F ("hZVtx", "hZVtx", 80, -4, 4);
	hZVtxErr[0] = new TH1F ("hZVtxErr", "hZVtxErr", 80, -4, 4);
	hMcEv[0] = new TH1F ("hMcEv", "hMcEv", 80, -4, 4);
	hMcEvErr[0] = new TH1F ("hMcEvErr", "hMcEvErr", 80, -4, 4);
	hFourJet[0] = new TH1F ("hFourJet", "hFourJet", 80, -4, 4);
	hFourJetErr[0] = new TH1F ("hFourJetErr", "hFourJetErr", 80, -4, 4);
	hFourJetVSPT[0] = new TH2F ("hFourJetVSPT", "hFourJetVSPT",100,0,500 ,80, -4, 4);
	hLepReco = new TH1F ("hLepReco", "hLepReco", 80, -4, 4);
	hLepRecoErr = new TH1F ("hLepRecoEff", "hLepRecoEff", 80, -4, 4);
	hLepId = new TH1F ("hLepId", "hLepId", 80, -4, 4);
	hLepIdErr = new TH1F ("hLepIdEff", "hLepIdEff", 80, -4, 4);
	hLepIso = new TH1F ("hLepIso", "hLepIso", 80, -4, 4);
	hLepIsoErr = new TH1F ("hLepIsoEff", "hLepIsoEff", 80, -4, 4);
	hLepTrig = new TH1F ("hLepTrig", "hLepTrig", 80, -4, 4);
	hLepTrigErr = new TH1F ("hLepTrigEff", "hLepTrigEff", 80, -4, 4);

	hPileUp[1] = new TH1F ("hPileUpB", "hPileUp", 80, -4, 4);
	hPileUpErr[1] = new TH1F ("hPileUpErrB", "hPileUpErr", 80, -4, 4);
	hZVtx[1] = new TH1F ("hZVtxB", "hZVtx", 80, -4, 4);
	hZVtxErr[1] = new TH1F ("hZVtxErrB", "hZVtxErr", 80, -4, 4);
	hMcEv[1] = new TH1F ("hMcEvB", "hMcEv", 80, -4, 4);
	hMcEvErr[1] = new TH1F ("hMcEvErrB", "hMcEvErr", 80, -4, 4);
	hFourJet[1] = new TH1F ("hFourJetB", "hFourJet", 80, -4, 4);
	hFourJetErr[1] = new TH1F ("hFourJetErrB", "hFourJetErr", 80, -4, 4);
	hFourJetVSPT[1] = new TH2F ("hFourJetVSPTB", "hFourJetVSPT", 100,0,500 ,80, -4, 4);

	hPileUp[2] = new TH1F ("hPileUpBB", "hPileUp", 80, -4, 4);
	hPileUpErr[2] = new TH1F ("hPileUpErrBB", "hPileUpErr", 80, -4, 4);
	hZVtx[2] = new TH1F ("hZVtxBB", "hZVtx", 80, -4, 4);
	hZVtxErr[2] = new TH1F ("hZVtxErrBB", "hZVtxErr", 80, -4, 4);
	hMcEv[2] = new TH1F ("hMcEvBB", "hMcEv", 80, -4, 4);
	hMcEvErr[2] = new TH1F ("hMcEvErrBB", "hMcEvErr", 80, -4, 4);
	hFourJet[2] = new TH1F ("hFourJetBB", "hFourJet", 80, -4, 4);
	hFourJetErr[2] = new TH1F ("hFourJetErrBB", "hFourJetErr", 80, -4, 4);
	hFourJetVSPT[2] = new TH2F ("hFourJetVSPTBB", "hFourJetVSPT", 100,0,500 ,80, -4, 4);

	testMuons=new TH1F("testMuons","testMuons",500,0,500);
	qweight= new TH1F("qweight","qcd_weight",100, -1.5, 1.5);
	TightvsWeight=new TH2F("TightvsWeight","TightvsWeight",2,-0.1,1.1,100,-1.5,1.5);
        mcweight = new TH1F("mcweight","mc_weight",100, -1.5, 1.5);
        pileupweight = new TH1F("pileupweight","pileup_weight",250, 0, 2.5);        
        leptonSFweight = new TH1F("leptonSFweight","leptonSF_weight",250, 0, 2.5); 
        bTagSFweight = new TH1F("bTagSFweight","bTagSF_weight",250, 0, 2.5);
        jvtweight = new TH1F("jvtweight","jvt_weight",250, 0, 2.5);
        sherpa22vjetsweight = new TH1F("sherpa22vjetsweight","sherpa22vjets_weight",100, -1.5, 1.5);

	char hname[64];

	for (int k=0; k<3; k++)
	{
		sprintf (hname,"HTut%i",k);    HTut[k] = new TH1F (hname,"Sum HT ut",250,0,5000);
	}
    for (int k=0; k<3; k++)
    {
        sprintf (hname,"HTJets%i",k);    HTJets[k] = new TH1F (hname,"Sum HT of all Jets",250,0,5000);
    }

	icount=0;
	return retval;
}


// --------------------
int E62dbxA:: readAnalysisParams() {
	int retval=0;
	TString CardName=getDataCardName();
	minpte  = ReadCard(CardName,"minpte",20.0);
	minptm  = ReadCard(CardName,"minptm",20.0);
	maxetae = ReadCard(CardName,"maxetae",2.5);
	maxetam = ReadCard(CardName,"maxetam",2.5);
	minmete  = ReadCard(CardName,"minmete",35.0);
	minmetmu  = ReadCard(CardName,"minmetmu",20.0);
	minptj  = ReadCard(CardName,"minptj",25.0);
	maxetaj = ReadCard(CardName,"maxetaj",2.5);
	mindrjm = ReadCard(CardName,"mindrjm",0.4);
	mindrje = ReadCard(CardName,"mindrje",0.2);
	mqhxmin = ReadCard(CardName,"mqhxmin",0.0);  // mHQ histo xmin
	mqhxmax = ReadCard(CardName,"mqhxmax",1000.0); // mHQ histo xmax
	maxMuPtCone = ReadCard(CardName,"maxMuPtCone",10.0);
	maxMuEtCone = ReadCard(CardName,"maxMuEtCone",10.0);
	TRGe = ReadCard(CardName,"TRGe",1);
	TRGm = ReadCard(CardName,"TRGm",1);
	jetVtxf2011 = ReadCard(CardName,"jetVtxf2011",0.75);
	jetVtxf2012 = ReadCard(CardName,"jetVtxf2012",0.50);
	maxz0e = ReadCard(CardName,"maxz0e",2);
	maxz0m = ReadCard(CardName,"maxz0m",2);
	// E6 Analysis Parameters----------
	is_this_zjetsCR = ReadCard(CardName,"is_this_zjetsCR",0);
	maxDeltaZmassEl = ReadCard(CardName,"maxDeltaZmassEl",20);
	maxDeltaZmassMu = ReadCard(CardName,"maxDeltaZmassMu",20);
	HerrorM = ReadCard(CardName,"HerrorM",100);
	DHerrorM = ReadCard(CardName,"DHerrorM",100);
	DZerrorM = ReadCard(CardName,"DZerrorM",100);
	HerrorE = ReadCard(CardName,"HerrorE",100);
	DHerrorE = ReadCard(CardName,"DHerrorE",100);
	DZerrorE = ReadCard(CardName,"DZerrorE",100);
	MaximumChi = ReadCard(CardName,"MaximumChi",100);
	MinRDist = ReadCard(CardName,"MinRDist",100);
	MinPJetPtE = ReadCard(CardName,"MinPJetPtE",170);
	MinPJetPtM = ReadCard(CardName,"MinPJetPtM",170);
	MinSubPJetPtE = ReadCard(CardName,"MinSubPJetPtE",170);
	MinSubPJetPtM = ReadCard(CardName,"MinSubPJetPtM",170);
// we read the HF if it is not previously initialized by hand.
        if (HFtype<0) HFtype = ReadCard(CardName,"HFtype",3);

// ---------------------------DBX style defs
         int kk=1;
         map < string, string > def_names;
         size_t apos = 0; 
         std::string subdelimiter = ":"; 
         while (1){
                  TString basedef="def";
                          basedef+=kk++; 
                  string def0 = ReadCardString(CardName,basedef,"XXX");
                  if (def0=="XXX") break;
                  cout << "==========-> def id:"<<kk-1<<") "<<def0<<endl;
                  if ((apos=def0.find(subdelimiter)) != std::string::npos )  {  
                         std::string subtoken0, subtoken1;
                         subtoken0 = def0.substr(0, apos); //
                         subtoken1 = def0.substr(apos+subdelimiter.length(),std::string::npos); //      
                         subtoken1+=" ";
//                       std::cout <<"DEFINE  "<<subtoken0<<" as "<<subtoken1<<std::endl;
                         TNamed *astr_tmp=new TNamed (subtoken0.data(), subtoken1.data());
//                       TParameter<std::string> *astr_tmp=new TParameter<std::string> (subtoken0.data(), subtoken1,'f');
//                                             astr_tmp->Write(); // can we overload merge on TName? or similar.
//TParameter<std::string> is Foobar because of line 142 on TParameter.h :   fVal *= c->GetVal(); // can't be done with strings
                         def_names[subtoken0]=subtoken1;
                  } else{
                  cout << "This def is problematic. STOP\n"; exit (3);
                  }
        }


// ---------------------------DBX style cuts
         e62cutlist.setTrigType( TRGm+(TRGe<<2) );//bitwise left shift by 2 for TRGe
	 eff->GetXaxis()->SetBinLabel(1,"all Events"); // this is hard coded.
         int kFillHistos=0;
         kk=1; 
         while (1){
                  TString basecut="cut";
                          basecut+=kk++; // BERARE I WISH YOU READ THIS LINE.
                  string cut0 = ReadCardString(CardName,basecut,"XXX");
                  if (cut0=="XXX") break;
                  cout << "\n~~~~~~~~~~-> cut id:"<<kk-1<<") "<<cut0<<"\t";
                  TString newLabels = cut0.data();
                  if (cut0.find("FillHistos")!=std::string::npos){
                   newLabels = "FillHistos-"+TString::Format("%d",++kFillHistos); //berare
                  }
// do we have a definition?
                 for( map<string,string>::reverse_iterator it=def_names.rbegin(); it!=def_names.rend(); ++it) {
//                  cout << "\ndef:"<<it->first<<endl;
                  std::size_t found =cut0.find(it->first); 
                  while ( found !=std::string::npos){
                    cout << "Replace with:"<<it->second<<"  ";
                    cut0.replace(found,it->first.length(),it->second);
                    cout << "\n~~~~~~~~~~-> cut id:"<<kk-1<<" becomes ) "<<cut0<<"\t";
                    found =cut0.find(it->first);
                  }
                 }
                  
                  eff->GetXaxis()->SetBinLabel(kk,newLabels);
                  std::vector<dbxCut*> acutlist;
                  acutlist.reserve(10);
                  std::vector<std::string> q;
                  q=e62cutlist.cutTokenizer(cut0, &acutlist); //e62cutlist is not a real list, name change and also just a local variable TODO
                  mycutlist.push_back(acutlist);
                  myopelist.push_back(q);
/////////DEBUG
/*
                  for ( unsigned int ic=0; ic<acutlist.size(); ic++) {
                      cout<<" name:"<<acutlist[ic]->getName()<<"\t";
                  }
                  cout<<endl;
          
                  unsigned int jc=0;
                  for ( unsigned int ic=0; ic<q.size(); ic++) {
                    if (q[ic]=="x"){
                      cout<<acutlist[jc++]->getName();
                      cout<<q[ic];
                    } else {
                      cout<<q[ic];
                    }
                  }
                  cout<<endl;
*/
         } // end of while 1
    double e6varlist[7];
    e6varlist[0]=(double)is_this_zjetsCR;
    e6varlist[1]=MinRDist;
    if (TRGe>0){
        e6varlist[2]=HerrorE;
        e6varlist[3]=DHerrorE;
        e6varlist[4]=DZerrorE;
        e6varlist[5]=MinPJetPtE;
        e6varlist[6]=MinSubPJetPtE;
    } else {
        e6varlist[2]=HerrorM;
        e6varlist[3]=DHerrorM;
        e6varlist[4]=DZerrorM;
        e6varlist[5]=MinPJetPtM;
        e6varlist[6]=MinSubPJetPtM;
    }

         cout << "\nWe have "<<mycutlist.size() << " CutLang Cuts\n";
    for ( unsigned int ic=0; ic<mycutlist.size(); ic++) {
        cout<<" name:"<<mycutlist[ic][0]->getName()<<"\t";
        if (mycutlist[ic][0]->getName() == "}Chi2E6") {
            mycutlist[ic][0]->setSpecialVariables(7, e6varlist); //////////////////NGU
        }
    }//end of for

	 eff->GetXaxis()->SetBinLabel(2+mycutlist.size(),"Chi Square");
     eff->GetXaxis()->SetBinLabel(3+mycutlist.size(),"b-tagging requirement");


////// PUT ANALYSIS PARAMETERS INTO .ROOT //////////////
	TParameter<double> *maxz0e_tmp=new TParameter<double> ("maxz0e", maxz0e,'f');
	TParameter<double> *maxz0m_tmp=new TParameter<double> ("maxz0m", maxz0m,'f');
	TParameter<double> *minpte_tmp=new TParameter<double> ("minpte", minpte,'f');
	TParameter<double> *minptm_tmp=new TParameter<double> ("minptm", minptm,'f');
	TParameter<double> *maxetae_tmp=new TParameter<double> ("maxetae", maxetae,'f');
	TParameter<double> *maxetam_tmp=new TParameter<double> ("maxetam", maxetam,'f');
	TParameter<double> *minmete_tmp=new TParameter<double> ("minmete", minmete,'f');
	TParameter<double> *minmetmu_tmp=new TParameter<double> ("minmetmu", minmetmu,'f');
	TParameter<double> *minptj_tmp=new TParameter<double> ("minptj", minptj,'f');
	TParameter<double> *maxetaj_tmp=new TParameter<double> ("maxetaj", maxetaj,'f');
	TParameter<double> *mindrjm_tmp=new TParameter<double> ("mindrjm", mindrjm,'f');
	TParameter<double> *mindrje_tmp=new TParameter<double> ("mindrje", mindrje,'f');
	TParameter<double> *mqhxmin_tmp=new TParameter<double> ("mqhxmin", mqhxmin,'f');
	TParameter<double> *mqhxmax_tmp=new TParameter<double> ("mqhxmax", mqhxmax,'f');
	TParameter<double> *maxMuPtCone_tmp=new TParameter<double> ("maxMuPtCone", maxMuPtCone,'f');
	TParameter<double> *maxMuEtCone_tmp=new TParameter<double> ("maxMuEtCone", maxMuEtCone,'f');
	TParameter<double> *TRGe_tmp=new TParameter<double> ("TRGe", TRGe,'f');
	TParameter<double> *TRGm_tmp=new TParameter<double> ("TRGm", TRGe,'f');
	TParameter<double> *jetVtxf2011_tmp=new TParameter<double> ("jetVtxf2011",jetVtxf2011,'f');
	TParameter<double> *jetVtxf2012_tmp=new TParameter<double> ("jetVtxf2012",jetVtxf2012,'f');
	// E6 Analysis Parameters----------
	TParameter<double> *maxDeltaZmassEl_tmp=new TParameter<double> ("maxDeltaZmassEl", maxDeltaZmassEl,'f');
	TParameter<double> *maxDeltaZmassMu_tmp=new TParameter<double> ("maxDeltaZmassMu", maxDeltaZmassMu,'f');
	TParameter<double> *BFlavor_tmp=new TParameter<double> ("BFlavor", BFlavor,'f');
	TParameter<double> *HerrorM_tmp=new TParameter<double> ("HerrorM", HerrorM,'f');
	TParameter<double> *DHerrorM_tmp=new TParameter<double> ("DHerrorM", DHerrorM,'f');
	TParameter<double> *DZerrorM_tmp=new TParameter<double> ("DZerrorM", DZerrorM,'f');
	TParameter<double> *HerrorE_tmp=new TParameter<double> ("HerrorE", HerrorE,'f');
	TParameter<double> *DHerrorE_tmp=new TParameter<double> ("DHerrorE", DHerrorE,'f');
	TParameter<double> *DZerrorE_tmp=new TParameter<double> ("DZerrorE", DZerrorE,'f');
	TParameter<double> *MaximumChi_tmp=new TParameter<double> ("MaximumChi", MaximumChi,'f');
	TParameter<double> *MinRDist_tmp=new TParameter<double> ("MinRDist", MinRDist,'f');
	TParameter<double> *MinPJetPtE_tmp=new TParameter<double> ("MinPJetPtE", MinPJetPtE,'f');
	TParameter<double> *MinPJetPtM_tmp=new TParameter<double> ("MinPJetPtM", MinPJetPtM,'f');
	TParameter<double> *MinSubPJetPtE_tmp=new TParameter<double> ("MinSubPJetPtE", MinSubPJetPtE,'f');
	TParameter<double> *MinSubPJetPtM_tmp=new TParameter<double> ("MinSubPJetPtM", MinSubPJetPtM,'f');
	TParameter<double> *HFtype_tmp=new TParameter<double> ("HFtype", HFtype,'f');

	// ---------------------------

	maxz0e_tmp->Write("maxz0e");
	maxz0m_tmp->Write("maxz0m");
	minpte_tmp->Write("minpte");
	minptm_tmp->Write("minptm");
	maxetae_tmp->Write("maxetae");
	maxetam_tmp->Write("maxetam");
	minmete_tmp->Write("minmete");
	minmetmu_tmp->Write("minmetmu");
	minptj_tmp->Write("minptj");
	maxetaj_tmp->Write("maxetaj");
	mindrjm_tmp->Write("mindrjm");
	mindrje_tmp->Write("mindrje");
	mqhxmin_tmp->Write("mqhxmin");
	mqhxmax_tmp->Write("mqhxmax");
	maxMuPtCone_tmp->Write("maxMuPtCone");
	maxMuEtCone_tmp->Write("maxMuEtCone");
	TRGe_tmp->Write("TRGe");
	TRGm_tmp->Write("TRGm");
	jetVtxf2011_tmp->Write("jetVtxf2011");
	jetVtxf2012_tmp->Write("jetVtxf2012");
	// E6 Analysis Parameters----------
	maxDeltaZmassEl_tmp->Write("maxDeltaZmassEl");
	maxDeltaZmassMu_tmp->Write("maxDeltaZmassMu");
	BFlavor_tmp->Write("BFlavor");
	HerrorM_tmp->Write("HerrorM");
	DHerrorM_tmp->Write("DHerrorM");
	DZerrorM_tmp->Write("DZerrorM");
	HerrorE_tmp->Write("HerrorE");
	DHerrorE_tmp->Write("DHerrorE");
	DZerrorE_tmp->Write("DZerrorE");
	MaximumChi_tmp->Write("MaximumChi");
	MinRDist_tmp->Write("MinRDist");
	MinPJetPtE_tmp->Write("MinPJetPtE");
	MinPJetPtM_tmp->Write("MinPJetPtM");
	MinSubPJetPtE_tmp->Write("MinSubPJetPtE");
	MinSubPJetPtM_tmp->Write("MinSubPJetPtM");
	HFtype_tmp->Write("HFtype");


	// ---------------------------


	return retval;
}

int E62dbxA:: printEfficiencies() {
	int retval=0;
	PrintEfficiencies(eff);
	return retval;
}

int E62dbxA:: initGRL() {
	grl_cut=true;
	return 0;
}


//////////////////////////////////
int E62dbxA::plotVariables(int sel){
	dbxA::plotVariables (sel);
	int retval=0;
	return retval;
}

// Make Analysis for normal runs, this calls the montecarlo version with null.
int E62dbxA::makeAnalysis(vector<dbxMuon> muons, vector<dbxElectron> electrons, vector<dbxPhoton> gams, vector<dbxJet> jets, vector<dbxLJet> ljets, vector<dbxTruth> truth, TVector2 met, evt_data anevt)

{
 
	if(TRGe>=1 || TRGm>=1) //only like this for now.
        return E62dbxA::makeAnalysis(muons, electrons,gams, jets, ljets, truth, met, anevt,NULL, NULL,NULL, NULL, NULL, NULL,
                                     NULL, NULL, NULL, NULL);
	else
		cout<<"Error Trying to run a Data Run in Monte Carlo Mode, Won't Make a Run"<<endl;
	return -1;

}

int E62dbxA::makeAnalysis(const vector<dbxMuon>& muons,const vector<dbxElectron>& electrons, const vector<dbxPhoton>& gams,
                          const vector<dbxJet>& jets, const vector<dbxLJet>& ljets, const vector<dbxTruth>& truth, const TVector2& met,evt_data anevt,vector<vector<int> > *mc_child_index, vector<vector<int> > *mc_parent_index,
                          vector<int> *mc_pdgId, vector<float> *mc_eta, vector<float> *mc_phi, vector<int> *mc_status,
                          vector<float> *mc_pt, vector<float> *mc_E, vector<float> *mc_m, vector<float> *mc_charge)
{

#ifdef __VERBOSE3__
	std::cout<<"--------------Starting New Event: "<<anevt.event_no<<" --------------"<<std::endl;
        std::cout <<"Event loop, this is:"<<getName()<<endl;      
#endif
	int cur_cut=1;
	bool QCD_IsLoose=true, QCD_IsTight=true;

	TLorentzVector alepton(0,0,0,0), Whad(0,0,0,0), Wlep(0,0,0,0), Wlep2(0,0,0,0);
	dbxParticle theLepton(TLorentzVector(0,0,0,0));
	double theLeptonTrkEta = -9999;
	double theLeptonTrkPhi = -9999;
	int theLeptonIndx = -1;
	bool trgmatch = false;
        bool IsTight=false; // Variable to determine to use two good leptons or 2 loose leptons

	vector<dbxElectron>  goodElectrons;
	vector<dbxMuon>      goodMuons;
	vector<dbxJet>       goodJets, overlapJets;
    vector<dbxLJet>      goodLJets;
	vector<dbxElectron>  emuoverlappedElectrons;

	// QCD
	vector<dbxMuon> looseMuons;
	vector<bool> goodMuonTag, goodElectronTag;
	vector<dbxElectron> looseElectrons;
	vector<dbxElectron>  emuoverlappedlooseElectrons;

//----------------------selection of good electrons-----------------
	for (UInt_t i=0; i<electrons.size(); i++) {
               if(electrons.at(i).isZCand()==0) continue;//This varible has just added in tree. Should we use?
               
	       if ( (electrons.at(i).lv().Pt()  > minpte)    // the electrons should have a minimum PT
                  &&(electrons.at(i).lv().Eta() < maxetae )  // and maximum eta.
                  )
                  goodElectrons.push_back( electrons.at(i) );                
       	}


#ifdef __VERBOSE3__
	std::cout<<"# electrons "<< electrons.size()  <<" after selection "<< goodElectrons.size()   <<std::endl;
#endif


//----------------------selection of good muons-----------------
	for (UInt_t i=0; i<muons.size(); i++) {
		if(muons.at(i).isZCand()==0) continue;//This varible has just added in tree. Should we use?
                TLorentzVector mu4p = muons.at(i).lv();
		if (       (mu4p.Pt()  > minptm)
			&& (fabs(mu4p.Eta()) < maxetam)
		)
		{
                        goodMuons.push_back(muons.at(i));
		}
	}
#ifdef __VERBOSE3__
	std::cout<<"muon Selection "<<anevt.event_no<<std::endl;
#endif

//------------selection of good jets----------------------------------
	for (UInt_t i=0; i<jets.size(); i++) {
		TLorentzVector jet4p = jets.at(i).lv();
		if (  (fabs(jet4p.Pt())  > minptj ) // this corresponds to 25GeV cut
				&& (jet4p.E() >= 0)			
                   )
		{
                        if (fabs(jet4p.Eta())<= maxetaj) //A.A 
                           goodJets.push_back(jets.at(i) );
		}
	}
#ifdef __VERBOSE3__
	std::cout<<"# jets "<< jets.size()  <<" after selection "<< goodJets.size()   <<std::endl;
#endif

//this currently (all values set to 0, does nothing.) but can be updated accordingly.
    
    //------------selection of good large R jets----------------------------------
    for (UInt_t i=0; i<ljets.size(); i++) {
        //cout << "there are large R jets!!! " << endl;
        TLorentzVector ljet4p = ljets.at(i).lv();
     
                goodLJets.push_back(ljets.at(i) );
        }
#ifdef __VERBOSE3__
    if (goodLJets.size() != 0 )
    std::cout<<"# large R jets "<< ljets.size()  <<" after selection "<< goodLJets.size()   <<std::endl;
#endif


    
///////
	double theLeptonWeight = 1;
	double theFourJetWeight = 1;
	unsigned int njets;
	double evt_weight = 1;
        
        if(TRGe==2 || TRGm== 2) evt_weight = anevt.weight_mc*anevt.weight_pileup*anevt.weight_jvt;//
        mcweight->Fill(anevt.weight_mc);
        pileupweight->Fill(anevt.weight_pileup);
        leptonSFweight->Fill(anevt.weight_leptonSF);
        bTagSFweight->Fill(anevt.weight_bTagSF_77);
        jvtweight->Fill(anevt.weight_jvt);
        sherpa22vjetsweight->Fill(anevt.weight_sherpa_22_vjets);
        
#ifdef __VERBOSE3__
	std::cout<<"Cut Init"<<endl;
	std::cout<<"Event Weight "<<evt_weight<<std::endl;
#endif

// --------- INITIAL  # events  ====> C0
	eff->Fill(cur_cut, 1);
	cur_cut++;
#ifdef __VERBOSE3__
	std::cout<<"INITIAL Count ========> C0 "<<anevt.event_no<<std::endl;
#endif
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    
	dbxA::addRunLumiInfo(anevt.run_no,anevt.lumiblk_no);

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    AnalysisObjects a0={goodMuons, goodElectrons, gams, goodJets, goodLJets, truth, met, anevt};
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~cutLang style preselection
    unsigned int bTagSf_counter=0;
    unsigned int FillHistos_counter=0;
             int ahistid; 
           float ahistval;
    //    std::cout<<"\n--------------Starting New Event: "<<anevt.event_no<<"  ";
    
    found_type_vecs.clear(); found_idx_vecs.clear(); found_idx_origs.clear();
    for (unsigned int k=0; k<mycutlist.size(); k++){
        if (mycutlist[k][0]->getOp()=="~=" || mycutlist[k][0]->getOp()=="!=")  // closest to or far away from
            mycutlist[k][0]->clearFoundVector(); //---- clear previous events findings.
    }
    
    // *************************************
    /// CutLang execution starts-------here*
    // *************************************
    for (unsigned int k=0; k<mycutlist.size(); k++){
        std::ostringstream oss;
        unsigned int j=0;
        double d;
#ifdef __VERBOSE3__
        cout << "cut:"<<k<< " "<<" # operations in this cut:"<<myopelist[k].size();
        cout << " name of this cut:"<<mycutlist[k][j]->getName();
#endif
//--------- we apply lepton scale factor 
        if (TRGe==2 || TRGm== 2) {
         if (mycutlist[k][j]->getName() == "LEPsf" ) evt_weight*=anevt.weight_leptonSF; 
         if (mycutlist[k][j]->getName() == "bTagSF" && bTagSf_counter == 0   ) {
                evt_weight*=anevt.weight_bTagSF_77;
                bTagSf_counter++;
         } //TO BE IMPROVED
        }
//----------------
        if (mycutlist[k][j]->getName() == "FillHistos") {
          FillHistos_counter++;
          dbxParticle ZBoscon;
          if(TRGe>0) {
              if( QCD && !IsTight) ZBoscon=looseElectrons.at(0)+looseElectrons.at(1);
              else                 ZBoscon=goodElectrons.at(0)+goodElectrons.at(1);
          }
          else if(TRGm>0) {
              if (QCD && !IsTight) ZBoscon=looseMuons.at(0)+looseMuons.at(1);
              else                 ZBoscon=goodMuons.at(0)+goodMuons.at(1);
          }

          switch ( FillHistos_counter ) {

           case 1:
                  anevt.mcevt_weight = evt_weight;//BG
                 break;

           case 2:
                 anevt.mcevt_weight = evt_weight;//BG
                 break;

           case 3:
           case 4:
           case 5:
           case 6:
           case 7:
           case 8:
                 break;

// if it is not defined---------------
          default:
                cout << "This PLOT section is NOT defined!!!!!!!!\n";
                break;
         }

        // no need to calculate cut value, since this is only plotting facility
        // continue; ---------TODO


//---------fill cutlang type histos
//      cout << "FillSet "<<FillHistos_counter<<endl;
        for (int ij=1; ij<histos_order.at(FillHistos_counter-1).size(); ij++){
           ahistid=histos_order.at(FillHistos_counter-1)[ij].first;
          ahistval=histos_order.at(FillHistos_counter-1)[ij].second.at(0)->calc(&a0);
//        cout << "Histo ID:"<<ahistid<<" to be filled with:"<<ahistval<<endl;
          a_histos[ahistid]->Fill(ahistval, evt_weight);
        }
//      cout << "\n";

        if (b_histos[FillHistos_counter] != -1 ) { //was set at the initialization also we start @1.
            dbxA::makeAnalysis (goodMuons, goodElectrons, goodJets, goodLJets, truth, met, anevt, b_histos[FillHistos_counter]);
        }

        }// end of histogram filling command


        if (myopelist[k].size()>1) { //more than one operator
           for ( unsigned int i=0; i<myopelist[k].size(); i++) { //loop over operations
//           cout<<myopelist[k][i]<<"  "; // this operator
           if (myopelist[k][i]=="x"){   //
//              cout<<mycutlist[k][j]->getName()<<"\t";
              oss<<mycutlist[k][j++]->select(&a0);
           } else {
              oss<<myopelist[k][i];
           }
           } // end of operations
//           oss << "  "; // very very important to finish with space.
//           cout<<"\n"<< oss.str()<<endl;

//--------------we now have a logic operation to perform    
           basic_parser pars;
           std::vector<std::string> rpn;
           std::vector<std::string> tokens = pars.getExpressionTokens( oss.str() );
           pars.infixToRPN( tokens, tokens.size(), &rpn );
           d = pars.RPNtoDouble( rpn );
        } else { //single operation
//           cout<<mycutlist[k][j]->getName()<<"\t";
            if (mycutlist[k][j]->isSearchable() ) {
                DEBUG(mycutlist[k][j]->getName()<<" is searchable.\n");
                int found_res_size=found_idx_vecs.size();
                if (found_res_size >0) {// do we have any previous results we can use?
                    ret_i.clear();
                    ret_t.clear();
                    find_idxtype_TObeused( mycutlist[k][j], &found_idx_vecs, &found_type_vecs, &found_idx_origs, &ret_i, &ret_t );
                    DEBUG("To be used idx_vec size:"<< ret_i.size()<<endl);
                    if (ret_i.size() >0) { mycutlist[k][j]->setFoundVectors( &ret_i, &ret_t, &found_idx_origs); }
                    forbidthese.clear();
                    for (int ikk=0; ikk<found_res_size; ikk++){
                        forbidthese.push_back( make_pair(found_idx_vecs[ikk],found_type_vecs[ikk]) );
                    }
                    mycutlist[k][j]->setForbiddenVector(&forbidthese);
                }
            }// end searchable
           d=mycutlist[k][j]->select(&a0); // execute the selection cut
            if (mycutlist[k][j]->getOp()=="~=" || mycutlist[k][j]->getOp()=="!=")
                //           if ((mycutlist[k][j]->getFoundVector()).size() >0)  // found any ?
            {
                DEBUG("storing found vecs. Tot="<<(mycutlist[k][j]->getFoundVector()).size());
                for (int iti=0; iti<(mycutlist[k][j]->getFoundVector()).size(); iti++) {
                    found_idx_vecs.push_back ( mycutlist[k][j]->getFoundVector()[iti]  );
                    found_type_vecs.push_back( mycutlist[k][j]->getFoundType(iti) );
                    found_idx_origs.push_back( mycutlist[k][j]->getOrigFoundIndexes(iti) );
                }
                //   DEBUG(" TOT#found:"<<found_idx_vecs.size()<<" tot#Oi:"<<found_idx_origs.size() <<" \n ");
                //   for (int iti=0; iti<found_idx_origs.size(); iti++) {DEBUG(" Oi:"<<found_idx_origs[iti]);}  DEBUG("\n");
            }
            j++;
        } //end of single operation
//        std::cout << "             Result = " << d << std::endl;
        if (d==0) return k; // quit the event.
        eff->Fill(k+2, evt_weight); // filling starts from 1 which is already filled.
    }
    cur_cut+=mycutlist.size(); // we continue


//--------------------------NOW the rest--------------------------
// Chi Square ========> C10
        dbxParticle ZBos;
        if(TRGe>0) {
              if( QCD && !IsTight) ZBos=looseElectrons.at(0)+looseElectrons.at(1);
              else                 ZBos=goodElectrons.at(0)+goodElectrons.at(1);
          }
          else if(TRGm>0) {
              if (QCD && !IsTight) ZBos=looseMuons.at(0)+looseMuons.at(1);
              else                 ZBos=goodMuons.at(0)+goodMuons.at(1);
          }


	double eventchi[3]={1000,1000,1000}, Hchi[3]={1000,1000,1000}, Dchi[3]={1000,1000,1000};
	unsigned int SJet[3][4];
	dbxParticle *DfH[3],*DfZ[3], *Hbos[3];
	Hbos[0]=NULL; Hbos[1]=NULL; Hbos[2]=NULL;
	DfH[0]=NULL; DfH[1]=NULL; DfH[2]=NULL;
	DfZ[0]=NULL; DfZ[1]=NULL; DfZ[2]=NULL;

	bool Success[3]={false,false,false};
    for(int BTAG=0; BTAG<3;BTAG++)
    {
        if(TRGe>0) {
            
            Success[BTAG] = ChiSquare(goodJets, goodLJets, ZBos,
                                      MinPJetPtE, MinSubPJetPtE, HerrorE, DHerrorE, DZerrorE,
                                      Hbos[BTAG], DfH[BTAG], DfZ[BTAG], SJet[BTAG],
                                      eventchi[BTAG], Hchi[BTAG], Dchi[BTAG],BTAG);
            
        }
        if(TRGm>0) {
            Success[BTAG] = ChiSquare(goodJets, goodLJets, ZBos,
                                      MinPJetPtM, MinSubPJetPtM, HerrorM, DHerrorM, DZerrorM,
                                      Hbos[BTAG], DfH[BTAG], DfZ[BTAG], SJet[BTAG],
                                      eventchi[BTAG], Hchi[BTAG], Dchi[BTAG],BTAG);
        }
        EventChiBF[BTAG]->Fill(eventchi[BTAG],evt_weight);
    }


	if (eventchi[0] > MaximumChi ) return cur_cut;
  	if (eventchi[1] > MaximumChi ) Success[1]=false;
  	if (eventchi[2] > MaximumChi ) Success[2]=false;
	eff->Fill(cur_cut, evt_weight);
	cur_cut++;

#ifdef __VERBOSE3__
	std::cout<<"Chi Square ========> C10 "<<anevt.event_no<<std::endl;
	cout<<"SJETS outside: "<<SJet[0][0]<<" "<<SJet[0][1]<<" "<<SJet[0][2]<<" "<<SJet[0][3]<<" "<<endl;
	cout<<"Higgs Mass: "<<Hbos[0]->lv().M()<<" De Mass from Higgs: "<<DfH[0]->lv().M()<<" De Mass from Z: "<<DfZ[0]->lv().M()<<" "<<endl;
#endif
    //-------------------------BTAGGING REQUIREMENT---------------------------
    // btagging requirement ========> C11

    if (is_this_zjetsCR)
    {
        //std::cout << "we are in zjetsCR" << endl;
        if ( goodJets.at(SJet[0][0]).isbtagged_77() == 1 ||  goodJets.at(SJet[0][1]).isbtagged_77() == 1   ) {//std::cout << "in Higgs daughters, there is at least 1 btagged jet, I am skipping this event " << endl;
	 return cur_cut;}
    }
    else
    {
        //std::cout << "we are in SR" << endl;
        if ( goodJets.at(SJet[0][0]).isbtagged_77() == 0 &&  goodJets.at(SJet[0][1]).isbtagged_77() == 0   ) {//std::cout << "in Higgs daughters, there are no btagged jets, I am skipping this event" << endl;
	return cur_cut;}
    }
    if ( bTagSf_counter==0 && (TRGe==2|| TRGm==2) )  //we WERE informed that applying bTag SF once is just enough for >= 1 bTAG
    { // counter takes care of ttbarCR, otherwise we need to apply btagSF once.
       evt_weight *= anevt.weight_bTagSF_77;
       anevt.mcevt_weight = evt_weight; //BG
    }
    eff->Fill(cur_cut, evt_weight);
    cur_cut++;
#ifdef __VERBOSE3__
    std::cout<<"btagging requirement========> C11 "<<anevt.event_no<<std::endl;
    cout<<"SJETS outside: "<<SJet[0][0]<<" "<<SJet[0][1]<<" "<<SJet[0][2]<<" "<<SJet[0][3]<<" "<<endl;
    cout<<"Higgs Mass: "<<Hbos[0]->lv().M()<<" De Mass from Higgs: "<<DfH[0]->lv().M()<<" De Mass from Z: "<<DfZ[0]->lv().M()<<" "<<endl;
#endif

	double htall[3]={fabs(ZBos.lv().Pt()),fabs(ZBos.lv().Pt()),fabs(ZBos.lv().Pt())};
	for(int BTAG=0; BTAG<3;BTAG++) // Each graphic is filled for the BTAGED Higgs and Non-BTAGED Higgs case
	{
      std::cout<<"\n------------"<<BTAG<<"\n";
		if (! Success[BTAG]) continue;

		if(BTAG==1 && (TRGe>1 || TRGm>1) )
		{
			theFourJetWeight=1;
			for (int ijet=0;ijet<goodJets.size();ijet++)
			{
				theFourJetWeight *= goodJets.at(ijet).btag_eventW();
				hFourJet[BTAG]->Fill(goodJets.at(ijet).btag_eventW());
				hFourJetErr[BTAG]->Fill(goodJets.at(ijet).btag_eventW()*goodJets.at(ijet).btag_eventW());
				hFourJetVSPT[BTAG]->Fill(goodJets.at(ijet).lv().Pt(),goodJets.at(ijet).btag_eventW());
			}
			//evt_weight*=theFourJetWeight;//A.A
		}
 /***** this is not done 20lines up, differently. NGU.       
        if(bTagSf_counter==0 && BTAG!=0 && (TRGe>1 || TRGm>1) ) //we informed that applying bTag SF once is just enough for >= 1 bTAG
        {
            evt_weight *= anevt.weight_bTagSF_77;
        }
	anevt.mcevt_weight = evt_weight; //BG
 ****/
        

        if (QCD) {
          dbxA::makeAnalysis (looseMuons, looseElectrons, goodJets, goodLJets, truth, met, anevt,BTAG);  // BTAG = 0,1,2
		} else {
            dbxA::makeAnalysis (goodMuons, goodElectrons, goodJets, goodLJets, truth, met, anevt,BTAG); // BTAG = 0,1,2
		}
        
		for (unsigned int jit=0; jit<4; jit++) htall[BTAG]+=fabs(goodJets.at(SJet[BTAG][jit]).lv().Pt());
        double htjets[3];
        for (unsigned int ijet=0;ijet<goodJets.size();ijet++) htjets[BTAG]+=fabs(goodJets.at(ijet).lv().Pt()); //BG
       

		//std::cout << "evt_weight : " << evt_weight << std::endl;//A.A
                // Histogram Filling
        HTJets[BTAG]->Fill(htjets[BTAG], evt_weight);
		HTut[BTAG]->Fill( htall[BTAG], evt_weight);
		HiggsMass[BTAG]->Fill(Hbos[BTAG]->lv().M(),evt_weight);
                HiggsPT[BTAG]->Fill(Hbos[BTAG]->lv().Pt(),evt_weight);//A.A
                HiggsETA[BTAG]->Fill(Hbos[BTAG]->lv().Eta(),evt_weight);//A.A
                HiggsPHI[BTAG]->Fill(Hbos[BTAG]->lv().Phi(),evt_weight);//A.A

		DeZMass[BTAG]->Fill(DfZ[BTAG]->lv().M(),evt_weight);
		DeZPT[BTAG]->Fill(DfZ[BTAG]->lv().Pt(),evt_weight);//A.A
                DeZETA[BTAG]->Fill(DfZ[BTAG]->lv().Eta(),evt_weight);//A.A                                                            
                DeZPHI[BTAG]->Fill(DfZ[BTAG]->lv().Phi(),evt_weight);//A.A

                DeHMass[BTAG]->Fill(DfH[BTAG]->lv().M(),evt_weight);
		DeHPT[BTAG]->Fill(DfH[BTAG]->lv().Pt(),evt_weight);//A.A
                DeHETA[BTAG]->Fill(DfH[BTAG]->lv().Eta(),evt_weight);//A.A
                DeHPHI[BTAG]->Fill(DfH[BTAG]->lv().Phi(),evt_weight);//A.A

                DeAMass[BTAG]->Fill((DfH[BTAG]->lv().M()+DfZ[BTAG]->lv().M())/2,evt_weight);
		EventChi[BTAG]->Fill(eventchi[BTAG],evt_weight);
		HiggsChi[BTAG]->Fill(Hchi[BTAG],evt_weight);
		DeChi[BTAG]->Fill(Dchi[BTAG],evt_weight);
		NHFlavorHist[BTAG]->Fill(goodJets.at(SJet[BTAG][3]).isbtagged_77(),evt_weight);//A.A
                NHFlavorHist[BTAG]->Fill(goodJets.at(SJet[BTAG][2]).isbtagged_77(),evt_weight);//A.A 
                HFlavorHist[BTAG]->Fill(goodJets.at(SJet[BTAG][0]).isbtagged_77(),evt_weight);//A.A
                HFlavorHist[BTAG]->Fill(goodJets.at(SJet[BTAG][1]).isbtagged_77(),evt_weight);//A.A
	          if( goodJets.at(SJet[BTAG][0]).isbtagged_77()==1 && goodJets.at(SJet[BTAG][1]).isbtagged_77()==1)//A.A		
                        hBtagMultJ_higgs[BTAG]->Fill(2.0,evt_weight);
		  else if( goodJets.at(SJet[BTAG][0]).isbtagged_77()==1 || goodJets.at(SJet[BTAG][1]).isbtagged_77()==1 )//A.A
                 	hBtagMultJ_higgs[BTAG]->Fill(1.0,evt_weight);
		else
			hBtagMultJ_higgs[BTAG]->Fill(0.0,evt_weight);

		METE[BTAG]->Fill(met.Mod(),evt_weight);
		METEvsZpt[BTAG]->Fill(met.Mod(),ZBos.lv().Pt(),evt_weight);
		METEvsHm[BTAG]->Fill(met.Mod(),Hbos[BTAG]->lv().M(),evt_weight);
		ZMass[BTAG]->Fill(ZBos.lv().M(),evt_weight);
		ZPT[BTAG]->Fill(ZBos.lv().Pt(),evt_weight);
                ZETA[BTAG]->Fill(ZBos.lv().Eta(),evt_weight);//A.A
                ZPHI[BTAG]->Fill(ZBos.lv().Phi(),evt_weight);//A.A

		if(TRGm>0)
		{
			float LeadMuonPt=0;
			if(QCD && !IsTight)
				if(looseMuons.at(0).lv().Pt() > looseMuons.at(1).lv().Pt()) LeadMuonPt=looseMuons.at(0).lv().Pt();
				else LeadMuonPt=looseMuons.at(1).lv().Pt();
			else
				if(goodMuons.at(0).lv().Pt() > goodMuons.at(1).lv().Pt() ) LeadMuonPt=goodMuons.at(0).lv().Pt();
				else LeadMuonPt=goodMuons.at(1).lv().Pt();

			hLeadMu[BTAG]->Fill(LeadMuonPt,evt_weight);
		}
		if(TRGe>0)
		{
			float LeadElPt=0;
			if(QCD && !IsTight)
				if(looseElectrons.at(0).lv().Pt() > looseElectrons.at(1).lv().Pt()) LeadElPt=looseElectrons.at(0).lv().Pt();
				else LeadElPt=looseElectrons.at(1).lv().Pt();
			else
				if(goodElectrons.at(0).lv().Pt() > goodElectrons.at(1).lv().Pt() ) LeadElPt=goodElectrons.at(0).lv().Pt();
				else LeadElPt=goodElectrons.at(1).lv().Pt();

			hLeadEl[BTAG]->Fill(LeadElPt,evt_weight);
		}


//-----------------DIKKAT burada sadece 0. jet icin doluyor! nicin tum jetler degil? NGU   ********** 
		hphij[BTAG]->Fill(goodJets.at(0).lv().Phi(), evt_weight);
                hphij_err[BTAG]->Fill(goodJets.at(0).lv().Phi(), evt_weight*evt_weight);
		hphiv[BTAG]->Fill(met.Phi_mpi_pi(met.Phi())-goodJets.at(0).lv().Phi() , evt_weight);
		hphiv_err[BTAG]->Fill(met.Phi_mpi_pi(met.Phi())-goodJets.at(0).lv().Phi() , evt_weight*evt_weight);

//-------------------- bunlarin en yuksek Pt li ve bir sonraki icin dolmasi normal
		hjpt_lead[BTAG]->Fill(goodJets.at(0).lv().Pt(), evt_weight);
		hjpt_lead_err[BTAG]->Fill(goodJets.at(0).lv().Pt(), evt_weight*evt_weight);
		hjpt_sublead[BTAG]->Fill(goodJets.at(1).lv().Pt(), evt_weight);
		hjpt_sublead_err[BTAG]->Fill(goodJets.at(1).lv().Pt(), evt_weight*evt_weight);
		hmultJ[BTAG]->Fill( goodJets.size(),     evt_weight );
		hmultJ_err[BTAG]->Fill( goodJets.size(),     evt_weight*evt_weight );

		//Niye Bilmiyorum Gokhan kullanmis
		if (!QCD)
		{
			hjmultip[BTAG]->Fill( goodJets.size(),     evt_weight );
			hjmultip_err[BTAG]->Fill( goodJets.size(),     evt_weight*evt_weight );
		}

		int btagged=0;
		for( int a=0; a<goodJets.size(); a++)
                         if( goodJets.at(a).isbtagged_77()==1) btagged++;//
		BTaggedJets[BTAG]->Fill(btagged,evt_weight);

		//hPileUp[BTAG]->Fill(anevt.pileup_weight);
		//hPileUpErr[BTAG]->Fill(anevt.pileup_weight*anevt.pileup_weight);
		//hZVtx[BTAG]->Fill(anevt.z_vtx_weight);
		//hZVtxErr[BTAG]->Fill(anevt.z_vtx_weight*anevt.z_vtx_weight);
		//hMcEv[BTAG]->Fill(anevt.mcevt_weight);
		//hMcEvErr[BTAG]->Fill(anevt.mcevt_weight*anevt.mcevt_weight);

                 hPileUp[BTAG]->Fill(anevt.weight_pileup);
                 hPileUpErr[BTAG]->Fill(anevt.weight_pileup*anevt.weight_pileup);

                 /*S.I : anevt_weight_mc yukarida uc farkli satirda su sekilde set edilmis:
                  anevt.weight_mc=evt_weight
                  yani son haliyle takip edebildigim kadar evt_weight=generatorxPUxJVTXxleptonSF
                  */ 
                anevt.mcevt_weight = evt_weight; //BG

                 hMcEv[BTAG]->Fill(anevt.mcevt_weight);
                 hMcEvErr[BTAG]->Fill(anevt.mcevt_weight*anevt.mcevt_weight);

	}

#ifdef __VERBOSE3__
	std::cout<<"Histogram Filling Complete  "<<anevt.event_no<<std::endl;
#endif


#ifdef __VERBOSE3__
	std::cout<<"MCH EventNumber : "<<anevt.event_no;
	std::cout << "WPileUp: " << anevt.pileup_weight << " WZwe:"<< anevt.z_vtx_weight<<endl;

	for (UInt_t i=0; i<goodElectrons.size(); i++) {
		std::cout<<"e :";
                goodElectrons.at(i).dump_b();
         }                                                                                                                  
        for (UInt_t i=0; i<goodMuons.size(); i++) {
                 std::cout<<"mu :";
                 goodMuons.at(i).dump_b();
         }
 
         for (UInt_t i=0; i<goodJets.size(); i++) {
                 std::cout<<"Jets :";
                 goodJets.at(i).dump_b();
         }
 
         std::cout<<"MET: "<<met.Mod()<<" MET_phi: "<<met.Phi()<<std::endl;
#endif

        for (int kc=cur_cut; kc<=eff->GetNbinsX(); kc++) eff->Fill(kc, evt_weight);
        return cur_cut;;
std::cout<<"END of MakeAnalysis\n";
} // end of analysis
