#ifndef E6_A2_H
#define E6_A2_H


#ifdef VLQLIGHT
#include "VLQLight/dbx_a.h"
#include "VLQLight/ReadCard.h"
#include "VLQLight/LeptonicWReconstructor.h"
#include "VLQLight/DBXNtuple.h"
#include "VLQLight/dbxCut.h"
#else
#include "dbx_a.h"
#include "ReadCard.h"
#include "LeptonicWReconstructor.h"
#include "DBXNtuple.h"
#include "dbxCut.h"
#endif

#include <TRandom.h>
#include <TSystem.h>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <TH1F.h>
#include <TH1.h>
class E62dbxA : public dbxA {
  public: 
      E62dbxA(char *aname) : dbxA ( aname)
      {
         
        Bool_t isSW2=TH1::GetDefaultSumw2();
        TH1::SetDefaultSumw2();
         
       int r=dbxA::setDir(aname);
       if (r) std::cout <<"Root Directory Set Failure in:"<<cname<<endl;
       QCD=false;
       grl_cut=false;

       char fname[CHMAX];
       sprintf (fname,"%sanalysis.lhco",cname); // we record the LHCO files
//     LHCOfile.open(fname);
//     LHCOfile << "#LHCO file - don't remove this line."<<endl;
      }
    bool ChiSquare(vector<dbxJet> goodJets, vector<dbxLJet> goodLJets, dbxParticle ZBos,
                   double MinLeadJetPt, double MinSubJetPt, double Herror, double DHerror, double DZerror,
                   dbxParticle *& Hbos, dbxParticle *& DfH, dbxParticle *& DfZ, unsigned int SJet[],
                   double & eventchi, double & Hchi, double & Dchi, int ForceBtag);
      int TrueLep(int id, vector<vector<int> > *mc_parent_index, vector<int> *mc_pdgId, vector<int> *mc_status);
      double deltaPhi(double phi1, double phi2);
      bool isCosmicEvent(vector<dbxMuon> muons);
      int indexNoSelfParentParticle(int particle_index, vector<vector<int> > *mc_parent_index, vector<int> *mc_pdgId);
      int initGRL();
      int setQCD();
      int bookAdditionalHistos();
      int readAnalysisParams();
      int printEfficiencies();
      int plotVariables(int sel);
    int makeAnalysis(vector<dbxMuon> muons, vector<dbxElectron> electrons, vector<dbxPhoton> gams, vector<dbxJet> jets, vector<dbxLJet> ljets, vector<dbxTruth> truth, TVector2 met, evt_data anevt );
    int makeAnalysis(const vector<dbxMuon>& muons,const vector<dbxElectron>& electrons, const vector<dbxPhoton>& gams,
                     const vector<dbxJet>& jets, const vector<dbxLJet>& ljets, const vector<dbxTruth>& truth, const TVector2& met,  evt_data anevt ,vector<vector<int> > *mc_child_index, vector<vector<int> > *mc_parent_index,
                     vector<int> *mc_pdgId, vector<float> *mc_eta, vector<float> *mc_phi, vector<int> *mc_status,
                     vector<float> *mc_pt, vector<float> *mc_E, vector<float> *mc_m, vector<float> *mc_charge);
      int saveHistos() {
        int r = dbxA::saveHistos();
//      LHCOfile.close();
        return r;
      }
   private:
        bool grl_cut;
        bool QCD;
        double qcd_weight;

        TH1F *a_histos[99];
         int  b_histos[9];
        vector < vector < pair<int, std::vector<dbxCut*>  > > > histos_order;

       
        int icount;
        ofstream LHCOfile;
                
        TH1F *HiggsMass[3],*HiggsPT[3],*HiggsETA[3],*HiggsPHI[3],*ZMass[3],*ZPT[3],*ZPTcon[3],*ZETA[3],*ZETAcon[3],*ZPHI[3],*ZPHIcon[3],*DeZMass[3],*DeZPT[3],*DeZETA[3],*DeZPHI[3],*DeHMass[3],*DeHPT[3],*DeHETA[3],*DeHPHI[3],*DeAMass[3],*EventChi[3],*EventChiBF[3],*HiggsChi[3],*DeChi[3],*NHFlavorHist[3],*HFlavorHist[3],*METE[3];
        TH1F *hphij[3], *hphij_err[3], *hphiv[3], *hphiv_err[3], *hjpt_lead[3],*hjpt_leadBF[3], *hjpt_sublead[3],*hjpt_subleadBF[3], *hjpt_lead_err[3], *hjpt_sublead_err[3];
        TH1F *hjmultip[3], *hjmultip_err[3], *hmultJ[3], *hmultJ_err[3], *hLeadMu[3], *hLeadEl[3], *BTaggedJets[3], *HTJets[3];
        TH2F *TightvsWeight;
        TH1F *mcweight, *pileupweight, *leptonSFweight, *bTagSFweight, *jvtweight, *sherpa22vjetsweight;
        TH1F *testMuons, *qweight, *HiggsRDist[3], *ZRDist[3], *hBtagMultJ_higgs[3] ;
        TH1F *HTut[3], *HiggsLeadJetMass[3],*HiggsSubJetMass[3],*HiggsAllJetMass[3] ;
        TH1F *hPileUp[3], *hPileUpErr[3], *hZVtx[3], *hZVtxErr[3], *hMcEv[3], *hMcEvErr[3], *hFourJet[3], *hFourJetErr[3], *hLepReco, *hLepRecoErr,
        *hLepId, *hLepIdErr, *hLepIso	, *hLepIsoErr, *hLepTrig, *hLepTrigErr ;
    	TH2F *hFourJetVSPT[3], *METEvsZpt[3],*METEvsHm[3];
        double maxz0e, maxz0m;
        int is_this_zjetsCR;
        // E6 Specific Parameters
        double maxDeltaZmassEl, maxDeltaZmassMu, BFlavor;
        double DHerrorM, DZerrorM, HerrorM, DHerrorE, DZerrorE, HerrorE, MaximumChi, MinRDist;
        double MinPJetPtM, MinPJetPtE, MinSubPJetPtM, MinSubPJetPtE;

//DBXcut relevant variables
        std::vector< std::vector<dbxCut*>     > mycutlist;
        std::vector< std::vector<std::string> > myopelist;
        dbxCutList                              e62cutlist;
    
        std::vector< pair< int , int >        > forbidthese;
        std::vector<int> ret_i;
        std::vector<int> ret_t;
        vector <int>  found_type_vecs, found_idx_vecs, found_idx_origs;

};

#endif // E6_A2_H
