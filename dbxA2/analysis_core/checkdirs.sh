#!/bin/bash

alldirs=`ls | grep user`

for i in $alldirs
do
rootfiles=`ls $i | grep root`
if [ -z "$rootfiles" ] 
then
echo $i "does not contain any root files. You may have corrupted results"
fi
done
