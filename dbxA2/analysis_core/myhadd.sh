#!/bin/bash

if [ $# -lt 1 ] ; then
 echo $0 output_histo_name [prefix for files]
 echo Examples:
 echo \  $0 ttbar
 echo '    '==\> Every root\* file in the current dir merged into ttbar.root
 echo \  $0 ttbar \'user.\*.analysisD132D\*/\'
 echo '    '==\> Everything in all the user.\*.analysisD132D\* directories added
 exit 
fi
 
nbreak=200

fl=`ls $2*.root*`

ns=0
nf=0
declare -a fls[100]

for f in $fl; do
 if [ $f -ef $1.root ] ; then  # if this is the same as we want to create
     continue   # skip it, thus protecting against adding ourselves in.
 fi
 fls[$ns]+=" "$f
 nf=$(( $nf  + 1 ))
 if [ $nf == $nbreak ] ; then
   ns=$(( $ns  + 1 ))
   nf=0
 fi
done

if [ $nf -eq 0 ] ; then
 echo No files found, sorry!
 exit
fi

echo will hadd $nf files in `expr $ns + 1` batch\(es\)
if [ $ns -ge 990 ] ; then
 echo OHA. Really too many files.
 exit
fi


rm -f __*.root # just in case we have some files lying around from a previous run

for ((i=0; i<=$ns; i++)) {
  hadd __$i.root  ${fls[$i]}
}

hadd $1.root __*.root
rm -f __*.root
