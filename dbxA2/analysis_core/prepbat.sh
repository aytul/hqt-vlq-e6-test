#!/bin/bash


abc=`root-config --cflags `

ret=`echo $abc | grep m64`

if [ ${#ret} -gt 0 ]; then
  ver=64
else
  ver=32
fi

echo $ver

./configure CXXFLAGS=-m${ver} CFLAGS=-m${ver} --prefix=`pwd`/installdir
make clean
make
make install

