#!/usr/bin/env python


#This scrpt scans all source files in a folder and creates alist of branchces used by those source codes. 
#The list is created by comparing the branchnames in a main headr file (most of the time it is the one crated by ROOT's MakeClass() utility )
#Extracted branch list can be used to disable irrelevant ones in order to increase performance in considerable amounts
#You can also use this script to list source files under a directory . (Subdirectories are considered too)
#You can change file extension definitions below in your favor anytime. (c,cc,cxx,hpp,h,C,CC,c++)
# Of course , use this script on your own risk ...

# author : Serhat Istin
#	  istin@cern.ch

import optparse
import string
import os

#Handle&parse commandline arguments
myoptparser=optparse.OptionParser()
myoptparser.add_option('-f','--headerFile',help="(optional) Supply a header file",dest="f",metavar='FILE')
myoptparser.add_option('-s','--sourceDir',help="(MANDATORY) Supply a directory: Scan all files recursively under this dir. (Recursive walk)",dest="s",metavar='FILE')
myoptparser.add_option('-o','--outFile',help="(OPTIONAL) Supply an outputfile where the function  is to be written. This option overrides the default name \"TuneBranches.h\"",dest='o',metavar='FILE')
myoptparser.add_option('-l','--listFiles',help="(optional) List All source files found in the dir and exit",dest='l',metavar='flag',action='store_true')
myoptparser.add_option('-x','--exclude',help="(optional) Give a list of strings separated by commas. If  filename or filepath includes one of these it will be ignored",dest='x',metavar='string')

(options, args)=myoptparser.parse_args()

if not (options.s or options.l):
	myoptparser.print_help()
	os.sys.exit(-1)

if not (options.s or options.f):
	myoptparser.print_help()
	os.sys.exit(-1)
ofilename=""
excludedList=[]
if options.x:
	excludedList=options.x.rsplit(",")

if options.o:
	ofilename=options.o
else:
	ofilename="TuneBranches.h"

filestamp= "#ifndef "+ofilename.rsplit(".")[0].upper()+"_H"+"\n"+"#define "+ofilename.rsplit(".")[0].upper()+"_H"+"\n"+"#include \"TTree.h\""

#check if user supplied a proper filename
if(os.path.isfile(ofilename)):
	print "File "+ofilename+" already exists. It will be overridden"

if ofilename.rfind(".")!=-1 and ofilename.rsplit(".")[1]=="h":
	print "Filename is good. continue..."
else:
	print "ERROR*** Filename "+ofilename+" is not type of a C header..."
	os.sys.exit(-1)
#----------------------------------------------------------------------------------------------------

#-----------------------------------------------
print "RUNNING..."
#get a list of all file in the directory&get alist of all branches present in the header
#-----------------------------------------------
#carry out a recursive walk through the sourceDir and pickup all files thru it
sourceFileList=[]
rootdir=options.s
for root, subFolders, files in os.walk(rootdir):
  for afile in files:
    print afile
    if afile.rfind(".")!=-1:
	fileExtension=afile.rsplit(".")[len(afile.rsplit("."))-1]
	#print "ext: "+fileExtension
	if not (fileExtension=="C" or fileExtension=="c" or fileExtension=="h" or fileExtension=="cpp" or fileExtension=="cxx" or fileExtension=="hpp" or fileExtension=="cc"):
	  #print "File is not type of a source code. Skipping :"+afile
	  continue
        sourceFileList.append(os.path.join(root,afile))

if options.l:
	for fname in sourceFileList:
		print fname
	os.sys.exit(0)

print "Found "+str(len(sourceFileList))+" source files in the dir: "+options.s
print "Removing multiple occurences in SourceFileList ( in case of present symlinks around)"
sourceFileList=set(sourceFileList)
print str(len(sourceFileList))+" source Files left after removing multiple occurences"
branchset=[]
brsinsource=[]
headerFile=open(options.f,'r')
for LineinHeader in headerFile:
	if (LineinHeader.rfind("SetBranchAddress")!=-1):
		LineinHeader.strip("\n")
		branchname=(LineinHeader.rsplit("\"")[1].rsplit("\""))[0]
                branchset.append(branchname)
#print sourceFileList
#os.sys.exit(-9)
print "Found "+str(len(sourceFileList))+" files"

fctr=0
#headerFile.close()
skipfile=0
for sourceFile in sourceFileList:
    skipfile=0
    if sourceFile.rsplit("/")[len(sourceFile.rsplit("/"))-1]==options.f.rsplit("/")[len(options.f.rsplit("/"))-1]:
	print "WARNING*** Nonsense to scan your header against itself. So skipping it ;)"
	continue
    for excludeStr in excludedList:
        if sourceFile.rfind(excludeStr)!=-1:
            #print "File: "+sourceFile+" is set to exclude... It is not scanned"
	    skipfile=1
   #print str(skipfile)
    if skipfile!=0:
        fctr+=1
        continue
    sourceFileObj=open(sourceFile,'r')#open&scan source files
    print "Scanning File#"+str(fctr)+" "+sourceFile
    for theline in sourceFileObj:
	#print theline
	for brName in branchset:
		theline.strip("\n")
		theline.strip("\t")
		theline.strip(" ")
		if theline.rfind(brName)!=-1:
			brsinsource.append(brName)
    fctr+=1

print "Scanned "+str(len(sourceFileList))+" files in dir :"+options.s 

print "Found "+str(len(branchset))+" branches in the header: "+options.f
print "Found "+str(len(brsinsource))+" lines of code in the directory where branches are used"

print "Removing multiple occurences "


brsinsource=set(brsinsource)
print str(len(brsinsource))+" branches are active..."    
#be generous now
if len(brsinsource)<50:
    print brsinsource


functionName=ofilename.rsplit(".")[0]
ofile=open(ofilename,'w')
ofile.write(filestamp+"\n")
ofile.write(" void "+functionName+"(TTree* fChain){"+"\n")
ofile.write("fChain->SetBranchStatus(\"*\",0);"+"\n")
for br in brsinsource:
	ofile.write("fChain->SetBranchStatus(\""+br+"\",1);"+"\n")
ofile.write("}"+"\n")
ofile.write("#endif")

print "Header File: "+ofilename+"  created."

#end of line

