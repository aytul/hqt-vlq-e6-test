#include "../analysis_core/compare.H"

#ifdef __ROOT6__
R__LOAD_LIBRARY(../analysis_core/ReadCardCINT.C+);
R__LOAD_LIBRARY(../analysis_core/dbxParticle.cpp+);
R__LOAD_LIBRARY(../analysis_core/TStringAddFloat.cpp+);
#else
gROOT->LoadMacro("../analysis_core/ReadCardCINT.C+");
gROOT->LoadMacro("../analysis_core/dbxParticle.cpp+");
gROOT->LoadMacro("../analysis_core/TStringAddFloat.cpp+");
#endif


float nxs(TString ps,       TString sample) { return ReadCard( TString(ps+ TString::Format("%s","xsectionsEdited.txt")).Data(),sample); } 
float nxsCorree(TString ps, TString sample) { return ReadCard( TString(ps+ TString::Format("%s","xsectionCorrections.txt")).Data(),sample); } 

float xs(string sample) { float xsl=ReadCard("xsections.txt",sample.c_str(),0,0);
                          if (xsl==0) cout << "Warning! Xsection is ZERO (0)\n";
                          return xsl;
                        }
string sampleName(string modeldir, int i, string info="N") {
	string file="sample-" + modeldir;
	file += ".txt";
	string sample = Form("sampleId-%d-",i);
	sample += info;

	string astr=ReadCardString(file.c_str(),sample.c_str(),"Error",0);
        if (astr=="Error") {
           int leptonindex=(int)ReadCard("ANA_DEFS","CHANNEL",0,0);
           sample = Form("sampleId-%d-",i);
           sample += info;
           if (leptonindex == 1) {
            sample +="e";
           } else {
            sample +="m";
           }
           astr=ReadCardString(file.c_str(),sample.c_str(),"Error",0);
        }
        return astr;
}
string typeName(string modeldir, int i, string info="N") {
	string file="sample-" + modeldir;
	file += ".txt";
	string sample = Form("typeId-%d-",i);
	sample += info;

	return	ReadCardString(file.c_str(),sample.c_str(),"Error",0);
}
int typeValue(string modeldir, int i, string info="Rmin") {
	string file="sample-" + modeldir;
	file += ".txt";
	string sample = Form("typeId-%d-",i);
	sample += info;
	return	ReadCard(file.c_str(),sample.c_str(),0,0);
}

void prepareSamples(int qm=350, float lumi=690, float skf=1.0, Sample **samples=NULL, int nsamples=1,  SampleType **type=NULL, int ntypes=1 )
{
	if ( gROOT->GetListOfGlobals()->FindObject("atlasStyle") == 0 ) {
		gROOT->ProcessLine(".x ../analysis_core/rootlogon.C");
		gROOT->SetStyle("ATLAS"); gROOT->ForceStyle();
	}
	string modeldir= ReadCardString("ANA_DEFS","MODEL","FF_1",0);
	string MCmodeldir= ReadCardString("ANA_DEFS","DIRECTORY",modeldir.c_str(),0);

	// ECM only 7,8 or 14 is defined
	int cme=(int)ReadCard("ANA_DEFS","ECM",7,0);
	string ecm= "xxx"; string secm="xxx";
	switch (cme) {
	case 7 : ecm= "bg7";   secm="si7_";  break ;
	case 8 : ecm= "bg8";   secm="si8_";  break ;
	case 13 : ecm= "bg13"; secm="si13_"; break ;
	case 14 : ecm= "bg14"; secm="si14_"; break ;
	default : cout << "UNKNOWN Center of Mass Energy "<<endl; exit (-1);
	}

	// the quark mass selection, with the fit ranges
	TString sigtype=secm;
	switch (qm) {
	case 600 :
	case 800 :
	case 900 :
	case 1000 : sigtype+=qm; break;
        case 1200 : sigtype+=qm; break;
	case 1400 : sigtype+=qm; break;
        case 1600 : sigtype+=qm; break;
	default  : cout << "qm must be a positive integer ="<< qm<<" \n"; exit (-1);
	}

	string file="sample-" + modeldir;
	file += ".txt";

	string sffx = ".root";     // file suffix for all files
	string pffx ; 

	string datafilename;
	string qcdfilename ;
	int leptonindex=(int)ReadCard("ANA_DEFS","CHANNEL",0,0);
	if (leptonindex==0) { datafilename=ReadCardString("ANA_DEFS","DATAF_m","deneme",0);
                              pffx=ReadCardString("ANA_DEFS", "MCD_m", "9", 0);
        }
	if (leptonindex==1) { datafilename=ReadCardString("ANA_DEFS","DATAF_e","deneme",0);
                              pffx=ReadCardString("ANA_DEFS", "MCD_e", "8", 0); 
        }
        pffx+="/";
	qcdfilename+=datafilename;
	qcdfilename+="_unc.root" ;

        if (datafilename.find("data") != std::string::npos ) {
	  datafilename+=".root"    ;
	  samples[0] = new Sample( datafilename.c_str(),    modeldir,  kBlack, -1, 0 ); //data -1: no SF and 0: no smooth
        } else {
           std::string subdelimiter = "/";
           size_t apos=datafilename.find(subdelimiter); 
           std::string subtoken1;
                       subtoken1 = datafilename.substr(apos+subdelimiter.length(),std::string::npos); //
          cout << "You are injecting :"<<datafilename<<"   XS from:"<<subtoken1<<endl;
          float injcoef=lumi*xs(subtoken1);
	  datafilename+=".root"    ;
	  samples[0] = new Sample( datafilename.c_str(),    modeldir,  kBlack, injcoef, 0 ); //data
        }
	samples[1] = new Sample( qcdfilename.c_str(),     modeldir,  kBlack, -1 );//QCD and uncertainty
	cout<<"Lumi: "<<lumi<< " nsamples: "<<nsamples<<endl;

//example
//    samples[ 28] = new Sample( pffx+"410501"+sffx,"E62_1", kRed  ,lumi/xsCorree("410501")*xs("410501") ); //ttbar

	for(int loop=2; loop<nsamples-2;loop++)
	{
         string sampleRunNo=sampleName(modeldir,loop,"F");
	 samples[ loop] = new Sample( pffx+sampleRunNo+sffx, MCmodeldir, kBlack, lumi*(nxs(pffx, sampleRunNo) / nxsCorree(pffx, sampleRunNo) )  ); //renorm
	 cout << loop << " "<< sampleRunNo << " "<< lumi*(nxs(pffx, sampleRunNo) / nxsCorree(pffx, sampleRunNo) ) <<" ok\n";
	}

	samples[nsamples-2] = new Sample( pffx+sigtype.Data() +sffx,  MCmodeldir, kRed , skf*lumi*xs(sigtype.Data()) ); //signal MC
        cout << "Signal MC is from :" << pffx+sigtype.Data() +sffx<<endl;
  	samples[nsamples-1] = new Sample( pffx+ sigtype.Data()+sffx, MCmodeldir, kRed , skf*lumi*xs(sigtype.Data()) ); //the SAME signal MC to inject in injection tests

// ALL SAMPLES ARE NOW READ.
// The functionality to make groups of samples.

	 cout<<"Starting BG defs"<<endl;
	 string CARDNAME = "sample-"+modeldir+".txt";
for (int typeL=0; typeL<ntypes ; typeL++)
	{
            string atype=typeName(modeldir,typeL,"N");
	    type[typeL] = new SampleType(atype);
            type[typeL]->SetDrawable( typeValue(modeldir,typeL,"Drawable")  ); // draw or not
            type[typeL]->SetForcedStyle(  typeName(modeldir,typeL,"Style")  ); // style

	    int Rmin = 0, Rmax=0;
            if ((typeL > 0) && (atype != "QCD") && (atype!="Uncert"))  { // data and QCD are not user modifiable
	     Rmin = typeValue(modeldir,typeL,"Rmin");
	     Rmax = typeValue(modeldir,typeL,"Rmax");	    
	    } else if (atype == "QCD" || atype == "Uncert") {Rmin=1; Rmax=1;}

	    if ( Rmin < 0 ) Rmin = Rmin + nsamples;
	    if ( Rmax < 0 ) Rmax = Rmax + nsamples;
	    cout<<atype <<" ID: "<<typeL<<" has samples:"<<Rmin<<" - "<<Rmax<<endl;
	    if( Rmin == Rmax ) 	    type[typeL]->Add(Rmin);
	    else type[typeL]->AddRange(Rmin,Rmax);
	    if( atype =="signal") {TString myl="signal x"; myl+=skf; type[typeL]->SetLeg(myl.Data()); }

	}
cout<<"End BG defs. nTYPES:"<<ntypes <<endl;
	
}
