#include <Math/Interpolator.h>

#ifndef __CROSSINGFINDER_H__
#define __CROSSINGFINDER_H__

class crossingFinder {

 public:
   crossingFinder(int n1, double* x1, double* y1, int n2, double* x2, double* y2 );
   crossingFinder(int n1, float* x1, float* y1, int n2, float* x2, float* y2 );
   ~crossingFinder();

   int findCrossing(bool verbose=false);
   double getCrossingX() {return p_xcross; };
   double getCrossingY() {return p_ycross; };

 private:
   int p_n1, p_n2;
   double *p_x1, *p_x2;
   double *p_y1, *p_y2;

   double p_xcross, p_ycross;
   ROOT::Math::Interpolator *p_inter1, *p_inter2;
};


#endif

