#!/bin/bash

logpath=$HOME/work/public/4grid/submit_logs

if [ $# -lt 3 ] ; then
 echo not enough arguments
 echo "$0 -[command] [run number] [analysis number]"
 kl=`cat $0 | grep ')'|grep -v '(' | grep -v '*' | cut -f1 -d')'`
 echo "     where [command] is one of these: " $kl
 exit 1
fi

rn=$2
ov=$3
verbose=1

if [ $# -ge 4 ] ; then
 verbose=$4
fi

#expn=`cat $HOME/public/4grid/submit_logs/${rn}_submit_ov${ov}.log| grep 'INFO : use' | awk ' BEGIN { s=0 } {s=s+$4} END { print s}' `
expn=`sed -n '/Running/{:1;p;n;/Running/{p;q};b1};p'  ${logpath}/${rn}_submit_ov${ov}.log| grep 'INFO : use' | awk ' BEGIN { s=0 } {s=s+$4} END { print s}' `
if [ $verbose -eq 1 ] ; then
 echo "  Expecting " $expn " jobs."
fi

 jid=`cat ${logpath}/${rn}_submit_ov${ov}.log| grep JobsetID| awk '{print $3}' | tail -n1`
# echo $jid;

control=`echo "$jid" | egrep "^[0-9]+$"`

if [ "$control" ] 
then
	while getopts :k:r:d:u:s:f: option
	do
    	case "$option" in
    	k)
		echo -n "kill command received ";
         	pbook -c kill\($jid\)
	 	;;
    	r)
		echo -n "retry command received. This command is not used anymore."
         	;;
    	s)
                if [ $verbose -eq 1 ] ; then
         	 echo -n "show command received for JID " $jid " "
                fi
		pbook -c show\($jid\) &> /tmp/$USER/sil
                cat /tmp/$USER/sil | grep -A3 -i 'status :'
		;;
    	f)
        	echo -n "show & retry command received "
		pbook -c "show($jid)" &> /tmp/$USER/sil
                cat /tmp/$USER/sil | grep -A3 status
                failnb=`cat /tmp/$USER/sil | grep 'failed :'|awk '{print $3}'` 
                cancelnb=`cat /tmp/$USER/sil | grep 'cancelled :'|awk '{print $3}'` 
                aa=`expr length "$failnb"`
                bb=`expr length "$cancelnb"`
                if [ $aa -gt 0 ] || [ $bb -gt 0 ] ; then
        	 echo Failed:${failnb}, Cancelled:${cancelnb} resubmitting
		 pbook -c "retry($jid, retryBuild=True )" &> /tmp/${USER}/sil
                 cp /tmp/${USER}/sil /tmp/${USER}/sil_a
                 njsid=`cat /tmp/${USER}/sil | grep 'Done. New' | cut -f3 -d'='`
                 if [ ${#njsid} -gt 1 ]; then
                    echo Submitted. Patching up the log files from new submission
                    cat /tmp/${USER}/sil >> ${logpath}/${rn}_submit_ov${ov}.log
                    echo "JobsetID : " ${njsid}  >>  ${logpath}/${rn}_submit_ov${ov}.log
                 else
                  njsid=`cat /tmp/${USER}/sil | grep 'already '| cut -f3 -d'=' | cut -f1 -d' '`
                  if [ ${#njsid} -gt 1 ]; then
                    echo was already submitted. Patching up the log files from old submission.
                    echo "JobsetID : " ${njsid}  >>  ${logpath}/${rn}_submit_ov${ov}.log
                  fi
                 fi
                 if [ ${#njsid} -eq 0 ]; then
                  echo can not resubmit.
                  cat /tmp/${USER}/sil
                 fi
                fi
                echo
		;;

    	d)
		;;
    	u)      pbook -c sync\(\)
                echo updating local repository
		;;
    	*)
        	echo "WRONG option was received!! Acceptable options -k,-s, -u and -r and require some arguments."
        echo "Here's the usage statement:"
        echo "checkjobs.sh -[option] [dataset] [analysis number]"
        ;;
        esac
done
else echo "Submit log unformated! Probably you had an error while submitting"
tail -n1 ${logpath}/${rn}_submit_ov${ov}.log
fi
exit 
