#!/bin/bash
export LD_LIBRARY_PATH=$PWD:$LD_LIBRARY_PATH
fn=`cat input.txt`
extra=" "
analizler=$1
runmodif=$2


# when working with data
if [ $runmodif -eq 1 ] ; then
 extra=${extra}" -Q 1 "
fi

# when working with Wjets
if [ $(($runmodif & 2)) -eq 2 ] ; then
 extra=${extra}" -HF 1 "
fi

# when working with systematics
if [ $(($runmodif & 4)) -eq 4 ] ; then
 extra=${extra}" -S 1 "
fi

# when working with 2012 files
if [ $(($runmodif & 8)) -eq 8 ] ; then
 extra=${extra}" -year 2012 "
fi

echo ./root_analysisd3pd $fn $extra $analizler
./root_analysisd3pd $fn $extra $analizler
retv=$?
hadd -f analysisd3pd.root histoOut-*.root
exit $retv
