#ifndef DBX_NT_H
#define DBX_NT_H

#include <vector>
#include <cmath>
#include "TObject.h"
#include "TLorentzVector.h"
#include "TVector2.h"
#include "TFile.h"
#include "TNtuple.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TCanvas.h"

#ifdef VLQLIGHT
#include "VLQLight/dbx_electron.h"
#include "VLQLight/dbx_muon.h"
#include "VLQLight/dbx_jet.h"
#else
#include "../analysis_core/dbx_electron.h"
#include "../analysis_core/dbx_muon.h"
#include "../analysis_core/dbx_jet.h"
#include "../analysis_core/dbx_ljet.h"
#include "../analysis_core/dbx_truth.h"
#endif


using namespace std;

class DBXNtuple: public TObject {
    public:
        DBXNtuple() ;
       ~DBXNtuple() ;

        int nEle;
        int nMuo;
        int nJet;
        int nLJet;
        int nTruth;
        void  Clean( );

        vector<dbxElectron> nt_eles;
        vector<dbxMuon>     nt_muos;
        vector<dbxJet>      nt_jets;
        vector<dbxLJet>     nt_ljets;
        vector<dbxTruth>    nt_truth;
        vector<double>      nt_uncs;
        vector<TVector2>    nt_sys_met;
        TVector2            nt_met;
        evt_data            nt_evt;
// http://root.cern.ch/root/Using.html

        ClassDef(DBXNtuple,4);
};

#endif
