#include<iostream>
#include<bitset>
#include<algorithm>

#ifdef  VLQLIGHT
#include"VLQLight/punch.h"
#else
#include"punch.h"
#endif

// improved atof that can also handle binary strings ( 0b0110 = 6 )
double satof( std::string str ) {
  std::string tmp = str;
  //tmp.erase(remove_if(tmp.begin(), tmp.end(), ::isspace), tmp.end());
  // we do the next two lines instead of the above line thanks to ROOT CINT!
  std::string::size_type pos = std::string::npos;
  while ((pos=tmp.find_first_of(" \t"))!=std::string::npos) tmp.erase(pos,1);
  if ( tmp[0] == '0' && tmp[1] == 'b' &&  // if binary, handle differently
       str.find_last_of('b') == 1 ) {
    return (std::bitset<64>(tmp.substr(2))).to_ulong(); }
  return ::atof(str.c_str());
}


// A semi-smart generic variable, handling strings and numerics roughly
//   on the same footing.
punch::operator double() const { return val; } // typecasting to double
punch::operator std::string() const { return str; } // casting to string

// the "constructors" below are really for typecasting
//   if the const char* ctor is absent, int ctor can also be dropped
//   we will also need one for long ints, again thanks to char* ctor
punch::punch(unsigned long v) : val(v) { std::stringstream s; s<<v; str=s.str(); }
punch::punch(int v)    : val(v) { std::stringstream s; s<<v; str=s.str(); }
punch::punch(double v) : val(v) { std::stringstream s; s<<v; str=s.str(); }
punch::punch(std::string s) : str(s) { val = satof(s); }  // ::atof(s.c_str())
punch::punch(const char* s) : str(s) { val = satof(s); }  // val = ::atof(s)

std::ostream& operator<<(std::ostream& os, const punch& p) {
    return (os << "\"" << p.str << "\":" << p.val); }

void testPunch() {
  using std::cout;
  using std::endl;
  punch x = 56.1, y = std::string("\t 0x45A"), z = 0, t("0b 0001 1000");
  cout << "x = " << x << "\t" << "y = " << y << endl;
  cout << "z = " << z << "\t" << "t = " << t << endl;
  cout << "x as an integer = " << (int)x << endl;
  cout << "x as a string = " << (std::string)x << endl;
}
