
#ifdef VLQLIGHT
#include "VLQLight/LeptonicWReconstructor.h"
#else
#include "LeptonicWReconstructor.h"
#endif

#include <iostream>

LeptonicWReconstructor::
LeptonicWReconstructor(double El_px, double El_py,
		       double El_pz, double Nu_px, double Nu_py) :
  m_El_px(El_px), m_El_py(El_py), m_El_pz(El_pz),
  m_Nu_px(Nu_px), m_Nu_py(Nu_py), m_mW(80.42e3) {}

LeptonicWReconstructor::
LeptonicWReconstructor(TLorentzVector El_P, TVector2 Nu_Pt) :
  m_mW(80.42e3) {
  m_El_px = El_P.Px();
  m_El_py = El_P.Py();
  m_El_pz = El_P.Pz();
  m_Nu_px = Nu_Pt.Px();
  m_Nu_py = Nu_Pt.Py(); }

LeptonicWReconstructor::
~LeptonicWReconstructor() {}

void LeptonicWReconstructor::
setWmass(double Wm) { 
  if ( Wm <= 0 ) std::cerr << "Invalid W mass entered!" << std::endl;
  else m_mW = Wm; }

std::vector<double> LeptonicWReconstructor::
neutrino_pz() {
  std::vector<double> out;

  double el_pt2 = m_El_px * m_El_px + m_El_py * m_El_py;  // lepton Pt squared
  double nu_pt2 = m_Nu_px * m_Nu_px + m_Nu_py * m_Nu_py;  // missing Pt squared
  double kappa  = m_mW * m_mW + 2 * ( m_El_px * m_Nu_px + m_El_py * m_Nu_py );

  double a =  4 * el_pt2;
  double b = -4 * kappa * m_El_pz;
  double c = 4 * ( el_pt2 + m_El_pz * m_El_pz ) * nu_pt2 - kappa * kappa;

  if ( (b*b >= 4*a*c) && (a!=0) ) {
    out.push_back( ( -b - sqrt(b*b-4*a*c) ) / (2*a) ); 
    out.push_back( ( -b + sqrt(b*b-4*a*c) ) / (2*a) );
  }

  return out;
}

double LeptonicWReconstructor::
getWpt() {
  double px = m_El_px + m_Nu_px;
  double py = m_El_py + m_Nu_py;
  return sqrt( px*px + py*py );
}

double LeptonicWReconstructor::
getPhiOpeningAngle() {
  TVector2 tmp1( m_El_px, m_El_py );
  TVector2 tmp2( m_Nu_px, m_Nu_py );
  return tmp1.DeltaPhi( tmp2 );
}

double LeptonicWReconstructor::
getVisibleMass(double leptonMass) {
  TLorentzVector tmp;
  tmp.SetXYZT( m_El_px + m_Nu_px, m_El_py + m_Nu_py, m_El_pz,
	       sqrt( m_El_px*m_El_px + m_El_py*m_El_py +
		     m_El_pz*m_El_pz + leptonMass*leptonMass )
	       + sqrt( m_Nu_px*m_Nu_px + m_Nu_py*m_Nu_py ) );
  return tmp.M();
}

double LeptonicWReconstructor::
getTransverseMass(double leptonMass) {
  TLorentzVector tmp;
  tmp.SetXYZT( m_El_px + m_Nu_px, m_El_py + m_Nu_py, 0,
	       sqrt( m_El_px*m_El_px + m_El_py*m_El_py + leptonMass*leptonMass )
	       + sqrt( m_Nu_px*m_Nu_px + m_Nu_py*m_Nu_py ) );
  return tmp.M();
}

double LeptonicWReconstructor::
getWeta(int solutionId) {
  std::vector<double> soln = this->neutrino_pz();
  if ( soln.empty() ) return -99.9;
  double pt = this->getWpt();
  double pz = m_El_pz + soln[solutionId];
  double eta = log ( std::fabs ( tan ( 0.5 * atan2(pt,pz) ) ) );
  if ( eta * pz < 0 ) eta *= -1; // eta and pz should have the same sign
  return eta;
}

TLorentzVector LeptonicWReconstructor::
getWp4(int solutionId) {
  std::vector<double> soln = this->neutrino_pz();
  if ( ! soln.empty() ) {
	double px = m_El_px + m_Nu_px;
	double py = m_El_py + m_Nu_py;
	double pz = m_El_pz + soln[solutionId]; 
    return TLorentzVector(px, py, pz,
        sqrt( m_mW*m_mW + px*px + py*py + pz*pz ));
  }
  return TLorentzVector(-1.0, -1.0, -1.0, -1.0);
}
