#ifndef __READCARD_H
#define __READCARD_H
#include<string>

#ifdef VLQLIGHT
#include"VLQLight/punch.h"
#else
#include "punch.h"
#endif

punch ReadCard(const char *filename, const char *varname, const punch defval = 1., const int verbose = 0) ;

std::string ReadCardString(const char *filename, const char *varname, const char *defval = "xxx", const int verbose = 0) ;

#endif

