#ifndef DBX_A_H
#define DBX_A_H

#include <vector>
#include <cmath>
#include "TLorentzVector.h"
#include "TFile.h"
#include "TNtuple.h"
#include "TH1F.h"
#include "TH2F.h"
#include <iostream>
#include "TCanvas.h"

#ifdef VLQLIGHT
#include "VLQLight/dbx_electron.h"
#include "VLQLight/dbx_photon.h"
#include "VLQLight/dbx_muon.h"
#include "VLQLight/dbx_jet.h"
#include "VLQLight/dbxCut.h"
#else
#include "dbx_electron.h"
#include "dbx_photon.h"
#include "dbx_muon.h"
#include "dbx_jet.h"
#include "dbx_ljet.h"
#include "dbx_truth.h"
#include "dbxCut.h"
#endif

using namespace std;

#define NPERIOD 11


class dbxA {
    
public:
    dbxA( char*);
    ~dbxA();
    //----------------this is the real function----------
    virtual int makeAnalysis(vector<dbxMuon>,   vector<dbxElectron>,    vector<dbxPhoton>,  vector<dbxJet>,  vector<dbxLJet>,  vector<dbxTruth>, TVector2, evt_data, int); // to be implemented in the mother class
    
    virtual int makeAnalysis(vector<dbxMuon>ms, vector<dbxElectron>es, vector<dbxPhoton>fs, vector<dbxJet>js, vector<dbxLJet> ljs,  TVector2 mt, evt_data et, int lv){
        vector<dbxTruth>tru;
        return makeAnalysis(ms, es, fs, js, ljs,tru, mt, et, lv);
    }
    
    virtual int makeAnalysis(vector<dbxMuon>ms, vector<dbxElectron>es, vector<dbxJet>js, vector<dbxLJet>ljs, vector<dbxTruth>tru,  TVector2 mt, evt_data et, int lv){
        vector<dbxPhoton>fs;
        return makeAnalysis(ms, es, fs, js, ljs,tru, mt, et, lv);
    }

    
    virtual int makeAnalysis(vector<dbxMuon>ms, vector<dbxElectron>es,     			      vector<dbxJet>js, vector<dbxLJet> ljs,  TVector2 mt, evt_data et, int lv){
        vector<dbxPhoton>fs;
        vector<dbxTruth>tru;
        return makeAnalysis(ms, es, fs, js, ljs,tru, mt, et, lv);
    }
    
    virtual int makeAnalysis(vector<dbxMuon>ms, vector<dbxElectron>es,    vector<dbxPhoton> fs,    vector<dbxJet>js,  TVector2 mt, evt_data et, int lv){
        vector<dbxLJet>ljs;
        vector<dbxTruth>tru;
        return makeAnalysis(ms, es, fs, js, ljs,tru, mt, et, lv);
    }
    virtual int makeAnalysis(vector<dbxMuon>ms, vector<dbxElectron> es,                       vector<dbxJet> js, TVector2 mt, evt_data et, int lv){
        vector<dbxPhoton>f;
        vector<dbxLJet>ljs;
        vector<dbxTruth>tru;
        return makeAnalysis(ms, es, f, js, ljs,tru, mt, et, lv); }
    
    
    virtual int makeAnalysis(vector<dbxMuon>ms, vector<dbxElectron> es, vector<dbxPhoton> f,  vector<dbxJet> js, vector<dbxLJet> ljs, vector<dbxTruth> tru, TVector2 mt, evt_data et){
        return makeAnalysis(ms, es, f, js, ljs, tru, mt, et, 0); }
    
    virtual int makeAnalysis(vector<dbxMuon>ms, vector<dbxElectron> es,                       vector<dbxJet> js, vector<dbxLJet> ljs, vector<dbxTruth> tru, TVector2 mt, evt_data et){
        vector<dbxPhoton>f;
        return makeAnalysis(ms, es, f, js, ljs, tru, mt, et, 0); }

    virtual int makeAnalysis(vector<dbxMuon>ms, vector<dbxElectron> es,  vector<dbxPhoton>f, vector<dbxJet> js, vector<dbxLJet>ljs, TVector2 mt, evt_data et)   {
        vector<dbxTruth>tru;
        return makeAnalysis(ms, es, f, js, ljs, tru, mt, et,0); }

    virtual int makeAnalysis(vector<dbxMuon>ms, vector<dbxElectron> es, vector<dbxPhoton> ps, vector<dbxJet> js, TVector2 mt, evt_data et) {
        vector<dbxLJet>ljs;
        vector<dbxTruth> tru;
        return makeAnalysis(ms, es, ps, js, ljs, tru, mt, et,0); }
    
    virtual int makeAnalysis(vector<dbxMuon>ms, vector<dbxElectron> es,                       vector<dbxJet> js, TVector2 mt, evt_data et)   {
        vector<dbxPhoton>f;
        vector<dbxLJet>ljs;
        vector<dbxTruth>tru;
        return makeAnalysis(ms, es, f, js, ljs, tru, mt, et,0); }
    

    virtual int makeAnalysis(vector<dbxMuon>,   vector<dbxElectron>,    vector<dbxPhoton>,    vector<dbxJet>,  vector<dbxLJet>, vector<dbxTruth> tru,  TVector2,    evt_data, map <int, TVector2>, vector <double>){std::cout<<"3RR0Rg!\n"; return 0;}
    virtual int makeAnalysis(vector<dbxMuon>,   vector<dbxElectron>,                          vector<dbxJet>,  vector<dbxLJet>, vector<dbxTruth> tru,  TVector2,    evt_data, map <int, TVector2>, vector <double>){std::cout<<"3RR0R!\n"; return 0;}
    
    virtual int plotVariables(int); // the detail level and save is controlled here
    virtual int saveHistos();
    string getName() { return cname;}
    virtual int initGRL() { return 0;}
    virtual int readAnalysisParams() { return 0;}
    virtual int bookAdditionalHistos() { return 0;}
    virtual int setQCD() { return 0;}
    virtual int printEfficiencies() { return 0;}
    virtual int setDir(char *); // to have multiple directories
    virtual int ChangeDir(char *); // to be able to cd later on
    virtual int addRunLumiInfo(int rn, int lbn); // to have list of run and lumi block numbers
    static const string perName[NPERIOD];
    static double deltaR(double eta1, double phi1, double eta2, double phi2);
    static int getPeriodIndx(unsigned int);
    static int getMCPeriodIndx(unsigned int, unsigned int);
    static double getPeriodLumi(unsigned int);
    static const double totalLumi2011() {  return 4713.11; } // lumi in pb-1.
    
    /////////////////////////////////////
    // Kaya's modification
    void initializeTTree(TTree* t, Double_t* adresler, int len_Fields, const char* branchNamePrefix, const char* fields[]);
    void initializeTTree4TLorentzVector(TTree* t, Double_t* adresler, const char* branchNamePrefix);
    void initializeTTree4dbxParticle(TTree* t, Double_t* adresler, const char* branchNamePrefix);
    void initializeTTree4dbxMuon(TTree* t, Double_t* adresler, const char* branchNamePrefix);
    void initializeTTree4dbxElectron(TTree* t, Double_t* adresler, const char* branchNamePrefix);
    void initializeTTree4dbxJet(TTree* t, Double_t* adresler, const char* branchNamePrefix);
    void initializeTTree4dbxLJet(TTree* t, Double_t* adresler, const char* branchNamePrefix);
    void initializeTTree4dbxTruth(TTree* t, Double_t* adresler, const char* branchNamePrefix);
    
    
    void fillTTree4LorentzVector(TTree* t, Double_t* adresler, TLorentzVector & vec);
    void fillTTree4dbxParticle(TTree* t, Double_t* adresler, dbxParticle & dbx_particle);
    void fillTTree4dbxMuon(TTree* t, Double_t* adresler, dbxMuon & dbx_muon);
    void fillTTree4dbxElectron(TTree* t, Double_t* adresler, dbxElectron & dbx_electron);
    void fillTTree4dbxJet(TTree* t, Double_t* adresler, dbxJet & dbx_jet);
    void fillTTree4dbxLJet(TTree* t, Double_t* adresler, dbxLJet & dbx_ljet);
    void fillTTree4dbxTruth(TTree* t, Double_t* adresler, dbxTruth & dbx_truth);
    /////////////////////////////////////
    
    void   setDataCardPrefix(string dcp) { dataCardPrefix = dcp; }
    string getDataCardPrefix() { return dataCardPrefix; }
    string getDataCardName()   { return dataCardPrefix+"-card.txt"; }
    
    char cname[128];
    TH1F *eff;
    // list the analysis parameters here.
    float minpte,minptm,minptm2011,minptm2012,maxetae,maxetam,minmetmu,minmwte,minmwtmu;  // leptons
    float minptg, maxetag, maxmet;
    float minmete,minmwtmete;  // met and mwt related
    float minmetm,minmwtmetm;  // met and mwt related
    float minptj, maxetaj;   // basic jet definitions
    float mindrjm, mindrje; // isolation related
    float minptj1, minptj2, minEj2,  minetaj2, mindrjj, minetajj;
    float mqhxmin, mqhxmax,maxdeltam;//related to reconstructed quark mass
    float maxMuPtCone, maxMuEtCone, maxElPtCone, maxElEtCone;
    float jetVtxf2011,jetVtxf2012,jetVtxf;
    
    //serhat
    float MW,MZ,SGMW,SGMZ,SGHQ,SGHQBAR,MINPTE0,MINPTE1,MINPTMO,MINPTM1,MINNJETS;
    float MINPTJ0,MINPTJ1,MINPTELES,MINWMASS,MAXWMASS,MINPTPR1CHI2,MINDRWPR1CHI2;
    float MINPTPR2CHI2,MAXZMASS,MINZMASS,MINDRZPR2CHI2,MINPTZREC,MINPTWREC,MINDELTAM;
    bool QCDRUN;
    //eof serhat
    int HFtype;
    int TRGe, TRGm;
    
    
    // mass values that could be used in the analysis
    Double_t m_mw, m_mz, m_mh;
    Double_t GEV;
    
    
private:
    // file to output histogtrams
    TFile *histoOut;
    
    // run and lumi block information
    int p_runno;
    int p_lumino;
#define m_idx 8
    
    string dataCardPrefix;
    
    // common histograms
    // these should be malloc'ed but I have no time to implement it. NGU.
    // so this thing will crap out if more than 7 jets
    TH1F *hmet[m_idx],  *hjpt[m_idx],  *hept[m_idx],  *hmpt[m_idx], *hgpt[m_idx], *hljpt[m_idx], *htruthpt[m_idx], *heeta[m_idx], *hmeta[m_idx], *hjeta[m_idx],
    *hgeta[m_idx], *hljeta[m_idx], *htrutheta[m_idx],*hephi[m_idx], *hmphi[m_idx], *hjphi[m_idx], *hmetphi[m_idx], *hjetm[m_idx], *hljetm[m_idx], *htruthetm[m_idx], *hgphi[m_idx], *hljphi[m_idx], *htruthphi[m_idx];
    
    TH1F *hlepq[m_idx];
    
    TH1F *hmet_err[m_idx],  *hjpt_err[m_idx],  *hept_err[m_idx],  *hmpt_err[m_idx],  *hgpt_err[m_idx], *hljpt_err[m_idx], *htruthpt_err[m_idx], *heeta_err[m_idx], *hmeta_err[m_idx], *hjeta_err[m_idx],             *hgeta_err[m_idx],*hljeta_err[m_idx],*htrutheta_err[m_idx], *hljetm_err[m_idx], *htruthetm_err[m_idx], *hephi_err[m_idx], *hgphi_err[m_idx], *hmphi_err[m_idx], *hjphi_err[m_idx],
    *hmetphi_err[m_idx], *hjetm_err[m_idx],  *hljphi_err[m_idx],  *htruthphi_err[m_idx];
    
    TH1F *metx[m_idx], *mety[m_idx];
    TNtuple *rntuple;
    
};

#endif
