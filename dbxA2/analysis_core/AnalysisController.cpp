#include "AnalysisController.h"
#include "ReadCard.h"
#include <string>

//#define __DEBUG__
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
AnalysisController::AnalysisController( analy_struct *iselect,  std::map <string, int> systematics) 
{
	extra_analysis_count=1;

	// ----- how many do we run in parallel ----
	if (iselect->dosystematics) {
		for( map<string,int>::iterator it=systematics.begin(); it!=systematics.end(); ++it) {
			syst_names.insert(*it);
		}
	}
	aselect=*iselect;

	cout << " #systematic included:"<<syst_names.size() <<endl;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void AnalysisController::Initialize(char *extname) {

	int analyindex=0;
        int k=0;
        int last_a=0;

	for (int j=0; j<aselect.FFcount; j++ ) 
        {
		analyindex++;
		char tmp[64], tmp2[64];
		k=dbxAnalyses.size();

		sprintf (tmp,"FF_%i",analyindex);
		cout <<k<<"th analysis (STD FF)  initialize FF with card: "<<tmp;
		dbxAnalyses.push_back( new FFdbxA(tmp) ); // FF analysis with name
		cout << endl;

		if (aselect.doQCD) {
			k++;
			sprintf (tmp2,"FF_%i_qcd",analyindex);    // same id and _qcd is essential
			cout <<k<<"th analysis (QCD) initialize FF with card: "<<tmp;
			dbxAnalyses.push_back(  new FFdbxA(tmp2) ); // FF analysis with new name
			dbxAnalyses.back()->setDataCardPrefix(tmp);  // but with old datacard
			cout << ", but with QCD enabled"<<endl;
			dbxAnalyses.back()->setQCD();        // turning on QCD is essential
			cout << endl;
		}

		if (aselect.dosystematics) { // another loop here for systematics
			char cn[64]; sprintf (cn,"%s-card.txt",tmp);
			unsigned long int iSystem  = ReadCard(cn,"systematics_bci",65535,1);
			cout << cn<<" :adding systematics bci:"<< iSystem<<"\n";
			for (map<string,int>::iterator it = syst_names.begin(); it != syst_names.end(); it++) {
				std::string s = it->first;
				std::string delimiter = "_";
				std::string token = s.substr(0, s.find(delimiter)); // token is "syst name"
				int this_syst=atoi ( token.data() );
				if (!((unsigned long)pow(2.0,this_syst-1) & iSystem)) continue;
				unsigned int nsyst = (*it).second ;
				for ( unsigned int isys=0; isys < nsyst; isys++ ) {
					k++;
					sprintf (tmp2,"FF_%i_%s_%i",analyindex,(*it).first.c_str(), isys);    // same id and _jesp is essential
					cout <<k<<"th syst (" << (*it).first.c_str() << ") analysis initialize FF with card: "<<tmp;
					dbxAnalyses.push_back( new FFdbxA(tmp2) ); // FF analysis with new name
					dbxAnalyses.back()->setDataCardPrefix(tmp);  // but with old datacard
					cout << endl;
				} // end of up/down etc syst
			} // end of number of syst
		}  // do sys
		if (aselect.doHF) { // another loop here for Heavy Flavours, Wjets only
			k++;
			sprintf (tmp2,"FF_%i_WHF1",analyindex);    // same id and _HF1 is essential
			cout <<k<<"th analysis initialize FF with card: "<<tmp;
			dbxAnalyses.push_back(new FFdbxA(tmp2)); // FF analysis with new name
			dbxAnalyses.back()->setDataCardPrefix(tmp);  // but with old datacard
			dbxAnalyses.back()->HFtype=1;
			cout << endl;

			// we need to take care of systematics for WHF1 here
			if (aselect.dosystematics) { // another loop here for systematics
				char cn[64]; sprintf (cn,"%s-card.txt",tmp);
				unsigned long iSystem  = ReadCard(cn,"systematics_bci",65535,1);
				for (map<string,int>::iterator it = syst_names.begin(); it != syst_names.end(); it++) {
					std::string s = it->first;
					std::string delimiter = "_";
					std::string token = s.substr(0, s.find(delimiter)); // token is "syst name"
					int this_syst=atoi ( token.data() );
					if (!((unsigned long)pow(2.0,this_syst-1) & iSystem)) continue;
					unsigned int nsyst = (*it).second ;
					for ( unsigned int isys=0; isys < nsyst; isys++ ) {
						k++;
						sprintf (tmp2,"FF_%i_WHF1_%s_%i",analyindex,(*it).first.c_str(), isys);    // same id and _jesp is essential
						cout <<k<<"th syst (" << (*it).first.c_str() << ") analysis initialize FF with card: "<<tmp;
						dbxAnalyses.push_back( new FFdbxA(tmp2) ); // FF analysis with new name
						dbxAnalyses.back()->setDataCardPrefix(tmp);  // but with old datacard
						cout << endl;
					}  // end of up/down syst
				} // end of number of syst
			}  // do sys
			k++;
			sprintf (tmp2,"FF_%i_WHF2",analyindex);    // same id and _HF2 is essential
			cout <<k<<"th analysis initialize FF with card: "<<tmp;
			dbxAnalyses.push_back( new FFdbxA(tmp2) ); // FF analysis with new name
			dbxAnalyses.back()->setDataCardPrefix(tmp);  // but with old datacard
			dbxAnalyses.back()->HFtype=2;
			cout << endl;
			// we need to take care of systematics for WHF2 here
			if (aselect.dosystematics) { // another loop here for systematics
				char cn[64]; sprintf (cn,"%s-card.txt",tmp);
				unsigned long int iSystem  = ReadCard(cn,"systematics_bci",65535,1);
				for (map<string,int>::iterator it = syst_names.begin(); it != syst_names.end(); it++) {
					std::string s = it->first;
					std::string delimiter = "_";
					std::string token = s.substr(0, s.find(delimiter)); // token is "syst name"
					int this_syst=atoi ( token.data() );
					if (!((unsigned long)pow(2.0,this_syst-1) & iSystem)) continue;
					unsigned int nsyst = (*it).second ;
					for ( unsigned int isys=0; isys < nsyst; isys++ ) {
						k++;
						sprintf (tmp2,"FF_%i_WHF2_%s_%i",analyindex,(*it).first.c_str(), isys);    // same id and _jesp is essential
						cout <<k<<"th syst (" << (*it).first.c_str() << ") analysis initialize FF with card: "<<tmp;
						dbxAnalyses.push_back( new FFdbxA(tmp2) ); // FF analysis with new name
						dbxAnalyses.back()->setDataCardPrefix(tmp);  // but with old datacard
						cout << endl;
					}  // end of up/down syst
				} // end of number of syst
			}  // do sys
		}  // end of doHF
	}  // end of FF analysis setup
	cout << "End of FF initialization,  #analyses:"<<dbxAnalyses.size()<<endl;

	/*
	 *
	 *  The E6 analysis, Z channel
	 *
	 */
	analyindex=0;
	for (int i=0; i<aselect.E61count; i++) {
		analyindex++;
		char tmp[64], tmp2[36];
		k=i;

		sprintf (tmp,"E61_%i",analyindex);
		cout <<i<<"th analysis initialize E61 with card: "<<tmp<<endl;
		dbxAnalyses.push_back( new E61dbxA(tmp) ); // E61 analysis with name

		if (aselect.doQCD) {
			k++;
			sprintf (tmp2,"E61_%i_qcd",analyindex);    // same id and _qcd is essential
			cout <<k<<"th analysis initialize E61 with card: "<<tmp;
			dbxAnalyses.push_back( new E61dbxA(tmp2) ); // E61 analysis with new name
			dbxAnalyses.back()->setDataCardPrefix(tmp);  // but with old datacard
			dbxAnalyses.back()->setQCD();        // turning on QCD is essential
			cout << ", but with QCD enabled"<<endl;
		}
		if (aselect.dosystematics) { // another loop here
			for (map<string,int>::iterator it = syst_names.begin(); it != syst_names.end(); it++) {
				bool do_negsys=true;
				unsigned int nsyst = (*it).second / 2;
				if ( (*it).second == 1 ) { nsyst = 1; do_negsys=false;}
				for ( unsigned int i=0; i < nsyst; i++ ) {
					k++;
					sprintf (tmp2,"E61_%i_jesp",analyindex);    // same id and _qcd is essential
					cout <<k<<"th pos syst (" << (*it).first.c_str() << ") analysis initialize E61 with card: "<<tmp;
					dbxAnalyses.push_back( new E61dbxA(tmp2) ); // E61 analysis with new name
					dbxAnalyses.back()->setDataCardPrefix(tmp);  // but with old datacard

					k++;
					sprintf (tmp2,"E61_%i_jesn",analyindex);    // same id and _qcd is essential
					cout <<k<<"th neg syst (" << (*it).first.c_str() << ") analysis initialize E61 with card: "<<tmp;
					dbxAnalyses.push_back( new E61dbxA(tmp2) ); // E61 analysis with new name
					dbxAnalyses.back()->setDataCardPrefix(tmp);  // but with old datacard
				}  // end of up/down syst
			} // end of number of sysy
		} // end dosystematic
	}  // end of E6 analysis setup
	cout << "End of E61 initialization,  #analyses:"<<dbxAnalyses.size()<<endl;

	// E62 analysis (ismet --> Berare & Aytul)
	analyindex=0;
	for (int i=0; i<aselect.E62count; i++)
	{
		analyindex++;
		char tmp[64];
		char tmp2[64];
		sprintf (tmp,"E62_%i",analyindex);
                cout << tmp<<endl;
		dbxAnalyses.push_back( new E62dbxA(tmp) ); // Skeleton analysis with name
		k=i;
		if (aselect.doQCD) {
			k++;
			sprintf (tmp2,"E62_%i_qcd",analyindex);    // same id and _qcd is essential
			dbxAnalyses.push_back( new E62dbxA(tmp2) ); // E62 analysis with new name
			dbxAnalyses.back()->setDataCardPrefix(tmp);
			dbxAnalyses.back()->setQCD();        // turning on QCD is essential

		}
		if (aselect.dosystematics) { // another loop here for systematics
			char cn[64]; sprintf (cn,"%s-card.txt",tmp);
			unsigned long iSystem  = ReadCard(cn,"systematics_bci",(unsigned long)4398046511104, 1);
			cout << cn<<" :adding systematics bci:"<< iSystem<<"  #systs:"<<syst_names.size()<<endl;
			// gSystem->Exec("cat E62_1-card.txt");
			for (map<string,int>::iterator it = syst_names.begin(); it != syst_names.end(); it++) {
				std::string s = it->first;
				std::string delimiter = "_";
				std::string token = s.substr(0, s.find(delimiter)); // token is "syst name"
				int this_syst=atoi ( token.data() );
//                          cout << (unsigned long)pow(2.0,this_syst-1) << "  vs  " << ((unsigned long)pow(2.0,this_syst-1) & iSystem) << endl; 
				if (!((unsigned long)pow(2.0,this_syst-1) & iSystem)) continue;
				unsigned int nsyst = (*it).second ;
				for ( unsigned int isys=0; isys < nsyst; isys++ ) { // initialize as many as second, maybe 168...
					k++;
					sprintf (tmp2,"E62_%i_%s_%i",analyindex,(*it).first.c_str(), isys);    // same id and _jesp is essential
					cout <<k<<"th syst (" << (*it).first.c_str() << ") analysis initialize E62 with card: "<<tmp <<" .\t";
					string ss = "E62 sys" + (*it).first + "(+)";
					dbxAnalyses.push_back( new E62dbxA(tmp2)); // E62 analysis with new name
					dbxAnalyses.back()->setDataCardPrefix(tmp);  // but with old datacard
				} // end of up/down etc syst
			} // end of number of syst
		}
		if (aselect.doHF)
		{ // another loop here for Heavy Flavours, Wjets only
			k++;
			sprintf (tmp2,"E62_%i_WHF1",analyindex);    // same id and _HF1 is essential
			dbxAnalyses.push_back( new E62dbxA(tmp2) );
			dbxAnalyses.back()->setDataCardPrefix(tmp);  // but with old datacard
			dbxAnalyses.back()->HFtype=1;
			if (aselect.dosystematics) { // another loop here for systematics
				char cn[64]; sprintf (cn,"%s-card.txt",tmp);
				unsigned long int iSystem  = ReadCard(cn,"systematics_bci",65535,1);
				cout << cn<<" :adding systematics bci:"<< iSystem<<endl;
				// gSystem->Exec("cat FF_1-card.txt");
				for (map<string,int>::iterator it = syst_names.begin(); it != syst_names.end(); it++) {
					std::string s = it->first;
					std::string delimiter = "_";
					std::string token = s.substr(0, s.find(delimiter)); // token is "syst name"
					int this_syst=atoi ( token.data() );
					if (!((unsigned long)pow(2.0,this_syst-1) & iSystem)) continue;
					unsigned int nsyst = (*it).second ;
					for ( unsigned int isys=0; isys < nsyst; isys++ ) {
						k++;
						sprintf (tmp2,"E62_%i_%s%i_WHF1",analyindex,(*it).first.c_str(), isys);    // same id and _jesp is essential
						cout <<k<<"th syst (" << (*it).first.c_str() << ") analysis initialize E62 with card: "<<tmp;
						dbxAnalyses.push_back( new E62dbxA(tmp2) ); // FF analysis with new name
						dbxAnalyses.back()->setDataCardPrefix(tmp);  // but with old datacard
						dbxAnalyses.back()->HFtype=1;
						cout << endl;
					}  // end of up/down syst
				} // end of number of syst
			}  // do sys
			k++;
			sprintf (tmp2,"E62_%i_WHF2",analyindex);    // same id and _HF2 is essential
			dbxAnalyses.push_back( new E62dbxA(tmp2) ); // FF analysis with new name
			dbxAnalyses.back()->setDataCardPrefix(tmp);  // but with old datacard
			dbxAnalyses.back()->HFtype=2;
			// we need to take care of systematics for WHF2 here
			if (aselect.dosystematics) { // another loop here for systematics
				char cn[64]; sprintf (cn,"%s-card.txt",tmp);
				unsigned long int iSystem  = ReadCard(cn,"systematics_bci",65535,1);
				cout << cn<<" :adding systematics bci:"<< iSystem<<endl;
				// gSystem->Exec("cat FF_1-card.txt");
				for (map<string,int>::iterator it = syst_names.begin(); it != syst_names.end(); it++) {
					std::string s = it->first;
					std::string delimiter = "_";
					std::string token = s.substr(0, s.find(delimiter)); // token is "syst name"
					int this_syst=atoi ( token.data() );
					if (!((unsigned long)pow(2.0,this_syst-1) & iSystem)) continue;
					unsigned int nsyst = (*it).second ;
					for ( unsigned int isys=0; isys < nsyst; isys++ ) {
						k++;
						sprintf (tmp2,"E62_%i_%s%i_WHF2",analyindex,(*it).first.c_str(), isys);    // same id and _jesp is essential
						cout <<k<<"th syst (" << (*it).first.c_str() << ") analysis initialize FF with card: "<<tmp;
						dbxAnalyses.push_back( new E62dbxA(tmp2) ); // FF analysis with new name
						dbxAnalyses.back()->setDataCardPrefix(tmp);  // but with old datacard
						dbxAnalyses.back()->HFtype=2;
						cout << endl;
					}  // end of up/down syst
				} // end of number of syst
			}  // do sys

		}  // end of doHF
	}
	cout << "End of E62 initialization,  #analyses:"<<dbxAnalyses.size()<<endl;

	/*
	 *
	 *  The v4 analysis, silver channel
	 *
	 */
	for (int i=0; i<aselect.N4count; i++) {
		char tmp[32];
		sprintf (tmp,"N4_%i",i+1);
		dbxAnalyses.push_back( new N4dbxA(tmp) ); // N4 analysis with name
	}
	cout << "End of V4 initialization"<<endl;

	/*
	 *
	 *  The v4 analysis, silver channel
	 *
	 */
	for (int i=0; i<aselect.BPcount; i++) {
		char tmp[32];
		sprintf (tmp,"BP_%i",i+1);
		dbxAnalyses.push_back( new BPdbxA(tmp) ); // BP analysis with name
	}
	cout << "End of BP initialization"<<endl;


	/*
	 *
	 *  Dump analysis
	 *
	 */
	for (int i=0; i<aselect.Dumpcount; i++) {
		char tmp[32];
		sprintf (tmp,"Dump_%i",i+1);
		dbxAnalyses.push_back( new DumpdbxA(tmp) ); // Dump analysis with name
	}
	cout << "End of Dumper initialization"<<endl;


	/*
	 *
	 *  Challenge analyses
	 *
	 */
	for (int i=0; i<aselect.MCHcount; i++) {
		char tmp[32], tmpk[32];
		if (extname!=NULL) {sprintf (tmpk,"MCH_%i%s",i+1,extname);
		} else {sprintf (tmpk,"MCH_%i",i+1); }
		sprintf (tmp,"MCH_%i",i+1);
		dbxAnalyses.push_back( new Challenge(tmpk) ); // challenge analysis with name
		dbxAnalyses.back()->setDataCardPrefix(tmp);  // but with old datacard
	}
	cout << "End of MCH initialization"<<endl;

	for (int i=0; i<aselect.DCHcount; i++) {
		char tmp[32], tmpk[64];
		if (extname!=NULL) {sprintf (tmpk,"DCH_%i%s",i+1,extname);
		} else {sprintf (tmpk,"DCH_%i",i+1); }
		sprintf (tmp,"DCH_%i",i+1);
		dbxAnalyses.push_back( new DiLepChal(tmpk) ); // analysis with name
		dbxAnalyses.back()->setDataCardPrefix(tmp);  // but with old datacard
	}
	cout << "End of DCH initialization"<<endl;

	/*
	 *
	 *  The Skeleton analysis
	 *
	 */
	for (int i=0; i<aselect.Skeletoncount; i++) {
		char tmp[32];
		sprintf (tmp,"Skeleton_%i",i+1);
		dbxAnalyses.push_back(  new SkeletondbxA(tmp) ); // Skeleton analysis with name
	}
	cout << "End of Skeleton initialization"<<endl;


// common suff for all analyses
// ----------------------------
        for (k=0; k<dbxAnalyses.size(); k++) {
		dbxAnalyses[k]->initGRL();
		dbxAnalyses[k]->readAnalysisParams();
		dbxAnalyses[k]->bookAdditionalHistos();
//              std::cout << " => " << k << " "<< dbxAnalyses[k]->getName() <<endl;
        }
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void AnalysisController::RunTasks( AnalysisObjects a0,  map <string,   AnalysisObjects> analysis_objs_map){

   int evret=0;
   std::string delimiter = "_";
   std::string sysnam="";
   for (int k=0; k<dbxAnalyses.size(); k++) 
   {
        bool aDumper=false; 
        int token_cnt=0;
        sysnam="";
        string ana=dbxAnalyses[k]->getName();
        size_t pos = 0;
        std::string token;
        while ((pos = ana.find(delimiter)) != std::string::npos) {
         token = ana.substr(0, pos);
         ana.erase(0, pos + delimiter.length());
         token_cnt++;
         if (token=="Dump") { aDumper=true; break;}
         if (token_cnt>1) { // systematics or QCD or WHF
           if ((ana.find("WHF") == std::string::npos) && (ana.find("qcd") == std::string::npos ) ){
            sysnam=ana;
            break;
           }
         }
        }
       
        if (sysnam.size()< 1){
             if (!aDumper){
#ifdef __DEBUG__
                cout << dbxAnalyses[k]->getName()<<" to be executed with defaults k:"<<k<<endl;
#endif
	        evret=dbxAnalyses[k]->makeAnalysis(a0.muos, a0.eles, a0.gams, a0.jets, a0.ljets, a0.truth, a0.met, a0.evt); // regular analysis
             } else {
#ifdef __DEBUG__
                cout << dbxAnalyses[k]->getName()<<" to be executed with Dumper specials"<<endl;
#endif
		std::map <int, TVector2> metsmap;
		int kmet=1;
		for ( std::map<string, AnalysisObjects>::iterator anit=analysis_objs_map.begin(); anit!=analysis_objs_map.end(); ++anit){
			AnalysisObjects these_objs= anit->second;
			TVector2        a_met     = these_objs.met ;
			metsmap.insert ( std::pair<int, TVector2> (kmet, a_met));
			kmet++;
		}
		evret=dbxAnalyses[k]->makeAnalysis(a0.muos, a0.eles, a0.gams, a0.jets, a0.ljets, a0.truth, a0.met, a0.evt, metsmap, m_quad_unc);
#ifdef __DEBUG__
                  cout<<"non syst retval=:"<<evret<<endl;
#endif
             }
        } else {
             map<string, AnalysisObjects>::iterator il=analysis_objs_map.find(sysnam);
             if (il==analysis_objs_map.end()) {
              // cout<<"Systematics name mismatch. "<<sysnam<<" not found, MAJOR error\n"; 
              // exit (888);
               break;
              } // or just break?
#ifdef __DEBUG__
                cout << k<<" "<<dbxAnalyses[k]->getName()<<" also known as "<< il->first<<" to be executed with systematics"<<endl;
#endif
		AnalysisObjects these_objs=il->second;
		std::vector<dbxMuon>     a_muons = (these_objs.muos.size()>0)  ? these_objs.muos  : a0.muos;
		std::vector<dbxElectron> a_eles  = (these_objs.eles.size()>0)  ? these_objs.eles  : a0.eles;
		std::vector<dbxPhoton>   a_gams  = (these_objs.gams.size()>0)  ? these_objs.gams  : a0.gams;
		std::vector<dbxJet>      a_jets  = (these_objs.jets.size()>0)  ? these_objs.jets  : a0.jets;
		std::vector<dbxLJet>     a_ljets = (these_objs.ljets.size()>0) ? these_objs.ljets : a0.ljets;
        std::vector<dbxTruth>    a_truth = (these_objs.truth.size()>0) ? these_objs.truth : a0.truth;
        TVector2 a_met      = (these_objs.met.Mod()>0  ) ? these_objs.met  : a0.met ;
		evt_data a_evt      = (these_objs.evt.year >0  ) ? these_objs.evt  : a0.evt ;
		evret=dbxAnalyses[k]->makeAnalysis(a_muons, a_eles, a_gams, a_jets, a_ljets, a_truth, a_met, a_evt); // generic
#ifdef __DEBUG__
                  cout<<"retval=:"<<evret<<endl;
#endif
        }
   }

#ifdef __DEBUG__
	std::cout << "An Event finished." <<std::endl;
#endif
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void AnalysisController::SetJetUncs(vector <double> uncs){
	m_quad_unc.clear();
	for (unsigned int u=0; u<uncs.size(); u++) m_quad_unc.push_back( uncs.at(u) );
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void AnalysisController::Finalize(){
	for ( int i=0; i<dbxAnalyses.size(); i++)
	{
		cout <<"Efficiencies for analysis : "<< dbxAnalyses[i]->cname <<endl;
		dbxAnalyses[i]->printEfficiencies();
		dbxAnalyses[i]->saveHistos();
	}

}

