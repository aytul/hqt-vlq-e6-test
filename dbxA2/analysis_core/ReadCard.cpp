#ifndef __READCARD_CPP
#define __READCARD_CPP

// sadly ROOT CINT does not have a standards-compliant std::string
// hence if we detect CINT or ACLiC running, we load the old ReadCard
//   PS: This code does compile with ACLiC, but not work with it. Sorry.
//       If you really MUST ACLiC, try .L ReadCardCINT.C++
#if defined(__CINT__) || defined (__MAKECINT__) || defined(__ACLIC__)
#include "ReadCardCINT.C"
#else

#include <fstream>
#include <iostream>
#include <sstream>

#ifdef VLQLIGHT
#include "VLQLight/ReadCard.h"
#else
#include "ReadCard.h"
#endif
// If the variable cannot be found in the cardfile or the cardfile
//   has problems, the 3rd argument defval is returned.
// The fourth argument can be set to -1,0,1. -1 suppresses the warning
//   issued when defval is being used. 1 prints the varname = value
//   pairs as read from the cardfile.
punch ReadCard(const char *filename, const char *varname,
	       const punch defval, const int verbose ) {

  using namespace std;

  ifstream cardfile(filename);
  if ( ! cardfile.good()) {
    cerr << "The cardfile " << filename << " file has problems... " << endl;
    return defval;
  }

  string tempLine, tmpvar;
  string value(defval);
  bool foundInFile(false);

  while ( ! cardfile.eof() ) {

    getline( cardfile, tempLine );
    if ( tempLine[0] == '#' ) continue; // skip comment lines
    istringstream lstr( tempLine );
    lstr >> tmpvar;
    if ( tmpvar != string(varname) ) continue;
    lstr >> tmpvar;
    if ( tmpvar != "=" ) continue;
    getline( lstr, tmpvar ); // next let's remove leading whitespace
    while ( tmpvar[0] == ' ' || tmpvar[0] == '\t' ) tmpvar.erase(0,1);
    if ( tmpvar[0] == '"' ) { // handle string that start with quotes
      string::size_type pos = tmpvar.find('"',1);
      if ( pos == string::npos ) continue;
      value = tmpvar.substr(1, pos-1);  }
    else { // handle anything else, delimiter is any whitespace
      value = tmpvar.substr(0, tmpvar.find_first_of(" \t\f\v\n\r")); }
    if ( verbose>0 ) cout << varname << " = " << value << endl;
    foundInFile = true;
    break;

  }

  if ( verbose>=0 && !foundInFile )
    cout << "Warning! ReadCard using the default value for " << varname << endl;
  cardfile.close();
  return punch(value);

}


std::string ReadCardString(const char *filename, const char *varname,
		const char *defval, const int verbose) {
 // std::cerr << "ReadCardString is deprecated, please use ReadCard" << std::endl;
  return ReadCard(filename, varname, defval, verbose); }


// g++ -D__RCTEST__ ReadCard.cpp punch.cpp
#ifdef __RCTEST__
int main() {
  const char* cardfile = "../toplu/ANA_DEFS";
  std::string modeldir = ReadCard(cardfile,"MODEL","DEFMOD",1);
  int    ecm      = ReadCard(cardfile,"ECM",0,1);
  bool   systerr  = (int)ReadCard(cardfile,"SYSTERR",1,1);
  float  inj_coef = ReadCard(cardfile,"INJECTION",0.55,1);
  double lumi     = ReadCard(cardfile,"INTLUMI",5000,1);
  ReadCard(cardfile,"DUMMYWARNTEST\n","DEFSTR"); // test warning mechanism

  testPunch();
}
#endif // __RCTEST__

#endif // else __CINT__

#endif // __READCARD_CPP
