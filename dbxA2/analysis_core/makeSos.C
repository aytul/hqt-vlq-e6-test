{
SysInfo_t sysinfo;
gSystem->GetSysInfo(&sysinfo);

/*
if(sysinfo.fOS == "Linux") {
	gSystem->SetMakeSharedLib("cd $BuildDir ; c++ -c $Opt -pipe -std=c++11 -m64 -msse -mfpmath=sse -W -Woverloaded-virtual -fPIC -pthread $IncludePath $SourceFiles ; c++ $ObjectFiles -shared -Wl,-soname,$LibName.so -m64 -O2  -Wl,--no-undefined -Wl,--as-needed $LinkedLibs -o $SharedLib");
}
*/

gROOT->LoadMacro("dbxParticle.cpp+");
gROOT->LoadMacro("dbx_muon.h+");
gROOT->LoadMacro("dbx_electron.h+");
gROOT->LoadMacro("dbx_photon.h+");
gROOT->LoadMacro("dbx_jet.h+");
gROOT->LoadMacro("dbx_ljet.h+");
gROOT->LoadMacro("dbx_truth.h+");
gROOT->LoadMacro("DBXNtuple.cpp+");
gROOT->LoadMacro("grl.C+");
gROOT->LoadMacro("dbxCut.cpp+");
}
