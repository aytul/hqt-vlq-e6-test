// emacs: this is -*- c++ -*-

// A class to reconstruct leptonic Ws from the 3-momentum of
// a lepton and the transverse missing momentum.

// There are two constructors, one of them takes 5 doubles:
//  px,py,pz of the lepton and the missing px, py
// The other constructor accepts a TLorentzVector for the lepton
//  and a TVector2 for the missing P_T.

// Currently the following methods return outputs:
//  neutrino_pz(): It returns a vector, which is empty if there
//   are no possible solutions, and has two entries if possible
//   solutions exist.
//  getWpt(): Returns the P_T of the W, which might or might not
//   be reconstuctable.
//  getWeta(int): Returns eta for the leptonic W from solution
//   #0 or #1.  If no W solutions exist, returns -99.9.
//  getWp4(int): Works like getWeta(), but returns a 4-vector
//   for the P_x, P_y, P_z and E.

// This class would benefit from caching the solution in some
//  internal variables, so as not to compute it everytime it is
//  asked for some output. The actual W object could have its
//  own class, and this one could be a friend to it.

#ifndef LEPTONICWRECONSTRUCTOR_H
#define LEPTONICWRECONSTRUCTOR_H

#include <vector>
#include <cmath>
#include "TLorentzVector.h"

class LeptonicWReconstructor {

public:
	LeptonicWReconstructor(double, double, double, double, double);
	LeptonicWReconstructor(TLorentzVector, TVector2);
	~LeptonicWReconstructor();

	std::vector<double> neutrino_pz();
	double getWpt();
	double getWeta(int);
	double getPhiOpeningAngle();
	double getVisibleMass(double leptonMass=0);
	double getTransverseMass(double leptonMass=0);
	TLorentzVector getWp4(int);
	void   setWmass(double);

private:
	double m_El_px, m_El_py, m_El_pz, m_Nu_px, m_Nu_py;
	double m_mW;

};

#endif
