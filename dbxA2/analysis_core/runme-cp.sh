#!/bin/bash
ln -s dbxParticle_cpp.so libdbxParticle_cpp.so
ln -s dbx_electron_h.so  libdbx_electron_h.so
ln -s dbx_muon_h.so      libdbx_muon_h.so
ln -s dbx_photon_h.so    libdbx_photon_h.so
ln -s dbx_jet_h.so       libdbx_jet_h.so
ln -s DBXNtuple_cpp.so   libDBXNtuple_cpp.so 

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ROOTCOREBIN/../analysis_core/
