#!/bin/bash

#run version
rv=201
targetlist=$1
mode=$2
usern=$USER  
# username is the unix id.

# user selection --
based=$HOME/work/public/4grid/
# put lvl0.root (dump analysis) or analysisd3pd.root(any other analysis) depending on your run 
expect=lvl0.root

if [ $# -eq 3 ] ; then
echo forcing the submission version $3 instead of the default $rv
rv=$3
fi

found=0
#it is a good idea to clean the empty or broken files
if [ -e cleanme ] ; then
 echo "cleaning root files of size less than 1k"
 find ./ -name "*.root*" -size -1k| xargs rm -f
 rm -f cleanme
fi

if [ $targetlist == "data_muons" ] ; then
 targetlist=`cat runlist.txt | grep _muons| cut -f1 -d'='`
fi

if [ $targetlist == "data_electrons" ] ; then
 targetlist=`cat runlist.txt | grep _electrons | cut -f1 -d'='`
fi
 
if [ $targetlist == "mc" ] ; then
 targetlist=`cat runlist.txt | grep '=' |grep -v '_'| cut -f1 -d'='`
fi
 
source runlist.txt
set_list=`grep '="' runlist.txt | cut -f1 -d'='`

for target in $targetlist; do
dl=""
if [ ${#target} -gt 3 ]; then
 targetval=$((${target:0:3}))
else 
 targetval=0
fi
 
if [ $targetval -ne 0 ] ; then
# echo Working with a run number.
 dl=$target
 found=1
else

 for s in $set_list ; do
   if [ $s == $target ] ; then
    found=1
    echo get the number from : $s
    eval tfl=\$$target
       for r in $tfl ; do
         trn=`echo $r | cut -f3 -d'.'`
         dl=$dl" "$trn
       done
   fi
 done

fi

if [ $found -eq 0 ] ; then
 echo "requested files " $target  " not found, possibilities are: "
 cat runlist.txt | grep '="' | cut -f1 -d'='
 exit
fi

rfilelist=" "
######################################
# ---------action here----------------
for d in $dl; do
 if [ $mode == "kill" ]; then 
  echo killing in background
  $based/checkjobs.sh -k $d $rv &
 else if [ $mode == "listrunnos" ]; then 
  echo "run# is:" $d
 else if [ $mode == "get" ]; then 
  nohup dq2-get -f "*.root*" user.${usern}.analysisD${rv}D${d}/ > $d.dq2log &
  echo downloading in background, tail $d.dq2log to see if finished or use checkget
 else if [ $mode == "add" ]; then
   rfn=`ls user.${usern}.analysisD${rv}D$d*/*${expect}* 2> /dev/null | wc -l `
   if [ $rfn -gt 0 ]; then
  #  rn=`echo $i|cut -f3 -d'.'|cut -f3 -d'D'`
    echo  "  run " $d has $rfn root files
    if [ -e ${d}.root ]; then
      echo root file for run $d already created
    else  
      hadd $d.root user.${usern}.analysisD${rv}D$d*/*${expect}*
    fi
   else 
    echo there are no root files for run $d 
   fi

  else if [ $mode == "remove" ]; then
      rm -f ${d}*.root

  else if [ $mode == "check" ] ||[ $mode == "checkget" ] || [ $mode == "checkdisp" ] || [ $mode == "checkresub" ] || [ $mode == "checkmerge" ] ; then
     if [ -e ${d}.root ]; then
      ls -lh ${d}.root
      if [ $mode == "checkmerge" ]; then rfilelist=$rfilelist" "`ls -1 ${d}.root` ; fi
     else
      checkmode=d
      getstat="ongoing"
      if [ $mode == "checkget" ];  then  
       getstat=`tail -n1 ${d}.dq2log | grep Finished`
       if [ ${#getstat} -lt 2 ] ; then getstat="ongoing" ; fi
       echo  "lcg-cp for " $d " is " $getstat
      fi
      if [ $mode == "checkdisp" ];  then checkmode=s; fi
      if [ $mode == "checkresub" ]; then checkmode=f; fi
      rfn=`ls  user.${usern}.analysisD${rv}D${d}*/*${expect}* 2> /dev/null | wc -l `
      retval=`$based/checkjobs.sh -$checkmode $d $rv`
      echo -n we have $rfn root files for run $d " ;   " ${retval}
      expn=`echo ${retval} | awk '{print $2}'`
      if [ $expn -eq $rfn ] ; then
         echo "..OK.."
      else  
         echo "..Error"
      fi      

      if [ $expn -eq $rfn ] && [ $getstat == 'Finished' ] ; then
          echo ready, will add automatically.
          $0 $d add
      fi
      if [ $expn -gt $rfn ] && [ $getstat == 'Finished' ] ; then
          $based/checkjobs.sh -s $d $rv 0
      fi
     fi  
  else
   echo "Command not found. Available commands are: (without the quotes)"
   cat $0 | grep 'if \[ $mode ==' | grep -v cat| cut -f2- -d"=" |  awk '{print $2}' | sort | uniq 
   exit 1
  fi
  fi
  fi
  fi
  fi
 fi 
done

if [ $mode == "checkmerge" ]; then
  if [ -e ${target}.root ] ; then
     echo -n "Already merged:  "
     ls -l ${target}.root
  else
     hadd ${target}.root $rfilelist
  fi
fi 
done
