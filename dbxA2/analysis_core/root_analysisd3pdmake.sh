#!/bin/bash

REL=`cat RELEASE`

if [ $REL == 16 ]; then 

if [ -e BAT-0.4.2.tar.gz ]; then
 tar zxf BAT-0.4.2.tar.gz
 mv BAT-0.4.2 BAT
 cp prepbat.sh BAT/
fi

cd BAT 
./prepbat.sh > f1
mv f1 ../
cd -
cd KLFitter
make clean > f2
make >> f2
mv f2 ../
cd -
cp KLFitter/libKLFitter.so .
cp BAT/installdir/lib/libBAT.so.3 .

#end of KLF and BAT
fi

make cleanall > f3
make >> f3

