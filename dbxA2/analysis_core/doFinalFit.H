#if !defined(__CINT__) || defined (__MAKECINT__)
#include<TH1.h>
#include<TF1.h>
#include<TROOT.h>
#include<TLatex.h>
#include<TCanvas.h>
#include<TSpectrum.h>
#include<TMath.h>
#endif
#include"colmsg.H"

// Does a fit to the supplied sig+bkg histogram using the signal and bkg
//   models chosen from the list of available TFormulas.
// One can also provide the underlying signal component as an optional
//   histogram. In this case the extracted signal will be "measured" against
//   it & a pure signal fit will also be performed to provide a comparison.
// Some example TFormulas are readily available inside the function itself,
//   but the enduser can implement any model into a TFormula (following the
//   convention below), and  call doFinalFit with its name & doFinalFit()
//   will get the TFormula from gROOT.
// If new TFormulas are being supplied, they should always have [0] as some
//   normalization parameter; [1] as some peak location and [2] as some sort
//   of width, because we will initialize them with input from a landaun fit.
//   For any other parameter beyond these three, it would be useful to set
//   default values to guide the fits.
// If the fit is successful, doFinalFit() returns the signal significance.
// If the bkgmodel is the default, ie. "Landau", a quicker response has been
//   implemented such that the bkg-only fit is skipped, using the results of the
//   preliminary fit and "landaun(0)" is used in the total fit to gain speed.
// Some (~7-8% in basic tests) gain in speed is achievable by compiling
//   and loading this file with gROOT->LoadMacro("core/doFinalFit.H+"); instead
//   of plainly #include'ing it.

// Colors and line styles to be used when drawing the various functions, in
//const unsigned int styles[5]={2,1,1,3,2};
//const unsigned int colors[5]={kGreen,kBlack,kBlue,kBlack,432};

//  order:  bkg-only fit, si+bg fit, sig component, bkg component, sig-only fit
const unsigned int styles[5]={2,1,1,2,3};
const unsigned int colors[5]={kBlack,kRed,kMagenta,kRed,432};

double doFinalFit(TH1 *toplamq,
		  TString sigmodel="BrWig", TString bkgmodel="Landau",
		  float fitxmin=0, float fitxmax=0,
		  TH1 *signal=0, bool cheat=false) {

  if ( toplamq == 0 ) return -2;

  using namespace colmsg;

  if ( TMath::IsNaN(toplamq->GetSumOfWeights()) || 
       toplamq->GetSumOfWeights() == 0 ) {
    cout << warn << "doFinalFit called with empty histogram." << endm;
    return -2; }

  // define a few formulas here for convenience.
  TFormula BrWig("BrWig","[0]*[2]/2/pi/((x-[1])*(x-[1])+[2]*[2]/4)");
  BrWig.SetParNames("norm","mpv","#gamma");
  TFormula Gauss("Gauss","[0]*exp(-0.5*((x-[1])/[2])**2)/(sqrt(2*pi)*[2])");
  Gauss.SetParNames("norm","mean","#sigma");
  TFormula Landau("Landau","[0]*TMath::Landau(x,[1],[2],1)");
  Landau.SetParNames("norm","mpv","#sigma");
  TFormula XtalBall("XtalBall","([2]!=0)*((x<[1]+[3]*[2])*([0]*exp(-0.5*((x-[1])/[2])**2))+(x>=[1]+[3]*[2])*(([3]!=0)*([1]*[3]/[2]!=[3]*([3]+(2*[1]-x)/[2])-[4])*([0]*exp([3]*[3]/-2.)*pow([4]/([3]*([1]/[2]-(2*[1]-x)/[2]-[3])+[4]),(x>=[1]+[3]*[2])*[4]))))");
  XtalBall.SetParameters(1,0,0.5,1,10);
  XtalBall.SetParNames("N","mpv","#sigma","#alpha","n");
  TFormula Empty("Empty","0");

  // From the given model names, extract the relevant TFormulas
  if ( sigmodel=="" ) sigmodel="Empty";
  if ( bkgmodel=="" ) bkgmodel="Empty";

  TObject *sigobject = gROOT->FindObject(sigmodel);
  TObject *bkgobject = gROOT->FindObject(bkgmodel);
  
  if ( sigobject == 0  ||  ! sigobject->InheritsFrom("TFormula") ) {
    cout << err << sigmodel+" not amongst known TFormulas" << endm; return -1; }
  if ( bkgobject == 0  ||  ! bkgobject->InheritsFrom("TFormula") ) {
    cout << err << bkgmodel+" not amongst known TFormulas" << endm; return -1; }

  TFormula *sigformula = (TFormula*)sigobject;
  TFormula *bkgformula = (TFormula*)bkgobject;

  int nsigpars = sigformula->GetNpar();
  int nbkgpars = bkgformula->GetNpar();


  // histogram and fit ranges
  float binwidth = toplamq->GetBinWidth(1);
  float endofhisto = toplamq->GetBinLowEdge(toplamq->GetNbinsX()+1);
  if ( fitxmin == fitxmax ) {
    fitxmin = toplamq->GetBinLowEdge(1); fitxmax = endofhisto; }
  else if ( fitxmax<fitxmin ) {
    if ( fitxmax<=0 )  fitxmax = endofhisto;
    else { cout << err << "Fit xmin>xmax" << endm; return -2; } }

  bool qcklnd = ( bkgmodel == "Landau" );
  if ( qcklnd ) bkgmodel = "landaun(0)"; // if bkg is Landau, get faster

  // start w/ pure landau fit to get initial values of parameters
  // -> This fit is very reliable and works well with no initial parameters set
  // -> However occasionally, the normalized landau fit fails, then revert to
  //    the basic landau fit and the normalize manually by dividing to sigma.
  TF1 *land = new TF1("land","landaun");
  int fitres = toplamq->Fit(land,qcklnd?"0":"q0","",fitxmin,fitxmax);
  if ( fitres != 0  &&  bkgmodel != "Empty" ) {
    cout << warn+"Initial norm-landau fit failed, trying non-normalized."+endm;
    TF1 lnd("lnd","landau"); lnd.SetParName(0,"area/sig");
    fitres = toplamq->Fit(&lnd,qcklnd?"0":"q0","",fitxmin,fitxmax);
    if ( fitres != 0 ) { cout << err+"Initial fit went boom!"+endm; return 0; }
    land->SetChisquare(lnd.GetChisquare());  land->SetNDF(lnd.GetNDF());
    land->SetParameter(0,lnd.GetParameter(0)*lnd.GetParameter(2));
    land->SetParameter(1,lnd.GetParameter(1));
    land->SetParameter(2,lnd.GetParameter(2)); }

  // Now do a background-only fit.
  TF1 *bkg = new TF1("bkg",bkgmodel,fitxmin,fitxmax);  bkg->SetLineWidth(2);
  bkg->SetLineStyle(styles[0]); bkg->SetLineColor(colors[0]);
  bkg->SetParameters( bkgformula->GetParameters() ); //default pars
  for (int i=0; i<3; ++i) bkg->SetParameter(i, land->GetParameter(i));
  bkg->SetParLimits(2,binwidth,endofhisto); // at least one bin width
  if ( ! qcklnd ) {
    for (int i=0; i<nbkgpars;++i) bkg->SetParName(i, bkgformula->GetParName(i));
    fitres = toplamq->Fit(bkg,"+","same",fitxmin,fitxmax);
    if ( fitres != 0 )  cout << warn << "Background-only fit failed." << endm; }
  else {
    bkg->SetNDF(land->GetNDF());  // copy fit results from land to bkg
    bkg->SetChisquare(land->GetChisquare());  bkg->Draw("same"); }

  // trying to determine our best estimate for the signal peak position
  float bestsigpos = bkg->GetParameter(1)+binwidth*4;
  TVirtualPad *cpad = gPad;
  TH1F *totmnsbkg = (TH1F*)gROOT->FindObject("totmnsbkg");
  if ( totmnsbkg ) delete totmnsbkg;
  totmnsbkg = new TH1F("totmnsbkg","Total-(fit bkg)",toplamq->GetNbinsX(),
		       toplamq->GetBinLowEdge(1), endofhisto);
  for ( int i=1; i<=toplamq->GetNbinsX(); ++i )
    totmnsbkg->SetBinContent(i, toplamq->GetBinContent(i)
			     - bkg->Integral(totmnsbkg->GetBinLowEdge(i),totmnsbkg->GetBinLowEdge(i+1))/binwidth);
  TCanvas *tmbcanv = (TCanvas*)gROOT->FindObject("tmbcanv");
  if ( tmbcanv == 0 ) tmbcanv = new TCanvas("tmbcanv","tmbcanv",400,300);
  tmbcanv->cd();
  totmnsbkg->GetXaxis()->SetRangeUser(fitxmin,fitxmax);
  TSpectrum *peakfind = new TSpectrum(8);
  Int_t nfound = peakfind->Search(totmnsbkg,2,"",0.3);
  if ( nfound ) {
    //peakfind->Background(totmnsbkg,15,"same");  peakfind->Print();
    Float_t *xpeaks = peakfind->GetPositionX();  // location of the peaks
    Float_t *ypeaks = peakfind->GetPositionY();  // height of the peaks
    int itop    =  0;  // index of highest peak
    int itoprgt = -1;  // index of highest peak to the right of bkg MPV
    float bkgmpv = bkg->GetParameter(1);
    for ( int i=0; i<nfound; ++i ) { 
      if ( i>0 && ypeaks[i]>ypeaks[itop] ) itop = i;
      if ( xpeaks[i]>bkgmpv && ( itoprgt<0 || ypeaks[i]>ypeaks[itoprgt] ) )
	itoprgt = i; }
    if ( itoprgt<0 ) bestsigpos = peakfind->GetPositionX()[itop];
    else bestsigpos = peakfind->GetPositionX()[itoprgt]; }
  cpad->cd(); cout << endl;

  if ( sigmodel=="Empty" ) {
    cout << "Prob(Fit) of bkg-only(" << bkgmodel
	 << ") fit = " << bkg->GetProb() << endl;
    return 0; }

  // fit to background + signal
  TF1 *total = new TF1("total",bkgmodel+"+"+sigmodel,fitxmin,fitxmax);
total->SetLineStyle(styles[1]); total->SetLineColor(colors[1]);
  for (int i=0; i<nbkgpars; ++i)  { // bkg params from above bkg-only fit
    total->SetParameter(i, bkg->GetParameter(i));
    total->SetParName(i, TString(bkgformula->GetParName(i))+"_bkg"); }
  for (int i=0; i<nsigpars; ++i)  { // default params of sig formula
    total->SetParameter(i+nbkgpars, sigformula->GetParameter(i));
    total->SetParName(i+nbkgpars, TString(sigformula->GetParName(i))+"_sig"); }
  total->SetParameter(nbkgpars, bkg->GetParameter(0)/2); // sig normalization
  total->SetParameter(nbkgpars+1, bestsigpos); // sig peak
  total->SetParameter(nbkgpars+2, binwidth*4); // sig width
  if ( cheat && signal!=0 ) { // if cheat, init to signal histo params
/*
TF1 *sigfit = new TF1("sigfit",sigmodel,fitxmin,fitxmax);
    sigfit->SetParameter(0, signal->GetSumOfWeights());
    sigfit->SetParameter(1, signal->GetMean());
    sigfit->SetParameter(2, signal->GetRMS());
   // sigfit->SetParameters(fitsig->GetParameters());
    for (int i=0; i<nsigpars; ++i)
      sigfit->SetParName(i, "sig_"+TString(sigformula->GetParName(i)));
    sigfit->SetLineWidth(2);
    sigfit->SetLineStyle(styles[4]); sigfit->SetLineColor(2);
    fitres = signal->Fit(sigfit,"0L+","same",fitxmin,fitxmax);
if ( fitres != 0 )  cout << warn << " cheat Signal-only fit failed." << endm;
    total->SetParameter(nbkgpars+0, sigfit->GetParameter(0));
    total->SetParameter(nbkgpars+1, sigfit->GetParameter(1));
    total->SetParameter(nbkgpars+2, sigfit->GetParameter(2)); 
*/
    total->SetParameter(nbkgpars+0, signal->GetSumOfWeights());
    total->SetParameter(nbkgpars+1, signal->GetMean());
    total->SetParameter(nbkgpars+2, signal->GetRMS()); 

   }

  total->SetParLimits(0,0,bkg->GetParameter(0)*100.);
  total->SetParLimits(2,binwidth,endofhisto); // at least one bin width
  total->SetParLimits(nbkgpars+1,0,endofhisto); // no negative mpvs
  total->SetParLimits(nbkgpars+2,0,endofhisto); // no negative widths
  fitres = toplamq->Fit(total,"embr+0");
  if ( fitres != 0 )  cout << warn << "bkg+sig fit result=" << fitres << endm;
  total->Draw("same");
  cout << "\nProb(Fit) of bkg-only(" << bkgmodel
       << ") vs. bkg+" << sigmodel << " = "
       << bkg->GetProb() << " vs. " << total->GetProb() << endl;


  // Draw extracted signal
  TF1 *fitsig = new TF1("fitsig",sigmodel,fitxmin,fitxmax);
  fitsig->SetParameters(total->GetParameters()+nbkgpars);
  fitsig->SetLineStyle(styles[2]); fitsig->SetLineColor(colors[2]);
  fitsig->Draw("same");

  // Draw extracted background
  TF1 *fitbkg = new TF1("fitbkg",bkgmodel,fitxmin,fitxmax);
  fitbkg->SetParameters(total->GetParameters());
  fitbkg->SetLineStyle(styles[3]); fitbkg->SetLineColor(colors[3]);
  fitbkg->Draw("same");


  // Calculate expected significance
  float ns = 2; // number of sigmas to integrate over
  float mpv   = total->GetParameter(nbkgpars+1);
  float sigma = total->GetParameter(nbkgpars+2);

  double nsig = (fitsig->Integral(mpv-ns*sigma,mpv+ns*sigma))/binwidth;
  double ntot = (total ->Integral(mpv-ns*sigma,mpv+ns*sigma))/binwidth;
  double nbkg = (bkg   ->Integral(mpv-ns*sigma,mpv+ns*sigma))/binwidth;
  double SIG  = sqrt(2*(ntot*log(1+nsig/(ntot-nsig))-nsig ));

  cout << "n_sig=" << nsig << "  n_tot=" << ntot << "  n_bkg=" << (ntot-nsig);
  cout << "   mpv=" << mpv << "  gamma=" << sigma << endl;
  cout << cyan << "Expected significance = " << SIG << endm;

  // Print expected significance of the canvas
  TLatex yax; char aaa[128];
  sprintf (aaa,"s=%2.1f   b=%2.1f      SS=%2.2f",nsig, ntot-nsig, SIG );
  yax.SetNDC(); yax.DrawLatex(0.12,0.03,aaa);

  // if the real underlying signal is given, compare to it.
  if ( signal != 0  &&  sigmodel!="Empty" ) {
    int fitxminbin = signal->FindBin(fitxmin);
    int fitxmaxbin = signal->FindBin(fitxmax);   
    float fxmin = signal->GetBinLowEdge(fitxminbin);
    float fxmax = signal->GetBinLowEdge(fitxmaxbin+1);
    float sigintegral = signal->Integral(fitxminbin, fitxmaxbin);
    float delmpv = mpv - signal->GetBinCenter(signal->GetMaximumBin());
    float delmpvbin = delmpv/binwidth;
    float delint = fitsig->Integral(fxmin,fxmax) / binwidth - sigintegral;
    cout << "\nComparison of extracted signal to the real histogram:\n"
	 << "  Delta(mpv) = " << delmpv << " " << (fabs(delmpvbin)>3?warn:"")
	 << "("  << delmpvbin << " bin-widths)" << endm
	 << "  Delta(integral in fit-range-bins) = " << delint << " ("
	 << delint/sigintegral*100. << "%)" << endl;
    // Pure signal fit as a test of the signal model
    TF1 *sigfit = new TF1("sigfit",sigmodel,fitxmin,fitxmax);
    sigfit->SetParameter(0, signal->GetSumOfWeights());
    sigfit->SetParameter(1, signal->GetMean());
    sigfit->SetParameter(2, signal->GetRMS());
   // sigfit->SetParameters(fitsig->GetParameters());
    for (int i=0; i<nsigpars; ++i)
      sigfit->SetParName(i, "sig_"+TString(sigformula->GetParName(i)));
    sigfit->SetLineWidth(2);
    sigfit->SetLineStyle(styles[4]); sigfit->SetLineColor(colors[4]);
    fitres = signal->Fit(sigfit,"0L+","same",fitxmin,fitxmax);
    if ( fitres != 0 )  cout << warn << "Signal-only fit failed." << endm;
    else {
      mpv   = sigfit->GetParameter(1);
      sigma = sigfit->GetParameter(2);
      nsig  = (sigfit->Integral(mpv-ns*sigma,mpv+ns*sigma))/binwidth;
      ntot  = (total ->Integral(mpv-ns*sigma,mpv+ns*sigma))/binwidth;
      double idealSIG = sqrt(2*(ntot*log(1+nsig/(ntot-nsig))-nsig ));
      double SIGREAL  = sqrt(2*((nsig+nbkg)*log(1+nsig/nbkg)-nsig ));
      cout << "   Sig-only LL fit:   Chi2/ndof=" << sigfit->GetChisquare()
	   << "/" << sigfit->GetNDF() << "\tProb=" << sigfit->GetProb() << endl;
      cout << "   Ideally, n_sig=" << nsig << "  n_tot(fit)=" << ntot
	   << "  n_bkg=" << (ntot-nsig) << "  signif=" << idealSIG << endl;
      cout << "   Really, n_sig=" << nsig << "  n_tot(fit)=" << nsig+nbkg
	   << "  n_bkg=" << nbkg << "  signif=" << SIGREAL << "   ";
      sigfit->Draw("same"); }
  }

  return SIG;

}
