unsigned int compareHistograms(TH1F* nominalhisto, TH1F *modhisto,
			       bool verbose = true) {
  int nbins = nominalhisto->GetNbinsX();
  if ( nbins != modhisto->GetNbinsX() ) {
    if (verbose)
      cout << "Number of bins (" << nbins << "," << modhisto->GetNbinsX()
	   << ") do not match! Error!" << endl;
    return 1; }

  unsigned int fouls = 0;
  unsigned int maxfouls = 4;
  
  for (int ib=0;ib<=nbins && fouls<=maxfouls;++ib) {

    float bincn = nominalhisto->GetBinContent(ib);
    float bincm = modhisto->GetBinContent(ib);
    float binerr = nominalhisto->GetBinError(ib);
    if ( fabs(bincn-bincm)/binerr > 5 && fabs(bincm/bincn-1)>0.3 ) {
      fouls++;
      if (verbose) cout << "5+ sig. & >30% fluctuation in bin " << ib << endl; }
    else if ( bincm>bincn*3 || bincm<bincn/3 )
      if ( fabs(bincn-bincm)>binerr ) {
	fouls++;
	if (verbose) cout << "Factor of 3+ change in bin " << ib << endl; }
  }
  if ( fouls >= maxfouls ) {
    if (verbose) cout << "Too many problematic issues identified" << endl;
    return 2; }

  return 0;
}


void checkSampleSystematicHistos(TFile *infile, TString sampleName, TString rejectKeyword, bool verbose = false) {
  TH1F *nominal = (TH1F*)(gROOT->FindObject(sampleName));
  if (!nominal) {
    cout << "Exiting! Something is wrong, can't find base histogram" << endl;
    return; }

  int noOfBrowsables = infile->GetListOfKeys()->GetEntries();
  for (int k=0; k<noOfBrowsables; k++) {
    TString objname(infile->GetListOfKeys()->At(k)->GetName());
    if ( objname.Index(sampleName) != 0 ) continue;
    if ( objname.Index(rejectKeyword) > 0 ) continue;
    TObject *myobj = gROOT->FindObject(objname);
    if ( myobj == 0 ) {
      cout << "Exiting! Could not read " << objname << " in the file" << endl;
      return; }
    if ( ! myobj->InheritsFrom("TH1F") ) {
      cout << "Exiting! " << objname << " is not a TH1F object" << endl;
      return; }
    if ( verbose ) cout << "Handling " << objname << endl;
    if ( compareHistograms(nominal, (TH1F*)myobj, verbose) )
      cout << "Problem with systematic " << objname << endl;
  }
}

void checkSystematicHistos(TString inputfile="~/Downloads/sibg_ELE_600_3_1_allsys.root", TString rejectKeyword = "_model") {
  TFile *infile = TFile::Open(inputfile);
  if ( !infile ) return;

  int noOfBrowsables = infile->GetListOfKeys()->GetEntries();
  for (int k=0; k<noOfBrowsables; k++) {
    TString objname(infile->GetListOfKeys()->At(k)->GetName());
    if ( objname == "signal" ||
	 ( objname.Index("sample_") == 0 &&
	   TString(objname(7,objname.Length())).IsDec() ) ) {
      cout << "\nNow handling the " << objname << " and its systematics\n";
      checkSampleSystematicHistos( infile, objname, rejectKeyword ); }
  }
  infile->Close();

}
