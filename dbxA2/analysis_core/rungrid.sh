#!/bin/bash

if [ $# -lt 2 ] ; then
 echo not enough arguments
 echo $0 [samplename] [channel]
 echo samplename: ttbar, singleT,...
 echo channel : muons, electrons,..
 exit 1
fi

hn=`hostname|cut -f1 -d'.'| grep lxplus`
if [ ${#hn} -gt 1 ] ; then
 b=`fs quota | awk '{print $1}' | cut -d "%" -f1`
 if [ $b -ge 80 ]; then
  echo "problem: not enough quota"
  exit
 fi
fi

target=$1
found=0
channel=$2
paruc=$3
pardort=$4
datatype=0 # 1 is data 2 is MC

#Users should edit this part for their own settings
#User Settings ******************************************
#********************************************************

ov=80       #your job version 
un="isiral"   #your grid nickname
aver=17.7.0 # athena version to be setup
dosyst=0      # 0:dont do systematics, 1:run with the systematics
analizler="'-D 1'"   # "-FF 1" OR "-D 1"
ECM=8 
#********************************************************
#********************************************************

if [[ "$analizler" =~ "-FF " ]]; then    # can be improved to also match -FF 2
  OutputFileList="analysisd3pd.root,FF_1analysis.lhco,common.root"
elif [[ "$analizler" =~ "-D " ]]; then
   OutputFileList="lvl0.root,common.root"
else
  echo "set your analysis"
fi
HFrun=0
#*******************************************************************************
#data and MC sets included  in the script 7TeV data, Wjets, Zjets, singleT, ttbar...
source runlist.txt
set_list=`grep '="' runlist.txt | cut -f1 -d'='`
if [ ${#target} -gt 3 ] ; then
 targetval=$((${target:0:3}))
else
 targetval=$((${target:0:1}))
fi
if [ $targetval -ne 0 ] ; then
 echo searchnig for the given run: $target
# check if the suggested run # is in or not very simple
 ffound=`grep $target runlist.txt | grep $ECM  | wc -l`
 if [ $ffound -eq 0 ] ; then
   echo This is not a valid run number.
   echo Exiting...
   exit
 fi
 for s in $set_list ; do
#   echo set is: $s
    if [ ${s:0:4} == "data" ] && [ ${s##*_} != $channel ] ; then
      continue
    fi
    eval tfl=\$$s
      for r in $tfl ; do
       trn=`echo $r | cut -f2 -d'.'`
       echo $trn vs $target
       if [ $target == $trn ] ; then
          echo FOUND RUN: $r
          wrun=`echo $r| cut -f3 -d'.'|grep AlpgenJimmyW`+`echo $r| cut -f3 -d'.'|grep AlpgenW`
          if [ ${#wrun} -gt 1 ] ; then
            HFrun=1
          fi 
          fl=$r
          found=1
          targettype=${s:0:4} #if is signal or MC
          break
       fi
      done
     if [ $found -eq 1 ] ; then
      break
     fi
 done
 if [ $found -eq 0 ] ; then
  echo Run number found but not the channel.
  echo Exiting..
  exit 
 fi
else
 targettype=${target:0:4} #if is signal or MC 
fi
if [ $targettype == "data" ] ; then
 datatype=1
else
 datatype=2
fi

if [ $channel == "muons" ] ; then
 muons=$datatype                     #set muon trigger 2 or 1
 electrons=0                         #set electron triger to 0
elif [ $channel == "electrons" ] ; then
 muons=0                             #set muon trigger to 0
 electrons=$datatype                 #set electron trigger 2 or 1
else 
 echo no such channel
 exit 2
fi

echo working on $target for $channel with TRGtype  $datatype
echo now fixing the trigger type and electron MET cuts on the FF datacards.
for f in `ls FF*-card.txt` ; do
 
 cat $f | grep -v "Trigger Type" > ${f}-bkp #copy FF-Card.txt

 #append trigger info to the card file (FF-card.txt)
 echo "TRGe = $electrons # electron Trigger Type: 0=dont trigger, 1=1st trigger (data) 2=2nd trigger (MC)" >>${f}-bkp
 echo "TRGm = $muons #     muon Trigger Type: 0=dont trigger, 1=1st trigger (data) 2=2nd trigger (MC)" >>${f}-bkp
 mv ${f}-bkp ${f}
done

#--------------------------------------------

if [ $found -eq 0 ] ; then 
  if [ $target == "dataALL" ] ; then
  for s in $set_list ; do
   if [ ${s:0:4} == "data" ] && [ ${s##*_} == $channel ] ; then
    eval tfl=\$$s
    fl=$fl" "$tfl
   fi
  done
  found=1
  fi 
  
  if [ $targettype == "data" ] ; then 
   target=${target}_${channel}
  fi
  
  for s in $set_list ; do
   if [ $s == $target ] ; then
    found=1
    echo will run for: $s
    eval fl=\$$target
  ####################################
   fi
  done
  
  if [ $found -eq 0 ] ; then
    echo 
    echo ERROR _____________________________________ unknown data or MC set: $target 
    echo Please use one of the below:
    echo $set_list
    echo ___________________________________________________________________
    exit
  fi 
# --------we now have a valid data set
fi

submitdir=submit_logs #the folder in which log files will reside (logs for prun command.. see below )
if [ ! -d $submitdir ];then
 mkdir $submitdir
fi

#loop over datasets (for a ttbar "list" for example)
for f in $fl ; do 

ecmmatch=0
# beware of the 7 or 8 TeV selection
if [[ "$f" =~ m*${ECM}TeV* ]] ; then
    echo "matched " ${ECM}" TeV"
    ecmmatch=1
else
    echo "no match at " ${ECM}" TeV" for file " " $f
fi

if [ $ecmmatch -eq 1 ]; then
  # exn=`echo $f | egrep -o '[[:digit:]]{6}' | head -n1` #extract run number from the filename 
  exn=`echo $f | cut -f2 -d'.' | head -n1` #extract run number from the filename 


  # if [ -e ./${submitdir}/${exn}_submit_ov${ov}.log ];then # if log file exist backup it first
  #  mv ./${submitdir}/${exn}_submit_ov${ov}.log ./${submitdir}//${exn}_submit_ov${ov}.log.old
  # fi
  # nope we should append.
  
  runmodif=0
  # is this a data d3pd?
  if [ $datatype -eq 1 ] ; then
   runmodif=1
  fi
  # is this a Wjets sample?
  if [ $target == "Wjets" ] || [ $target == "Whc" ] || [ $target == "Wh" ] || [ $HFrun -eq 1 ]; then
   runmodif=2
  fi
  # do we want to run with systematics
  if [ $dosyst -eq 1 ] ; then
   if [ $datatype -eq 2 ] ; then
    runmodif=$(( $runmodif + 4 )) 
   fi
  fi
  # do we want to run with 2012 or not
  if [ $ECM -eq 8 ] ; then
    runmodif=$(( $runmodif + 8 ))
  fi
  
   echo "Running on data $f with user_set_ov=$ov " >>./${submitdir}/${exn}_submit_ov${ov}.log 2>&1 # put a header info
   echo "You can later fetch results  manually from" user.${un}.analysisD${ov}D${exn} >>./${submitdir}/${exn}_submit_ov${ov}.log
   echo "*********************************************" >>./${submitdir}/${exn}_submit_ov${ov}.log
  dryrun=0
  matchstr=""
  if [ ${#paruc} -ne 0 ] ; then
    if [ $paruc == "dryrun" ] ; then
       dryrun=1
    else
       matchstr="--match NTUP*${paruc}*.root.*"
    fi
  fi

if [ ${#pardort} -ne 0 ] ; then
  if [ $pardort == "dryrun" ] ; then
     dryrun=1
  else
     matchstr="--match NTUP*${pardort}*.root.*"
  fi
fi




  if [ $dryrun -eq 1 ] ; then
    echo Dryrun:  $f "    ov:"$ov "    modif:" $runmodif
    echo prun $matchstr --excludedSite=ANALY_MWT2_SL6,ANALY_DESY-ZN,ANALY_BNL_LONG,ANALY_BNL_SHORT,ANALY_CSCS,ANALY_JINR,ANALY_FZK,ANALY_SFU,ANALY_GOEGRID,ANALY_NIKHEF-ELPROD  --inDS $f --exec "echo %IN > input.txt ; ./ntra.sh $analizler $runmodif " --bexec "root_analysisd3pdmake.sh" --athenaTag=${aver} --outDS user.${un}.analysisD${ov}D${exn} --outputs ${OutputFileList} --nFilesPerJob 1 --extFile ilumicalc_histograms_None_178044-184169_new.root, TopCalibrations_EPS_2011_v0.root, mu_mc10b.root 
  else
  
  #build&execute prun command (below) pipe stdout stderr in log files
   echo submitting $f
   prun $matchstr --excludedSite=ANALY_BNL_SHORT,ANALY_MWT2_SL6,ANALY_ORNL_Titan,ANALY_DESY-ZN,ANALY_BU_ATLAS_Tier2_SL6,ANALY_HU_ATLAS_Tier2,ANALY_DESY-HH,ANALY_ROMANIA07,ANALY_ECDF_SL6,ANALY_IFAE,ANALY_SLAC_LMEM,ANALY_SWT2_CPB,ANALY_AGLT2_SL6,ANALY_BNL_LONG,ANALY_CSCS,ANALY_JINR,ANALY_FZK,ANALY_SFU,ANALY_GOEGRID,ANALY_NIKHEF-ELPROD --inDS $f --exec "echo %IN > input.txt ; ./ntra.sh $analizler $runmodif " --bexec "root_analysisd3pdmake.sh" --cmtConfig=x86_64-slc5-gcc43-opt --rootVer=5.34/07 --outDS user.${un}.analysisD${ov}D${exn} --outputs ${OutputFileList} --nFilesPerJob 1 --extFile ilumicalc_histograms_None_178044-184169_new.root, TopCalibrations_EPS_2011_v0.root, mu_mc10b.root >>./${submitdir}/${exn}_submit_ov${ov}.log 2>&1
  fi
  sleep 2; 
  quotachk=`grep -i "quota" ./${submitdir}/${exn}_submit_ov${ov}.log`
  if  [ ${#quotachk} -ne 0 ] ; then
   echo '-------- QUOTA PROBLEM ---------- '
   exit 
  fi
  failchk=`grep -i "failed" ./${submitdir}/${exn}_submit_ov${ov}.log`
  if  [ ${#failchk} -ne 0 ] ; then
   echo '-------- CONNECTION PROBLEM ---------- '
   exit 
  fi
fi # ecmmatch
done
