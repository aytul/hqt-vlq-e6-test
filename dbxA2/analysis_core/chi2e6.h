//How to define an external function?
//write such a .h file and include it in dbxCut.cpp
//
#ifndef DBX_CHI2E6_H
#define DBX_CHI2E6_H

//#define _CLV_
#ifdef _CLV_
#define DEBUG(a) std::cout<<a
#else
#define DEBUG(a)
#endif


class dbxCutChi2E6 : public dbxCut {
 public:
      dbxCutChi2E6( ): dbxCut("}Chi2E6"){}
      dbxCutChi2E6(std::vector<int> ts, std::vector<int> is, int v ): dbxCut("}Chi2E6",ts,is,v){}

      bool select(AnalysisObjects *ao){
          float result;
          if (getOp()=="~=" || getOp()=="~!") {
            clearFoundResults();
            result=calc(ao);
            return true;
          } else {
            result=calc(ao);
          }
          return (Ccompare(result) ); 
      }
// 0 is muon // 1 is electron // 8 is gammas      
// 2 is any jet // 3 is a b jet // 4 is light jet 7 met
//-------------------------- public variables of the class to be read and fed by the BP code
// E6 Specific Parameters
    int is_this_zjetsCR;
    double MinRDist;
    double Herror;
    double DZerror;
    double DHerror;
    double MinLeadJetPt;
    double MinSubJetPt;
    void setSpecialVariables(int abc, double *varlist){ 
       is_this_zjetsCR = (int) varlist[0];
          MinRDist     =  varlist[1];
          Herror       =  varlist[2];
          DHerror      =  varlist[3];
          DZerror      =  varlist[4];
          MinLeadJetPt =  varlist[5];
          MinSubJetPt  =  varlist[6];

      std::cout <<"------\n";
      std::cout << Herror <<"\n";
      std::cout << DZerror <<"\n";
      std::cout << DHerror <<"\n";
      std::cout << MinRDist <<"\n";
      std::cout << MinLeadJetPt <<"\n";
      std::cout << MinSubJetPt <<"\n";

     }

///////////////////////////////////////////////
  float calc(AnalysisObjects *ao){
        float retval;
        double eventchi=999999;
        double Hchi=1000;
        double Dchi=1000;
        dbxParticle *Hbos;
        dbxParticle *DfZ;
        dbxParticle *DfH;
        Hbos=NULL;
        dbxParticle ZBos; 
        DfH=NULL;
        DfZ=NULL;
        int ForceBtag=0;

        const int unk_MAX=6;
        int found_idx[unk_MAX]={-1, -1, -1, -1, -1, -1}, ip_N[unk_MAX]={0,0,0,0,0,0}; int pp_target[unk_MAX]={-1, -1, -1, -1, -1, -1}; // assume max variables
        int search_types[unk_MAX]={-1, -1, -1, -1, -1, -1}, oldvals[unk_MAX]={0,0,0,0,0,0};

/*
HerrorM = 17 # Higgs Chisquare sigma Value of Muon
DHerrorM = 54 # De from Higgs Chisquare sigma Value of Muon
DZerrorM = 42 # De from Z Chisquare sigma Value of Muon
HerrorE = 18 # Higgs Chisquare sigma Value of Elec
DHerrorE = 62 # De from Higgs Chisquare sigma Value of Elec
DZerrorE = 35 # De from Z Chisquare sigma Value of Elec
*/
Herror = 18 ;
DHerror = 62 ;
DZerror = 35 ;
        std::vector<int> * param=getParams();
        int TrigType=param->at(0);

        if ((TrigType>0) && (TrigType<4) ) { // muons
           ZBos=ao->muos[0]+ao->muos[1]; 
        }
        if ((TrigType>3) && (TrigType<16)) { // electrons
           ZBos=ao->eles[0]+ao->eles[1]; 
        }

        for (int pp=0; pp<getParticleIndex() && pp<unk_MAX; pp++ ){ 
             pp_target[pp] = pp;                       // now I know which particle index to replace
               oldvals[pp] = getParticleIndex(pp);
          search_types[pp] = getParticleType(pp);
        }


        if (getOp()!="~=" && getOp()!="~!") { // are we NOT called as a minimizer.
          if ( (getFoundVector()).size() == 0 ){   std::cout<< " You need to call a minimizer first : ~= or != \n"; }

          unsigned int jet1, jet2, jet3, jet4;
          std::vector<int> found_list=getFoundVector();
          if (found_list.size()<4) return 999999.9999;
          jet1=found_list[0];       
          jet2=found_list[1];
          jet3=found_list[2];
          jet4=found_list[3];
 
          DEBUG(jet1<<" "<<jet2 <<" "<< jet3<<" "<<jet4<<"\t");

          if (jet1+jet2+jet3+jet4 > 9999) return 99999; // chi2 failed in previous step

          dbxParticle HRecon = ao->jets.at(jet1) + ao->jets.at(jet2);
          double chisqH = ((HRecon.lv().M()-126)*(HRecon.lv().M()-126)/(Herror*Herror));
          dbxParticle DeHiggs = HRecon + ao->jets.at(jet3);
          dbxParticle DeZ= ZBos + ao->jets.at(jet4);
          double chisqD = (DeHiggs.lv().M()-DeZ.lv().M())*(DeHiggs.lv().M()-DeZ.lv().M())/(DHerror*DHerror + DZerror*DZerror);
          retval=chisqD+chisqH;

          DEBUG("Cchi2="<<retval<<"\n");
          return retval;
        }

        for(unsigned int jet1=0;jet1<(ao->jets.size()-1); jet1++) {
          for(unsigned int jet2=jet1+1;jet2<ao->jets.size(); jet2++) {

             if(ForceBtag==1 && ao->jets.at(jet1).isbtagged_77() == 0 && ao->jets.at(jet2).isbtagged_77() == 0) continue; //
             if(ForceBtag==2 && ( ao->jets.at(jet1).isbtagged_77() == 0  || ao->jets.at(jet2).isbtagged_77() == 0)) continue;//
             dbxParticle HRecon = ao->jets.at(jet1) + ao->jets.at(jet2);
             double chisqH = ((HRecon.lv().M()-126)*(HRecon.lv().M()-126)/(Herror*Herror));

             if (chisqH>eventchi ) {
                         HRecon.~dbxParticle();
                         continue;
             }

             for ( unsigned int jet3=0; (jet3<(ao->jets.size())); jet3++) {
                  if(jet3==jet2 || jet3==jet1) continue;
                  for ( unsigned int jet4=0; (jet4<(ao->jets.size()));jet4++) {
                       if(jet4==jet2 || jet4==jet1 || jet4==jet3) continue;

                       if ( (ao->jets.at(jet4).lv().Pt()<MinLeadJetPt || ao->jets.at(jet3).lv().Pt()<MinSubJetPt )
                          && (ao->jets.at(jet3).lv().Pt()<MinLeadJetPt || ao->jets.at(jet4).lv().Pt()<MinSubJetPt ) ) continue;
                       if(ao->jets.at(jet3).lv().DeltaR(HRecon.lv())<MinRDist) continue;
                       if(ao->jets.at(jet4).lv().DeltaR(ZBos.lv())<MinRDist) continue;

                       dbxParticle DeHiggs = HRecon + ao->jets.at(jet3);
                       dbxParticle DeZ= ZBos + ao->jets.at(jet4);
                       double chisqD = (DeHiggs.lv().M()-DeZ.lv().M())*(DeHiggs.lv().M()-DeZ.lv().M())/(DHerror*DHerror + DZerror*DZerror);

                       if ((chisqD+chisqH)>eventchi ) {
                            DeHiggs.~dbxParticle();
                            DeZ.~dbxParticle();
                            continue;
                       }
                       eventchi=chisqD+chisqH;
                       Hchi = chisqH;
                       Dchi = chisqD;
                       found_idx[0]=jet1;   //higgs1
                       found_idx[1]=jet2;   //higgs2
                       found_idx[2]=jet3;   //higgs associated jet
                       found_idx[3]=jet4;   //Z     associated jet
                       delete Hbos;
                       delete DfZ;
                       delete DfH;
                       Hbos = new dbxParticle(HRecon);
                       DfZ = new dbxParticle(DeZ);
                       DfH = new dbxParticle(DeHiggs);
                  }
             }
             HRecon.~dbxParticle();
          }//loop over jet2
        }//loop over jet1

        if (Hbos == NULL) { retval=99999999.999; 
        } else{              retval=eventchi;
        }

// put back the best values
          clearFoundVector();
          clearOrigFoundVector();

          for (int ik=0; ik<getSearchableType(); ik++) {
                             DEBUG("Best@"<<  pp_target[ik] <<" :"<<found_idx[ik] << ",Oi"<<oldvals[ik]<<" ,Ty:"<<search_types[ik]<<"  ");
                             addFoundIndex(   found_idx[ik] ); // I store
                             addFoundType (search_types[ik] );
                             setParticleIndex(pp_target[ik] , oldvals[ik] ); // I replaced dummy particle index with VALUE
                             addOrigFoundVector(              oldvals[ik] );
          }

   DEBUG(getFoundVector().size()<<"\t chi2="<<retval<<"\n");
   return retval;
   }
private:
};

//#undef _CLV_

#endif
