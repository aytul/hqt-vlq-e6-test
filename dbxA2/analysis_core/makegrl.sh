#!/bin/bash
# ./makegrl.sh [one-or-more-grl-xml-files]

if [ -z "$1" ]; then
   echo Usage: $0 [one-or-more-grl-xml-files]
   echo ==\> Since no input was given this time, you will get an empty grl.C now.
fi

ls $* >& /dev/null  && \
( for file in $*; do
awk '
 /<LumiBlockCollection>/{on=1;run=0; if (fn!=FILENAME) {fn=FILENAME; print "//Runs from",fn;}}
 /Run/{if (on) {sub(">"," "); sub("</"," "); if ( $4=="") {run=$2;} else {run=$4} }}
 /LBRange/{if (on) {if (run==0) {print "error";exit(1)} gsub("\""," "); print "{"run","$3","$5"},";}}
 /<\/LumiBlockCollection>/{on=0}
 END{if (on) {print"error";exit(1)}}' $file | sort -n
done ) |\
awk '
 BEGIN{print " #ifndef __GRL__" 
print " #define __GRL__" 
print "static const int grl[][3] = {"}
 { print }
 END {print "{0,0,0}};"; print "bool isGoodRun(int run,int lb) { static int prevrun=-1, prevlb=-1; static bool prevresult=true; if ( prevrun==run && prevlb==lb ) return prevresult; else { prevrun=run; prevlb=lb; } for (int i=0;;++i) { if (grl[i][0]==run && lb>=grl[i][1] && lb<=grl[i][2]) {prevresult=true;return true;} if (grl[i][0]==0) {prevresult=false;return false;} }}" 
print "#endif" } 
' > grl.C

for f in $*; do
 echo 'string grl_filename="'$f'";' >> grl.C
done
