#ifndef __PUNCH_H
#define __PUNCH_H

#include<string>
#include<sstream>

// improved atof that can also handle binary strings ( 0b0110 = 6 )
double satof( std::string str );

// A semi-smart generic variable, handling strings and numerics roughly
//   on the same footing.
struct punch { 
  double val; 
  std::string str;
  operator double() const; // typecasting to double
  operator std::string() const; // casting to string
  // the "constructors" below are really for typecasting
  //   if the const char* ctor is absent, int ctor can also be dropped
  //   we will also need one for long ints, again thanks to char* ctor
  punch(unsigned long v=0);
  punch(int v=0);
  punch(double v=0);
  punch(std::string s);
  punch(const char* s);
  friend std::ostream& operator<<(std::ostream& os, const punch& p);
};

// function for unit testing punch
void testPunch();

#endif
