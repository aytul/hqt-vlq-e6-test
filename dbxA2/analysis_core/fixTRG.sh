#!/bin/bash
trgt=$1

# initial values
nFF=0
nE61=0
nSkeleton=0
nE62=0

while [ $# -gt 1 ] ; do
case $1 in
-FF)   nFF=$2 ; shift 2 ;;
-E61)  nE61=$2 ; shift 2 ;;
-E62)  nE62=$2 ; shift 2 ;;
-Skeleton) nSkeleton=$2 ; shift 2 ;;
*) shift 1 ;;
esac
done
#opt possibilities are E61, FF, N4, Skeleton

echo FF: $nFF
echo E61: $nE61
echo E62: $nE61
echo Skeleton: $nSkeleton
tfilelist=" "

for ((i=1; i<=$nFF; i++)) { 
  tfilelist+="FF_${i}-card.txt " 
}
for ((i=1; i<=$nE61; i++)) { 
  tfilelist+="E61_${i}-card.txt "
}
for ((i=1; i<=$nE62; i++)) { 
  tfilelist+="E62_${i}-card.txt "
}
for ((i=1; i<=$nSkeleton; i++)) { 
  tfilelist+="Skeleton_${i}-card.txt " 
}

for tfile in $tfilelist ; do

echo fixing $tfile

#find  e or mu
mu=`grep 'TRGm' ${tfile}| awk '{print $3}'`
el=`grep 'TRGe' ${tfile}| awk '{print $3}'`

if [ $mu -eq 0 ] ; then
 echo electron channel is selected
 cat ${tfile} | grep -v 'TRGe' > tmp_1
 echo "TRGe = ${trgt} # electron Trigger Type: 0=dont trigger, 1=1st trigger (data) 2=2nd trigger (MC)" >> tmp_1
fi
if [ $el -eq 0 ] ; then
 echo muon channel is selected
 cat ${tfile} | grep -v 'TRGm' > tmp_1
 echo "TRGm = ${trgt} #     muon Trigger Type: 0=dont trigger, 1=1st trigger (data) 2=2nd trigger (MC)" >> tmp_1
fi

mv tmp_1 ${tfile}
done
