#!/bin/bash
debug=0
ifn=f1
ofn=mylumi.xml
trigger=`cat ANA_DEFS | grep -v SUM | grep CHANNEL| grep -v "#" |cut -f3 -d' '`
numarg=$#

if [[ $trigger == 1 ]]
    then 
	echo "Electron trigger read"
	trig_name=EF_e20_medium
	else 
	echo "Muon trigger read"
	trig_name=EF_mu18
fi

if [ $numarg -le 0 ]; then
 echo run as $0 1.root [2.root] ...
 exit 1
fi 



b=`which iLumiCalc.exe`
c=${#b}
if [ $c -eq 0 ]; then
 echo iLumiCalc.exe not found. Please source an ATLAS release.
fi

mdl_name=`cat ANA_DEFS| grep MODEL | awk '{print $3}'`

 echo "
{
${mdl_name}->cd();
rntuple->SetScanField(0);
rntuple->Scan();
}
" > dump.C

root -l $1 -q -x dump.C  > f1

echo Ntuple dump achieved.

args=("$@")
for ((i=1; i<$numarg; i++)){
 root -l ${args[$i]} -q -x dump.C  >> f1
}


cat ${ifn} |grep -v Row | sort -n -k4 -k6| awk '{ if ($6!="") print $4,$6}' > f2

prolog="
<?xml version=\"1.0\" ?>
<!DOCTYPE LumiRangeCollection
  SYSTEM 'http://atlas-runquery.cern.ch/LumiRangeCollection.dtd'>
<!-- Good-runs-list created by AtlRunQuery.py on 2011-05-03 18:42:49.714576 -->
<LumiRangeCollection>
   <NamedLumiRange>
      <Name>Top_allchannels</Name>
      <Version>2.1</Version>
      <LumiBlockCollection>
"

epilog='
</LumiBlockCollection>
</NamedLumiRange>
</LumiRangeCollection>
'


echo $prolog > $ofn

prn=0;
pln=0;
sln=-2;

while read line
do
 testln=`expr $pln + 1`;
 rn=`echo $line| awk '{print $1}'`
 ln=`echo $line| awk '{print $2}'`
 if [ $debug -eq 1 ]; then  
   echo READ $line
 fi



 if [ $ln -ne $testln ] && [ $prn -ne 0 ]; then
  if [ $debug -eq 1 ]; then  
   echo new series at run $prn : from $sln to $pln AND current run:$rn  lb:$ln
  fi
 

  echo "         <LBRange Start=\"${sln}\" End=\"${pln}\"/>" >> $ofn
  sln=$ln
  pln=$ln

  if [ $prn -ne $rn ]; then
# this is a new run with a series
   echo "      </LumiBlockCollection>
      <LumiBlockCollection>" >> $ofn

  fi 

 fi

 if [ $ln -eq $testln ] && [ $prn -eq $rn ]; then
  pln=$ln
 fi 

 if [ $prn -ne $rn ]; then
  if [ $debug -eq 1 ]; then  
   echo new run, lumi: $rn $lb
  fi
   echo "        <Run>${rn}</Run>" >> $ofn
  
  prn=$rn;
  pln=$ln;
  sln=$ln;
 fi


done < "f2"


echo XML body made.

 if [ $debug -eq 1 ]; then
  echo final: $rn $ln
  echo new series at run $rn : from $sln to $ln
 fi
  if [ $prn -ne $rn ]; then
# this is a new run with a series
   echo "        <Run>${prn}</Run>" >> $ofn
  fi 
  echo "         <LBRange Start=\"${sln}\" End=\"${pln}\"/>" >>$ofn



  echo $epilog >> $ofn



 lumi_tag=`cat ANA_DEFS| grep LUMI_TAG | awk '{print $3}'`
 live_trig=`cat ANA_DEFS| grep LIVE_TAG | awk '{print $3}'` 
 echo -n "TOTAL integrated Lumi (pb-1) is: "
 #iLumiCalc.exe --lumitag=$lumi_tag --livetrigger=$live_trig --trigger=$trig_name --xml=$ofn > iLumi.out # 2011
 iLumiCalc.exe --lumitag=$lumi_tag --livetrigger=$live_trig --xml=$ofn > iLumi.out
 cat iLumi.out | grep "Total IntL recorded" | cut -f3 -d":"| awk '{ print $1/1e6}'

