#include "TH2.h"
class cutncount {

public:
  cutncount(TH2* hs, TH2* hb, TH2* hd) {
    sh= (TH2 *)hs->Clone(); 
    bh= (TH2 *)hb->Clone(); 
    dh= (TH2 *)hd->Clone();
    nbx=hs->GetNbinsX()+1;
    nby=hs->GetNbinsY()+1;
    a_best=0; b_best=0; p_best=0;
  };
  ~cutncount(){};
  void setxlimits(double amin, double amax){
   xmx=amax; xmn=amin; 
  };
  void setylimits(double amin, double amax){
   ymx=amax; ymn=amin; 
  };
  double scanprob(double stepx, double stepy, int selector);  
  double calcprob(double a, double b, int sel);  
  double getbestp(){ return p_best; }; 
  double getbesta(){ return a_best; };
  double getbestb(){ return b_best; }; 
  void print_best(){ cout << "best line y=ax+b a:"<< a_best<<" b:"<<b_best<< "  maxP:"<<p_best<<endl; };

private:
  TH2*sh, *bh, *dh;
  double a_best, b_best, p_best;
  double xmx, xmn;
  double ymx, ymn;
  int xstep, ystep;
  int nbx, nby;
};

double cutncount::calcprob( double a, double b, int selector) {
    double sums=0;
    double sumb=0;
    double sumd=0;
    double x,y;
    double prob=0;
// we need protection against different binnings for x and y
    for (int j=0;j<nby;j++) {
     for (int i=0;i<nbx;i++) {
      y=sh->GetYaxis()->GetBinCenter(j);
      x=sh->GetXaxis()->GetBinCenter(i);
      if (y > a*x+b) {
       sums+= sh->GetBinContent(i,j); 
       sumb+= bh->GetBinContent(i,j);
      } 
     }
    }
// we know how many signal and signal+background candidates we have passing the triangular cut
  if (selector ==1 ) {
// Poisson(k,l): k:seen,  l:expected
   prob=TMath::Poisson(sums+sumb, sumb);
  } elseif (selector==2) {
   prob=sums/sumb;
  } else {
    for (int j=0;j<nby;j++) {
     for (int i=0;i<nbx;i++) {
      y=sh->GetYaxis()->GetBinCenter(j);
      x=sh->GetXaxis()->GetBinCenter(i);
      if (y > a*x+b) {
       sumd+= dh->GetBinContent(i,j);
      }
     }
    }
   cout << setprecision(4);
   cout << "Si:" << sums << " Bg:" <<sumb << "  Tot exp:"<< sums+sumb<<" Data:"<<sumd;
   cout << "  Poisson-Bg:"<<TMath::Poisson(sumd, sumb);
   cout << " Poisson-Si:"<<TMath::Poisson(sumd, sumb+sums); 
   cout << " Bg/Si Ratio:"<<TMath::Poisson(sumd, sumb)/TMath::Poisson(sumd, sumb+sums)<<endl; 
  }
    
   return prob;    
};


double cutncount::scanprob(double stepx, double stepy, int sel) {
   double p=0.0;
   double a,b;
   for (float x=xmn; x<=xmx; x+=stepx)
    for (float y=ymn; y<=ymx; y+=stepy){
     b=y; a=-b/x;
     p=calcprob(a,b, sel);
     if (p>p_best) { 
       a_best=a; b_best=b; p_best=p;
    }
   }
   print_best();
   return p_best;
};


