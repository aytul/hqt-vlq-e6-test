#include "../analysis_core/CrossingFinder.C"

void dene_crossing() {

 int npx=5;
 int npy=6;

 double lin1x[5];
 double lin1y[5];
 double lin2x[6];
 double lin2y[6];



 lin1x[0]=200; lin1x[1]=250; lin1x[2]=300; lin1x[3]=350; lin1x[4]=400;
 lin1y[0]=10; lin1y[1]=20; lin1y[2]=12; lin1y[3]=13; lin1y[4]=14;

 lin2x[0]=200; lin2x[1]=240; lin2x[2]=260; lin2x[3]=300; lin2x[4]=335; lin2x[5]=450;
 //lin2y[0]=20; lin2y[1]=24; lin2y[2]=26; lin2y[3]=30; lin2y[4]=33; lin2y[5]=45;
 lin2y[0]=20; lin2y[1]=14; lin2y[2]=16; lin2y[3]=10; lin2y[4]=13; lin2y[5]=15;

/*
double tlin1x[11];
tlin1x[0]=250; tlin1x[1]=300; tlin1x[2]=350; tlin1x[3]=400; tlin1x[4]=450; tlin1x[5]=500; 
tlin1x[6]=550; tlin1x[7]=600; tlin1x[8]=650; tlin1x[9]=700; tlin1x[10]=800; 
double tlin1y[11];
tlin1y[0]=22.52; tlin1y[1]=8.023; tlin1y[2]=3.241; tlin1y[3]=1.436; tlin1y[4]=0.683; tlin1y[5]=0.343; 
tlin1y[6]=0.18; tlin1y[7]=0.098; tlin1y[8]=0.0495; tlin1y[9]=0.0283, tlin1y[10]=0.0098; 
*/

double tlin1x[8];
tlin1x[0]=250; tlin1x[1]=300; tlin1x[2]=350; tlin1x[3]=400; tlin1x[4]=450; tlin1x[5]=500; 
tlin1x[6]=550; tlin1x[7]=600; 
double tlin1y[8];
tlin1y[0]=22.52; tlin1y[1]=8.023; tlin1y[2]=3.241; tlin1y[3]=1.436; tlin1y[4]=0.683; tlin1y[5]=0.343; 
tlin1y[6]=0.18; tlin1y[7]=0.098;

double tlin2x[9];
tlin2x[0]=300; tlin2x[1]=350; tlin2x[2]=400; tlin2x[3]=450; tlin2x[4]=500; tlin2x[5]=600; 
tlin2x[6]=650; tlin2x[7]=700; tlin2x[8]=800; 
double tlin2y[9];
tlin2y[0]=1.28368; tlin2y[1]=0.9723; tlin2y[2]=0.73236; tlin2y[3]=0.56689; tlin2y[4]=0.46991; tlin2y[5]=0.30772; 
tlin2y[6]=0.249975; tlin2y[7]=0.253285; tlin2y[8]=0.25039; 

// crossingFinder cross(npx, lin1x, lin1y, npy, lin2x, lin2y);
 crossingFinder cross(8, tlin1x, tlin1y, 9, tlin2x, tlin2y);
 cross.findCrossing(1);

TCanvas* c4 = new TCanvas("LimitWithErrors","LimitWithErrors");
         c4->Draw(); 
TGraph* g1 = new TGraph(11,tlin1x,tlin1y);
        g1->SetLineWidth(2);
        g1->SetLineColor(2);
TGraph* g2 = new TGraph(9,tlin2x,tlin2y);


g2->Draw("alp3");
g1->Draw("lp3");
} 
