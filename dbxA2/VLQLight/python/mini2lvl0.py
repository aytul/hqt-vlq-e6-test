#!/usr/bin/env python

#This program dumps mini-ntuples into dbxA lvl0 files......
#Author     :       Serhat Istin
#Mail       :       istin@cern.ch


import optparse
import os
from os.path import basename
from glob import glob
#and import some regex  
import re
import ROOT
from ROOT import gROOT
ROOT.gROOT.Macro( '$ROOTCOREDIR/scripts/load_packages.C' )
from ROOT import AddressOf,TFile,TTree,TDirectory,TopMiniData,evt_data

#the hack below was needed to store Int_t branch adresses 
gROOT.ProcessLine(
    "struct channels_str{\
     Int_t           ee_2016;\
     Int_t          mumu_2016;\
     Int_t           ee_2015;\
     Int_t           mumu_2015;\
     Int_t           EventNumber;\
     };" )


SIG_NOINPUTFILE=-1
SIG_SUCCESS=0
GeV=1000.

if os.environ.has_key("ROOTCOREBIN"):
  rootcorebin=os.environ["ROOTCOREBIN"]
else:
  print "Error.ROOTCORE not setup!"
  os.sys.exit(-1)



#The below method automatically finds TDirectories and copies them to your target file
#**with all of their contents automatically...

#Each minintuple has a directory containing its selection Histograms.
#Saving those hostograms plays a vital role in normalising MC samples later!
#the directories have the same names with the event selections..
def WriteSelections(sourcefile,targetfile,verbose=False):
    for key1 in sourcefile.GetListOfKeys():
        sourcefile.cd()
        classname=key1.GetClassName()
        CL=gROOT.GetClass(classname)
        if CL.InheritsFrom("TDirectory"):
            if verbose:
                print("Writing Selection Histograms for "+key1.GetName())
            obj=key1.ReadObj()
            targetfile.cd()
            targetfile.mkdir(key1.GetName())
            sourcefile.cd(key1.GetName())
            for key2 in gROOT.CurrentDirectory().GetListOfKeys():
                obj=key2.ReadObj()
                targetfile.cd(key1.GetName())
                obj.Write()
# S.I : one can implement this recursively to get a deep copy of a TDirectory inside a root file...                
"""
Below method converts a single mini ntuple root file into a lvl0 rootfile
Resulting lvl0 file is automatically named
"""
def convert(inputfile,treename,isObjSys,verbose=False):    
    #if not os.path.isfile(inputfile):
    #    print("Error! No such file: "+inputfile)
    #    os.sys.exit(SIG_NOINPUTFILE)
    #guard above is commented out because python method 'isFile' is useless when crm:// on grid W.Ns
    print("Converting file "+inputfile)
    f=TFile.Open(inputfile,"READ")
    f.cd()
    minitree=f.Get(treename)
    tmd=TopMiniData(minitree)
    Nentries=tmd.fChain.GetEntries()

    
    anevt=evt_data()
    outname_ee=basename(inputfile).split(".root")[0]+"-"+treename+"-lvl0_ee.root"
    outname_mm=basename(inputfile).split(".root")[0]+"-"+treename+"-lvl0_mm.root"
    outfile_ee=TFile(outname_ee,"RECREATE")
    outfile_mm=TFile(outname_mm,"RECREATE")
    dbxEvent=tmd.DBXEvent()
    ttsave_ee=TTree("nt_tree", "nt_tree")
    ttsave_mm=TTree("nt_tree", "nt_tree")
    ttsave_ee.SetDirectory(outfile_ee)
    ttsave_mm.SetDirectory(outfile_mm)
    
    channels=ROOT.channels_str()
    #ttsave.Branch('channels',channels)
    
    ttsave_ee.Branch("dbxAsave", dbxEvent)
    ttsave_mm.Branch("dbxAsave", dbxEvent)
    
    ttsave_ee.SetAutoSave(1000000)
    ttsave_mm.SetAutoSave(1000000)
    icurr=0
    print("FOUND "+str(tmd.fChain.GetEntries())+" ENTRIES !")
    for i in range(Nentries):
        if (i%1000==0 and verbose):
            print("Converting Event "+str(i))
        tmd.fChain.GetEntry(i)
        channels.ee_2015=tmd.ee_2015
        channels.ee_2016=tmd.ee_2016
        channels.mumu_2015=tmd.mumu_2015
        channels.mumu_2016=tmd.mumu_2016
        channels.EventNumber=tmd.eventNumber
        tmd.toDBXEvent(isObjSys)
        if tmd.ee_2015==1 or tmd.ee_2016==1:
            ttsave_ee.Fill()
        if tmd.mumu_2015==1 or tmd.mumu_2016==1:
            ttsave_mm.Fill()
        icurr+=1
    #outfile.cd()
    outfile_ee.Write()
    outfile_mm.Write()
    WriteSelections(f,outfile_ee,verbose)
    WriteSelections(f,outfile_mm,verbose)
    outfile_ee.Close()
    outfile_mm.Close()
def main():
    
    commparser = optparse.OptionParser()
    commparser.add_option('-i', '--inputfile',help="a single input top mini ntuple file (Mandatory)",dest="i",metavar='FILE')
    commparser.add_option('-f', '--filelist',help="text file containing a list of root files to be converted",dest="f",metavar='FILE')
    commparser.add_option('-d', '--directory',help="input top mini ntuple directory (optional)",dest="d",metavar='DIRECTORY')
    commparser.add_option('-O', '--objSys',help="Name of the object systematic to dump (optional/default is nominal)",dest="O",metavar='STRING')
    commparser.add_option('-v', '--verbose',help="verbose mode (optional)",dest="v",metavar='FLAG',action='store_true')
    (options, args) = commparser.parse_args()
    ####End of Cammandline Parsing #########
    import subprocess
    #To ignore INFO messages, 1001 is enough. To ignore WARNING it has to be above 2001. 
    #To ignore ERROR it has to be above 3001. Check what level the printouts are and set it as appropriate.
    ROOT.gErrorIgnoreLevel=3003
    
    if options.f is not None and options.i is not None:
        print("Error! Both -f and -i cannot be supplied!")
        commparser.print_help()
        os.sys.exit(-1)
    
    if options.i is None and options.d is None and options.f is None:
        commparser.print_help()
        os.sys.exit(-1)
    verbose=False
    if options.v:
        verbose=True
        ROOT.gErrorIgnoreLevel=1001
    TreeName="nominal"
    isObjSys=False
    if options.O is not None and options.O!="nominal":
        TreeName=options.O
        isObjSys=True
   
   #S.I removed the below guards as they blow up while on grid WNs because $ROOTCORE var is not correctly handled there and such
   #ObjectSystematics=[line.rstrip('\n') for line in open(rootcorebin+'/../VLQLight/data/ObjectSystematics.txt')]
    
   #if TreeName not in  ObjectSystematics:
   #    print("Nonsense Systematic "+TreeName)
   #    os.sys.exit()
   #if a single rootfile is suppplied by the commandline option "-i" convert it and exit
    if options.i is not None:
        convert(options.i,TreeName,isObjSys,verbose)
        return
    if options.f is not None:
        with open(options.f,'r') as filelist:
            for x in filelist:
                x = x.rstrip()
                if not x: continue
                convert(x,TreeName,isObjSys,verbose)
        return
    #if a directory is supplied by the commandline option "-d". Loop over all root files under it and convert each of them then exit
    if options.d is not None:
        if  not os.path.isdir(options.d):
            print("No such directory : "+options.d)
            return
        glob_pattern = os.path.join(options.d, '*.root')
        files = sorted(glob(glob_pattern), key=os.path.getctime)
        currdir=os.getcwd()
        outdir=os.path.abspath(options.d)
        outdir=os.path.basename(outdir)
        outdir+="_"+TreeName+"_lvl0"
        print(outdir)
        subprocess.call("mkdir "+outdir,shell=True)
        os.chdir(outdir)
        for f in files:
            convert(f,TreeName,isObjSys,verbose)
        os.chdir(currdir)    
    return
if __name__ == "__main__":
    main()
    
