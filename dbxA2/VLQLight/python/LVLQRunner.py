#!/usr/bin/env python

#A driving pyroot program for some EventLoop Algorithms
#author     : Serhat Istin
#contact    : istin@cern.ch


import optparse
import  os
import re #for regex


if os.environ.has_key("ROOTCOREBIN"):
  rootcorebin=os.environ["ROOTCOREBIN"]
else:
  print "Error.ROOTCORE not setup!"
  os.sys.exit(-1)


import ROOT
from ROOT import gROOT
ROOT.gROOT.Macro('$ROOTCOREDIR/scripts/load_packages.C')
from ROOT import *    

#comment this our if you wish to see root errors
#To ignore INFO messages, 1001 is enough. To ignore WARNING it has to be above 2001. 
#To ignore ERROR it has to be above 3001. Check what level the printouts are and set it as appropriate.
gROOT.ProcessLine( "gErrorIgnoreLevel = 3001;")

ObjectSystematics=[line.rstrip('\n') for line in open(rootcorebin+'/../VLQLight/data/ObjectSystematics.txt')]
SFSystematics=[line.rstrip('\n') for line in open(rootcorebin+'/../VLQLight/data/SFSysts.txt')]


def Run(_algorithm_,_samplesdir_,_driver_,_nevents_,_nskip_,_override_,_submitdir_,_treename_):
    sh=SH.SampleHandler()
    scndir=SH.ScanDir()
    scndir.sampleDepth(0)
    #use below regex pattern to run on MC samples (as they have 6 digit DSIDs)
    #scndir.samplePattern("*[[:digit:]]{6}*")#use such posix standart regex as used in boost lib
    scndir.scan(sh,_samplesdir_)
    sh.setMetaString("nc_tree",_treename_)
    job=EL.Job()
    job.sampleHandler(sh)
    job.options().setInteger("nc_EventLoop_MaxEvents",_nevents_)
    job.options().setInteger("nc_EventLoop_SkipEvents",_nskip_)
    job.options().setInteger("nc_EventLoop_RemoveSubmitDir",_override_)# be careful with this!
    
    #Loop over samples... print some info e.x cross sections ...
    print("***********LVLQRUNNER RUN INFO************")
    for samp in sh:
        sampname=samp.getMetaString("sample_name")
        pattern=re.findall('[0-9]{8}', sampname )
        if(len(pattern)<=0):
            pattern=re.findall('[0-9]{6}', sampname )
            dsid=pattern[0]
        else:
            print("Running on Data")
    print("*******************************************")    
    # You can also control these job parameters
    # "nc_EventLoop_EventsPerWorker" --> some number
    # "nc_EventLoop_EventsPerWorker" --> some number
    # "nc_EventLoop_RemoveSubmitDir" --> ifyou set it to a nonzero value, already existing submission directories will be removed once you submit again
    #alg1=E6AlgZqX0b()
    job.algsAdd(_algorithm_)
    dv="None"
    if _driver_=="Direct":
        dv=EL.DirectDriver()
    elif _driver_=="Proof":
        dv=EL.ProofDriver()
    else:
        print("Driver not supported : "+driver)
        return
    dv.submit(job, _submitdir_)
    return 

def main():
    commparser = optparse.OptionParser()
    commparser.add_option('-A', '--algorithm',help="Choose the algorithm to run<E6AlgZqX0b/E62_EL(dbxA)> (default E6AlgZqX0b)",dest="A",metavar='ALGORITHM')
    commparser.add_option('-i', '--inputsample',help="provided sample directory (Mandatory)",dest="i",metavar='DIR')
    commparser.add_option('-o', '--outdir',help="submission directory where results are written (Mandatory)",dest="o",metavar='DIR')
    commparser.add_option('-c', '--channel',help="Channel <EE | MM> (Valid only for E6AlgZqX0b)",dest="c",metavar='STRING')
    commparser.add_option('-y', '--year',help="Year (Valid only for E6AlgZqX0b / Default is 2015+2016)",dest="y",metavar='NUMBER')
    commparser.add_option('-O', '--ObjSys',help="Name of the OBJECT Systematic to run. Default is nominal (Optional)",dest="O",metavar='STRING')
    commparser.add_option('-S', '--SFSys',help="Name of the Scale Factor Systematic to run. Default is nominal (Optional)",dest="S",metavar='STRING')

    commparser.add_option('-p', '--proof',help="use proof drive (optional (not working with dbxA algs))",dest="p",metavar='FLAG',action='store_true')

    

    
    (options, args) = commparser.parse_args()

    AllowedAlgorithms=['E6AlgZqX0b','E62_EL']
    algstr="E6AlgZqX0b" #default
    Alg=None
    if options.A is not None:
        if options.A not in AllowedAlgorithms:
            print("Error ! Algorithm "+options.A+" is not allowed")
            print("List of Supported Algorithms are ")
            print(AllowedAlgorithms)
            os.sys.exit(-1)
        algstr=options.A
    
    if options.i is None or options.o is None or options.c is None :
        commparser.print_help()
        os.sys.exit(-1)
    if not os.path.isdir(options.i):
        print("Error ! Path does not exist ! "+options.i)
        os.sys.exit(-1)
    
    treename="nominal"
    isdata=False
    if options.i.find('_data_') != -1:
        isdata=True
        
    if options.O is not None:
        treename=options.O

    if treename not in ObjectSystematics and not isdata:
        print("No such object systematic    :   "+treename)
        os.sys.exit()
    Driver="Direct"
    if options.p:
        Driver="Proof"
    
    if algstr=="E6AlgZqX0b":
        Alg=E6AlgZqX0b()
    if algstr=="E62_EL":
        Alg=E62_EL()
    
    Alg.SetChannel(options.c)    
    outdir=options.o
    outdir=outdir+"_"+options.c
    if options.S is not None:
        if options.O is not None:
            print("***FATAL*** SF variations must be applied to nominal objects only")
            os.sys.exit()
        Alg.SetSFSys(options.S)        
    
    prefix=""
    if options.S is not None:
        prefix=options.S
    if options.O is not None:
        prefix=options.O
    if not isdata and prefix!="" :
        outdir=outdir+"_"+prefix
    if options.y is not None:
        outdir=outdir+"_"+options.y
        Alg.SetYear(int(options.y))
    
    Run(Alg,options.i,Driver,-1,0,1,outdir,treename)
    
    #Run(E6AlgZqX0b(),options.i,Driver,-1,0,1,options.o+"_1",treename)
    #Run(E6AlgZqX0b(),options.i,Driver,-1,0,1,options.o+"_2",treename)
    
    #you can define multiple runs here such as...
    #Run(E6AlgZqX0b(),"/home/istin/ATLAS/data/minitops/singletop/","Direct",-1,0,1,"bokemon2")
    #Run(E6AlgZqX0b(),"/home/istin/ATLAS/data/minitops/singletop/","Direct",-1,0,1,"bokemon3")
if __name__ == "__main__":
    main()
