//Author Serhat Istin :
//While coding a LinkDef you have to make all your custom Classes available for cling...

#include <VLQLight/basic_parser.h>
#include <VLQLight/dbx_a.h>
#include <VLQLight/dbxCut.h>
#include <VLQLight/punch.h>
#include <VLQLight/LeptonicWReconstructor.h>
#include <VLQLight/e6_a2.h>

#include <VLQLight/dbxParticle.h>
#include <VLQLight/DBXNtuple.h>
#include <VLQLight/dbx_photon.h>
#include <VLQLight/dbx_jet.h>
#include <VLQLight/dbx_ljet.h>
#include <VLQLight/dbx_muon.h>
#include <VLQLight/dbx_electron.h>
#include <VLQLight/TopMini.h>
#include <VLQLight/TopMiniData.h>

//Make some vector of objects available for pyROOT/cling
typedef  std::vector<dbxMuon> VecDBXMuons;
typedef  std::vector<dbxElectron> VecDBXElectrons;
typedef  std::vector<dbxJet> VecDBXJets;
typedef  std::vector<dbxLJet> VecDBXLJets;


#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ class AnyOption+;

//Start dbxa related stuff
#pragma link C++ class basic_parser+;
#pragma link C++ class dbxA+;
#pragma link C++ class dbxCut+;
#pragma link C++ class dbxCutALL+;
#pragma link C++ class dbxCutNEle+;
#pragma link C++ class dbxCutNMu+;
#pragma link C++ class dbxCutNJet+;
#pragma link C++ class dbxCutNPho+;
#pragma link C++ class dbxCutGRL+;
#pragma link C++ class dbxCutTilErr+;
#pragma link C++ class dbxCutLarErr+;
#pragma link C++ class dbxCutTilTri+;
#pragma link C++ class dbxCutTrig+;
#pragma link C++ class dbxCutVtxTrks+;
#pragma link C++ class dbxCutNLEP+;
#pragma link C++ class dbxCutQQLEP+;
#pragma link C++ class dbxCutTrigMatch+;
#pragma link C++ class dbxCutOverlapEle+;
#pragma link C++ class dbxCutBadgoodJets+;
#pragma link C++ class dbxCutMET+;
#pragma link C++ class dbxCutMWT+;
#pragma link C++ class dbxCutMETMWT+;
#pragma link C++ class dbxCutbJet+;
#pragma link C++ class dbxCutPtof+;
#pragma link C++ class dbxCutQof+;
#pragma link C++ class dbxCutMof+;
#pragma link C++ class dbxCutList+;

#pragma link C++ class dbxCutR_Z_J0+;
#pragma link C++ class dbxCutm2LEP+;
#pragma link C++ class dbxCutnBJet+;
#pragma link C++ class dbxCutSumHTJET+;
#pragma link C++ class dbxCutpt2LEP+;
#pragma link C++ class dbxCutLEPsf+;
#pragma link C++ class dbxCutdoPlots+;
#pragma link C++ class dbxCutFillHistos+;
#pragma link C++ class dbxCutRof+;
#pragma link C++ class dbxCutPhiof+;
#pragma link C++ class dbxCutEtaof+;


#pragma link C++ class LeptonicWReconstructor+;
#pragma link C++ class E61dbxA+;
#pragma link C++ class dbxParticle+;
#pragma link C++ class DBXNtuple+;
#pragma link C++ class evt_data+;
#pragma link C++ class dbxPhoton+;
#pragma link C++ class dbxJet+;
#pragma link C++ class dbxLJet+;
#pragma link C++ class dbxMuon+;
#pragma link C++ class dbxElectron+;
#pragma link C++ class VecDBXJets+;
#pragma link C++ class VecDBXLJets+;
#pragma link C++ class VecDBXElectrons+;
#pragma link C++ class VecDBXMuons+;
//end dbx related stuff

#pragma link C++ class TopMini+;
#pragma link C++ class TopMiniData+;
#pragma link C++ class E6AlgZqX0b+;
#pragma link C++ class E62_EL+;

#pragma link C++ class VLQLight::Electron+;
#pragma link C++ class VLQLight::Muon+;
#pragma link C++ class VLQLight::Jet+;
#pragma link C++ class VLQLight::EventData+;
#pragma link C++ class SFCalculator+;
#pragma link C++ function VLQLight::SetAtlasStyle+;
#pragma link C++ function VLQLight::AtlasStyle+;

#endif
