#include <VLQLight/TopMiniData.h>
#include <iostream>
#define GeV 1E3

TopMiniData::TopMiniData() { dbxnt = new DBXNtuple(); }

TopMiniData::TopMiniData(TTree *inputtree) : TopMini(inputtree) {
  dbxnt = new DBXNtuple();
}

TopMiniData::~TopMiniData() {
  if (dbxnt)
    delete dbxnt;
}

void TopMiniData::toDBXEvent(bool isObjSys) {
  dbxnt->Clean();
  TLorentzVector alv;
  dbxMuon *adbxm;
  dbxElectron *adbxe;
  dbxJet *adbxj;
  for (unsigned int j = 0; j < mu_eta->size(); j++) {
    alv.SetPtEtaPhiE(mu_pt->at(j) / GeV, mu_eta->at(j), mu_phi->at(j),mu_e->at(j) / GeV);
    adbxm = new dbxMuon(alv);
    adbxm->setCharge(mu_charge->at(j));
    adbxm->setEtCone(mu_topoetcone20->at(j));
    adbxm->setPtCone(mu_ptvarcone30->at(j));
    adbxm->setd0(mu_d0sig->at(j));
    
    adbxm->setdelta_z0_sintheta(mu_delta_z0_sintheta->at(j));
    //adbxm->settrue_type(mu_true_type->at(j));
    //adbxm->settrue_origin(mu_true_origin->at(j));
    adbxm->settrigMatch_HLT_mu26_ivarmedium(mu_trigMatch_HLT_mu26_ivarmedium->at(j));
    adbxm->settrigMatch_HLT_mu50(mu_trigMatch_HLT_mu50->at(j));
    adbxm->settrigMatch_HLT_mu20_iloose_L1MU15(mu_trigMatch_HLT_mu20_iloose_L1MU15->at(j));
    adbxm->setisZCand(mu_isZCand->at(j));
    
    dbxnt->nt_muos.push_back(*adbxm);
    delete adbxm;
  }
  for (unsigned int j = 0; j < el_eta->size(); j++) {
    alv.SetPtEtaPhiE(el_pt->at(j) / GeV,el_eta->at(j),el_phi->at(j),el_e->at(j) / GeV);
    adbxe = new dbxElectron(alv);
    adbxe->setCharge(el_charge->at(j));
    adbxe->setClusterEta(el_cl_eta->at(j));
    adbxe->setEtCone20(el_topoetcone20->at(j));
    adbxe->setPtCone(el_ptvarcone20->at(j));
    
    adbxe->setd0sig(el_d0sig->at(j));
    adbxe->setdelta_z0_sintheta(el_delta_z0_sintheta->at(j));
    //adbxe->settrue_type(el_true_type->at(j));
    //adbxe->settrue_origin(el_true_origin->at(j));
    //adbxe->settrue_typebkg(el_true_typebkg->at(j));
    //adbxe->settrue_originbkg(el_true_originbkg->at(j));
    adbxe->settrigMatch_HLT_e60_lhmedium_nod0(el_trigMatch_HLT_e60_lhmedium_nod0->at(j));
    adbxe->settrigMatch_HLT_e26_lhtight_nod0_ivarloose(el_trigMatch_HLT_e26_lhtight_nod0_ivarloose->at(j));
    adbxe->settrigMatch_HLT_e140_lhloose_nod0(el_trigMatch_HLT_e140_lhloose_nod0->at(j));
    adbxe->settrigMatch_HLT_e60_lhmedium(el_trigMatch_HLT_e60_lhmedium->at(j));
    adbxe->settrigMatch_HLT_e24_lhmedium_L1EM20VH(el_trigMatch_HLT_e24_lhmedium_L1EM20VH->at(j));
    adbxe->settrigMatch_HLT_e120_lhloose(el_trigMatch_HLT_e120_lhloose->at(j));
    adbxe->setisZCand(el_isZCand->at(j));
    
    dbxnt->nt_eles.push_back(*adbxe);
    
    delete adbxe;
  }
  for (unsigned int j = 0; j < jet_eta->size(); j++) {
    alv.SetPtEtaPhiE(jet_pt->at(j) / GeV, jet_eta->at(j),jet_phi->at(j),jet_e->at(j) / GeV);
    adbxj = new dbxJet(alv);
    adbxj->set_isbtagged_77(bool(jet_isbtagged_77->at(j)));
    adbxj->setmv2c00(jet_mv2c00->at(j));
    adbxj->setmv2c10(jet_mv2c10->at(j));
    adbxj->setmv2c20(jet_mv2c20->at(j));
    adbxj->setip3dsv1(jet_ip3dsv1->at(j));
    adbxj->setjvt(jet_jvt->at(j));
    //adbxj->settruthflav(jet_truthflav->at(j));
    //adbxj->setisTrueHS(jet_isTrueHS->at(j));    
    dbxnt->nt_jets.push_back(*adbxj);
    delete adbxj;
  }
  
  dbxnt->nEle=dbxnt->nt_eles.size();
  dbxnt->nMuo=dbxnt->nt_muos.size();
  dbxnt->nJet=dbxnt->nt_jets.size();
  
  dbxnt->nt_evt.run_no=randomRunNumber; //RunNumber;
    
  TVector2 met;
  met.SetMagPhi(met_met/GeV,met_phi);
 //ikisine de mi ekliyoruz ne yapiyoruz ?? hic bisey anlamadim. S.I 
  dbxnt->nt_met.SetMagPhi(met_met/GeV,met_phi);
  dbxnt->nt_sys_met.push_back(met);  
    
  dbxnt->nt_evt.lumiblk_no=678;//lbn;
  dbxnt->nt_evt.top_hfor_type=1; //top_hfor_type;
  dbxnt->nt_evt.event_no=eventNumber;
  dbxnt->nt_evt.TRG_e= 1;
  dbxnt->nt_evt.TRG_m= 0;
  dbxnt->nt_evt.TRG_j= 0;
  dbxnt->nt_evt.vxp_maxtrk_no=1; // vxp_maxtrk;
  dbxnt->nt_evt.badjet=0; //bad_jet;
  dbxnt->nt_evt.mcevt_weight=1.0;//mc_weight;
  dbxnt->nt_evt.pileup_weight=weight_pileup; //m_pileup_weight;
  dbxnt->nt_evt.z_vtx_weight = 1.0;
  dbxnt->nt_evt.vxpType=0; //vxp_type->at(0);
  dbxnt->nt_evt.lar_Error=0; //larError;
  dbxnt->nt_evt.year=2016; //year;

  dbxnt->nt_evt.weight_mc=weight_mc;
  dbxnt->nt_evt.weight_pileup=weight_pileup;
  dbxnt->nt_evt.weight_leptonSF=weight_leptonSF;
  dbxnt->nt_evt.weight_bTagSF_77=weight_bTagSF_77;
  dbxnt->nt_evt.weight_jvt=weight_jvt;
  //dbxnt->nt_evt.weight_sherpa_22_vjets=weight_sherpa_22_vjets;
  if(isObjSys) return;
  dbxnt->nt_evt.weight_pileup_UP=weight_pileup_UP;
  dbxnt->nt_evt.weight_pileup_DOWN=weight_pileup_DOWN;
  dbxnt->nt_evt.weight_leptonSF_EL_SF_Trigger_UP=weight_leptonSF_EL_SF_Trigger_UP;
  dbxnt->nt_evt.weight_leptonSF_EL_SF_Trigger_DOWN=weight_leptonSF_EL_SF_Trigger_DOWN;
  dbxnt->nt_evt.weight_leptonSF_EL_SF_Reco_UP=weight_leptonSF_EL_SF_Reco_UP;
  dbxnt->nt_evt.weight_leptonSF_EL_SF_Reco_DOWN=weight_leptonSF_EL_SF_Reco_DOWN;
  dbxnt->nt_evt.weight_leptonSF_EL_SF_ID_UP=weight_leptonSF_EL_SF_ID_UP;
  dbxnt->nt_evt.weight_leptonSF_EL_SF_ID_DOWN=weight_leptonSF_EL_SF_ID_DOWN;
  dbxnt->nt_evt.weight_leptonSF_EL_SF_Isol_UP=weight_leptonSF_EL_SF_Isol_UP;
  dbxnt->nt_evt.weight_leptonSF_EL_SF_Isol_DOWN=weight_leptonSF_EL_SF_Isol_DOWN;
  dbxnt->nt_evt.weight_leptonSF_MU_SF_Trigger_STAT_UP=weight_leptonSF_MU_SF_Trigger_STAT_UP;
  dbxnt->nt_evt.weight_leptonSF_MU_SF_Trigger_STAT_DOWN=weight_leptonSF_MU_SF_Trigger_STAT_DOWN;
  dbxnt->nt_evt.weight_leptonSF_MU_SF_Trigger_SYST_UP=weight_leptonSF_MU_SF_Trigger_SYST_UP;
  dbxnt->nt_evt.weight_leptonSF_MU_SF_Trigger_SYST_DOWN=weight_leptonSF_MU_SF_Trigger_SYST_DOWN;
  dbxnt->nt_evt.weight_leptonSF_MU_SF_ID_STAT_UP=weight_leptonSF_MU_SF_ID_STAT_UP;
  dbxnt->nt_evt.weight_leptonSF_MU_SF_ID_STAT_DOWN=weight_leptonSF_MU_SF_ID_STAT_DOWN;
  dbxnt->nt_evt.weight_leptonSF_MU_SF_ID_SYST_UP=weight_leptonSF_MU_SF_ID_SYST_UP;
  dbxnt->nt_evt.weight_leptonSF_MU_SF_ID_SYST_DOWN=weight_leptonSF_MU_SF_ID_SYST_DOWN;
  dbxnt->nt_evt.weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP=weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;
  dbxnt->nt_evt.weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN=weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;
  dbxnt->nt_evt.weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP=weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;
  dbxnt->nt_evt.weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN=weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;
  dbxnt->nt_evt.weight_leptonSF_MU_SF_Isol_STAT_UP=weight_leptonSF_MU_SF_Isol_STAT_UP;
  dbxnt->nt_evt.weight_leptonSF_MU_SF_Isol_STAT_DOWN=weight_leptonSF_MU_SF_Isol_STAT_DOWN;
  dbxnt->nt_evt.weight_leptonSF_MU_SF_Isol_SYST_UP=weight_leptonSF_MU_SF_Isol_SYST_UP;
  dbxnt->nt_evt.weight_leptonSF_MU_SF_Isol_SYST_DOWN=weight_leptonSF_MU_SF_Isol_SYST_DOWN;
  dbxnt->nt_evt.weight_leptonSF_MU_SF_TTVA_STAT_UP=weight_leptonSF_MU_SF_TTVA_STAT_UP;
  dbxnt->nt_evt.weight_leptonSF_MU_SF_TTVA_STAT_DOWN=weight_leptonSF_MU_SF_TTVA_STAT_DOWN;
  dbxnt->nt_evt.weight_leptonSF_MU_SF_TTVA_SYST_UP=weight_leptonSF_MU_SF_TTVA_SYST_UP;
  dbxnt->nt_evt.weight_leptonSF_MU_SF_TTVA_SYST_DOWN=weight_leptonSF_MU_SF_TTVA_SYST_DOWN;
  dbxnt->nt_evt.weight_bTagSF_77_extrapolation_up=weight_bTagSF_77_extrapolation_up;
  dbxnt->nt_evt.weight_bTagSF_77_extrapolation_down=weight_bTagSF_77_extrapolation_down;
  dbxnt->nt_evt.weight_bTagSF_77_extrapolation_from_charm_up=weight_bTagSF_77_extrapolation_from_charm_up;
  dbxnt->nt_evt.weight_bTagSF_77_extrapolation_from_charm_down=weight_bTagSF_77_extrapolation_from_charm_down; 
  dbxnt->nt_evt.weight_jvt_UP=weight_jvt_UP;
  dbxnt->nt_evt.weight_jvt_DOWN=weight_jvt_DOWN;
  
  
  //S.I Vectors ... we trust in default STL copy ctors
  dbxnt->nt_evt.mc_generator_weights=*mc_generator_weights;
  dbxnt->nt_evt.weight_bTagSF_77_eigenvars_B_up=*weight_bTagSF_77_eigenvars_B_up;
  dbxnt->nt_evt.weight_bTagSF_77_eigenvars_C_up=*weight_bTagSF_77_eigenvars_C_up;
  dbxnt->nt_evt.weight_bTagSF_77_eigenvars_Light_up=*weight_bTagSF_77_eigenvars_Light_up;
  dbxnt->nt_evt.weight_bTagSF_77_eigenvars_B_down=*weight_bTagSF_77_eigenvars_B_down;
  dbxnt->nt_evt.weight_bTagSF_77_eigenvars_C_down=*weight_bTagSF_77_eigenvars_C_down;
  dbxnt->nt_evt.weight_bTagSF_77_eigenvars_Light_down=*weight_bTagSF_77_eigenvars_Light_down;  
  return;
  
}

DBXNtuple *const TopMiniData::DBXEvent() { return dbxnt; }

// usage ::
