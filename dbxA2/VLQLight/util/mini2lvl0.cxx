#include <VLQLight/TopMiniData.h>
#include <VLQLight/anyoption.h>
#include <iostream>


typedef DBXNtuple DBXEvent;
int main(int argc,char* argv[]) {
    
  AnyOption *opt = new AnyOption();
  opt->addUsage( "" );
  opt->addUsage( "Usage: " );
  opt->addUsage( "" );
  opt->addUsage( " -h  --help         : Prints this help" );
  opt->addUsage( " -i  --inputfile    : Input file (FILE) mandatory" );
  opt->addUsage( " -o  --outputfile   : Outputfile (FILE) mandatory" );
  opt->addUsage( "" );  

  opt->setOption("inputfile",'-i');
  opt->setOption("outputfile",'-o');
  opt->processCommandArgs( argc, argv );  
  
  if(opt->getValue('i')==NULL || opt->getValue('o')==NULL){
      std::cout<<"Commandline error!"<<std::endl;
      opt->printUsage();
      return 1;
  }
    
  std::string infile(opt->getValue('i'));
                       
  TFile *f = TFile::Open(TString(infile), "READ");
  TTree *minitree = (TTree *)f->Get("nominal");
  
  /*
   *or you can get any systematics tree above such as 'EG_RESOLUTION_ALL__1up'
    Scale factor systematics are in the nominal tree
  */
  
  TH1F *normhist = (TH1F *)f->Get("ee_2016/cutflow_mc_pu_zvtx");
  /* Even though we only need the first bin of the above histogram for the normalisation
   * let us write the whole histogram
  */
  TopMiniData *tmd = new TopMiniData(minitree);
  unsigned int N = minitree->GetEntries();
  DBXEvent* dbxEvent=tmd->DBXEvent();
  
  
  //Prepare output tree & branches
  int EventNumber;
  std::string oname(opt->getValue('o'));
  TFile* outfile=new TFile(TString(oname),"RECREATE");
  TTree *ttsave=new TTree ("nt_tree", "saving data");
  ttsave->SetDirectory (outfile);
  ttsave->Branch("EventNumber", &EventNumber);
  ttsave->Branch("dbxAsave",dbxEvent);
  
  
  
  for (unsigned int i = 0; i < N; ++i) {//your event loop
    minitree->GetEntry(i);
    // ********mini to dbx conversion ********
    if(i%1000==0) std::cout<<"Processing Event: "<<i<<std::endl;
    tmd->toDBXEvent();
    //dbxEvent = tmd->DBXEvent();
    //****************************************
    //from now on continue working as usual as if you are working with dbxa as in old times
        
    /*
     *For example in your analysis FF E6_1 E62 etc :  
    you can call makeAnalysis(dbxEvent->nt_muos,dbxEvent->nt_eles,dbxEvent->nt_jets,dbxEvent->nt_evt)
    */
    
    ttsave->Fill();
  }
    normhist->Write();
    ttsave->Write();
    outfile->Close();
  return 0;
}
