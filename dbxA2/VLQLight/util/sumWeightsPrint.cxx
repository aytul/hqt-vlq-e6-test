#include <TFile.h>
#include <TTree.h>
#include <iostream>
#include <TString.h>
#include "VLQLight/sumWeights.h"


int main(int argc,char* argv[]){

if(argc!=2){
    std::cout<<"Commandline Error"<<std::endl;
    exit(7);
    
}
TFile* F=TFile::Open(TString(argv[1]),"READ");

TTree*T=0;
T=(TTree*)F->Get("sumWeights");

if(!T){
    std::cout<<"No tree of sumWeights"<<std::endl;
    exit(4);
}
sumWeights W(T);

double res=0;
for(unsigned int i=0;i<T->GetEntriesFast();i++){
    W.GetEntry(i);
    res+=W.totalEventsWeighted;
}
std::cout<<res<<std::endl;
F->Close();
return 0;
}
