#include <SampleHandler/SampleHandler.h>
#include <SampleHandler/Sample.h>
#include <SampleHandler/ScanDir.h>
#include <EventLoop/Job.h>
#include <VLQLight/E6AlgZqX0b.h>
#include <EventLoop/DirectDriver.h>
#include <EventLoop/ProofDriver.h>
#include <iostream>
//A driving program for E6 EventLoop Analyses
//author    : Serhat Istin
//contact   : istin@cern.ch

int main(int argc,char* argv[]){
    SH::SampleHandler sh;
    SH::ScanDir().sampleDepth(0).scan(sh,argv[1]);
    sh.setMetaString ("nc_tree", "nominal");
    
     for(unsigned iter = 0; iter != sh.size(); ++ iter){
      std::cout<<sh.at(iter)->name()<<std::endl;
      
     }
    
    sh.print();
    
    EL::Job job;
    job.sampleHandler(sh);
    job.options()->setDouble (EL::Job::optMaxEvents, -1);
    E6AlgZqX0b *alg1=new E6AlgZqX0b();
    job.algsAdd (alg1);
    //EL::DirectDriver driver;
    EL::ProofDriver driver;
    
    driver.submit(job, "pokemon");

    return 0;
}
