#!/bin/bash

FailedLogs(){

  for L in $(egrep -Hrin 'Exited with' *.log | awk -F: '{ print $1 }' | sort -u)
  do
   echo $L | grep .log
  done

}

Retry(){

  logs=`FailedLogs`

  for L in $logs
  do
    script=`basename $L .log`.sh
    if [ ! -f $script ];then
      echo "No such bsub script "$script
      exit
    fi
    #remove old log file
    rm $L
    bsub -q 8nh -J `basename $script .sh` -o $L < $script
  done
}

if [ $# -ne 1 ];then
  echo "Commandline error!"
  echo "Usage :  $0 <\"list\"|\"retry\">"
  exit
fi

if [ "$1" == "list" ];then
  FailedLogs
elif [ "$1" == "retry" ];then
  Retry
else 
  echo "Error! No such argument "$1
  exit
fi
