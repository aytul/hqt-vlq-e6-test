#!/bin/bash
#A metascript to submit converting jobs to the CERN computing cluster
#usage : Mini2LVL0.sh <sampledir>  <object_systematic>
#The command above submits a set of LSF jobs to the CERN Cluster
#while keeping a local copy of each submission script and the output logs in the current working directory (where this script is run)
#Those scripts are useful to keep track of your work ( used to resubmit the failed jobs / also useful archiving & debugging). 
#Spotting the failed jobs and resubmission is done via 'VLQLight-LSFHandler.sh <list> <retry>'
#adding this to your path  as for your taste is your own responsibility

#Author		:	Serhat Istin
#contact	:	istin@cern.ch



#Your VLQLight installation / sample locations / destination / are defined in the NodeRunner script (see lines below)
#********************* Please change the NodeRunner according to your own setup and storage plan **********************************

NodeRunner='convertDefs.sh'

#Job submission routine / almost takes care of anything
Submit(){

 if [ $# -ne 2 ];then
  echo "MINI2LVL0 :: INFO Missing Commandline Arguments"
  exit 112
 fi	

 sample=$1
 objSys=$2


 if [ ! -d $sample ];then
   echo "MINI2LVL0 :: INFO Sample" $sample "does not exist ! Check your argument"
   exit 112
 fi


 if [ ! -f $NodeRunner ];then
   echo "MINI2LVL0 $NodeRunner does not exists here ! EXIT"
   exit 112
 fi

 sampname=$( basename $sample )
 #echo $sampname

 script=bsub_${sampname}_${objSys}.sh
 #echo $script


 #Now Create a special script to be run on the node
 #creating it for the first time  / overriding if already exists
 cat $NodeRunner >$script
 command=""

 if [ "$sampname" == "Zjets_Sherpa221" ];then
  command="convert \$eosZjets221 $objSys"

 elif [ "$sampname" == "ttbar" ];then
   command="convert \$eosttbar $objSys"

 elif [ "$sampname" == "Diboson_Sherpa221" ];then
   command="convert \$eosVVSherpa $objSys"
 else
    echo "MINI2LVL0 :: Your must supply Zjets_Sherpa221, ttbar or Diboson_Sherpa221 as an input sample directory sorry"
    exit 112
fi


 echo $command >> $script

 chmod +x $script

 bsub -q 8nh -o $(basename $script .sh).log < $script
}


sysfile=/afs/cern.ch/work/i/istin/AnalysisTop/VLQLight/data/ObjectSystematics.txt

if [ ! -f $sysfile ];then
  echo "MINI2LVL0 Systematics file not found"
  exit 112
fi

sampledir=$1
sysType=$2
if [ $# -ne 2 ];then
  echo "MINI2LVL0 Usage: Mini2LVL0.sh <inputsample> <sysType : nominal/all>"
  exit 112
fi
if [ ! -d $sampledir ];then
   echo "MINI2LVL0 Sample directory does not exist"
   exit 112
fi
if [ "$sysType" != "nominal" ] && [ "$sysType" != "all" ];then
  echo "MINI2LVL0 Systematic Type must be nominal or all.."
  exit 112
fi

#if nominal only do it and exit
if [ "$sysType" == "nominal" ];then
    Submit $sampledir "nominal"
    exit
fi


# Just a simple Systematic loop 
if [ "$sysType" == "all" ];then
  for O in $(cat $sysfile);do
    Submit $sampledir $O
  done
fi
