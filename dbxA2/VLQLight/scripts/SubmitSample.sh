#!/bin/bash

#A metascript to submit LSF jobs for a given sample
#N jobs will be launched per systematic where N is the number of DSIDS
#which is a lot faster. A 1nh queue is much more than enough

#author  : Serhat Istin
#contact : istin@cern.ch


#source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q
#lsetup 'rcSetup -u'
#source ../../rcSetup.sh

obj_systematics=$PWD"/../data/ObjectSystematics.txt"
obj_systematics_EE=$PWD"/../data/ObjectSystematics_EE.txt"
obj_systematics_MM=$PWD"/../data/ObjectSystematics_MM.txt"


submit(){
 if [ $# -ne 4 ];then
   echo commandline error
   echo "Usage $0 <sampledir> <channel> <obj_systematic> <algtype>"
   exit
 fi

 if [ "$2" != "EE" ] && [ "$2" != "MM" ];then
     echo "Nonsene Channel Given"
     exit
 fi

 if [ ! -d $1 ];then
   echo "No such sample dir : "$1
   exit
 fi


 if [ "$4" != 0 ] && [ "$4" != 1 ];then
     echo "argument 3 must be 0 or 1"
     echo "0 : E6AlgZqXob"
     echo "1 : E62_EL (dbxa)"
     exit
 fi

 alg="None"

 if [ "$4" == 0 ];then
   alg="E6AlgZqX0b"
 elif [ "$4" == 1 ];then
   alg="E62_EL"
 fi
 
 osys=$3
 
 dbxAcard=/afs/cern.ch/work/i/istin/AnalysisTop/E62_EL-card.txt


 if [ ! -f $dbxAcard ];then
   echo "dbxA card file not found"
   exit
 fi


 SCRIPTS=()
 sampdir=$1


 #You can put the below block inside a systematic loop
   scriptname=LSF-`basename $1`-$2_$3.sh
   cat NodeDefs.sh > $scriptname
   outdirname=vlql-`basename $1`
   echo "mkdir bazlama;cd bazlama" >>$scriptname
   echo "cp $dbxAcard .">>$scriptname
   echo "LVLQRunner.py -A $alg -i $sampdir -o ${outdirname} -c $2 -s $osys" >> $scriptname 
   resultdir=${outdirname}_$2_$osys
   echo "normalizeSamples.py -i $resultdir -o ${resultdir} -D" >>$scriptname 
   echo "tar cvzf ${resultdir}.tar.gz ${resultdir}">>$scriptname
   echo " rm -r ${resultdir}" >>$scriptname
   echo "cp -r ${resultdir}* \$destination" >>$scriptname
   echo "cd ..;rm -r bazlama" >>$scriptname
   SCRIPTS=("${SCRIPTS[@]}" $scriptname)
#once temporary scripts are created bsub them....

 for SCR in ${SCRIPTS[@]}
  do
   chmod +x $SCR
   bsub -q 8nh -J `basename $SCR .sh` -o `basename $SCR .sh`.log < $SCR
   #rm $SCR
 done
}

if [ $# -ne 3 ];then
 echo "Commandline Error Usage :"
 echo "$0 <sampledir> <channel> <alg>"
 exit
fi 

sysfile="none"
if [ "$2" == "EE" ];then
 sysfile=$obj_systematics_EE
fi
 
if [ "$2" == "MM" ];then
 sysfile=$obj_systematics_MM
fi 
 
for S in `cat $sysfile`
do
  submit $1 $2 $S $3
done
