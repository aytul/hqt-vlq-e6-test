#!/bin/bash
#**************** DO NOT RUN THIS SCRIPT MANUALLY ******************************

set -e

#this script will be running on LSF worker nodes
#to dump mini ntuples located on the eos atlascern group disk
#the resulting lvl0 sample is transferred to your eos user space
#so please make sure that you have enough space in your eosuser storage
#before running this


#Author : Serhat Istin
#mail   : istin@cern.ch


#************************************************************************************
#change these two lines according to your own vlqlight installation and eos disk path
vlqlightdir=/afs/cern.ch/user/i/istin/work/AnalysisTop
destination=/eos/user/i/istin/LVL0
#************************************************************************************

if [ ! -d $destination ];then
  echo "NODERUNNER :: INFO destination does not exist"
  exit 112

fi
 
eosserver=''
if [ ! -z $(echo $destination | grep '/eos/atlas/') ];then
    eosserver="root://eosatlas.cern.ch//"
elif [ ! -z $(echo $destination | grep '/eos/user/') ];then
    eosserver="root://eosuser.cern.ch//"
else
    echo "NODERUNNER :: INFO Nonsense eos destination type"
    exit 113
fi



#these sample paths are not to be changed (or be very careful while changing them)
eosZjets221=/eos/atlas/atlascerngroupdisk/phys-exotics/hqt/OSML/julyProd/bkg/Zjets_Sherpa221
eosttbar=/eos/atlas/atlascerngroupdisk/phys-exotics/hqt/OSML/julyProd/bkg/ttbar
eosVVSherpa=/eos/atlas/atlascerngroupdisk/phys-exotics/hqt/OSML/julyProd/bkg/Diboson_Sherpa221

#these lines below sets up  RC and VLQLight
currdir=$PWD
cd $vlqlightdir
source rcSetup.sh
cd $currdir


#converter routine to be stamped in the node script
convert(){
   cd $destination
   objsys=$2
   for D in `ls $1`
    do
     indir=$1/$D
     if [ -d $indir ];then
      mini2lvl0.py -d $(readlink -e $indir) -O $objsys 2>&1
    fi
   done
  #now check whether the run is successful or not . exit if appropriate
  if [ $?  -ne ];then
     echo " Run was not successfull "
     exit 113
  fi 
}


# ********************* NEVER EVER UNCOMMENT THE LINES BELOW YOU MAY DROP INTO RECURSION  *******
# ********************* THESE LINES ARE JUST TO SHOW HOW TO USE THE METHOD ABOVE  **************
#*********************  THIS SCRIPT WILL AUTOMATICALLY BE CONCATENATED TO SOMETHING ELSE WHILE RUNNING ON LSF **** 


#bsub -q 8nh -J <YourncustomJobName> <convert_node.sh 

#convert $eosttbar $1

#convert $eosVVSherpa $1

#convert $eosZjets221 $1
