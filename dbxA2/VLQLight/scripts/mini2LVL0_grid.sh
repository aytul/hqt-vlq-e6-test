#!/bin/bash

set -e

Diboson_Sherpa221='user.nikiforo.363356.Diboson_Sherpa221.ZqqZll.EXOT4.e5525_s2726_r7772_r7676_p2949.2017.07.18_bkg_sys_output.root
user.nikiforo.363358.Diboson_Sherpa221.WqqZll.EXOT4.e5525_s2726_r7772_r7676_p2949.2017.07.18_bkg_sys_output.root
user.nikiforo.363489.Diboson_Sherpa221.WlvZqq.EXOT4.e5525_s2726_r7772_r7676_p2949.2017.07.18_bkg_sys_output.root
user.nikiforo.363490.Diboson_Sherpa221.llll.EXOT4.e5332_s2726_r7772_r7676_p2949.2017.07.18_bkg_sys_output.root
user.nikiforo.363491.Diboson_Sherpa221.lllv.EXOT4.e5332_s2726_r7772_r7676_p2949.2017.07.18_bkg_sys_output.root
user.nikiforo.363492.Diboson_Sherpa221.llvv.EXOT4.e5332_s2726_r7772_r7676_p2949.2017.07.18_bkg_sys_output.root
user.nikiforo.363493.Diboson_Sherpa221.lvvv.EXOT4.e5332_s2726_r7772_r7676_p2949.2017.07.18_bkg_sys_output.root
'

Zjets_Sherpa221='user.nikiforo.364100.Zjets_Sherpa221.Zmumu_MAXHTPTV0_70_CVetoBVeto.EXOT4.e5271_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364101.Zjets_Sherpa221.Zmumu_MAXHTPTV0_70_CFilterBVeto.EXOT4.e5271_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364102.Zjets_Sherpa221.Zmumu_MAXHTPTV0_70_BFilter.EXOT4.e5271_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364103.Zjets_Sherpa221.Zmumu_MAXHTPTV70_140_CVetoBVeto.EXOT4.e5271_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364104.Zjets_Sherpa221.EXOT4.e5271_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364105.Zjets_Sherpa221.Zmumu_MAXHTPTV70_140_BFilter.EXOT4.e5271_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364106.Zjets_Sherpa221.EXOT4.e5271_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364107.Zjets_Sherpa221.EXOT4.e5271_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364108.Zjets_Sherpa221.Zmumu_MAXHTPTV140_280_BFilter.EXOT4.e5271_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364109.Zjets_Sherpa221.EXOT4.e5271_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364110.Zjets_Sherpa221.EXOT4.e5271_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364111.Zjets_Sherpa221.Zmumu_MAXHTPTV280_500_BFilter.EXOT4.e5271_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364112.Zjets_Sherpa221.Zmumu_MAXHTPTV500_1000.EXOT4.e5271_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364113.Zjets_Sherpa221.Zmumu_MAXHTPTV1000_E_CMS.EXOT4.e5271_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364114.Zjets_Sherpa221.Zee_MAXHTPTV0_70_CVetoBVeto.EXOT4.e5299_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364115.Zjets_Sherpa221.Zee_MAXHTPTV0_70_CFilterBVeto.EXOT4.e5299_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364116.Zjets_Sherpa221.Zee_MAXHTPTV0_70_BFilter.EXOT4.e5299_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364117.Zjets_Sherpa221.Zee_MAXHTPTV70_140_CVetoBVeto.EXOT4.e5299_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364118.Zjets_Sherpa221.Zee_MAXHTPTV70_140_CFilterBVeto.EXOT4.e5299_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364119.Zjets_Sherpa221.Zee_MAXHTPTV70_140_BFilter.EXOT4.e5299_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364120.Zjets_Sherpa221.Zee_MAXHTPTV140_280_CVetoBVeto.EXOT4.e5299_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364121.Zjets_Sherpa221.EXOT4.e5299_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364122.Zjets_Sherpa221.Zee_MAXHTPTV140_280_BFilter.EXOT4.e5299_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364123.Zjets_Sherpa221.Zee_MAXHTPTV280_500_CVetoBVeto.EXOT4.e5299_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364124.Zjets_Sherpa221.EXOT4.e5299_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364125.Zjets_Sherpa221.Zee_MAXHTPTV280_500_BFilter.EXOT4.e5299_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364126.Zjets_Sherpa221.Zee_MAXHTPTV500_1000.EXOT4.e5299_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364127.Zjets_Sherpa221.Zee_MAXHTPTV1000_E_CMS.EXOT4.e5299_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364128.Zjets_Sherpa221.Ztautau_MAXHTPTV0_70_CVetoBVeto.EXOT4.e5307_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364129.Zjets_Sherpa221.EXOT4.e5307_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364130.Zjets_Sherpa221.Ztautau_MAXHTPTV0_70_BFilter.EXOT4.e5307_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364131.Zjets_Sherpa221.EXOT4.e5307_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364132.Zjets_Sherpa221.EXOT4.e5307_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364133.Zjets_Sherpa221.Ztautau_MAXHTPTV70_140_BFilter.EXOT4.e5307_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364134.Zjets_Sherpa221.EXOT4.e5307_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364135.Zjets_Sherpa221.EXOT4.e5307_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364136.Zjets_Sherpa221.Ztautau_MAXHTPTV140_280_BFilter.EXOT4.e5307_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364137.Zjets_Sherpa221.EXOT4.e5307_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364138.Zjets_Sherpa221.EXOT4.e5313_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364139.Zjets_Sherpa221.Ztautau_MAXHTPTV280_500_BFilter.EXOT4.e5313_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364140.Zjets_Sherpa221.Ztautau_MAXHTPTV500_1000.EXOT4.e5307_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
user.nikiforo.364141.Zjets_Sherpa221.Ztautau_MAXHTPTV1000_E_CMS.EXOT4.e5307_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root
'
ttbar='user.nikiforo.410501.ttbar.ttbar_hdamp258p75_nonallhad.EXOT4.e5458_s2726_r7772_r7676_p2949.2017.07.18_bkg_output.root'



#We override this command #################
command=""
############################################
execfile='runme_grid.sh'
if [ -f $execfile ];then
 echo "Removing previous $execfile"
 rm $execfile
fi

suffx=0N
for inds in $Zjets_Sherpa221
#for inds in $ttbar
do
 for SYS in $(cat ObjectSystematics_all.txt)
  do
  outds=$( echo $inds | sed 's/nikiforo/istin/' | sed 's/_bkg_output.root//' | sed 's/.e5271_s2726_r7772_r7676_p2949.2017.07.18//')
  outds=$( echo $outds  | awk -F '.EXOT4' '{print $1}')_${SYS}_${suffx}
  command="prun \
--exec \"./inputFilePicker.sh %IN >inputFiles.txt; cat inputFiles.txt; ./noderunner.sh inputFiles.txt $SYS\" \
--inDS=$inds \
--outDS=$outds \
--useRootCore \
--nFilesPerJob=1 \
--outputs=*.root \
--inTarBall=top-xaod.tar.gz"
 
 echo -e "$command" >>$execfile
 done
done
chmod +x $execfile
echo "$execfile created..."
