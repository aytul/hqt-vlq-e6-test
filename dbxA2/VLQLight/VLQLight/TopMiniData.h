#ifndef TOPMINIDATA_H
#define TOPMINIDATA_H
#include <VLQLight/DBXNtuple.h>
#include <VLQLight/TopMini.h>
#include <TTree.h>
//Author Serhat Istin
/*Implement a class derived from TopMini which is automatically created from MakeClass()
 * always use this proxy
*/
class TopMiniData : public TopMini{

    public:
        TopMiniData();
        TopMiniData(TTree*);
        ~TopMiniData();
        /*Put your data converter methods here... They could be used for otf conversion (no need for lvl0s?)*/
        void toDBXEvent(bool);//call this inside an event loop as usual
        DBXNtuple* const DBXEvent();
    private:
        DBXNtuple* dbxnt;
};


#endif
