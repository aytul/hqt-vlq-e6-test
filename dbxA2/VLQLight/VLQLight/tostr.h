#ifndef TOSTR_H
#define TOSTR_H
//herhangi bişeyi std::stringe çevirmek için
#include <string>
#include <sstream>

template <typename T> std::string tostr(const T& t) {
   std::ostringstream os; os<<t; return os.str();
 }
#endif
