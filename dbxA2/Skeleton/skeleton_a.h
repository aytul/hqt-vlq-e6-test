#ifndef SKELETON_A_H
#define SKELETON_A_H

#include "dbx_a.h"
#include "ReadCard.h"
#include "LeptonicWReconstructor.h"
#include <iostream>
#include "analysis_core.h"


class SkeletondbxA : public dbxA {
  public: 
      SkeletondbxA(char *aname) : dbxA ( aname)
         {
         sprintf (cname,"%s",aname); // keep the current analysis name in the class variable
         int r=dbxA::setDir(cname);  // make the relevant root directory
         if (r)  std::cout <<"Root Directory Set Failure in:"<<cname<<std::endl;
         //grl_cut=false;
         }

      int initGRL();
      int readAnalysisParams();
      int plotVariables(int sel);
      int printEfficiencies();
      int bookAdditionalHistos();
      int makeAnalysis(vector<dbxMuon> muons, vector<dbxElectron> electrons, 
                               vector<dbxJet> jets, TVector2 met, evt_data anevt); 
      int saveHistos() { int r = dbxA::saveHistos(); return r; }

   private:
      bool grl_cut;
      char cname[CHMAX];
      TH1F *hmmult; 
      TH1F *lepphi;
      TH1F *mZ;
     
      Double_t minwidthw, minwidthz, minptm, minmassmm, maxetam, mindrmm, maxetconem, minptj;
      Double_t maxetaj, maxmet, mindrjm, maxresn4m, n4peak, minwidthn4, jmasscut ;
      Int_t chargeveto, minnjetsw;  
      int TRGe, TRGm;
   
};
#endif
