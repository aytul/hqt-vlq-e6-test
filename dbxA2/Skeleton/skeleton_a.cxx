#include "skeleton_a.h"
#include "TParameter.h"
#include "WIndices.C"
#include <TRandom.h>

int SkeletondbxA::plotVariables(int sel) {
 dbxA::plotVariables (sel);
  int retval=0;
  const Double_t small=1e-4;
  char tmp[128], idxn[128];
  sprintf(tmp,"c4-%s",cname);
  sprintf(idxn,"inv_mass-%s",cname);

  // I define my canvases here
  TCanvas *c1 = new TCanvas("inv_mass_plots", "c1", 150, 10, 650, 500);
  c1->SetFillColor(0); c1->SetBorderMode(0); c1->Divide(1,3,small,small);
  c1->Draw();


//------------------CANVAS 1----------------
//------------------plots
    c1->cd(1);
    gPad->SetRightMargin(small);
    hmmult->SetLineColor(2);
    hmmult->Draw();
// Efficiencies    
    c1->cd(2);
    gPad->SetRightMargin(small);
    eff->Draw();

// PT leptons
     c1->cd(3);
     gPad->SetRightMargin(small);
     lepphi->Draw();

/////////  SAVE /////////////////
  char aaa[64];
  sprintf (aaa, "%s-1.png",cname); c1->SaveAs(aaa);
  sprintf (aaa, "%s-1.eps",cname); c1->SaveAs(aaa);

 return retval;  
}

int SkeletondbxA:: readAnalysisParams() {
  int retval=0;
  TString CardName=cname;
          CardName+="-card.txt";

   minwidthw  = ReadCard(CardName,"minwidthw" ,20.);
   minwidthz  = ReadCard(CardName,"minwidthz" ,25.);
   maxetconem = ReadCard(CardName,"maxetconem",6.0);
   minptm     = ReadCard(CardName,"minptm",15.);
   maxetam    = ReadCard(CardName,"maxetam",2.5);
   minptj     = ReadCard(CardName,"minptj", 15.);
   maxetaj    = ReadCard(CardName,"maxetaj",2.5);
   minmassmm  = ReadCard(CardName,"minmassmm", 5.);
   mindrmm    = ReadCard(CardName,"mindrmm",2.0);
   maxmet     = ReadCard(CardName,"maxmet", 30.);
   mindrjm    = ReadCard(CardName,"mindrjm",0.4);
   maxresn4m  = ReadCard(CardName,"maxresn4m", 0.25);
   n4peak     = ReadCard(CardName,"n4peak" , 0.);
   minwidthn4 = ReadCard(CardName,"minwidthn4", 1e4);
   chargeveto= Int_t(ReadCard(CardName,"chargeveto",0));
   minnjetsw = Int_t(ReadCard(CardName,"minnjetsw",2));
// Minimum number of jets to reconstruct the hadronic Ws from: 
   if ( minnjetsw!=1 && minnjetsw!=2 ) minnjetsw = 2;

   TRGe = ReadCard(CardName,"TRGe",1);
   TRGm = ReadCard(CardName,"TRGm",1);

	
	////// PUT ANALYSIS PARAMETERS INTO .ROOT //////////////
	
	TParameter<double> *minwidthw_tmp=new TParameter<double> ("minwidthw", minwidthw);
	TParameter<double> *minwidthz_tmp=new TParameter<double> ("minwidthz", minwidthz);
	TParameter<double> *maxetconem_tmp=new TParameter<double> ("maxetconem", maxetconem);
	TParameter<double> *minptm_tmp=new TParameter<double> ("minptm", minptm);
	TParameter<double> *maxetam_tmp=new TParameter<double> ("maxetam", maxetam);
	TParameter<double> *minptj_tmp=new TParameter<double> ("minptj", minptj);
	TParameter<double> *maxetaj_tmp=new TParameter<double> ("maxetaj", maxetaj);
	TParameter<double> *minmassmm_tmp=new TParameter<double> ("minmassmm", minmassmm);
	TParameter<double> *mindrmm_tmp=new TParameter<double> ("mindrmm", mindrmm);
	TParameter<double> *maxmet_tmp=new TParameter<double> ("maxmet", maxmet);
	TParameter<double> *mindrjm_tmp=new TParameter<double> ("mindrjm", mindrjm);
	TParameter<double> *maxresn4m_tmp=new TParameter<double> ("maxresn4m", maxresn4m);
	TParameter<double> *n4peak_tmp=new TParameter<double> ("n4peak", n4peak);
	TParameter<double> *minwidthn4_tmp=new TParameter<double> ("minwidthn4", minwidthn4);
	TParameter<double> *chargeveto_tmp=new TParameter<double> ("chargeveto", chargeveto);
	TParameter<double> *minnjetsw_tmp=new TParameter<double> ("minnjetsw", minnjetsw);
	//TParameter<double> *mqhxmin_tmp=new TParameter<double> ("mqhxmin", mqhxmin);
	//TParameter<double> *mqhxmax_tmp=new TParameter<double> ("mqhxmax", mqhxmax);
	//TParameter<double> *maxMuPtCone_tmp=new TParameter<double> ("maxMuPtCone", maxMuPtCone);
	//TParameter<double> *maxMuEtCone_tmp=new TParameter<double> ("maxMuEtCone", maxMuEtCone);
	//TParameter<double> *maxElPtCone_tmp=new TParameter<double> ("maxElPtCone", maxElPtCone);
	//TParameter<double> *maxElEtCone_tmp=new TParameter<double> ("maxElEtCone", maxElEtCone);
	TParameter<double> *TRGe_tmp=new TParameter<double> ("TRGe", TRGe);
	TParameter<double> *TRGm_tmp=new TParameter<double> ("TRGm", TRGm);
	
	minwidthw_tmp->Write("minpte");
    minwidthz_tmp->Write("minptm");
    maxetconem_tmp->Write("maxetae");
    minptm_tmp->Write("maxetam");
	maxetam_tmp->Write("minmet");
	minptj_tmp->Write("minptj");
	maxetaj_tmp->Write("maxetaj");
	minmassmm_tmp->Write("mindrjm");
	mindrmm_tmp->Write("mindrje");
	maxmet_tmp->Write("minptj1");
	mindrjm_tmp->Write("minptj2");
	maxresn4m_tmp->Write("minEj2");
	n4peak_tmp->Write("minetaj2");
	minwidthn4_tmp->Write("mindrjj");
	chargeveto_tmp->Write("minetajj");
	minnjetsw_tmp->Write("maxdeltam");
	//mqhxmin_tmp->Write("mqhxmin");
	//mqhxmax_tmp->Write("mqhxmax");
	//maxMuPtCone_tmp->Write("maxMuPtCone");
	//maxMuEtCone_tmp->Write("maxMuEtCone");
	//maxElPtCone_tmp->Write("maxElPtCone");
	//maxElEtCone_tmp->Write("maxElEtCone");
	TRGe_tmp->Write("TRGe");
	TRGm_tmp->Write("TRGm");

  return retval;
}

int SkeletondbxA:: printEfficiencies() {
  int retval=0;
  PrintEfficiencies(eff);
  return retval;
}

int SkeletondbxA:: initGRL() {
  int retval=0;
  grl_cut=true;
  return retval;
}

int SkeletondbxA:: bookAdditionalHistos() {
//additional histograms are defined here
 hmmult   = new TH1F("hmmult" ,"Muon multiplicity",20,-0.5,19.5);
   lepphi = new TH1F("lepphi" ,"phi of leptons",100,-5.,5.);
   mZ = new TH1F("mZ" ,"mass of 2 leptons",100,0,200);

  eff= new TH1F("eff","selection efficiencies",15,0.5,15.5);
 int retval=0;
  return retval;
}

/////////////////////////
int SkeletondbxA::makeAnalysis(vector<dbxMuon> muons, vector<dbxElectron> electrons,
                               vector<dbxJet> jets, TVector2 met, evt_data anevt) {

  int retval=0;
  int cur_cut=1;

  eff->GetXaxis()->SetBinLabel(cur_cut,"all");
  eff->Fill(cur_cut, anevt.mcevt_weight);
  cur_cut++;


// force no GRL for MC events
  if ((TRGm>1) || (TRGe>1)) { grl_cut=false; }


// add the goodrunsList xml for this analysis
  eff->GetXaxis()->SetBinLabel(cur_cut,"GRL");
  if ( grl_cut) {
    bool pass = isGoodRun(anevt.run_no,anevt.lumiblk_no);
    if ( !pass ) return cur_cut;
  }
  eff->Fill(cur_cut, anevt.mcevt_weight);
  cur_cut++;

/* The below is commented out since anevt is not yet filled in 2017.
// Trigger cut
  bool trgpass=false;
  if ( TRGm>0 ) { if (anevt.TRG_m)  trgpass=true; }
  if ( TRGe>0 ) { if (anevt.TRG_e)  trgpass=true; }

  eff->GetXaxis()->SetBinLabel(cur_cut,TString("TRiGger "));
  if (!trgpass) return cur_cut;
    eff->Fill(cur_cut, anevt.mcevt_weight);
    cur_cut++;

// jet size cut
  eff->GetXaxis()->SetBinLabel(cur_cut,"#jets <6");
  if (jets.size()>=6) return cur_cut;
    eff->Fill(cur_cut, anevt.mcevt_weight);
    cur_cut++;

// vertex cut
// clean wrt the primary vertex number of tracks
  eff->GetXaxis()->SetBinLabel(cur_cut,"Vtx Trks>4");
  if (anevt.vxp_maxtrk_no<5) return cur_cut;
    eff->Fill(cur_cut, anevt.mcevt_weight);
    cur_cut++;

// fill some histograms after the cuts
// Fill muon multiplicity
  hmmult->Fill(muons.size(), anevt.mcevt_weight);

// fill lepton phi
  if (TRGm>0) for (UInt_t i=0; i<muons.size(); i++) {
    lepphi->Fill(muons.at(i).lv().Phi(), anevt.mcevt_weight);
  } 
  if (TRGe>0) for (UInt_t i=0; i<electrons.size(); i++) {
    lepphi->Fill(electrons.at(i).lv().Phi(), anevt.mcevt_weight);
  } 
*/

  if (electrons.size() == 2) {
    dbxParticle ZBos;
    ZBos.setTlv(electrons.at(0).lv()+electrons.at(1).lv());
    mZ->Fill(ZBos.lv().M() );
  }
// mother class is called to plot some basic quantities
// ............ do NOT remove .....
    dbxA::makeAnalysis (muons, electrons, jets, met, anevt,0);
   if (jets.size() == 1) dbxA::makeAnalysis (muons, electrons, jets, met, anevt,4); // this is an example;
// ............ do NOT remove .....

// we are done with the simple example
  return retval;
}

