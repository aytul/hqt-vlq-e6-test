//////////////////////////////////////////////////////////////////////////////////
// Example program to show the usage of RSLimit, the wrapper library for RooStats
// it also compares the RSLimit results and TLimit results
// contact gokhan.unel@cern.ch for bug reports and suggestions
// v0.1   --    28 April 2011 initial version, cint compatible
// v0.2   --    29 April 2011 aclic compatible
// v0.3   --    09 Febry 2012 TLimit comparison in HTI mode
// v0.4   --    08 March 2012 TLimit and RSLimit comparison with multi channels
// v0.5   --    18 April 2012 errors added by Umit Kaya
//////////////////////////////////////////////////////////////////////////////////

#include <iomanip>
#include "../analysis_core/PolFitter.cpp"

void testRSLimit(int interactif=0, int clsclsb=1) {

bool isHTI=false;
bool showplot=false;
bool dodump=true;
bool doratesyst=false;
int numch=1;

gROOT->LoadMacro("../RSL/RSLimit.C+");

if (interactif) {
  cout << " # channels (1,2 or 3)  ?:"; cin >> numch; 
  cout << " HTI (0 or 1)  :" ;          cin >> isHTI; 
  cout << " Dump RooWS (0 or 1)    :" ; cin >> dodump; 
  cout << " cls=0 clsb=1  :" ;          cin >> clsclsb; 
  cout << " si/bg/dt plot (0 or 1) ?:"; cin >> showplot; 
}


// load the histograms here
// should be already normalized to the appropriate X-sections

 TFile  *bb = new TFile("examplefit.root");
  TH1F *sih = (TH1F*)((TH1F*)bb->Get("signal"))->Clone();
  TH1F *bgh = (TH1F*)((TH1F*)bb->Get("ttbar"))->Clone();
  TH1F *dth = (TH1F*)((TH1F*)bb->Get("data"))->Clone();
if (numch>1) {
  sih->Scale(0.5);
  bgh->Scale(3.4); //was 6.8
  dth->Scale(3.1); //was 6.2
  TH1F *sih2 = sih->Clone();
  TH1F *bgh2 = bgh->Clone();
  TH1F *dth2 = dth->Clone();
} else {
  sih->Scale(1);
  bgh->Scale(6.8); //was 6.8
  dth->Scale(6.2); //was 6.2
}
/*
  bgh->Scale(6.8);
  dth->Scale(6.2);
  TH1F *sih2 = sih->Clone();
  sih2->Scale(2.3);
*/
/*
  TFile  *bb = new TFile("sibg.root");
  TH1F *sih = (TH1F*)((TH1F*)bb->Get("signal"))->Clone();
  TH1F *bgh = (TH1F*)((TH1F*)bb->Get("sample_1"))->Clone();
  TH1F *dth = (TH1F*)((TH1F*)bb->Get("data"))->Clone();
*/

 if (showplot) {
  dth->Draw();
  bgh->Draw("same");
  bgh->SetLineColor(4);
  sih->SetLineColor(2);
  sih->Draw("same");
 }

int nb_toymc=8000;
// initialize the LimitRS class

#ifdef __rate_uncert__
RSLimit u4limit(dth);
u4limit.AddSignalHistogram(sih, 1.0, 1.2, NULL, NULL);
u4limit.AddBackgroundHistogram(bgh, 0.99, 1.01, NULL, NULL);
u4limit.AddBackgroundHistogram(bgh, 0.8, 1.2, NULL, NULL);
//u4limit.AddBackgroundHistogram(bgh, 1.0, 1.0, NULL, NULL);
//u4limit.AddBackgroundHistogram(bgh, 1.0, 1.0, NULL, NULL);
#else
RSLimit u4limit(sih, bgh, dth);
#endif

if (numch>1){
 u4limit.AddChannel(dth,"CH2");
 u4limit.AddSignalHistogram(sih2, 1.0, 1.0, NULL, NULL,"CH2");
 u4limit.AddBackgroundHistogram(bgh, 1.0, 1.0, NULL, NULL,"CH2");
}

if (numch>2){
 u4limit.AddChannel(dth,"CH3");
 u4limit.AddSignalHistogram(sih2, 1.0, 1.0, NULL, NULL,"CH3");
 u4limit.AddBackgroundHistogram(bgh, 1.0, 1.0, NULL, NULL,"CH3");
}

u4limit.VerbosityLevel(2);
u4limit.SetDiscriminator("m",10,500);
u4limit.UseProof(2);
u4limit.SetToyMC(nb_toymc,nb_toymc);
u4limit.SelectTestStats(1); // 0:simple likelihood ratio 1:LogLikelihood 2:LHC_type 
u4limit.SetCLsCLsb(clsclsb); //0:CLs  1:CLs+b
u4limit.Initialize();
u4limit.DoHTI(isHTI);
if (dodump) u4limit.Dump();
u4limit.SaveAs("ws.root");
u4limit.TestHypothesis(10, 2, 12); // the real computation is here

double u4lim_kf_m2s, u4lim_kf_m1s, u4lim_kf_med, u4lim_kf_p2s, u4lim_kf_p1s, u4lim_kf_obs;
if (isHTI) {

 u4limit.DrawHTIPlot();
  u4lim_kf_m2s= u4limit.GetKFactorExp(2);
  u4lim_kf_m1s= u4limit.GetKFactorExp(1);
  u4lim_kf_med= u4limit.GetKFactorExp(0);
u4limit.Dump();
  u4lim_kf_p1s= u4limit.GetKFactorExp(-1);
  u4lim_kf_p2s= u4limit.GetKFactorExp(-2);
  u4lim_kf_obs= u4limit.GetKFactorObs();

cout << "xxxxxxxxxxxxxxx HTI is over. Now extracting CLb and CLsxxxxxxxxxxxxxxxxxxx"<<endl;
RSLimit t4limit(sih, bgh, dth);
if (numch>1){
 t4limit.AddChannel(dth,"CH2");
 t4limit.AddSignalHistogram(sih2, 1.0, 1.0, NULL, NULL,"CH2");
 t4limit.AddBackgroundHistogram(bgh, 1.0, 1.0, NULL, NULL,"CH2");
}
if (numch>2){
 t4limit.AddChannel(dth,"CH3");
 t4limit.AddSignalHistogram(sih2, 1.0, 1.0, NULL, NULL,"CH3");
 t4limit.AddBackgroundHistogram(bgh, 1.0, 1.0, NULL, NULL,"CH3");
}
t4limit.UseProof(2);
t4limit.SetToyMC(nb_toymc,nb_toymc);

t4limit.SelectTestStats(1); // 0:simple likelihood ratio 1:LogLikelihood 2:LHC_type 
t4limit.SetCLsCLsb(clsclsb); //0:CLs  1:CLs+b
t4limit.Initialize();
t4limit.DoHTI(false);
t4limit.TestHypothesis(2, 1.1, 2.1); // the real computation is here
// si hypo
  double u4lim_obs    = t4limit.ConfidenceLevelObs();
  double u4lim_exp_m2s= t4limit.ConfidenceLevelExp(-2);
  double u4lim_exp_m1s= t4limit.ConfidenceLevelExp(-1);
  double u4lim_exp    = t4limit.ConfidenceLevelExp(0);
  double u4lim_exp_p1s= t4limit.ConfidenceLevelExp(1);
  double u4lim_exp_p2s= t4limit.ConfidenceLevelExp(2);

  double u4lim_obs_err    = t4limit.ConfidenceLevelObsErr();
  double u4lim_exp_m2s_err= t4limit.ConfidenceLevelExpErr(-2);
  double u4lim_exp_m1s_err= t4limit.ConfidenceLevelExpErr(-1);
  double u4lim_exp_err    = t4limit.ConfidenceLevelExpErr(0);
  double u4lim_exp_p1s_err= t4limit.ConfidenceLevelExpErr(1);
  double u4lim_exp_p2s_err= t4limit.ConfidenceLevelExpErr(2);

//bg hypo
  double u4lim_obs_b    = t4limit.ConfidenceLevelObs(1);
  double u4lim_exp_m2s_b= t4limit.ConfidenceLevelExp(-2,1);
  double u4lim_exp_m1s_b= t4limit.ConfidenceLevelExp(-1,1);
  double u4lim_exp_b    = t4limit.ConfidenceLevelExp(0,1);
  double u4lim_exp_p1s_b= t4limit.ConfidenceLevelExp(1,1);
  double u4lim_exp_p2s_b= t4limit.ConfidenceLevelExp(2,1);

  double u4lim_obs_b_err    = t4limit.ConfidenceLevelObsErr(1);
  double u4lim_exp_m2s_b_err= t4limit.ConfidenceLevelExpErr(-2,1);
  double u4lim_exp_m1s_b_err= t4limit.ConfidenceLevelExpErr(-1,1);
  double u4lim_exp_b_err    = t4limit.ConfidenceLevelExpErr(0,1);
  double u4lim_exp_p1s_b_err= t4limit.ConfidenceLevelExpErr(1,1);
  double u4lim_exp_p2s_b_err= t4limit.ConfidenceLevelExpErr(2,1);

char fname[128]; sprintf (fname,"Limit.txt");
std::ofstream of(fname,fstream::app);
 of << fixed;
 of << endl;
 of << "KF  -2 "<< std::setw(6)<<std::setprecision(4)<< u4lim_kf_m2s << " cls1k " << u4lim_exp_m2s<<" clb1k " << u4lim_exp_m2s_b<< endl;
 of << "KF  -1 "<< std::setw(6)<<std::setprecision(4)<< u4lim_kf_m1s << " cls1k " << u4lim_exp_m1s<<" clb1k " << u4lim_exp_m1s_b<< endl;
 of << "KF med "<< std::setw(6)<<std::setprecision(4)<< u4lim_kf_med << " cls1k " << u4lim_exp    <<" clb1k " << u4lim_exp_b    << endl;
 of << "KF  +1 "<< std::setw(6)<<std::setprecision(4)<< u4lim_kf_p1s << " cls1k " << u4lim_exp_p1s<<" clb1k " << u4lim_exp_p1s_b<< endl;
 of << "KF  +2 "<< std::setw(6)<<std::setprecision(4)<< u4lim_kf_p2s << " cls1k " << u4lim_exp_p2s<<" clb1k " << u4lim_exp_p2s_b<< endl;
 of << "KF obs "<< std::setw(6)<<std::setprecision(4)<< u4lim_kf_obs << " cls1k " << u4lim_obs    <<" clb1k " << u4lim_obs_b    << endl;
 of << "XS 0   "<< std::setw(6)<<std::setprecision(4)<< u4limit.GetXS0()  << endl << endl;

for (int sigma=-2; sigma<4; sigma++) {
// now the same calculation using TLimit
int aaa=999; // this is to see what happens in each step
float err=0.005; // this is how close we want to get the 5x10^-2 which is the 95% CL.
float epsi=9.9; // this is the initial value of the calculated CLs and Expected CLS=4.55x10^-2
float epsi_prev; // we heep track of the previous epsilon to see if the calculation converges or not.
float coeff=0.10; // initially we search the k with 5% variations and as epsilon gets smaller so does this percentage.
int sgnChangeCnt=0; // keep track of the number of sign changes to count if the calculation oscillates or converges.
float k=1.0; // the initial coefficient
float target=0.05; // this is the target 1.96 sigma AKA 95% CL., 4.55E-2 is 2 sigma
int mcevts=5000; // ngu made this 5k instead of 50k, he is not patient. feel free to increase
float tmpcl_prev;

//Variables in vector form ; vectors initialized
std::vector<double>* kvec = new std::vector<double>(0);
std::vector<double>* CLvec= new std::vector<double>(0);

if (sigma==3) sigma=99;

do {
// limits and such
 TH1* toplams = (TH1*) sih->Clone();
 TH1* toplamb = (TH1*) bgh->Clone();
 TH1* toplamq = (TH1*) dth->Clone();
  toplams->Scale(k);
if (numch>1) {
 TH1* toplams2 = (TH1*) sih2->Clone();
 toplams2->Scale(k);
}
 // the 1D case
  if (sigma!=99) {
   toplamq->Add(toplams,toplamb);    // signal +bg ~= data
    cout << "MC mode @"<< " ";
  } else {
   toplamq = (TH1*) dth->Clone();
    cout << "Data mode @"<<" ";
  }
   TLimitDataSource* mydatasource  = new TLimitDataSource();
   mydatasource->AddChannel(toplams,toplamb,toplamq);
   if (numch>1) mydatasource->AddChannel(toplams2,bgh2,dth2);
   TConfidenceLevel *myconfidence = TLimit::ComputeLimit(mydatasource,mcevts,0);
// if staterr=0: no statistical fluctiations
// if staterr=1: with statistical fluctiations

   if (sigma!=99) {
      tmpclb=myconfidence->GetExpectedCLb_b(sigma);  // bg only
      if (clsclsb) {
       tmpcl=myconfidence->GetExpectedCLsb_b(sigma);  // expected
      } else {
       tmpcl=myconfidence->GetExpectedCLs_b(sigma);  // expected
      }
   } else {
      tmpclb=myconfidence->CLb(0);  // bg only
      if (clsclsb) {
       tmpcl=myconfidence->CLsb(); //observed aka measured
      } else {
       tmpcl=myconfidence->CLs(); //observed aka measured
                  tmp2cl=myconfidence->CLsb();
      }
   }
 // myconfidence->Draw();
   mydatasource->Delete();
   myconfidence->Delete();


// at this point k and tmpcl are calculated.
   epsi_prev=epsi;
   epsi=(target-tmpcl);
   if (!clsclsb && (k==1)) { // we save the measured CLs value for Sinan's CLs plots
    cls_1k=tmpcl;
    clb_1k=tmpclb;
   }
   cout << "k="<<k<<"  currentCL="<<tmpcl<<"  epsi="<<epsi<<endl;
   kvec->push_back(k);
   CLvec->push_back(tmpcl);
   if ((fabs(epsi)<err) || (k<1e-4) || (coeff<1e-4)) {
      char filen[128];
      char outtxt[128];

       of << setprecision(2)<<k;
       switch (sigma)  {
        case -2: sprintf (filen,"KF  -2"); break;
        case -1: sprintf (filen,"KF  -1"); break;
        case  0: sprintf(filen, "KF Med"); break;
        case 99: sprintf(filen, "KF Obs"); break;
        case +1: sprintf(filen, "KF  +1"); break;
        case +2: sprintf(filen, "KF  +2"); break;
       }
       sprintf (outtxt,"  %s ",filen);
       of << outtxt;
       of << setprecision(5)<<" +- "<< epsi<< " cls1k "<< cls_1k << " clb1k "<< clb_1k<<endl;
#ifdef __DEBUG__
       TCanvas* c2 = new TCanvas();
       c2->SetLogy();
       toplams->SetMaximum( toplamb->GetMaximum()*1E3);
       toplams->SetMinimum( toplamb->GetMaximum()/1E5);
       toplams->SetMarkerStyle(20); toplams->SetMarkerColor(40); toplams->SetLineColor(40);
       toplamb->SetMarkerStyle(22); toplamb->SetMarkerColor(kBlack); toplamb->SetLineColor(kBlack);
       toplamq->SetMarkerStyle(21); toplamq->SetMarkerColor(kBlue); toplamq->SetLineColor(kBlue);
       toplams->Draw("e"); toplamb->Draw("same"); toplamq->Draw("same");
       TLegend *leg  = new TLegend( 0.61, 0.62, 0.97, 0.90 );
       leg->AddEntry(toplams,"signal","lp");
       leg->AddEntry(toplamb,"background","lp");
       leg->AddEntry(toplamq,"total/data","lp");
       leg->Draw();
       sprintf (outtxt,"%d-%s.eps",qm,filen);
       c2->SaveAs(outtxt);
#endif
       epsi=0.0;
   } else {
   // we have to adjust
   if (aaa!=999) { cout << "0 for next step, 999 to continue, -1 to exit"<<endl;
                   cin >> aaa; if (aaa==-1) { exit(1);}
   }

   if ((epsi_prev/fabs(epsi_prev)) != (epsi/fabs(epsi))) sgnChangeCnt++;
   if (sgnChangeCnt>10) {
     PolFitter P(CLvec,kvec,2);
//   P.graph(true);
//   TF1* CLfit = P.fitfunction();
//   P.profile(30,0.,0.4,0.,5.,true);
     TF1* CLfit = P.fitprofile(30,0.,0.4,0.,6.);
     k=fabs(CLfit->Eval(0.05));
     cout << "FROM FIT.........................................."<<k<<endl;
   //  k=k+0.0123;
     sgnChangeCnt=0;
     cout << "Kick to make it converge...................................................."<<endl;
     coeff/=1.5;
   } else {
  // work in steps of coeff
     if (epsi>0) {
        k=k*(1-coeff);
     } else {
        k=k*(1+coeff);
     }
   }

   if (tmpcl==tmpcl_prev ) { k=k/1.2;}
   if (tmpcl==1) k=k/1.1;
   if (tmpcl==0) k=0.25;
   if (k<0) k=0.25;
   }
  
   tmpcl_prev=tmpcl;
   toplams->Delete();
   toplamb->Delete();
   toplamq->Delete();
} while ((fabs(epsi)>err) ) ;
  cout << "k="<<k<<"  currentCL="<<tmpcl<<"  epsi="<<epsi<<endl;

}// end of sigma loop

cout << "=======================finished===================="<<endl;
cout << "Look at the last 14 lines of Limit.txt and compare."<<endl<< endl; 

} else {

  cout << "Getting results from RSLimit Calculations " << endl;
// get results
  double u4lim_obs    = u4limit.ConfidenceLevelObs();
  double u4lim_exp_m2s= u4limit.ConfidenceLevelExp(-2);
  double u4lim_exp_m1s= u4limit.ConfidenceLevelExp(-1);
  double u4lim_exp    = u4limit.ConfidenceLevelExp(0);
  double u4lim_exp_p1s= u4limit.ConfidenceLevelExp(1);
  double u4lim_exp_p2s= u4limit.ConfidenceLevelExp(2);
  cout << "Done with RSLimit Calculations " << endl;
// get error results
  double u4lim_obs_err    = u4limit.ConfidenceLevelObsErr();
  double u4lim_exp_m2s_err= u4limit.ConfidenceLevelExpErr(-2);
  double u4lim_exp_m1s_err= u4limit.ConfidenceLevelExpErr(-1);
  double u4lim_exp_err    = u4limit.ConfidenceLevelExpErr(0);
  double u4lim_exp_p1s_err= u4limit.ConfidenceLevelExpErr(1);
  double u4lim_exp_p2s_err= u4limit.ConfidenceLevelExpErr(2);
  cout << "Done with RSLimit Error Calculations " << endl;
// now work the same numbers with TLimit class
  TLimitDataSource* mydatasource = new TLimitDataSource(sih,bgh,dth);
       if (numch>1) mydatasource->AddChannel(sih2,bgh,dth);
       if (numch>2) mydatasource->AddChannel(sih2,bgh,dth);

  TConfidenceLevel *myconfidence = TLimit::ComputeLimit(mydatasource,90000);
  if (clsclsb) {
    double cls_tl=myconfidence->CLsb();
    double cls_tl_exp0= myconfidence->GetExpectedCLsb_b();
    double cls_tl_exp_m1s=myconfidence->GetExpectedCLsb_b(-1);
    double cls_tl_exp_p1s=myconfidence->GetExpectedCLsb_b(+1);
    double cls_tl_exp_m2s=myconfidence->GetExpectedCLsb_b(-2);
    double cls_tl_exp_p2s=myconfidence->GetExpectedCLsb_b(+2);
  } else {
    double cls_tl=myconfidence->CLs();
    double cls_tl_exp0= myconfidence->GetExpectedCLs_b();
    double cls_tl_exp_m1s=myconfidence->GetExpectedCLs_b(-1);
    double cls_tl_exp_p1s=myconfidence->GetExpectedCLs_b(+1);
    double cls_tl_exp_m2s=myconfidence->GetExpectedCLs_b(-2);
    double cls_tl_exp_p2s=myconfidence->GetExpectedCLs_b(+2);
  }
  cout << "Done with TLimit Calculations " << endl;

// print results
  TString b="";
  if (clsclsb) b="b";
  cout << "Comparing results from TLimit vs RSLimit " << endl;
  cout << "CLs"<<b<<" Obs TL: "<< std::setw(6)<<std::setprecision(4)<< cls_tl         << "   RSL:" << u4lim_obs << "   +/-" << u4lim_obs_err << endl;
  cout << "CLs"<<b<<" Exp TL: "<< std::setw(6)<<std::setprecision(4)<< cls_tl_exp0    << "   RSL:" << u4lim_exp << "   +/-" << u4lim_exp_err << endl;
  cout << endl;
  cout << "CLs"<<b<<" -1s TL: "<< std::setw(6)<<std::setprecision(4)<< cls_tl_exp_m1s << "   RSL:" << u4lim_exp_m1s << "   +/-" << u4lim_exp_m1s_err << endl;
  cout << "CLs"<<b<<" +1s TL: "<< std::setw(6)<<std::setprecision(4)<< cls_tl_exp_p1s << "   RSL:" << u4lim_exp_p1s << "   +/-" << u4lim_exp_p1s_err << endl;
  cout << endl;
  cout << "CLs"<<b<<" -2s TL: "<< std::setw(6)<<std::setprecision(4)<< cls_tl_exp_m2s << "   RSL:" << u4lim_exp_m2s << "   +/-" << u4lim_exp_m2s_err << endl;
  cout << "CLs"<<b<<" +2s TL: "<< std::setw(6)<<std::setprecision(4)<< cls_tl_exp_p2s << "   RSL:" << u4lim_exp_p2s << "   +/-" << u4lim_exp_p2s_err << endl;
}


}
