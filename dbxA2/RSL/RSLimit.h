//////////////////////////////////////////////////////////////////////////////////
// RSLimit, a wrapper library for RooStats Hybrid Calculator similar to TLimit
// contact gokhan.unel@cern.ch for bug reports and suggestions
// v0.1   --    28 April  2011  cint compatible -- initial version
// v0.2   --    29 April  2011  aclic compatible
// v0.4   --    29 August 2011  HypoTestInverter added
//////////////////////////////////////////////////////////////////////////////////

#ifndef RSLIMIT_H
#define RSLIMIT_H
#include "RooGlobalFunc.h"
#include "RooRealVar.h"
#include "RooProdPdf.h"
#include "RooWorkspace.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooHistPdf.h"
#include "TCanvas.h"
#include "TH1.h"
#include "TFile.h"
#include "TROOT.h"
#include "RooPlot.h"
#include "RooMsgService.h"
#include <iomanip>
#include "TLimit.h"
#include "TLimitDataSource.h"
#include "TConfidenceLevel.h"
#include <vector>

#include "RooStats/NumberCountingUtils.h"
#include "RooStats/HybridCalculator.h"
#include "RooStats/HybridResult.h"
#include "RooStats/ToyMCSampler.h"
#include "RooStats/HypoTestPlot.h"
#include "RooStats/ProfileLikelihoodTestStat.h"
#include "RooStats/SimpleLikelihoodRatioTestStat.h"
#include "RooStats/RatioOfProfiledLikelihoodsTestStat.h"
#include "RooStats/MaxLikelihoodEstimateTestStat.h"
#include "RooStats/HypoTestInverterResult.h"
#include "RooStats/HypoTestInverter.h"
#include "RooStats/HypoTestInverterPlot.h"
#include "RooStats/NumEventsTestStat.h"
#include "RooStats/ProofConfig.h"
#include "RooSimultaneous.h"
#include "RooCategory.h"

using namespace RooFit;
using namespace RooStats;
using namespace std;


typedef struct histodata
{
  TH1 *histo;
  float rate_uncert[2];
  bool nuis;
  TString chname;
}histodata;


class RSLimit{
 public:
        RSLimit(TH1*, TH1*, TH1*, TString chname="CH1", double xmin=0, double xmax=10000.0); // signal, background, data histograms
        RSLimit(TH1*, TString chname="CH1", double xmin=0, double xmax=10000.0); // data histogram only
       ~RSLimit();

        int    AddChannel(TH1*, TString chname="CH1");
        int    AddSignalHistogram(TH1*, float, float, TH1**, TH1**, TString chname="CH1");
        int    AddBackgroundHistogram(TH1*, float, float, TH1**, TH1**, TString chname="CH1");
        double ConfidenceLevelExp(int, int bg=0); // sigmas
        double ConfidenceLevelObs(int bg=0);
        double ConfidenceLevelExpErr(int, int bg=0); // sigmas
        double ConfidenceLevelObsErr(int bg=0);
        double GetKFactorExp(int); // sigmas
        double GetKFactorObs( ); // sigmas
        void   SetToyMC(int, int); // signal, background
        void   Initialize();
        void   VerbosityLevel(short int);
        int    UseProof(short int);
        void   DoHTI(bool v) {p_use_hti=v; };
        void   Dump() {p_w->Print(); };
        void   SaveAs(const char *v) {p_w->SaveAs(v); };
        void   SetCL(float v) {p_cl=v;};
        void   DrawHTIPlot(TString ofn="RSL_HTI.pdf");
        double GetXS0();
        void   SetCLsCLsb(int); //0: cls 1:CLsb , default CLsb
        void   SetDiscriminator(string, int, int); // name of the variable, range_min, range_max
        void   SelectTestStats(int); // this could be an enumerated type 0=Simple, 1=Profile, 2=NumEvents...
        int    TestHypothesis(int nb=10, float smin=0.0, float smax=50.0); // where the flute says zirt 

 private:
        void InitInternals(double, double);
        TH1 *p_sih, *p_bgh, *p_dth;
        vector<histodata> p_si_data;
        vector<histodata> p_bg_data;
        vector<RooDataHist> p_data;
        vector<TString> p_ch_names; 
        vector<double> p_crosssection;
        int p_si_toymc, p_bg_toymc;
        int p_clsclsb;
        int p_teststats_id;        
        float p_cl;        
        bool p_use_hti;
        bool p_use_nui;
        string p_xname;
        int p_ch_count;

        RooWorkspace* p_w;
        RooRealVar *p_x;
        RooDataHist *p_combData;
        ModelConfig *p_sb_model, *p_b_model;
        HybridResult *p_testresult;
        HypoTestInverterResult * p_invresult;
        SamplingDistribution *p_hti_exp;
        ProofConfig* p_proofcfg;
        double p_done_once;
        double p_rs_exp, p_rs_exp_m1s, p_rs_exp_p1s, p_rs_exp_m2s, p_rs_exp_p2s;
        double p_rs_exp_err, p_rs_exp_m1s_err, p_rs_exp_p1s_err, p_rs_exp_m2s_err, p_rs_exp_p2s_err;
        double p_rs_expb, p_rs_exp_m1b, p_rs_exp_p1b, p_rs_exp_m2b, p_rs_exp_p2b;
        double p_rs_expb_err, p_rs_exp_m1b_err, p_rs_exp_p1b_err, p_rs_exp_m2b_err, p_rs_exp_p2b_err;
};
#endif
