#### NB: In conjunction to this file, it is advised to at least skim through the files README_188 & 
######## README_188_WORKERS. ####

----------------------------------------------------------------------------------------------------------------------
##################################### running the dbxa framework #####################################################

dbxa is run in two different ways.  The first of these is based on running on the input d3pd files on the grid.  
Since each modification of the input parameters, or any of the cuts requiring resubmitting the whole thing to 
the grid, this method is cumbersome and slow, depending on your grid priorities.  The second method of running 
dbxa is based on the "Dump" analysis.  In this option, an "empty" analysis instance is created that extracts 
information from the d3pds and produces so-called lvl0.root files.  The Dump analysis hence runs one time on the 
grid and the output lvl0 files are then used as the basis for detailed local analysis.  The local analysis is 
designed to "run parallel" with the help of PROOF, possibly on a local farm of computers.

PROOF itself supports two different kinds of parallel running.  The first is called PROOF lite, which allows the 
use of the multiple cores on a given machine (like your laptop).  The second makes use of a PROOF farm, with 
multiple computers and cores on each computer to highly parallelize the computation.


####### running with dbxArunner.C - A simple tutorial ################################################################

To run dbxa on our own computers without the help of a proof farm, we have a piece of driver code, aptly named 
dbxArunner.C, which resides in the toplu folder.  We can use this driver to do some quick exercises.  Before we 
start let us obtain an example lvl0 file, say named 76.lvl0.root.  As a quick hack, we can modify the 17th line 
of dbxArunner.C to set the name of our input file:
   TFile *f = TFile::Open("76.lvl0.root");

Now let us start ROOT and run the dbxArunner.C code by typing "root -l dbxArunner.C" (or by typing "root" and 
then entering ".X dbxArunner.C").  It will print to the screen the total number of events in the input file and 
will then proceed to printing the numbers of electrons, muons and jets, the run and event numbers for the first 
three events in the file.  It will finish up by counting and printing the total number of good muons.  (Please 
see the code itself for how a good muon is being defined.)

dbxArunner.C is the code that allows us to exercise with the objects in lvl0.root files locally.  We suggest 
using it as a simple "dbxa hello world" application.  For serious running, we suggest using the runSelector.C, 
which allows us to using multiple machines/cores to parallelize the work done with dbxArunner.C or other similar 
driver codes.

####### running with runSelector.C ###################################################################################

In order to utilize the parallelization feature, we make use of the macro runSelector.C in the toplu folder.  
After having implemented our analysis within the dbxa code (or having modified an already existing one, see the 
main() function of root_analysisd3pd.C for examples), we start root ("root -l") and execute runSelector.C:
  .X runSelector.C("1",1,"-FF 1")

In this example, we are locally running a single instance of the fourth-family (FF) analysis with the parameters 
read from the first FF card file.  An proof interface appears on the screen which allows us to monitor the 
errors and speed of processing, etc.

The first argument of runSelector is a string that plays two roles.  The first is the identification of a text 
file which is going to be fed into root's TFileCollection, a text file which provides a list of input lvl0 files 
to be run on.  So in this example, string "1" means that runSelector will look for a file named "1.txt", which 
should contain the list of lvl0 files we want to run on.  The second use of the first argument is the naming of the
output root file: it will be named "1.root" at the end.

The second argument of runSelector is an integer, with 1 representing prooflite, and 0 for running on our proof 
server (pc-atb-srv-01, if you have a different xrootd server, you need to search and replace the name of the 
proof server).

As an example, consider that we obtained an test file from the /media/data1/mc_rel17_7teV/ folder on our server 
atb@pc-atb-srv-01.cern.ch.  Imagine this file is named as 1.lvl0.root (or for the sake of the example, simply 
rename it as such).  Now we create a text file named "1.txt" and we write the location (path) of our lvl0 file 
into 1.txt (ex: /home/test/1.lvl0.root).  Having done that, if we execute runSelector.C as written above, we 
will be running one instance of the FF analysis with prooflite on 1.lvl0.root and the output will be named 1.root.


###### running run_proof.sh on our atb server #######################################################################

While running on individual files can easily be done with runSelector, running on entire datasets is handled by 
run_proof.sh.  On the second line of this bash script requires, the variable uselight can set either to 1, which
will allow us to use prooflite on our local machine, or to 0, which instructs run_proof to use the proof cluster.

ssh to atb@pc-atb-srv-01.cern.ch and edit run_proof.sh located in the folder /dbxa/dbxa/toplu/.  Around line 30, 
you will see a for loop that sets the variable mcf.  Removing the # sign on a given line and putting # signs at 
the start of others will allow you to choose a particular data/mc set to be run.  (# is the commenting character 
in bash.) Consider we choose to use (uncomment) the line "for mcf in $mcList ; do".  When you run run_proof.sh 
(using the command: "nohup ./run_proof.sh &" ), we will get the cluster to run on the whole of Monte Carlo 
(signal and background).

The analysis arguments are given on line 39. For example:
  opt=" -FF 2 -S 0 "

This means running two instances of FF analysis, one using the input parameters in FF_1-card.txt and the other 
using the parameters in FF_2-card.txt, both located under the FF folder.  "-S 0 " tells the code not to bother 
the systematic variations.  "-S 1 " would have the systematics run as well.

#### Important: Please make sure you follow the convention about the white spaces. There is an inline warning in 
run_proof.sh about this issue. ####

Finally, it is important note that when we want to run data (instead of MC), one needs to choose between $Data_e 
and $Data_m, for electrons and muons.  Running on both in parallel is not currently supported.
####################################################################################################################
