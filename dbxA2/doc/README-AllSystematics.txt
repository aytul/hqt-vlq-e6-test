nominal ---> SF variations are applied to nominal objects
EG_RESOLUTION_ALL__1down
EG_RESOLUTION_ALL__1up
EG_SCALE_ALL__1down
EG_SCALE_ALL__1up
JET_21NP_JET_BJES_Response__1down
JET_21NP_JET_BJES_Response__1up
JET_21NP_JET_EffectiveNP_1__1down
JET_21NP_JET_EffectiveNP_1__1up
JET_21NP_JET_EffectiveNP_2__1down
JET_21NP_JET_EffectiveNP_2__1up
JET_21NP_JET_EffectiveNP_3__1down
JET_21NP_JET_EffectiveNP_3__1up
JET_21NP_JET_EffectiveNP_4__1down
JET_21NP_JET_EffectiveNP_4__1up
JET_21NP_JET_EffectiveNP_5__1down
JET_21NP_JET_EffectiveNP_5__1up
JET_21NP_JET_EffectiveNP_6__1down
JET_21NP_JET_EffectiveNP_6__1up
JET_21NP_JET_EffectiveNP_7__1down
JET_21NP_JET_EffectiveNP_7__1up
JET_21NP_JET_EffectiveNP_8restTerm__1down
JET_21NP_JET_EffectiveNP_8restTerm__1up
JET_21NP_JET_EtaIntercalibration_Modelling__1down
JET_21NP_JET_EtaIntercalibration_Modelling__1up
JET_21NP_JET_EtaIntercalibration_NonClosure__1down
JET_21NP_JET_EtaIntercalibration_NonClosure__1up
JET_21NP_JET_EtaIntercalibration_TotalStat__1down
JET_21NP_JET_EtaIntercalibration_TotalStat__1up
JET_21NP_JET_Flavor_Composition__1down
JET_21NP_JET_Flavor_Composition__1up
JET_21NP_JET_Flavor_Response__1down
JET_21NP_JET_Flavor_Response__1up
JET_21NP_JET_Pileup_OffsetMu__1down
JET_21NP_JET_Pileup_OffsetMu__1up
JET_21NP_JET_Pileup_OffsetNPV__1down
JET_21NP_JET_Pileup_OffsetNPV__1up
JET_21NP_JET_Pileup_PtTerm__1down
JET_21NP_JET_Pileup_PtTerm__1up
JET_21NP_JET_Pileup_RhoTopology__1down
JET_21NP_JET_Pileup_RhoTopology__1up
JET_21NP_JET_PunchThrough_MC15__1down
JET_21NP_JET_PunchThrough_MC15__1up
JET_21NP_JET_SingleParticle_HighPt__1down
JET_21NP_JET_SingleParticle_HighPt__1up
JET_JER_SINGLE_NP__1up
LARGERJET_Medium_JET_Comb_Baseline_Kin__1down
LARGERJET_Medium_JET_Comb_Baseline_Kin__1up
LARGERJET_Medium_JET_Comb_Modelling_Kin__1down
LARGERJET_Medium_JET_Comb_Modelling_Kin__1up
LARGERJET_Medium_JET_Comb_TotalStat_Kin__1down
LARGERJET_Medium_JET_Comb_TotalStat_Kin__1up
LARGERJET_Medium_JET_Comb_Tracking_Kin__1down
LARGERJET_Medium_JET_Comb_Tracking_Kin__1up
LARGERJET_Medium_JET_Rtrk_Baseline_Sub__1down
LARGERJET_Medium_JET_Rtrk_Baseline_Sub__1up
LARGERJET_Medium_JET_Rtrk_Modelling_Sub__1down
LARGERJET_Medium_JET_Rtrk_Modelling_Sub__1up
LARGERJET_Medium_JET_Rtrk_TotalStat_Sub__1down
LARGERJET_Medium_JET_Rtrk_TotalStat_Sub__1up
LARGERJET_Medium_JET_Rtrk_Tracking_Sub__1down
LARGERJET_Medium_JET_Rtrk_Tracking_Sub__1up
LARGERJET_Strong_JET_Comb_Baseline_All__1down
LARGERJET_Strong_JET_Comb_Baseline_All__1up
LARGERJET_Strong_JET_Comb_Modelling_All__1down
LARGERJET_Strong_JET_Comb_Modelling_All__1up
LARGERJET_Strong_JET_Comb_TotalStat_All__1down
LARGERJET_Strong_JET_Comb_TotalStat_All__1up
LARGERJET_Strong_JET_Comb_Tracking_All__1down
LARGERJET_Strong_JET_Comb_Tracking_All__1up
LARGERJET_Weak_JET_Rtrk_Baseline_Split23__1down
LARGERJET_Weak_JET_Rtrk_Baseline_Split23__1up
LARGERJET_Weak_JET_Rtrk_Baseline_Tau32WTA__1down
LARGERJET_Weak_JET_Rtrk_Baseline_Tau32WTA__1up
LARGERJET_Weak_JET_Rtrk_Baseline_pT__1down
LARGERJET_Weak_JET_Rtrk_Baseline_pT__1up
LARGERJET_Weak_JET_Rtrk_Modelling_Split23__1down
LARGERJET_Weak_JET_Rtrk_Modelling_Split23__1up
LARGERJET_Weak_JET_Rtrk_Modelling_Tau32WTA__1down
LARGERJET_Weak_JET_Rtrk_Modelling_Tau32WTA__1up
LARGERJET_Weak_JET_Rtrk_Modelling_pT__1down
LARGERJET_Weak_JET_Rtrk_Modelling_pT__1up
LARGERJET_Weak_JET_Rtrk_TotalStat_Split23__1down
LARGERJET_Weak_JET_Rtrk_TotalStat_Split23__1up
LARGERJET_Weak_JET_Rtrk_TotalStat_Tau32WTA__1down
LARGERJET_Weak_JET_Rtrk_TotalStat_Tau32WTA__1up
LARGERJET_Weak_JET_Rtrk_TotalStat_pT__1down
LARGERJET_Weak_JET_Rtrk_TotalStat_pT__1up
LARGERJET_Weak_JET_Rtrk_Tracking_Split23__1down
LARGERJET_Weak_JET_Rtrk_Tracking_Split23__1up
LARGERJET_Weak_JET_Rtrk_Tracking_Tau32WTA__1down
LARGERJET_Weak_JET_Rtrk_Tracking_Tau32WTA__1up
LARGERJET_Weak_JET_Rtrk_Tracking_pT__1down
LARGERJET_Weak_JET_Rtrk_Tracking_pT__1up
MET_SoftTrk_ResoPara
MET_SoftTrk_ResoPerp
MET_SoftTrk_ScaleDown
MET_SoftTrk_ScaleUp
MUON_ID__1down
MUON_ID__1up
MUON_MS__1down
MUON_MS__1up
MUON_SAGITTA_RESBIAS__1down
MUON_SAGITTA_RESBIAS__1up
MUON_SAGITTA_RHO__1down
MUON_SAGITTA_RHO__1up
MUON_SCALE__1down
MUON_SCALE__1up


