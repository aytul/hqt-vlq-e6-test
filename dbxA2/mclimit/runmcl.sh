#!/bin/bash

echo root is here: $ROOTSYS
export PATH=$PATH:$ROOTSYS/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ROOTSYS/lib:.
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$XRDSYS/lib
export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:$XRDSYS/lib/:$ROOTSYS/lib:.

inp1=$1
inp2=$2
numbg=$3
errt=$4
mcevents=$5
# this is in fact the number of shapes

if [ -e mclimit.out ]; then
 echo output file exists. MCL will not run again, just retrieve results.
else
 echo running to get limits...
# ../mclimit/dbx_mcl.exe nosyst sibg.root sibg.root $numbg -1  > mclimit.out
# ../mclimit/dbx_mcl.exe nosyst sibg_ELE_350.root sibg_MUON_350.root $numbg -1  > mclimit.out
### below has worked correctly w/o syst
 ../mclimit/dbx_mcl.exe ${errt} ${inp1} ${inp2} $numbg -1 ${mcevents} > mclimit.out
 echo ../mclimit/dbx_mcl.exe ${errt} ${inp1} ${inp2} $numbg -1 ${mcevents}
###../mclimit/dbx_mcl.exe syst ${inp1} ${inp2} 1 -1  > mclimit.out
### above I put 1 syst shape only: the other has a negative bin somewhere 
fi


