#include <stddef.h>
#include "TFile.h"
#include "TROOT.h"
#include "mclimit_csm.h"
#include <iostream>
#include <stdlib.h>
#include "TH1.h"
#include "TH1F.h"
#include "TCanvas.h"
#include "TRandom3.h"
#include "TParameter.h"
#include <string>
#include <math.h>

using namespace std;

int main(int argc, const char *argv[])
{
  if(argc < 6){
    cout << "Usage: " << argv[0] << " <withsyst or nosyst or testsyst or teststat> <CH1InputFile> <CH2InputFile> <Number of shapes> <-1=s95_all 0=s95(data), 1=s95med, 2=s95p1, 3=s95p2, 4=s95m1; 5=s95m2> <opt mcevents, default 10000> <opt sys to exclude, -1 excludes none> <opt: number of profiled parameters>" << endl;
    cout << "=> testsyst: systematic machinery check with zero systematics" << endl;
    cout << "=> teststat: check the two channels using same input with no systematics" << endl;
    // The following two runs should yield results that agree within stats:
    // ./dbx_mcl.exe nosyst sibg_MUON_400_8_2_syst.root{,} 6 -1
    // ./dbx_mcl.exe teststat sibg_MUON_400_8_2_syst.root{,} 6 -1
    return 0;
  }

  int NumberOfBgProfiles = 0;
  int NumberOfSigProfiles = 0;
  int SysToExclude = -1;
  const int s95type = atoi(argv[5]); // -1 : means do them all.
  int mc_npe = 10000;
  if (argc > 6 ) { mc_npe=atoi(argv[6]);
    cout << "mc_npe = " << mc_npe << endl; }
  if (argc > 7 ) SysToExclude = atoi(argv[7]);
  if (argc > 8) {
    NumberOfBgProfiles = atoi(argv[8]);
    NumberOfSigProfiles = NumberOfBgProfiles;
  }

  //Set up the misc. arrays that will act as inputs to the hypothesis tests
  int hsn = 0;  // highest background sample number
  TFile *ftmp = new TFile(argv[2]);
  if ( ! ftmp->IsZombie() ) {
    TList *ftmpls = ftmp->GetListOfKeys();
    for ( int il = 0; il<ftmpls->GetEntries(); ++il) {
      TString objn = ftmpls->At(il)->GetName();
      // focus on strings starting with sample and not containing syst
      if ( objn.BeginsWith("sample_") && objn.Index("syst")<0 ) {
	objn.Remove(0,7); // remove "sample_"
	hsn = ( hsn < objn.Atoi() ? objn.Atoi() : hsn ); }
    }
    ftmp->Close();
  }
  delete ftmp;
  cout << "Using the number of bkg samples determined from the 1st file = "
       << hsn << endl;

  const int nbg = hsn; // 6 // 1
  const int CH_MAX = 2; // electrons and/or muons

  // varous hypotheses, to be used later
  csm_model *nullhyp, *testhyp, *nullhyp_pe, *testhyp_pe; // last two are pseudo exp?
  nullhyp = new csm_model();
  testhyp = new csm_model();
  nullhyp_pe = new csm_model(); // pseudo exp?
  testhyp_pe = new csm_model(); // pseudo exp?

  // channel name
  char* ChanName[CH_MAX] ;
  ChanName[0] = (char*)"CHAN1";
  ChanName[1] = (char*)"CHAN2";

  //Number of shape systematics + scale systematics 
  //For example, it should be 7 for 6 shapes, 1 rate
  int nps_b = atoi(argv[4])+1 ; // # bg systematics, read from the cmdline
  int nps_s = nps_b;
  int nps_b_fakes = 1;
  const int npsmax = 22; //  max systematic sources, including the rate
  if ( nps_b >= npsmax ) {
    cout << "The max number of systematics allowed is " << npsmax << endl;
    exit(-1); }
  // it does not make much sense to set this to any value except unity, as our MC histograms
  //  are non-Poisson and are already scaled to their respective luminosities
  const Double_t sf = 1.0;

  std::string type = argv[1];
  if(type == "nosyst" || type == "teststat"){
    nps_b = 0; nps_s = 0; 
    NumberOfBgProfiles = 0; NumberOfSigProfiles = 0;
    nps_b_fakes = 0;
  }
  else if ( type != "withsyst" && type != "testsyst" ) {
    cout << "Only four options are allowed: nosyst, withsyst, testsyst, teststat!\n";
    exit(-1);
  }

  cout << "nps_s = nps_b = "<< nps_b <<endl;

  double* scale_s_low = new double[npsmax];  // signal scale = rate low
  double* scale_s_high = new double[npsmax]; //  --- high
  double** scale_b_low = new double*[nbg];  // bg scale = rate low
  double** scale_b_high = new double*[nbg]; // --- high
  double* lowsigma_s = new double[npsmax]; 
  double* highsigma_s = new double[npsmax];
  double** lowsigma_b = new double*[nbg];
  double** highsigma_b = new double*[nbg];

  TH1F* h_signal = 0; TH1F* h_data[CH_MAX]; 
  TH1F** h_bg = new TH1F*[nbg];

  TH1F** lowshape_s = new TH1F*[npsmax];
  TH1F** highshape_s = new TH1F*[npsmax];
  TH1F*** lowshape_b = new TH1F**[nbg];
  TH1F*** highshape_b = new TH1F**[nbg];
  double s_scale_up   = 0.0;
  double s_scale_down = 0.0;
  double* b_scale_up   = new double[nbg];
  double* b_scale_down = new double[nbg];
  char** ename_s = new char*[npsmax]; 
  char*** ename_b = new char**[nbg];

  for(int i=0; i < nbg; i++){
    scale_b_low[i]  = new double[npsmax];
    scale_b_high[i] = new double[npsmax];
     lowsigma_b[i]  = new double[npsmax];
    highsigma_b[i]  = new double[npsmax];
        ename_b[i]  = new char*[npsmax];
    lowshape_b[i]   = new TH1F*[npsmax];
    highshape_b[i]  = new TH1F*[npsmax];
  }

  //Initialize everything, set points to 0, set values to 0
  for(int i=0; i < npsmax; i++){
    scale_s_low[i] = 0.0; scale_s_high[i] = 0.0;
    lowsigma_s[i] = 0.0; highsigma_s[i] = 0.0;
    lowshape_s[i] = 0; highshape_s[i] = 0;

    for(int j=0; j < nbg; j++){
       b_scale_up[j]    = 0.0; b_scale_down[j]    = 0.0;
      scale_b_low[j][i] = 0.0; scale_b_high[j][i] = 0.0;
       lowshape_b[j][i] = 0;    highshape_b[j][i] = 0;
       lowsigma_b[j][i] = 0.0;  highsigma_b[j][i] = 0.0;
          ename_b[j][i] = (Form("Sys_%d",i+1)); // what is this???
	  // if (j == nbg - 1) ename_b[j][i] = (Form("FakeSys_%d",i));
    }
  }

  cout << "Getting input histograms" << endl;

  //Fetch histograms from the input files
  TFile* ch_file[2]; 
  int max_channels=1;
  if ( strcmp(argv[2], argv[3]) != 0 || type == "teststat" ) max_channels=2;
  for ( int i_channel=0; i_channel<max_channels; i_channel++){
    // if teststat option is selected, all channels are replicates of the first channel
    if ( type == "teststat" && i_channel>0 )
      ch_file[i_channel] = ch_file[0];
    else
      ch_file[i_channel] = new TFile(argv[2+i_channel]);

    // Fetch rate uncertainties from the root files
    if ( type != "nosyst" && type != "teststat" )
    for(int j=0; j < nbg; j++){
      TParameter<double> *unpar = (TParameter<double>*)(ch_file[i_channel]->Get(Form("uncert_%d",j+1)));
      if (!unpar) {
	cout << "Could not read the rate uncertainty of bg " << j+1 << endl;
	exit(-1); }
      double unc_val=unpar->GetVal() / 22.; // veo! warning we need to fix 22!
      b_scale_up[j] = unc_val;
      b_scale_down[j] = unc_val;
    }

    cout << "Getting nominal histograms" << endl;
//Get histograms and shape uncertainties and scale uncertainty
    if(!(h_signal=(TH1F*) ch_file[i_channel]->Get("signal")->Clone()))
      { cout << "Error: histogram signal not found in root file" << endl;
	exit(-1); }

    if(!(ch_file[i_channel]->Get("data")))
      { cout << "Error: histogram data not found in root file" << endl;
	exit(-1); }
    else
      // we prefer to clone here, as the data histograms are with us all the way
      //   whereas the other histograms are essentially given to mclimit and
      //   don't get used as themselves afterwards
      h_data[i_channel]=(TH1F*) (ch_file[i_channel]->Get("data")->Clone());
    if ( h_data[i_channel]->GetSumw2N() != 0 ) {
      cout << "Warning, the data histo has error bars. Note that mclimit ignores them." << endl; }

    //Get all bg histograms
    for(int i=1; i<=nbg; i++) {
      //bg histo
      cout << "Getting background histogram "<< i <<endl;
      if (!(h_bg[i-1]=(TH1F*) ch_file[i_channel]->Get(Form("sample_%d",i))))
	{ cout << "Error: bg histogram " << i << " not found in root file" << endl;
	  exit(-1); }
    }

    // Divide all the input histos by two as part of teststat, so that we will be feeding
    //  each "half" of data as a separate channel
    // Strangely enough this scaling works when we are running with only one channel, but
    //  mclimit fails when it is used with two channels. Why???
    // Perhaps we need to try it with some other input file.
    if ( type == "teststat" ) {
      double hsf = 0.5; // hard scale factor, we apply it on the histos themselves directly
      cout << "Hard scaling all input histograms" << endl;
      // note that if hsf is not an integer, data bins will be rounded to nearest int by mclimit
      h_data[i_channel]->Scale(hsf);
      h_signal->Scale(hsf);
      for(int i=0; i<nbg; i++) h_bg[i]->Scale(hsf);
    }

    // ******** EXPERIMENTAL - PROTECTION AGAINST NAN RESULTS **************
    // Here is the logic of this section: NaN results are obtained when the
    //   data is well modelled by background MC at high values of the mass, ht, etc,
    //   but is poorly understood at low values (possibly due to some missing bkg
    //   processes).  As the signal we search for is not peaked at low end of
    //   of the histograms, an excess seen there should be interpreted as MC issues.
    //   Hence we look at the low-end region (at masses/energies at least 1 sigma
    //   lower than the signal peak position) and if there is a significant excess,
    //   we remove those bins from the histograms.
    int imb = h_signal->GetMaximumBin();
    int inz = 1; // first non-zero bin in the data histo
    for ( inz=1; inz<h_signal->GetNbinsX(); ++inz) {
      if ( h_data[i_channel]->GetBinContent(inz) > 0 ) break; }
    int ict = (imb-inz)*2/3+inz;
    int swt = int(h_signal->GetRMS() / h_signal->GetXaxis()->GetBinWidth(1));
    if ( ict > imb-swt ) ict = imb-swt; // cannot come closer than 1 std. dev.
    int tbg = 0; // total bkg in given bins
    int tdt = 0, tdte2; // total data and the squared error in given bins
    for ( int k=inz; k<ict; ++k ) {
      for(int i=0; i<nbg; i++) {
	tbg += h_bg[i]->GetBinContent(k)*(1+b_scale_up[i]); }
      tdt += h_data[i_channel]->GetBinContent(k);
      tdte2 += pow( h_data[i_channel]->GetBinError(k), 2); }
    if ( tbg < tdt - sqrt(tdte2)*2 ) {
      cout << "Cutting off the bins to the left of the " << ict <<"th bin" << endl;
      for ( int k=1; k<ict; ++k ) {
	h_data[i_channel]->SetBinContent(k,0);
	h_data[i_channel]->SetBinError(k,0);
	h_signal->SetBinContent(k,0);
	for(int i=0; i<nbg; i++) h_bg[i]->SetBinContent(k,0);
      }
    }

 /*   
  01_jes0 01_jes1
  02_jer0 02_jer1
  03_msf0 03_msf1
  04_esf0 04_esf1
  05_jespileup0 05_jespileup1
  06_jesbjet0 06_jesbjet1
  07_jetreco0
  08_larfeb0 08_larfeb1
  09_mptsmear0 09_mptsmear1 09_mptsmear2 09_mptsmear3
  10_escaler0 10_escaler1
*/
//Get the shape variations
      cout << "Getting shape and scale variations" << endl;

//////---------- si scale up/down--------------
      //s_scale_up=0.05;    // ngu
      //s_scale_down=0.05;  // ngu

      // signal rate uncertainty:
      scale_s_low [0] = s_scale_down;
      scale_s_high[0] = s_scale_up;
      lowsigma_s [0] = -1.0;
      highsigma_s[0] =  1.0;
      lowshape_s[0] = 0;
      highshape_s[0]= 0;

      //Signal shape uncertainties:
      for(int j=1; j < nps_s; ++j) {
	if(!(lowshape_s[j]=(TH1F*) ch_file[i_channel]->Get(Form("signal_syst_%d_1",j)))) 
	  { cout << "Error: histogram " << Form("signal_syst_%d_1",j) << " not found in root file" << endl; exit(-1);}
	if(!(highshape_s[j]=(TH1F*) ch_file[i_channel]->Get(Form("signal_syst_%d_0",j)))) 
	  { cout << "Error: histogram " << Form("signal_syst_%d_0",j) << " not found in root file" << endl; exit(-1);}
	lowsigma_s [j] = -1.0;
	highsigma_s[j] =  1.0;
	scale_s_low [j] = 0.0;
	scale_s_high[j] = 0.0;
      }

//////---------- bg scale up/down--------------
      for(int i=1; i <= nbg; i++) {
        cout << "Bg " << i << " of " << nbg << "  with syst#"<<nps_b<<endl;
//bg shape uncertainties
        int Offset = 1; // the names in file start with 1 not 0 hence the offset
	// We will use the offset as a way to handle rate systematics
	lowshape_b[i-1][0] = 0;
	highshape_b[i-1][0] = 0;
	lowsigma_b[i-1][0] = -1.0;
	highsigma_b[i-1][0] = 1.0;
	scale_b_low[i-1][0] = b_scale_down[i-1];
	scale_b_high[i-1][0] = b_scale_up[i-1];
	cout << "Bg scale up/down: " << b_scale_up[i-1] << " " << b_scale_down[i-1] << endl;

	for(int j=1; j < nps_b; j++) {
          cout << "reading bg shape low and high uncertainty #"<<j
	       <<"  Name: sample_"<<i<<"_syst_"<<j<<"_1"
	       <<   " and sample_"<<i<<"_syst_"<<j<<"_0"<<endl; 

          if (!(lowshape_b[i-1][j]=(TH1F*)  ch_file[i_channel]->Get(Form("sample_%d_syst_%d_1",i,j)))) 
              cout << "Error: histogram " << Form("sample_%d_syst_%d_1",i,j) << " not found in root file" << endl;
          if (!(highshape_b[i-1][j]=(TH1F*) ch_file[i_channel]->Get(Form("sample_%d_syst_%d_0",i,j)))) 
              cout << "Error: histogram " << Form("sample_%d_syst_%d_0",i,j) << " not found in root file" << endl;
          lowsigma_b[i-1][j] = -1.0;
	  highsigma_b[i-1][j] = 1.0;
          //cout << "#bins in i:"<<i-1<<" j:"<<j<<"  is:"<<highshape_b[i-1][j]->GetNbinsX()<<endl;
 
          scale_b_low[(i-1)][j] = 0; // these are all zero, as the rate
          scale_b_high[(i-1)][j] = 0; // uncertainty is handled separately
        } // end of loop over all shape systematics
       
      } //end of loop over all bgs

      // Test case with zero systematics, ie. we exercise the machinery for
      //   systematics, but the rate systematic has been set to zero and
      //   the shape systematics are run with the nominal shape for both
      //   low and high cases.
      // The obtained results are to be compared to the nosyst output.
      if ( type == "testsyst" ) {
	cout << "Resetting for doing tests of the systematic mode\n";
	scale_s_low [0] = scale_s_high[0] = 0;
	for(int j=1; j < nps_s; ++j) {
	  highshape_s[j]=(TH1F*)(h_signal->Clone());
	  lowshape_s [j]=(TH1F*)(h_signal->Clone()); }
	for(int i=0; i < nbg; i++) {
	  scale_b_low[i][0] = 0;
	  scale_b_high[i][0] = 0;
	  for(int j=1; j < nps_b; ++j) {
	    highshape_b[i][j] = (TH1F*)(h_bg[i]->Clone());
	    lowshape_b [i][j] = (TH1F*)(h_bg[i]->Clone()); }
	}
      }

      cout << "Setting systematic names" << endl;
      string sysnames[17] = {"XSEC", "JES", "JER", "MSF", "ESF",
			     "PUP" , "BJT", "REC", "Etmiss",
			     "IsoPt", "IsoNVtx", "EES", "LARHOLE",
			     "MCGEN", "MCFRAG", "ZGEN", "Other"};

      // Since the cross-sections for each sample is independent of one another
      //  we give them different names so that they became independent nps
      ename_s[0] = (char*)"sig xsec";
      for(int i=0; i < nbg; i++)
	ename_b[i][0] = Form("bg%d xsec",i+1);

      // For all the shape systematics we systematically give the same names
      //  across multiple samples so the correlations are treated properly
      for (int j=1; j<16; ++j) {
	ename_s[j] = Form("%s",sysnames[j].c_str());
	for(int i=0; i < nbg; i++){
	  ename_b[i][j] = Form("%s",sysnames[j].c_str());
	}
      }
	/*ename_s[0] = ((char *)"JES");
      for(int i=0; i < nbg; i++){
       ename_b[i][0] = ((char *)"JES");
      }
      ename_s[1] = ((char *)"JER");      ename_b[0][1] = ((char *)"JER");
      ename_s[2] = ((char *)"MSF");      ename_b[0][2] = ((char *)"MSF");
      ename_s[3] = ((char *)"ESF");      ename_b[0][3] = ((char *)"ESF");
      ename_s[4] = ((char *)"PUP");      ename_b[0][4] = ((char *)"PUP");
      ename_s[5] = ((char *)"BJT");      ename_b[0][5] = ((char *)"BJT");
      ename_s[6] = ((char *)"REC");      ename_b[0][6] = ((char *)"REC");
// the rest is not done.
      ename_s[7] = ((char *)"EtMiss");   ename_b[0][7] = ((char *)"EtMiss");
      ename_s[8] = ((char *)"IsoPt");    ename_b[0][8] = ((char *)"IsoPt");
      ename_s[9] = ((char *)"IsoNVtx");  ename_b[0][9] = ((char *)"IsoNVtx");
      ename_s[10] = ((char *)"EES");     ename_b[0][10] = ((char *)"EES");
      ename_s[11] = ((char *)"LARHOLE"); ename_b[0][11] = ((char *)"LARHOLE");
      ename_s[12] = ((char *)"MCGEN");   ename_b[0][12] = ((char *)"MCGEN");
      ename_s[13] = ((char *)"MCFRAG");  ename_b[0][13] = ((char *)"MCFRAG");
      ename_s[14] = ((char *)"ZGEN");    ename_b[0][14] = ((char *)"ZGEN");
      ename_s[15] = ((char *)"Other");   ename_b[0][15] = ((char *)"Other");
	*/
      cout << "Printing bg nuisance information" << endl;
      int NbinsX = h_signal->GetNbinsX();
      for(int i=0; i < nbg; i++){
        cout << "Print bg " << i << ": shape_low, shape_high, lowsigma, highsigma, scale_low, scale_high" << endl;
        //for(int j=0; j < npsmax-1; j++){
        for(int j=0; j < nps_b; j++){
          cout << "  " << lowshape_b[i][j] << " " << highshape_b[i][j] << " " << lowsigma_b[i][j] << " " << highsigma_b[i][j] << " " << scale_b_low[i][j] << " " << scale_b_high[i][j] << endl;
          if(lowshape_b[i][j]){
            //cout << "low shape setting i:"<<i<< "  j:"<<j<<endl;
            lowshape_b[i][j]->Scale(1.0/sf);
            lowshape_b[i][j]->SetBinContent(NbinsX+1,0);
            lowshape_b[i][j]->SetBinContent(0,0);
          }
          if(highshape_b[i][j]){
            //cout << "high shape setting i:"<<i<< "  j:"<<j<<endl;
            highshape_b[i][j]->Scale(1.0/sf);
            highshape_b[i][j]->SetBinContent(NbinsX+1,0);
            highshape_b[i][j]->SetBinContent(0,0);
            if (i>=3) {
	      // blank out bins with negative content
	      for (int nn=0; nn<=h_bg[i]->GetNbinsX(); nn++){
		if (h_bg[i]->GetBinContent(nn)<0)
		  h_bg[i]->SetBinContent(nn,0);
		if (highshape_b[i][j]->GetBinContent(nn)<0) 
		  highshape_b[i][j]->SetBinContent(nn,0);
		if (lowshape_b[i][j]->GetBinContent(nn)<0)
		  lowshape_b[i][j]->SetBinContent(nn,0);
	      }
	    }
          }
        }
        h_bg[i]->Scale(1.0/sf);
        h_bg[i]->SetBinContent(NbinsX+1,0);
        h_bg[i]->SetBinContent(0,0);
      }
      cout << endl << "Print sig nuisance information: shape_low, shape_high, lowsigma, highsigma, scale_low, scale_high" << endl;
      //for(int i=0; i < npsmax-1; i++){
      for(int i=0; i < nps_s; i++){
        cout << "  " << lowshape_s[i] << " " << highshape_s[i] << " " << lowsigma_s[i] << " " << highsigma_s[i] << " " << scale_s_low[i] << " " << scale_s_high[i] << endl;
        if(lowshape_s[i]){
          lowshape_s[i]->Scale(1.0/sf);
          lowshape_s[i]->SetBinContent(NbinsX+1,0);
          lowshape_s[i]->SetBinContent(0,0);
        }
        if(highshape_s[i]){
          highshape_s[i]->Scale(1.0/sf);
          highshape_s[i]->SetBinContent(NbinsX+1,0);
          highshape_s[i]->SetBinContent(0,0);
        }
      }
      h_signal->Scale(1.0/sf);
      h_signal->SetBinContent(NbinsX+1,0);
      h_signal->SetBinContent(0,0);
      h_data[i_channel]->SetBinContent(NbinsX+1,0);
      h_data[i_channel]->SetBinContent(0,0);

      cout << "Verifying NBins is the same everywhere (it's fine if no messages are printed about it)" << endl;
      if(h_data[i_channel]->GetNbinsX() != NbinsX)
	cout << "Error: Signal has " << NbinsX << " bins but data has " 
	     << h_data[i_channel]->GetNbinsX() << " bins" << endl;
      for(int i=0; i < nbg; i++){
        if(h_bg[i]->GetNbinsX() != NbinsX)
	  cout << "Error: Signal has " << NbinsX << " bins but Bg" << i
	       << " has " << h_bg[i]->GetNbinsX() << " bins" << endl;
        {
          for(int j=1; j < nps_b; j++){
            if(highshape_b[i][j]->GetNbinsX() != NbinsX)
	      cout << "Error: Signal has " << NbinsX << " bins but Bg" << i 
		   << " high-s systematic " << j << " has "
		   << highshape_b[i][j]->GetNbinsX() << endl;
            if(lowshape_b[i][j]->GetNbinsX() != NbinsX)
	      cout << "Error: Signal has " << NbinsX << " bins but Bg" << i
		   << " low-s systematic " << j << " has "
		   << lowshape_b[i][j]->GetNbinsX() << endl;
          }
        }
      }
// for signal
      for(int j=1; j < nps_s; j++){
	if(highshape_s[j]->GetNbinsX() != NbinsX) cout << "Error: Signal has " << NbinsX << " bins but Sig highshape systematic " << j << " has " << highshape_s[j]->GetNbinsX() << endl;
	if(lowshape_s[j]->GetNbinsX() != NbinsX) cout << "Error: Signal has " << NbinsX << " bins but Sig lowshape systematic " << j << " has " << lowshape_s[j]->GetNbinsX() << endl;
      }
      cout << "NBins check over with bgs:"<< nps_s<<endl;
    
//////=======================================
      double TotalBGIntegral = 0.0;
      for(int i=0; i < nbg; i++) TotalBGIntegral += (h_bg[i]->Integral()*sf);
      cout << "Total Bg: " << TotalBGIntegral << " Total Observed: " << h_data[i_channel]->Integral() << endl;
//////=======================================
    
      for (int i=0; i<nbg; i++) {
       for (int j=0; j<nps_s; j++){
        cout << "Name="<<ename_b[i][j]<<endl;
        cout << "High sig:" << highsigma_b[i][j] << "   low sig:" << lowsigma_b[i][j] << " at j=" << j<< endl;
        cout << "High rate:" << scale_b_high[i][j] << "   low rate:" << scale_b_low[i][j] << " at j=" << j<< endl;
       }
      }

      for (int i=0; i<nbg; i++) {
         cout << "Adding "<< i<<"th bg to HYPS, bg integral and NBins: " << h_bg[i]->Integral() << " " << h_bg[i]->GetNbinsX() << endl;
       
         nullhyp_pe->add_template(h_bg[i], sf, nps_b, ename_b[i],
				  scale_b_low[i], scale_b_high[i],
                                  (TH1**)lowshape_b[i], lowsigma_b[i],
				  (TH1**)highshape_b[i], highsigma_b[i],
				  0, 0, ChanName[i_channel]);
         testhyp_pe->add_template(h_bg[i], sf, nps_b, ename_b[i],
				  scale_b_low[i], scale_b_high[i], 
                                  (TH1**)lowshape_b[i], lowsigma_b[i],
				  (TH1**)highshape_b[i], highsigma_b[i],
				  0, 0, ChanName[i_channel]);
       
         nullhyp->add_template(h_bg[i], sf, NumberOfBgProfiles, ename_b[i],
			       scale_b_low[i], scale_b_high[i], 
                               (TH1**)lowshape_b[i], lowsigma_b[i],
			       (TH1**)highshape_b[i], highsigma_b[i],
			       0, 0, ChanName[i_channel]);
         testhyp->add_template(h_bg[i], sf, NumberOfBgProfiles, ename_b[i],
			       scale_b_low[i], scale_b_high[i], 
                               (TH1**)lowshape_b[i], lowsigma_b[i],
			       (TH1**)highshape_b[i], highsigma_b[i],
			       0, 0, ChanName[i_channel]);
      }

      cout << "Adding signal to testhyp and testhyp_pe, we are profiling " <<  NumberOfSigProfiles << " parameters" << endl;
      cout << "input sig integral: " << h_signal->Integral()*sf << endl;
      testhyp_pe->add_template(h_signal, sf, nps_s, ename_s,
			       scale_s_low, scale_s_high, 
                               (TH1**)lowshape_s, lowsigma_s,
			       (TH1**)highshape_s, highsigma_s,
			       0, 1, ChanName[i_channel]);
      testhyp->add_template   (h_signal, sf, NumberOfSigProfiles, ename_s,
			       scale_s_low, scale_s_high, 
                               (TH1**)lowshape_s, lowsigma_s,
			       (TH1**)highshape_s, highsigma_s,
			       0, 1, ChanName[i_channel]);
     
     cout << "0++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"<<endl;
     cout << ChanName[i_channel] << "  added."<<endl; 
     testhyp_pe->print();
//   cout << "1++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"<<endl;
//   testhyp->print();
     cout << "2++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"<<endl;

} // loop over channels

// get a distribution of chisqure for the different hypotheses
  mclimit_csm* mymclimit = (mclimit_csm*) new mclimit_csm();
   	       mymclimit->set_null_hypothesis(nullhyp);
               mymclimit->set_test_hypothesis(testhyp);
               mymclimit->set_null_hypothesis_pe(nullhyp_pe);
               mymclimit->set_test_hypothesis_pe(testhyp_pe);

  for ( int i_channel=0; i_channel<max_channels; i_channel++){
               mymclimit->set_datahist(h_data[i_channel],ChanName[i_channel]); // like this we can add many channels and their data 
  }
  cout << "-=-=-=-=-=--=-=--=--=--="<<endl;
	       mymclimit->set_npe(mc_npe);
               //mymclimit->setminuitstepsize(0.01);
               //mymclimit->setminuitmaxcalls(50000);
  cout << "----------------------Getting results-------> this is LONG!" << endl;
               mymclimit->run_pseudoexperiments();


  //expected confidences for exclusion and discovery under H0 and H1.
  
  cout << "cls_exp_bmed NULL: " << mymclimit->clsexpbmed() << endl;
  cout << "cls_exp_smed TEST: " << mymclimit->clsexpsmed() << endl;

double b1k_obs=mymclimit->clb();
double b1k_up2=mymclimit->clbexpbm2();
double b1k_up1=mymclimit->clbexpbm1();
double b1k_med=mymclimit->clbexpbmed();
double b1k_dn1=mymclimit->clbexpbp1();
double b1k_dn2=mymclimit->clbexpbp2();
               

double s1k_obs=mymclimit->cls();
double s1k_up2=mymclimit->clsexpbm2();
double s1k_up1=mymclimit->clsexpbm1();
double s1k_med=mymclimit->clsexpbmed();
double s1k_dn1=mymclimit->clsexpbp1();
double s1k_dn2=mymclimit->clsexpbp2();
               


   cout << "-------------------------expected CLs in H0  ---------------------------" << endl;
   cout << "CLsb observ  (bkg): " << s1k_obs << endl;
   cout << "CLsb -2sigma (bkg): " << s1k_up2 << endl;
   cout << "CLsb -1sigma (bkg): " << s1k_up1 << endl;
   cout << "CLsb median  (bkg): " << s1k_med << endl;
   cout << "CLsb +1sigma (bkg): " << s1k_dn1 << endl;
   cout << "CLsb +2sigma (bkg): " << b1k_dn2 << endl;
   cout << "-------------------------expected CLb in H0  ---------------------------" << endl;
   cout << "CLb observ   (bkg): " << b1k_obs << endl;
   cout << "CLb -2sigma  (bkg): " << b1k_up2 << endl;
   cout << "CLb -1sigma  (bkg): " << b1k_up1 << endl;
   cout << "CLb median   (bkg): " << b1k_med << endl;
   cout << "CLb +1sigma  (bkg): " << b1k_dn1 << endl;
   cout << "CLb +2sigma  (bkg): " << b1k_dn2 << endl;

/*
   cout << "-------------------------expected CLs in H1  ---------------------------" << endl;
   cout << "CLsb median  (sig): " << mymclimit->clsbexpsmed() << endl;
   cout << "CLsb -2sigma (sig): " << mymclimit->clsbexpsm2() << endl;
   cout << "CLsb -1sigma (sig): " << mymclimit->clsbexpsm1() << endl;
   cout << "CLsb +1sigma (sig): " << mymclimit->clsbexpsp1() << endl;
   cout << "CLsb +2sigma (sig): " << mymclimit->clsbexpsp2() << endl;
*/ 



  

   cout << "----------------------------------    CL @ 95% for  Limits  ----------------------------------" << endl;

   double middle = 0.0;
   double p2 = 0.0;
   double p1 = 0.0;
   double m2 = 0.0;
   double m1 = 0.0;
   double measdata = 0.0;
   double pval = 0.0;
   double tmiddle = mymclimit->tssmed();
   double tp2 = mymclimit->tssm2();
   double tp1 = mymclimit->tssm1();
   double tm2 = mymclimit->tssp2();
   double tm1 = mymclimit->tssp1();
  cout << "[" << tmiddle << "," << tp1 << "," << tm1 << "," << tp2 << "," << tm2 << "]" << endl;
  cout << "finding test-statistic values for bg: med, p1, m1, p2, m2, meas" << endl;
  cout << "Finding measured test statistic value" << endl;
   double tbmiddle = mymclimit->tsbmed();
   double tbp2 = mymclimit->tsbm2();
   double tbp1 = mymclimit->tsbm1();
   double tbm2 = mymclimit->tsbp2();
   double tbm1 = mymclimit->tsbp1();
   double tmeasdata = mymclimit->ts();


  if(s95type >= 0){
    if(s95type == 0){ cout << "s95data: " << mymclimit->s95() << endl;}
    else if(s95type == 1){ cout << "s95med: "<< mymclimit->s95med() << endl;}
    else if(s95type == 2){ cout << "s95p1: " << mymclimit->s95p1() << endl;}
    else if(s95type == 3){ cout << "s95p2: " << mymclimit->s95p2() << endl;}
    else if(s95type == 4){ cout << "s95m1: " << mymclimit->s95m1() << endl;}
    else if(s95type == 5){ cout << "s95m2: " << mymclimit->s95m2() << endl;}
    cout << "Done" << endl;
  }
  else{
   cout << "s95: ";  measdata = mymclimit->s95();
   cout << "s95med: "; middle = mymclimit->s95med();
   cout << "s95m2 : ";  p2 = mymclimit->s95m2();
   cout << "s95m1 : ";  p1 = mymclimit->s95m1();
   cout << "s95p2 : ";  m2 = mymclimit->s95p2();
   cout << "s95p1 : ";  m1 = mymclimit->s95p1();
   cout << "omclb : "; pval = mymclimit->omclb();

   cout << "Scale Factors on signal for exclusion at 95% CL exactly: " << endl;
   cout << "s95 +2sigma " << p2 << endl;
   cout << "s95 +1sigma " << p1 << endl;
   cout << "s95  middle " << middle << endl;
   cout << "s95 -1sigma " << m1 << endl;
   cout << "s95 -2sigma " << m2 << endl;
   cout << "s95 data    " << measdata << endl;
   cout << "p-value " << pval << endl;

  TFile bb("mcl_out.root","recreate");
  TParameter<double> *b_up2_tmp=new TParameter<double> ("b_up2", m2);b_up2_tmp->Write();
  TParameter<double> *b_up1_tmp=new TParameter<double> ("b_up1", m1);b_up1_tmp->Write();
  TParameter<double> *b_dn1_tmp=new TParameter<double> ("b_dn1", p1);b_dn1_tmp->Write();
  TParameter<double> *b_dn2_tmp=new TParameter<double> ("b_dn2", p2);b_dn2_tmp->Write();
  TParameter<double> *b_med_tmp=new TParameter<double> ("b_med", middle);b_med_tmp->Write();
  TParameter<double> *b_obs_tmp=new TParameter<double> ("lim_obs", measdata);b_obs_tmp->Write();
  TParameter<double> *b1k_obs_t=new TParameter<double> ("b1k_obs", b1k_obs) ;b1k_obs_t->Write();
  TParameter<double> *b1k_up2_t=new TParameter<double> ("b1k_up2", b1k_up2) ;b1k_up2_t->Write();
  TParameter<double> *b1k_up1_t=new TParameter<double> ("b1k_up1", b1k_up1) ;b1k_up1_t->Write();
  TParameter<double> *b1k_med_t=new TParameter<double> ("b1k_med", b1k_med) ;b1k_med_t->Write();
  TParameter<double> *b1k_dn1_t=new TParameter<double> ("b1k_dn1", b1k_dn1) ;b1k_dn1_t->Write();
  TParameter<double> *b1k_dn2_t=new TParameter<double> ("b1k_dn2", b1k_dn2) ;b1k_dn2_t->Write();
  TParameter<double> *s1k_obs_t=new TParameter<double> ("s1k_obs", s1k_obs) ;s1k_obs_t->Write();
  TParameter<double> *s1k_up2_t=new TParameter<double> ("s1k_up2", s1k_up2) ;s1k_up2_t->Write();
  TParameter<double> *s1k_up1_t=new TParameter<double> ("s1k_up1", s1k_up1) ;s1k_up1_t->Write();
  TParameter<double> *s1k_med_t=new TParameter<double> ("s1k_med", s1k_med) ;s1k_med_t->Write();
  TParameter<double> *s1k_dn1_t=new TParameter<double> ("s1k_dn1", s1k_dn1) ;s1k_dn1_t->Write();
  TParameter<double> *s1k_dn2_t=new TParameter<double> ("s1k_dn2", s1k_dn2) ;s1k_dn2_t->Write();
  TH1F *dh = (TH1F*)( h_data[0]->Clone() );
  TCanvas *cchan1 = new TCanvas("cchan1","Plot of first channel",1);
  testhyp->plotwithdata(ChanName[0],dh);
  cchan1->SaveAs("test.pdf");
  cchan1->Write();
  bb.Close();
  cout << "[" << middle << "," << p1 << "," << m1 << "," << p2 << "," << m2 << "," << measdata << "," << tmiddle << "," << tp1 << "," << tm1 << "," << tp2 << "," << tm2 << "," << tbmiddle << "," << tbp1 << "," << tbm1 << "," << tbp2 << "," << tbm2 << "," << tmeasdata << "]" << endl;
  cout << tmeasdata << endl;
  }
  
  /*
  TH1F* testhyp_ts = new TH1F("testhyp_ts", ";TS testhyp", 40, -40, 40);
  TH1F* nullhyp_ts = new TH1F("nullhyp_ts", ";TS nullhyp", 40, -40, 40);
  mymclimit->tshists(testhyp_ts, nullhyp_ts);
  TCanvas *cts = new TCanvas();
  testhyp_ts->Draw("HIST");
  cts->SaveAs("testhyp_ts.eps");
  nullhyp_ts->Draw("HIST");
  cts->SaveAs("nullhyp_ts.eps");
  testhyp_ts->GetXaxis()->SetTitle("TS test hyp (black) and null hyp (blue)");
  testhyp_ts->Draw("HIST");
  nullhyp_ts->SetLineColor(4);
  nullhyp_ts->Draw("HIST SAME");
  cts->SaveAs("testANDnullhyp_ts.eps");
  */
}
