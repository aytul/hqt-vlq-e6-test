This directory contains simple applications of the various tools in analysis_core.

* At the moment, we generate pseudodata and two toy mc histograms and use a very simple compare.C
  to produce their combined plots. To test, first run generateSample.C to get the histogram root files,
  then run compare.C to get the results.
