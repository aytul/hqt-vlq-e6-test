#include "e6_a1.h"
#include "TParameter.h"
#include <TRandom.h>
#define GeV 1
void E61dbxA::saveAsLHCO ( vector<dbxParticle> Lep, vector <dbxJet> Jets, TVector2 Met, evt_data anevt ) {
    LHCOfile <<"  0"<<setw(16)<<anevt.event_no<<"  "<<setw(6)<<anevt.run_no<<endl;
    for( unsigned int j=0; j<Lep.size();j++){
     LHCOfile <<"  "<<j<<"    2    ";; Lep.at(j).dumpLHCO( LHCOfile);
    }
    for (unsigned int i=0; i<Jets.size(); i++){
      LHCOfile <<"  "<<i<<"    4    ";;Jets.at(i).dumpLHCO( LHCOfile);
    }
    LHCOfile <<fixed<<"  6    6    0.000"<<setw(8)<<setprecision(1)<<Met.Phi()<<setw(8)<<setprecision(1)<<sqrt(Met.Mod2())<<"    0.0     0.0     0.0     0.0     0.0     0.0"<<endl;
}

//additional histograms are defined here
int E61dbxA::bookAdditionalHistos(){
  int retval=0;
  h_mZreco=new TH1F("h_mZreco","Reconstucted Z mass in GeV",100,0,1000);
  h_mWreco=new TH1F("h_mWreco","Reconstructed W mass in GeV",100,0,1000);
  h_mDreco=new TH1F("h_mDreco","Reconstructed D mass(leptonic) in GeV",100,0,1000);
  h_mDbarreco=new TH1F("h_mDbarreco","Reconstructed D mass (hadronic) in GeV",100,0,1000);
  h_mDavg=new TH1F("h_mDavg"," Reconstructed D quark mass (wght avg) in GeV",100,0,1000);
  h_dPhiZW=new TH1F("h_dPhiZW","#DeltaR_{ZW}",100,0,10);
  h_dRWconst=new TH1F("h_dRWconst","#DeltaR of W constituents",100,0,10);
  h_dRZconst=new TH1F("h_dRZconst","#DeltaR of Z constitutents",100,0,10);
  h_dRZJ0=new TH1F("h_dRZJ0","#DeltaR_{ZJ0}",100,0,10);
  h_dRZJ1=new TH1F("h_dRZJ1","#DeltaR_{ZJ1}",100,0,10);
  h_dRWJ0=new TH1F("h_dRWJ0","#DeltaR_{WJ0}",100,0,10);
  h_dRWJ1=new TH1F("h_dRWJ1","#DeltaR_{WJ1}",100,0,10);
  h_deltaM=new TH1F("h_deltaM","#DeltaM of reconstructed HQs in GeV",60,-300,300);
  h_ptEle=new TH1F("h_ptEle","Electron pt in GeV",100,0,1000);
  h_Nchi2=new TH1F("h_Nchi2","# of chi2 in the event",200,0,200);
  h_ptJets=new TH1F("h_ptJets","ptjets all in GeV",200,0,2000);
  return retval;
}

int E61dbxA:: readAnalysisParams() {
  int retval=0;
  TString  CardName=getDataCardName();
  /*if (QCD) CardName.Replace(CardName.Length()-4,4,"");
    CardName+="-card.txt";*/

    minpte  = ReadCard(CardName,"minpte",10.0);
    minptm2011  = ReadCard(CardName,"minptm2011",10.0);
    minptm2012  = ReadCard(CardName,"minptm2012",10.0);
    maxetae = ReadCard(CardName,"maxetae",2.5);
    maxetam = ReadCard(CardName,"maxetam",2.5);
    minmete = ReadCard(CardName,"minmete",35.0);
 minmwtmete = ReadCard(CardName,"minmwtmete",30.0);
    minmetm = ReadCard(CardName,"minmetm",20.0);
 minmwtmetm = ReadCard(CardName,"minmwtmetm",60.0);
    minptj  = ReadCard(CardName,"minptj",5.0);
    maxetaj = ReadCard(CardName,"maxetaj",5.0);
    mindrjm = ReadCard(CardName,"mindrjm",0.4);
    mindrje = ReadCard(CardName,"mindrje",0.4);
    minptj1 = ReadCard(CardName,"minptj1",100);
    minptj2 = ReadCard(CardName,"minptj2",20);
    minEj2  = ReadCard(CardName,"minEj2",20);
   minetaj2 = ReadCard(CardName,"minetaj2",0.);
    mindrjj = ReadCard(CardName,"mindrjj",1.0);
   minetajj = ReadCard(CardName,"minetajj",1.0);
  maxdeltam = ReadCard(CardName,"maxdeltam",100.);
    mqhxmin = ReadCard(CardName,"mqhxmin",0.0);  // mHQ histo xmin
    mqhxmax = ReadCard(CardName,"mqhxmax",1000.0); // mHQ histo xmax
maxMuPtCone = ReadCard(CardName,"maxMuPtCone",10.0);
maxMuEtCone = ReadCard(CardName,"maxMuEtCone",10.0);
maxElPtCone = ReadCard(CardName,"maxElPtCone",10.0);
maxElEtCone = ReadCard(CardName,"maxElEtCone",10.0);
       TRGe = ReadCard(CardName,"TRGe",1);
       TRGm = ReadCard(CardName,"TRGm",1);
jetVtxf2011 = ReadCard(CardName,"jetVtxf2011",0.75);
jetVtxf2012 = ReadCard(CardName,"jetVtxf2012",0.50);
// we read the HF if it is not previously initialized by hand.
if (HFtype<0) HFtype = ReadCard(CardName,"HFtype",3);

 //Serhat
/*
    MW = ReadCard(CardName,"MW");
    MZ = ReadCard(CardName,"MZ");
    SGMW = ReadCard(CardName,"SGMW");
    SGMZ = ReadCard(CardName,"SGMZ");
    SGHQ = ReadCard(CardName,"SGHQ");
    SGHQBAR = ReadCard(CardName,"SGHQBAR");
    MINPTE0 = ReadCard(CardName,"MINPTE0");
    MINPTE1 = ReadCard(CardName,"MINPTE1");
    MINPTMO = ReadCard(CardName,"MINPTMO");
    MINPTM1 = ReadCard(CardName,"MINPTM1");
    MINNJETS = ReadCard(CardName,"MINNJETS");
    MINPTJ0 = ReadCard(CardName,"MINPTJ0");
    MINPTJ1 = ReadCard(CardName,"MINPTJ1");
    MINPTELES = ReadCard(CardName,"MINPTELES");
    MINWMASS = ReadCard(CardName,"MINWMASS");
    MAXWMASS = ReadCard(CardName,"MAXWMASS");
    MINPTPR1CHI2 = ReadCard(CardName,"MINPTPR1CHI2");
    MINDRWPR1CHI2 = ReadCard(CardName,"MINDRWPR1CHI2");
    MINPTPR2CHI2 = ReadCard(CardName,"MINPTPR2CHI2");
    MAXZMASS = ReadCard(CardName,"MAXZMASS");
    MINZMASS = ReadCard(CardName,"MINZMASS");
    MINDRZPR2CHI2 = ReadCard(CardName,"MINDRZPR2CHI2");
    MINPTZREC = ReadCard(CardName,"MINPTZREC");
    MINPTWREC = ReadCard(CardName,"MINPTWREC");
    MINDELTAM = ReadCard(CardName,"MINDELTAM");
    TRGe = ReadCard(CardName,"TRGe",1);
    TRGm = ReadCard(CardName,"TRGm",1);
    QCDRUN=ReadCard(CardName,"QCDRUN");
*/
    if ((TRGm>0) && (TRGe>0)) {
	cout<< "ERROR : Can not trigger both on electrons and muons." <<endl;
	cout<< "ERROR : EXITING." <<endl;
	exit(8);
     }
    if ((TRGm>0) && (TRGe==0)) {
	cout<< "TRiGgering on muons." <<endl;
    }
    if ((TRGm==0) && (TRGe>0)) {
	cout<< "TRiGgering on electrons." <<endl;
    }
    return retval;
}

int E61dbxA:: printEfficiencies() {
  int retval=0;
  PrintEfficiencies(eff);
  return retval;
}


int E61dbxA:: setQCD() {
  int retval=0;
  QCD=true;
  return retval;
}

int E61dbxA:: initGRL() {
  int retval=0;
  grl_cut=true;
  return retval;
}


//Implement 2l+4j reconstruction template method here
template <class LEPTONS, class JETS>
comb_element E61dbxA::runE6Reco1( const LEPTONS& theleptons, const JETS& thejets){

  double T1=-1;
  double T2=-1;
  double T3=-1;//3 terms of chi2

  //Require at least 2 leptons+4 jets!!!
  if(theleptons->size()<2){
    std::cerr<<"#Leptons must be >=2 check your cutflow"<<std::endl;
    exit(8);
  }
  if(thejets->size()<4){
        std::cerr<<"#Jets must be >=4 check your cutflow"<<std::endl;
        exit(8);
  }

  comb_element evcfg;//To become current lepton+jets combination in chi2 search
  comb_element min_cfg;
  min_cfg.NChi2=0;
  unsigned int NCh=0;
  double minchi2=0;
  for(unsigned short int i=0;i<thejets->size();i++){//W const1 loop
    //if(thejets->at(i).lv().Pt()<minptj) continue;//30 optimal gozukuyor d3pd de. Lakin delpheste sinyal buna çok hassas gozukuyor!(20 makul delphes için)
    evcfg.iW1=i;
    evcfg.WC1=thejets->at(i);
      for(unsigned short int j=i+1;j<thejets->size();j++){//W const2 loop...start from i+1 to avoid indice overlap
	//if(thejets->at(j).lv().Pt()<minptj) continue;
	evcfg.iW2=j;
	evcfg.WC2=thejets->at(j);
	evcfg.Wreco=evcfg.WC1+evcfg.WC2;
	//if(evcfg.Wreco.lv().Pt()<100) continue;
	//if(evcfg.Wreco.M()<kin("minWmass") || evcfg.Wreco.M()>kin("maxWmass")) continue;  //enable to speed up
	for(unsigned short int k=0;k<thejets->size();k++){//prompt1 loop
	  if(k==j|| k==i ) continue;
	  //if(thejets->at(k).lv().Pt()<MINPTPR1CHI2) continue;
	  evcfg.ipr1=k;
	  evcfg.pr1=thejets->at(k);
	  //if(dbxParticle::deltaR(evcfg.Wreco,evcfg.pr1)<MINDRWPR1CHI2) continue;//min. distance between W and its promptjet
	  evcfg.De=evcfg.Wreco+evcfg.pr1;
	  for(unsigned short int m=0;m<thejets->size();m++){//prompt2 loop
	    if(m==k || m==j || m==i) continue;
	    //if(thejets->at(m).lv().Pt()<MINPTPR2CHI2) continue;
	    evcfg.ipr2=m;
	    evcfg.pr2=thejets->at(m);
	    for(unsigned short int n=0;n<theleptons->size();n++){//e loop
	      if(theleptons->at(n).q()>0) continue;
	      evcfg.iZ1=n;
	      evcfg.ZC1=theleptons->at(n);
	      for(unsigned int p=0;p<theleptons->size();p++){//ebar loop
		if(theleptons->at(p).q()<0)continue;
		if(p==n) continue;
		evcfg.iZ2=p;
		evcfg.ZC2=theleptons->at(p);
		evcfg.Zreco=evcfg.ZC1+evcfg.ZC2;
		//if(evcfg.Zreco.M()>kin("maxZmass") || evcfg.Zreco.M()<kin("minZmass")) continue;//enable to speed up
		//if(dbxParticle::deltaR(evcfg.Zreco,evcfg.pr2)<MINDRZPR2CHI2) continue;
		T1=((evcfg.Wreco.lv().M()-MW)*(evcfg.Wreco.lv().M()-MW))/(SGMW*SGMW);//(mw-mwreco)^2/sg_W^2
		T2=((evcfg.Zreco.lv().M()-MZ )*(evcfg.Zreco.lv().M()-MZ))/(SGMZ*SGMZ);
		evcfg.Debar=evcfg.Zreco+evcfg.pr2;
		double mD,mDbar;
		mD=evcfg.De.lv().M();
		mDbar=evcfg.Debar.lv().M();
		T3=(mD-mDbar)*(mD-mDbar)/(2*SGHQ*SGHQBAR);//better to use this term?
		double m_chi2=T1+T2+T3;
		evcfg.chi2=m_chi2;
		evcfg.deltaM=mD-mDbar;
		NCh++;
		evcfg.NChi2=NCh;

		if(NCh==1){
		  min_cfg=evcfg;//for the initialization of the minimum as the first element in the configuration space
		  minchi2=m_chi2;
		}

		if(m_chi2<=minchi2){//Assign the next chi2 configuration as the minimum if it s less than the prev. one...
		  min_cfg=evcfg;
		  minchi2=m_chi2;
		}
              //W+pr1, Z+pr2
              }//lepton loop
            }//leptonbar loop
         }//prompt2 loop
       }//prompt1 loop
     }//WC2 loop
  }//WC 1 loop
  //Now you have an access to min_cfg!!!
  return min_cfg;
};
//Endof 2l+4j reconstruction

/* --------------- ANALYSIS make it template to avoid shitty copy paste!!! this is not fortran ;)--------------- */
int E61dbxA::makeAnalysis(vector<dbxMuon> muons, vector<dbxElectron> electrons,
			 vector<dbxJet> jets ,TVector2 met, evt_data anevt) {

  //Implement Object cleaning & cutflow here..
  int cur_cut=1;
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= imported from the rel17 challenger -=-=-=-=-=-=
  vector<dbxElectron>  goodElectrons;
  vector<dbxMuon>      goodMuons;
  vector<dbxJet>       goodJets, looseJets;
  vector<dbxElectron>  emuoverlappedElectrons;
  vector<dbxElectron>  emuoverlappedlooseEles;

//--- for qcd backgroud estimation ---
  vector<dbxMuon> loose_mus;
  vector<dbxElectron> loose_eles;

    if (anevt.year==2012) {
   	 minptm=minptm2012;
         jetVtxf=jetVtxf2012;
  } else {
   	 minptm=minptm2011;
         jetVtxf=jetVtxf2011;
  }
// select electrons with a minimum set of requirements.
// this will later be used in overlap removal etc.
/*2017 SI
  for (UInt_t i=0; i<electrons.size(); i++) {
          TLorentzVector ele4p=electrons.at(i).lv();
          if (   (fabs(electrons.at(i).BestEt())  > minpte) // electron recommended Et
              && (fabs(ele4p.Eta()) < maxetae )
              && (fabs(ele4p.Eta())>=1.52 || fabs(ele4p.Eta())<=1.37)
             )   {
//loose_eles to be used in LL Matrix Method:
//iso1 : Medium++ pid, conversion veto isEM bit, no isolation
                loose_eles.push_back( electrons.at(i) );
                if (  (electrons.at(i).EtCone20() == 1 )  // EtCone20 @90%
                   && (electrons.at(i).PtCone30() == 1 )  // PtCone30 @90%
                   && (electrons.at(i).isTight() == 1) // 1:tight, 0: not_tight -- We receive the mediums
                   ) {
                      goodElectrons.push_back( electrons.at(i) );
                } // good close
          } // loose close
   }

// select jets  with a minimum set of requirements.
  for (UInt_t i=0; i<jets.size(); i++) {
    TLorentzVector jet4p = jets.at(i).lv();
     // if ( (TRGm+TRGe) > 1)
     jet4p.SetPtEtaPhiM(jet4p.Pt()/GeV, jet4p.Eta(), jet4p.Phi(), jet4p.M()/GeV); // needed for MC, as jets got here in MEV
     if ((fabs(jet4p.Pt()) > minptj) // this corresponds to 25GeV cut
          && (fabs(jets.at(i).JetVtxF() ) >= jetVtxf)//this should be 0.5 for 2012 data
           && (jet4p.E() >= 0))
     {
                  //if ( (TRGm+TRGe) > 1)
        jets.at(i).setTlv(jet4p); // needed for MC, as jets got here in MEV
        goodJets.push_back(jets.at(i) );
        looseJets.push_back(jets.at(i) );
      }
   }

// select muons with a minimum set of requirements.
  for (UInt_t i=0; i<muons.size(); i++) {
    TLorentzVector mu4p = muons.at(i).lv();
    if((mu4p.Pt() > minptm)
      && (fabs(mu4p.Eta()) < maxetam) )
    {//z0max
      loose_mus.push_back(muons.at(i));
      if( (muons.at(i).EtCone() < maxMuEtCone)
          && (muons.at(i).PtCone() < maxMuPtCone) )
        {
         goodMuons.push_back(muons.at(i));
        }
     }
  }  // muon loop

//---------------------------
// now the overlap removal BS.
//---------------------------
// overlap removal between good Electron and Muons
  for (UInt_t iele=0; iele<goodElectrons.size(); iele++) {
    for (UInt_t imuon=0; imuon<goodMuons.size(); imuon++) {
      if(std::fabs(goodElectrons.at(iele).TrkPhi() - goodMuons.at(imuon).MuIdPhi())<0.005
         && std::fabs(goodElectrons.at(iele).TrkTheta() - goodMuons.at(imuon).MuIdTheta())<0.005 )
       {
        emuoverlappedElectrons.push_back(goodElectrons.at(iele));
        //break;
       }
    }
  } // end of loop over electrons
// overlap removal between loose Electron and Muons
   for (UInt_t iele=0; iele<loose_eles.size(); iele++) {
       for (UInt_t imuon=0; imuon<goodMuons.size(); imuon++) {
         if (   std::fabs(loose_eles.at(iele).TrkPhi() - goodMuons.at(imuon).MuIdPhi())<0.005
             && std::fabs(loose_eles.at(iele).TrkTheta() - goodMuons.at(imuon).MuIdTheta())<0.005 )
         {
                emuoverlappedlooseEles.push_back(loose_eles.at(iele));
           //     break;
         }
       }
   } // end of loop over electrons


// overlap removal between good Muons and Jets , we remove Muons from Good Muons list
  for (UInt_t imuon=0; imuon<goodMuons.size(); imuon++) {
      bool mu_jet_overlap=false;
      for (UInt_t ijet=0; ijet<goodJets.size(); ijet++) {
            double dR = dbxA::deltaR(goodMuons.at(imuon).lv().Eta(), goodMuons.at(imuon).lv().Phi(),
                                      goodJets.at(ijet).lv().Eta(),  goodJets.at(ijet).lv().Phi() );
             if(dR < 0.4) {mu_jet_overlap=true; break;}
      }
      if ( mu_jet_overlap ) {
               goodMuons.erase(goodMuons.begin()+imuon);
               imuon--;
      }
   }// end of loop over muons

// overlap removal between loose Muons and Jets , we remove Muons from Loose Muons list
   for (UInt_t imuon=0; imuon<loose_mus.size(); imuon++) {
      bool mu_jet_overlap=false;
      for (UInt_t ijet=0; ijet<goodJets.size(); ijet++) {
            double dR = dbxA::deltaR(loose_mus.at(imuon).lv().Eta(), loose_mus.at(imuon).lv().Phi(),
                                      goodJets.at(ijet).lv().Eta(),  goodJets.at(ijet).lv().Phi() );
             if(dR < 0.4) {mu_jet_overlap=true; break;}
       }
       if ( mu_jet_overlap ) {
               loose_mus.erase(loose_mus.begin()+imuon);
               imuon--;
       }
   }// end of loop over muons


// now we add the jet eta cut, we remove Jets
   for (UInt_t ijet=0; ijet<goodJets.size(); ijet++) {
       if ( (fabs(goodJets.at(ijet).EmEta()+goodJets.at(ijet).EtaCorr()) >= maxetaj)) {
               goodJets.erase(goodJets.begin()+ijet);
               ijet--;
       }
   }

// overlap removal between Jets and Electrons, we remove Jets
  if (!QCDRUN && (TRGe==1)) {
   for (UInt_t ijet=0; ijet<goodJets.size(); ijet++) {
      bool jet_ele_overlap=false;
      for (UInt_t iele=0; iele<goodElectrons.size(); iele++) {
            double dR = dbxA::deltaR(goodElectrons.at(iele).TrkEta(), goodElectrons.at(iele).TrkPhi(),
                                      goodJets.at(ijet).lv().Eta(),  goodJets.at(ijet).lv().Phi() );
             if(dR < 0.2) {jet_ele_overlap=true; break;}
       }
       if ( jet_ele_overlap ) {
               goodJets.erase(goodJets.begin()+ijet);
               ijet--;
       }
   }// end of loop over jets
  } else { // in QCD we NEED to use loose e-
   for (UInt_t ijet=0; ijet<goodJets.size(); ijet++) {
      bool jet_ele_overlap=false;
      for (UInt_t iele=0; iele<loose_eles.size(); iele++) {
            double dR = dbxA::deltaR(loose_eles.at(iele).TrkEta(), loose_eles.at(iele).TrkPhi(),
                                      goodJets.at(ijet).lv().Eta(),  goodJets.at(ijet).lv().Phi() );
             if(dR < 0.2) {jet_ele_overlap=true; break;}
       }
       if ( jet_ele_overlap ) {
               goodJets.erase(goodJets.begin()+ijet);
               ijet--;
       }
   }// end of loop over jets  for eQCD
  } // end of eQCD
// overlap removal between good Electrons and Jets, we remove good Electrons
   for (UInt_t iele=0; iele<goodElectrons.size(); iele++) {
       bool el_jet_overlap=false;
       for (UInt_t ijet=0; ijet<goodJets.size(); ijet++) {
            TLorentzVector ajet = goodJets.at(ijet).lv();
                double dR = dbxA::deltaR(goodElectrons.at(iele).TrkEta(), goodElectrons.at(iele).TrkPhi(),
                                         ajet.Eta(),  ajet.Phi() );
                if ((dR < 0.4) &&  (fabs(ajet.Pt())  > 30.)) { el_jet_overlap=true; break;}
       }
       if ( el_jet_overlap ) {
               goodElectrons.erase(goodElectrons.begin()+iele);
               iele--;
       }
    } // end of loop over electrons
// looose
   for (UInt_t iele=0; iele<loose_eles.size(); iele++) {
       bool el_jet_overlap=false;
       for (UInt_t ijet=0; ijet<goodJets.size(); ijet++) {
            TLorentzVector ajet = goodJets.at(ijet).lv();
                double dR = dbxA::deltaR(loose_eles.at(iele).TrkEta(), loose_eles.at(iele).TrkPhi(),
                                         ajet.Eta(),  ajet.Phi() );
                if ((dR < 0.7) &&  (fabs(ajet.Pt())  > 30.)) { el_jet_overlap=true; break;}
       }
       if ( el_jet_overlap ) {
               loose_eles.erase(loose_eles.begin()+iele);
               iele--;
       }
    } // end of loop over electrons
end 2017 SI */
    //========================================================
    //Select the analysed leptons&jets here. When running QCD mode use loose leptons
    std::vector<dbxJet> analy_jets;
    analy_jets=jets;
    std::vector<dbxElectron> analy_electrons;
    std::vector<dbxMuon> analy_muons;

/* 2017 SI
    if(TRGe==1){
      if(QCDRUN){
	analy_electrons=loose_eles;
      }
       else analy_electrons = goodElectrons;
    }
    if (TRGe==2) analy_electrons = goodElectrons;
    if(TRGm==1){
      if(QCDRUN){
	analy_muons = loose_mus;
      }
      else analy_muons = goodMuons;
    }
    if(TRGm==2) analy_muons = goodMuons;
  end 2017 SI */

 //End of analyses objects
 //============================================================
 analy_electrons=electrons;
 analy_muons=muons;
  if(TRGe>0){
    eff->GetXaxis()->SetBinLabel(cur_cut,">=2 electrons");
    if(analy_electrons.size()<2) return cur_cut;
    eff->Fill(cur_cut);
    cur_cut++;
  }
  if(TRGm>0){
    eff->GetXaxis()->SetBinLabel(cur_cut,">=2 muons");
    if(analy_muons.size()<2) return cur_cut;
    eff->Fill(cur_cut);
    cur_cut++;
  }
  eff->GetXaxis()->SetBinLabel(cur_cut,">=4 jets");
  if(analy_jets.size()<4) return cur_cut;
  eff->Fill(cur_cut);
  cur_cut++;



  comb_element min_cfg;

  if(TRGe>0) min_cfg=runE6Reco1(&analy_electrons,&analy_jets);
  if(TRGm>0) min_cfg=runE6Reco1(&analy_muons,&analy_jets);

  eff->GetXaxis()->SetBinLabel(cur_cut,">=1 #chi^{2}");
  if(min_cfg.NChi2<1 ) return cur_cut;//The NChi2 cut is mandatory !!!
  eff->Fill(cur_cut);
  cur_cut++;
  //Require at least 1 chi2


  //Unpack&use reconstructed objects here
  dbxParticle Zconst1,Zconst2,Zreco,Dreco,Dbarreco, Wconst1,Wconst2,Wreco,pr1Reco,pr2Reco;
  Wconst1=min_cfg.WC1;
  Wconst2=min_cfg.WC2;
  Wreco=min_cfg.Wreco;
  Zconst1=min_cfg.ZC1;
  Zconst2=min_cfg.ZC2;
  Zreco=min_cfg.Zreco;
  pr1Reco=min_cfg.pr1;
  pr2Reco=min_cfg.pr2;
  Dreco=min_cfg.De;
  Dbarreco=min_cfg.Debar;
  h_deltaM->Fill(min_cfg.deltaM);
  h_mZreco->Fill(min_cfg.Zreco.lv().M());

  double deltaRZJ0=dbxParticle::deltaR(jets.at(0),Zreco);
  double deltaPhiZJ0=dbxParticle::deltaPhi(jets.at(0),Zreco);
  double deltaPhiZJ1=dbxParticle::deltaPhi(jets.at(1),Zreco);
  double deltaPhiZJ2=dbxParticle::deltaPhi(jets.at(2),Zreco);
  double deltaPhiZJ3=dbxParticle::deltaPhi(jets.at(3),Zreco);
  double deltaRZconst=dbxParticle::deltaR(Zconst1,Zconst2);
  double DeltaRZJ=dbxParticle::deltaR(pr2Reco,Zreco);
  double DeltaRWJ=dbxParticle::deltaR(pr1Reco,Wreco);
  double deltaPhiZconst=dbxParticle::deltaPhi(Zconst1,Zconst2);


  eff->GetXaxis()->SetBinLabel(cur_cut,"#DeltaM<100");
  if(min_cfg.deltaM>MINDELTAM) return cur_cut;
  eff->Fill(cur_cut);
  cur_cut++;

  double wgtD=1.0/SGHQ;
  double wgtDbar=1.0/SGHQBAR;

  h_mDreco->Fill(Dreco.lv().M());
  h_mDbarreco->Fill(Dbarreco.lv().M());
  h_mDavg->Fill((wgtD*Dreco.lv().M()+wgtDbar*Dbarreco.lv().M())/(wgtD+wgtDbar));
  return 0;
}
