#ifndef e6_A1_H
#define e6_A1_H

#include "dbx_a.h"
#include "ReadCard.h"
#include "LeptonicWReconstructor.h"
#include "TParameter.h"

#include "analysis_core.h"

#include <TRandom.h>
#include <TSystem.h>
#include <fstream>
#include <iomanip>
struct comb_element{
  unsigned short int iW1;//Wconstitent1 
  unsigned short int iW2;//Wconstituent2
  unsigned short int ipr1;//promptjet1
  unsigned short int ipr2;//promptjet2
  unsigned short int iZ1;//ZCandidate1
  unsigned short int iZ2;//ZCandidate2
  dbxParticle pr1,pr2,Wreco,WC1,WC2,Zreco,De,Debar,ZC1,ZC2;
  double deltaM;
  double chi2;
  unsigned int NChi2;
};

class E61dbxA : public dbxA {
  public:

      
      E61dbxA(char *aname) : dbxA ( aname)
      {
	//thejets=new std::vector<dbxParticle>();
       sprintf (cname,"%s",aname); // keep the current analysis name in the class variable
       int r=dbxA::setDir(cname);  // make the relevant root directory
       if (r) cout <<"Root Directory Set Failure in:"<<cname<<endl;
       grl_cut=false;
       QCD=false;
       char fname[CHMAX];
       sprintf (fname,"%sanalysis.lhco",cname); // we record the LHCO files
       LHCOfile.open(fname);
       LHCOfile << "#LHCO file - don't remove this line."<<endl;
      };
      ~E61dbxA(){
	//delete thejets;
      };
      int initGRL();
      int setQCD();
      int bookAdditionalHistos();
      int readAnalysisParams();
      int printEfficiencies();
      int makeAnalysis(vector<dbxMuon> muons, vector<dbxElectron> electrons,
			vector<dbxJet> jets,TVector2 met, evt_data anevt); 
      int saveHistos() {
        LHCOfile.close();
        int r = dbxA::saveHistos();
        return r;
      }
      

      TH1F* h_mZreco;//!
      TH1F* h_mWreco;//!
      TH1F* h_mDreco;//!
      TH1F* h_mDbarreco;//!
      TH1F* h_mDavg;//!
      TH1F* h_dPhiZW;//!
      TH1F* h_dRWconst;//!
      TH1F* h_dRZconst;//!
      TH1F* h_dRZJ0;//!
      TH1F* h_dRZJ1;//!
      TH1F* h_dRWJ0;//!
      TH1F* h_dRWJ1;//!
      TH1F* h_deltaM;//!
      TH1F* h_ptEle;//!
      TH1F* h_Nchi2;//!
      TH1F* h_ptJets;
   private:
// create an instance of the fitter
      void saveAsLHCO(vector<dbxParticle>, vector <dbxJet>, TVector2, evt_data);
      int icount;
      bool grl_cut; 
      bool QCD;
      char cname[CHMAX];
      ofstream LHCOfile;
      template <class LEPTONS,class JETS>//needed to abstract from e-mu definiton
      comb_element runE6Reco1(const LEPTONS&,const JETS&);//may return a default element so apply NChi2 cut in analysis !

};

#endif
