////////////////////////////////////////////////////////////////////////////////
//
//  Fast Power Constrained Limits with profile likelihood ratio and frequentist one-sided limits
//
//  Daniel Whiteson, daniel@uci.edu, May 2011
//
////////////////////////////////////////////////////////////////////////////////


#include <numeric>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>
#include <iterator>
#include <cmath>
#include <cctype> // for toupper
#include <vector>

#include "TH1.h"
#include "TParameter.h"
#include "TFile.h"
#include "TNtuple.h"

#include "model.h"
#include "neyman.h"
#include "fit.h"

// constants: upper limit on POI, nsteps in POI, nMC for NeyCon, nMC for exp limits
const float poi_range = 20.0;
const int nsteps_poi = 50;
const int npe_nc = 1000;
const int npe_exp = 1000;


TNtuple *make_ntuple(model *m)
{
  TNtuple *nt;
  char ntform[500] = "imu:true:nev:fit:ipe:lh_best:lh_mu:lambda_mu:q_mu";

  for (int z=0;z<m->nsamples;z++)
    strcat(ntform,Form(":scale%d_true:scale%d_mu:scale%d_fit",z+1,z+1,z+1));

  for (int z=0;z<m->nsyst;z++)
    strcat(ntform,Form(":shape%d_true:shape%d_mu:shape%d_fit",z+1,z+1,z+1));
  
  std::cout << "Ntuple string is " << ntform << std::endl;
  
  nt = new TNtuple("neyman","",ntform);
  return nt;
}


int main(int argc, char *argv[])
{

  if (argc <4)
    {
      std::cout << "Usage: ./pcl <bg+data file> Nbackgrounds Nsyst <sig file>" << std::endl;
      exit(0);
    }
  // inputs:  sig file, bg file, nsamp, nsyst
  char fname_bg[100];
  strcpy(fname_bg,argv[1]);
  int nsamples = atoi(argv[2]);
  int nsyst = atoi(argv[3]);
  char fname_sig[100];
  strcpy(fname_sig,argv[4]);
  
  std::cout << " Bg file = " << fname_bg << std::endl;
  std::cout << " Nsamples = " << nsamples << std::endl;

  std::cout << " Nsystem. = " << nsyst << std::endl;
  std::cout << " Sig file = " << fname_sig << std::endl;

  // build the model
  model *mymodel = new model( fname_sig, fname_bg, nsamples, nsyst, poi_range);

  // open an output file
  TString foutname;
  foutname.Form("pcl_bg-%s_samp_%d_sys_%d_sig-%s",fname_bg,nsamples,nsyst,fname_sig);
  foutname.ReplaceAll('/','_');
  std::cout << " Output file = " << foutname << std::endl;
  TFile *tf = new TFile(foutname,"recreate");

  // define an ntuple that tracks fitter performance etc
  TNtuple *nt = make_ntuple(mymodel);

  std::cout << "ooo Building Neyman Construction with " << nsteps_poi << " of the POI between 0 and " << poi_range << " ooo " << std::endl;
  /// build thresholds, nt of fitting performance
  neyman mync(mymodel, poi_range, nsteps_poi,npe_nc,nt);
  
  // get nu for mu=0 in data
  float mu;
  nuis_params nu_mu0_obs;
  nu_mu0_obs.init( mymodel->nsamples,mymodel->nsyst);
  fit_histo( mymodel->obs , mymodel, mu, nu_mu0_obs, 0.0 );

  /// expected limits
  TH1F *thlim = new TH1F("thlim","",1000,-0.1,poi_range);
  std::vector<double> ul_back;

  std::cout << "ooo Generated pseudo-data at POI=0 (no signal) to get expected limits ooo " << std::endl;

  TNtuple *tnte = new TNtuple("perf","","ipe:nev:lim");
  for (int ipe=0; ipe < npe_exp; ipe++)
    {

      TH1F *toy_data =  mymodel->gen_exp(0, nu_mu0_obs );
      mymodel->randomize_priors();
      float toy_lim = mync.get_limits ( toy_data );

      std::cout << " PE " << ipe << " " << toy_data->Integral() << " events in toy data. Limit is " << toy_lim << std::endl;
      //store it
      ul_back.push_back(toy_lim);
      tnte->Fill(ipe,toy_data->Integral(),toy_lim);
      thlim->Fill(toy_lim);
    }
  mymodel->reset_priors();
  ////// get bands

  // sort it
  sort(ul_back.begin(), ul_back.end());

  float pcl=ul_back[(int)(ul_back.size()*0.16)];

  // analyze it
  std::cout << "Median UL = " << ul_back[(int)(ul_back.size()/2)]
	    << " + " << ul_back[(int)(ul_back.size()*0.84)]
	    << " - " << pcl
	    << " ++ " << ul_back[(int)(ul_back.size()*0.95)]
	    << " -- " << ul_back[(int)(ul_back.size()*0.05)]
	    << std::endl;

  /// observed limit
  float lim = mync.get_limits ( mymodel->obs );
  
  float lim_pcl = lim;
  std::cout << "Observed limit " << lim << std::endl;
  if (lim < pcl) 
    lim_pcl = pcl;
  std::cout << "      PCL limit " << lim_pcl << std::endl;


  // write useful stuff out
  tf->cd();
  nt->Write();
  tnte->Write();
  thlim->Write();
  TParameter<double> *b_med = new TParameter<double>("b_med",ul_back[(int)(ul_back.size()/2)]);
  TParameter<double> *b_up1 = new TParameter<double>("b_up1",ul_back[(int)(ul_back.size()*0.84)]);
  TParameter<double> *b_up2 = new TParameter<double>("b_up2",ul_back[(int)(ul_back.size()*0.95)]);
  TParameter<double> *b_dn1 = new TParameter<double>("b_dn1",ul_back[(int)(ul_back.size()*0.16)]);
  TParameter<double> *b_dn2 = new TParameter<double>("b_dn2",ul_back[(int)(ul_back.size()*0.05)]);
  TParameter<double> *lim_obs = new TParameter<double>("lim_obs",lim);
  TParameter<double> *tlim_pcl = new TParameter<double>("lim_pcl",lim_pcl);
  b_med->Write();
  b_up1->Write();
  b_up2->Write();
  b_dn1->Write();
  b_dn2->Write();
  lim_obs->Write();
  tlim_pcl->Write();
  
  mync._thresh->Write("thresholds");
  tf->Close();

}
