
#ifndef MODELH
#define MODELH
#include "TH1.h"
#include <cmath>
#include "TRandom3.h"

const int max_samples=25;
const int max_shapes=10;

struct syst_res
{
  TH1F *h_samp[max_samples];
  TH1F *hsig;
};

struct nuis_params
{
  float overall[max_samples];
  float shape[max_shapes];
  void set(double *par, int nsamp,int nsyst);
  void fill(double *par, int nsamp,int nsyst);
  void print(int nsamp,int nsyst);
  void init(int nsamp,int nsyst);
};


struct syst_info_samp
{
  TH1F *h_samp;
  TH1F **h_samp_p;
  TH1F **h_samp_n;
};

class model
{
  TRandom3 R;
  syst_info_samp *signal;
  syst_info_samp *samples;

  bool checkbins();
  void distort_hist(TH1F *h_samp,TH1F **h_samp_n,TH1F **h_samp_p, float *sfluc, int nsyst, TH1F *res);
public:

  float uncert[max_samples];
  float prior[max_samples];
  float shape_prior[max_shapes];

  void print();

  syst_res *new_syst(nuis_params &nuis);
  // fixed memory for results, to avoid so much Cloning
  syst_res *sys_res;
  TH1F *hexp;
  
  void reset_priors();
  void randomize_priors();
  
  float poi_upper;
  int nsyst;
  int nsamples;
  TH1F *obs;
  model( char sigfile[1000], char bgfile[1000], int nsamp, int nsyst, float poi_up );
  ~model();
  TH1F * gen_exp( float poi, nuis_params &nuis);
  float log_lhood ( TH1F *data, float poi, nuis_params &nu, bool verbose=false);
  float lhood( TH1F *data, float poi, nuis_params &nu, bool verbose) {return exp( log_lhood ( data, poi, nu, verbose ) ); }
};

#endif MODELH
