#include "TH1.h"
#include <vector>
#include "model.h"

float fit_histo(TH1F *data, model *m, float &poi, nuis_params &nuis, bool verbose=false,float poi_fix = -1.0);

