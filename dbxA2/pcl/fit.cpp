#include <cstdlib>
#include "fit.h"
#include "TMinuit.h"
#include <iostream>
#include <cmath>

model *g_model;
TH1F *g_data;
bool g_verbose;

void fcn(Int_t &npar, Double_t *gin, Double_t &f, Double_t *par, Int_t iflag)
{
  float poi = par[0];

  // go from ugly array of parameters to our poi + nuis
  nuis_params nuis;
  nuis.set(&(par[1]),g_model->nsamples,g_model->nsyst);
  //std::cout << "  fit: fcn called with npar  = " << npar << " poi = " << poi << std::endl;
  //nuis.print(g_model->nsamples,g_model->nsyst);
  
  // call the model likelihood
  f = - g_model->log_lhood( g_data, poi, nuis, false);
}

float fit_histo(TH1F *data, model *m, float &poi, nuis_params &nuis, bool verbose,float poi_fix)
{

  if (!data) { std::cout << " Fit called with null data!" << std::endl; exit(0); }
  if (!m) {std::cout << " Fit called with null model!" << std::endl; exit(0); }
  if (verbose) 
    {
      std::cout << " -- fit histogram " << data << " with " << data->Integral() << " events "
		<< " poi = " << poi << " fixed? " << poi_fix <<std::endl;

      nuis.print(m->nsamples,m->nsyst);
      // m->print();
    }

  g_model = m;
  g_data  = data;
  g_verbose = verbose;

  TMinuit *gMinuit = new TMinuit(2);
  
  gMinuit->SetPrintLevel(-1); // shut the fuck up, donny!
  gMinuit->SetFCN(fcn);
  
  Double_t arglist[10];
  Int_t ierflg = 0;

  // fix the POI or not?
  if (poi_fix > -1)
    {
      // use fixed value as initial value
      gMinuit->mnparm(0,"poi",poi_fix,1.0/m->poi_upper,0.0,m->poi_upper,ierflg);
      gMinuit->FixParameter(0);
    }
  else
    // use 0 as the initial value
    gMinuit->mnparm(0,"poi",0.0,1.0/m->poi_upper,0.0,m->poi_upper,ierflg);

  if (ierflg) std::cout << " error "  << ierflg << std::endl;

  // np are
  // 1 per sample
  for (int is=0;is<m->nsamples;is++)
    {
      if (verbose) std::cout << "  fit:adding scaling parameter " << is << std::endl;

      if (m->uncert[is]==0.0)
	{
	  if (verbose) std::cout << "  -> fixing to value " << m->prior[is] << std::endl;
	  gMinuit->mnparm(1+is, Form("o%d",is+1), 0, 1.0/5.0,m->prior[is] -0.1,m->prior[is] + 0.1,ierflg);
	  gMinuit->FixParameter(1+is);
	}
      else
	{
	  gMinuit->mnparm(1+is, Form("o%d",is+1), 0, 1.0/5.0,std::max((float)0.0,m->prior[is] - 3 * m->uncert[is]),m->prior[is] + 3* m->uncert[is],ierflg);
	  if (verbose) std::cout << " range [" << std::max((float)0.0,(m->prior[is] - 3 * m->uncert[is])) << " - " << (m->prior[is] + 3* m->uncert[is]) <<  "] " << std::endl;
	}

      if (ierflg) std::cout << " error "  << ierflg << std::endl;
    }

  // 1 per shape syst
  for (int is=0;is<m->nsyst;is++)
    {
      if (verbose) std::cout << "  fit: dding shape parameter " << is << std::endl;
      gMinuit->mnparm(1+m->nsamples+is, Form("s%d",is+1), 0, 1.0/5.0, -3,3,ierflg);
      if (ierflg) std::cout << " error "  << ierflg << std::endl;
    }

  arglist[0] = 500;
  arglist[1] = 1.;
  gMinuit->mnexcm("MIGRAD", arglist ,2,ierflg);

  // Print results
  Double_t amin,edm,errdef;
  Int_t nvpar,nparx,icstat;
  gMinuit->mnstat(amin,edm,errdef,nvpar,nparx,icstat);
  double dum, dum2;

  // get POI
  double fit_poi,fit_poi_err;
  gMinuit->GetParameter(0 ,fit_poi , fit_poi_err);
  if (verbose) std::cout << "   fit: poi = " << fit_poi << " +- " << fit_poi_err << std::endl;

  // get NPs
  double fit_np[max_shapes+max_samples];
  double fit_np_err[max_shapes+max_samples];
  for (int is=0;is<m->nsamples+m->nsyst;is++)
    {
      gMinuit->GetParameter(1+is ,fit_np[is],fit_np_err[is]);
      if (verbose) std::cout << "   fit: np" << is << " = " << fit_np[is] << " +- " << fit_np_err[is] << std::endl;
    }

  // set the outputs
  nuis.set(fit_np,m->nsamples,m->nsyst);
  //  nuis.print(m->nsamples,m->nsyst);
  poi = fit_poi;

  delete gMinuit;

  if (verbose)  g_model->log_lhood( g_data, poi, nuis, true );
  if (verbose) std::cout << " fit: -log lhood = " << amin << " lhood = " << exp(-amin) << std::endl;
  // amin is -log lhood
  return exp(-amin);
}
