#include "model.h"
#include <iostream>
#include "TFile.h"
#include "TParameter.h"
#include "TMath.h"

void nuis_params::set(double *par, int nsamp,int nsyst)
{
  int ip=0;
  for (int i=0;i<nsamp;i++)
    overall[i] = par[ip++];
  for (int i=0;i<nsyst;i++)
    shape[i] = par[ip++];
}

void nuis_params::init( int nsamp,int nsyst)
{
  for (int i=0;i<nsamp;i++)
    overall[i] = 1.0;
  for (int i=0;i<nsyst;i++)
    shape[i] = 0.0;
}

void nuis_params::print(int nsamp,int nsyst)
{
  for (int i=0;i<nsamp;i++)
    std::cout << " nuis: overall " << i << " = " << overall[i] << std::endl;
  for (int i=0;i<nsyst;i++)
    std::cout << " nuis: shape " << i << " = " << shape[i] << std::endl;
}

void nuis_params::fill(double *par, int nsamp,int nsyst)
{
  int ip=0;
  // -1 because one sample scale is the POI
  for (int i=0;i<nsamp;i++)
    par[ip++] = overall[i];
  for (int i=0;i<nsyst;i++)
    par[ip++] = shape[i];
}


model::model( char sname[1000], char fname[1000], int nsamp, int nsys, float poi_up )
{
  poi_upper = poi_up;
  nsamples = nsamp;
  nsyst = nsys;

  samples = new syst_info_samp[nsamples];

  TFile *tf = new TFile(fname);
  tf->ls();
  TFile *tfs = new TFile(sname);
  tfs->ls();

  obs = (TH1F*)(tf->Get("data"))->Clone();
  if (!obs) { std::cout << "Can't find data " << std::endl; exit(0); }

  for (int i=0;i<nsamp;i++)
    {
      samples[i].h_samp = (TH1F*)((TH1F*)(tf->Get(Form("sample_%d",i+1))))->Clone();
      std::cout << " sample " << i << " is " << samples[i].h_samp << " orig is " << tf->Get(Form("sample_%d",i+1)) << std::endl;
      if (!samples[i].h_samp)
	{
	  std::cout << "Couldn't read " << Form("sample_%d",i+1) << " from " << fname << std::endl;
	  exit(0);
	}
      std::cout << "Read " << Form("sample_%d",i+1) << " with Nev = " << samples[i].h_samp->Integral(0,-1) << std::endl;

      // check binning
      if (i>0)
	{
	  if (samples[i].h_samp->GetNbinsX() != samples[0].h_samp->GetNbinsX())
	    {
	      std::cout << " Mismatch in binning for sample " << i 
			<< " bins " << samples[i].h_samp->GetNbinsX() 
			<< " vs " << samples[0].h_samp->GetNbinsX() << std::endl;
	    }
	  
	  if (samples[i].h_samp->GetXaxis()->GetXmin() != samples[0].h_samp->GetXaxis()->GetXmin())
	    {
	      std::cout << " Mismatch in binning for sample " << i << " lo edge " << samples[i].h_samp->GetXaxis()->GetXmin() << " != " << samples[0].h_samp->GetXaxis()->GetXmin() << std::endl;
	    }

	  if (samples[i].h_samp->GetXaxis()->GetXmax() != samples[0].h_samp->GetXaxis()->GetXmax())
	    {
	      std::cout << " Mismatch in binning for sample " << i << " hi edge " << samples[i].h_samp->GetXaxis()->GetXmax() << " != " << samples[0].h_samp->GetXaxis()->GetXmax() << std::endl;
	    }
	  
	}

      // set arrays for systematic shapes
      if (nsyst>0)
	{
	  samples[i].h_samp_p = new TH1F*[nsyst];
	  samples[i].h_samp_n = new TH1F*[nsyst];
	}

    }
  // signal case
  signal = new syst_info_samp;
  {
    signal->h_samp = (TH1F*)((TH1F*)(tfs->Get("signal")->Clone()));
    if (!signal->h_samp)
	{
	  std::cout << "Couldn't read signal from " << sname << std::endl;
	  exit(0);
	}
    std::cout << "Read signal  with Nev = " << signal->h_samp->Integral(0,-1) << std::endl;
    
    // set arrays for systematic shapes
    if (nsyst>0)
      {
	signal->h_samp_p = new TH1F*[nsyst];
	signal->h_samp_n = new TH1F*[nsyst];
      }
  }
  std::cout << "About to read " << nsyst << " systematics " << std::endl;
  for (int isyst=0;isyst<nsyst;isyst++)
    {
      shape_prior[isyst] = 0.0;
      std::cout << " syst " << isyst << "=======" << std::endl;
      // positive
      {
	// backgrounds
	for (int i=0;i<nsamp;i++)
	  {
	    samples[i].h_samp_p[isyst]= (TH1F*)((TH1F*)(tf->Get(Form("sample_%d_sys_%d_p",i+1,isyst+1))))->Clone();
	    if (!samples[i].h_samp_p[isyst])
	      {
		std::cout << "Couldn't read " << Form("sample_%d_sys_%d_p",i+1,isyst+1)<< " from " << fname << std::endl;
		exit(0);
	      }
	    std::cout << "Read " << Form("sample_%d_sys_%d_p",i+1,isyst+1) << " with Nev = " << samples[i].h_samp_p[isyst]->Integral(0,-1) << std::endl;
	  }
	
	// signal
	signal->h_samp_p[isyst]= (TH1F*)((TH1F*)(tfs->Get(Form("signal_sys_%d_p",isyst+1))))->Clone();
	if (!signal->h_samp_p[isyst])
	  {
	    std::cout << "Couldn't read " << Form("signal_sys_%d_p",isyst+1)<< " from " << sname << std::endl;
	    exit(0);
	  }
	std::cout << "Read " << Form("signal_sys_%d_p",isyst+1) << " with Nev = " << signal->h_samp_p[isyst]->Integral(0,-1) << std::endl;
	


      }
      // negative
      {
	// backgrounds
	for (int i=0;i<nsamp;i++)
	  {
	    samples[i].h_samp_n[isyst] = (TH1F*)((TH1F*)(tf->Get(Form("sample_%d_sys_%d_n",i+1,isyst+1))))->Clone();
	    if (!samples[i].h_samp_n[isyst])
	      {
		std::cout << "Couldn't read " << Form("sample_%d_sys_%d_n",i+1,isyst+1) << " from " << fname << std::endl;
		exit(0);
	      }
	    std::cout << "Read " << Form("sample_%d_sys_%d_n",i+1,isyst+1) << " with Nev = " << samples[i].h_samp_n[isyst]->Integral(0,-1)<< std::endl;
	  }
	// signal
	{
	  signal->h_samp_n[isyst] = (TH1F*)((TH1F*)(tfs->Get(Form("signal_sys_%d_n",isyst+1))))->Clone();
	  if (!signal->h_samp_n[isyst])
	    {
	      std::cout << "Couldn't read " << Form("signal_sys_%d_n",isyst+1) << " from " << fname << std::endl;
	      exit(0);
	    }
	  std::cout << "Read " << Form("signal_sys_%d_n",isyst+1) << " with Nev = " << signal->h_samp_n[isyst]->Integral(0,-1) << std::endl;
	}
      }

    }

  hexp = (TH1F*)samples[0].h_samp->Clone();
  sys_res = new syst_res;

  for (int i=0;i<nsamples;i++)
    {
      std::cout << " looking for uncertainty on " << i << std::endl;
      if (i<nsamples)
	{
	  std::cout << "Trying to load " << Form("uncert_%d",i+1) << " from " << tf << std::endl;
	  tf->ls();
	  TParameter<double> *tp = (TParameter<double>*)(tf->Get(Form("uncert_%d",i+1)));
	  std::cout << " ... got " << tp << std::endl;
	  if (!tp)
	    {
	      std::cout << "Couldn't load uncertainty " << Form("uncert_%d",i+1) << " from file " << fname << std::endl;
	      uncert[i] = 100.0;
	    }
	  else
	    uncert[i] = tp->GetVal();
	}
      else
	uncert[i] = 100.0;

      prior[i] = 1.0;
      std::cout << " Sample " << i << " prior = " << prior[i] << " +- " << uncert[i] << std::endl;
      std::cout << " Cloning " << i << " from " << samples[i].h_samp->Clone() << std::endl;
      sys_res->h_samp[i] = (TH1F*)samples[i].h_samp->Clone();
    }
  sys_res->hsig = (TH1F*)signal->h_samp->Clone();
  //  tf->Close();
  //tfs->Close();

  reset_priors();
  
  // check that it's all good
  print();
  if (!checkbins())
    {
      std::cout << "Please correct bins. Exiting. " << std::endl;
      //exit(0);
    }
}

void model::reset_priors()
{
  for (int isyst=0;isyst<nsyst;isyst++)
    shape_prior[isyst] = 0.0;
  for (int i=0;i<nsamples;i++)
    prior[i] = 1.0;
}


void model::randomize_priors()
{
  for (int isyst=0;isyst<nsyst;isyst++)
    {
      shape_prior[isyst] = R.Gaus(0,1.0);
      // std::cout << " rand shape prior " << isyst << " " << shape_prior[isyst] << std::endl;
    }

  for (int i=0;i<nsamples;i++)
    {
      prior[i] = R.Gaus(1.0,uncert[i]);
      //      std::cout << " rand overall prior " << i << " " << prior[i] << std::endl;
    }
}


void model::print()
{
  std::cout << "===== Model with " << nsamples << " samples  ==== " << std::endl;
  for (int i=0;i<nsamples;i++)
    {
      std::cout << " Sample " << i << std::endl;
      std::cout << " Nominal histogram address  " << samples[i].h_samp 
		<< " with Nev = " << samples[i].h_samp->Integral() << std::endl;
      for (int isyst=0;isyst<nsyst;isyst++)
	{
	  std::cout << " Syst " << isyst  << " + is  " << samples[i].h_samp_p[isyst]
		    << " with Nev = " << samples[i].h_samp_p[isyst]->Integral() << std::endl;
	  std::cout << " Syst " << isyst  << " - is  " << samples[i].h_samp_n[isyst]
		    << " with Nev = " << samples[i].h_samp_n[isyst]->Integral() << std::endl;
	}
    }
  {
    std::cout << " Signal " << std::endl;
    std::cout << " Nominal is  " << signal->h_samp 
	      << " with Nev = " << signal->h_samp->Integral() << std::endl;
    for (int isyst=0;isyst<nsyst;isyst++)
      {
	std::cout << " Syst " << isyst  << " + is  " << signal->h_samp_p[isyst]
		  << " with Nev = " << signal->h_samp_p[isyst]->Integral() << std::endl;
	std::cout << " Syst " << isyst  << " - is  " << signal->h_samp_n[isyst]
		  << " with Nev = " << signal->h_samp_n[isyst]->Integral() << std::endl;
      }
  }
  std::cout << " Observed has " << obs->Integral() << " events " << std::endl;

}

void model::distort_hist(TH1F *h_samp,TH1F **h_samp_n,TH1F **h_samp_p, float *sflucs, int nsyst, TH1F *res)
{
  for (int b = 0; b <= h_samp->GetNbinsX(); b++)
    {
      float nom = h_samp->GetBinContent(b);
      float distort = 0.0;
      
      // do the distortions
      for (int is=0;is<nsyst;is++)
	{
	  float this_diff =  0.0;
	  if (sflucs[is]>=0)
	    {
	      this_diff = (h_samp_p[is]->GetBinContent(b)-nom);
	      //std:: cout << " diff = " << sys->h_samp_p[is]->GetBinContent(b) << "-" << nom << " = " << this_diff << std::endl;
	    }
	  else
	    {
	      this_diff = (h_samp_n[is]->GetBinContent(b)-nom);
	      //std:: cout << " diff = " << sys->h_samp_n[is]->GetBinContent(b) << "-" << nom << " = " << this_diff << std::endl;
	    }
	  
	  distort += this_diff * sflucs[is];  
	  //  std::cout << "bin " << b << " samp " << isamp << " fluc " << is << " diff = " << this_diff << " fluc = " << sflucs[is] << " distort = " << this_diff * sflucs[is] << " total distort = " << distort << std::endl;
	  
	  
	}
      //std::cout << "bin " << b << " shifted result is nom: " << nom << " + " << distort << " = " << (nom+distort) << std::endl;
      res->SetBinContent(b, nom + distort);
      
    }
}

syst_res *model::new_syst(nuis_params &nuis)
{

  // bg
  for (int isamp=0;isamp<nsamples;isamp++)
    {
      distort_hist(samples[isamp].h_samp,
		   samples[isamp].h_samp_n,
		   samples[isamp].h_samp_p,
		   nuis.shape, nsyst, sys_res->h_samp[isamp]);

    }

  // signal
  distort_hist(signal->h_samp,signal->h_samp_n,signal->h_samp_p,nuis.shape,nsyst, sys_res->hsig);

  return sys_res;

}


model::~model()
{

}

TH1F * model::gen_exp( float poi, nuis_params &nuis)
{

  
  bool verbose=false;
  // make new shapes corresponding to these shape uncertainties
  sys_res = new_syst(nuis);

  // now scale them by their overall scale
  sys_res->hsig->Scale(poi);

  for (int isamp=0;isamp<nsamples;isamp++)
    sys_res->h_samp[isamp]->Scale(nuis.overall[isamp]);

  if (verbose) std::cout << " gen_exp: poi = " << poi;
  if (verbose) nuis.print(nsamples,nsyst);

  for (int b = 0; b <= hexp->GetNbinsX(); b++)
    {
      //  std::cout << " bin " << b << "  bg = " << sys_res->bg -> GetBinContent( b ) << std::endl;
      //std::cout << " bin " << b << " sig = " << sys_res->sig -> GetBinContent( b ) << std::endl;
      double m = 0;

      for (int i=0;i<nsamples;i++)
	m += sys_res->h_samp[i]->GetBinContent(b);

      m += sys_res->hsig->GetBinContent(b);
      
      hexp -> SetBinContent( b ,R.Poisson ( m ) );
      if (verbose) std::cout << " bin " << b << " exp mean = " << m << " val " << hexp->GetBinContent(b) << std::endl;
    }
  if (verbose) std::cout << "Done. " << std::endl;
  return hexp;


}



bool model::checkbins()
{

  for (int i=0;i<obs->GetNbinsX();i++)
    {
      float nbg=0;
      for (int isamp=0;isamp<nsamples;isamp++)
	nbg += samples[isamp].h_samp->GetBinContent(i);
      float nsig=signal->h_samp->GetBinContent(i);
      float nobs=obs->GetBinContent(i);
      if ( (nsig > 0 || nobs > 0) && nbg <= 0 )
	{
	  std::cout << " Model invalid. Bin " << i << " has bg prediction of " << nbg << " with sig " << nsig << " obs " << nobs <<std::endl;
	  return false;
	}
    }
  return true;
}

float model::log_lhood ( TH1F *data, float poi, nuis_params &nuis, bool verbose)
{
  double llh = 0.0;
  if (verbose)
    {
      std::cout << " log_lhood with data " << data << " entries " << data->GetEntries() << std::endl;
      nuis.print(nsamples,nsyst);
      //      print();
    }
  // change the shapes of the histograms to match these shapes
  syst_res *res = new_syst(nuis);

  // per-bin poisson
  for (int b = 0; b <= data->GetNbinsX(); b++)
    {
      
      float pred =0.0;

      for (int i=0;i<nsamples;i++)
	{
	  if (verbose) std::cout << "   lhood: bin " << b << " sample " << i << " pred = " << nuis.overall[i] << " * " << res->h_samp[i]->GetBinContent(b) <<" = " << (nuis.overall[i] * res->h_samp[i]->GetBinContent(b)) << std::endl;
	  pred += nuis.overall[i] * res->h_samp[i]->GetBinContent(b);
	}

      if (verbose) std::cout << "   lhood: bin " << b << " signal pred = " << poi  << " * " <<  res->hsig->GetBinContent(b) << " = " << (poi * res->hsig->GetBinContent(b)) << std::endl;
      pred += poi * res->hsig->GetBinContent(b);


      if (pred<0)
	{
	  //	  std::cout << " Warning: bin " << b << " has negative prediction " << pred << std::endl;
	  pred = 0.01;
	}

      int n = data->GetBinContent(b);

      double binp = TMath::Poisson ( n, pred );

      llh += log(binp);
      if (verbose) std::cout << " lhood: bin " << b << " pred = " << pred<< " obs " << n << " p = " << binp << " log(p) = " << log(binp) << " llh = " << llh << std::endl;
    }

  // constraints on nuisance parameters (scales)
  for (int is =0; is<nsamples;is++)
    {
      double c = TMath::Gaus( nuis.overall[is],prior[is],uncert[is] );
      llh += log(c);
      if (verbose) std::cout << " lhood: scale constraint val = " << nuis.overall[is] << " mean = " << prior[is] << " +- " << uncert[is] << " constr = " << c << " log(constr) = " << log(c) << " llh = " << llh << std::endl;
    }


  // constraints on nuisance parameters (shapes)
  for (int is =0; is<nsyst;is++)
    {
      double c = TMath::Gaus( nuis.shape[is],shape_prior[is],1.0 );
      llh += log(c);
      if (verbose) std::cout << " lhood: shape constraint val = " << nuis.overall[is] << " mean = " << prior[is] << " +- " << uncert[is] << " constr = " << c << " log(constr) = " << log(c) << " llh = " << llh << std::endl;
    }
  
  return llh;

}
