
void performance(char fname[500])
{
  gROOT->SetStyle("Plain");
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);
  
  gStyle->SetTitleSize(0.05593);
  gStyle->SetTitleXOffset(0.95);
  gStyle->SetTitleXSize(0.08);
  gStyle->SetTitleFont(132,"xyz");
  gStyle->SetLabelFont(132,"xyz");
  
  gStyle->SetTitleYOffset(1.0);
  gStyle->SetTitleYSize(0.06593);
  gStyle->SetTitleXSize(0.06593);
  
  gStyle->SetTitleBorderSize(0);
  gStyle->SetNdivisions(10,"xyz");
  gStyle->SetLabelSize(0.035,"xyz");
  gStyle->SetOptStat(000000);
  gStyle->SetPadBottomMargin(0.15);
  gStyle->SetPadLeftMargin(0.15);
  
  TFile *tf = new TFile(fname);
  TNtuple *neyman = (TNtuple*)tf->Get("neyman");
  TNtuple *perf   = (TNtuple*)tf->Get("perf");
  
  TGraph *thresh  = (TGraph*)tf->Get("thresholds");
  
  TCanvas *tc = new TCanvas("tc","",600,600);
  tc->Divide(2,2);
  tc->cd(1);
  thresholds->Draw("ALP");
  thresholds->GetXaxis()->SetTitle("Signal Strength");
  thresholds->GetYaxis()->SetTitle("Threshold on test stat.");
  
  tc->cd(2);
  neyman->Draw("fit:true","1","prof");
  tc->cd(3);
  neyman->Draw("q_mu:true","1","box");
  tc->cd(4);
  perf->Draw("lim");

  tc->Print(Form("performance_%s.eps",fname));

}
