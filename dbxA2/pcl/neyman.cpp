#include <algorithm>
#include "neyman.h"
#include "fit.h"
#include <cmath>
#include <iostream>
#include <cstdlib>


neyman::neyman(model *m, float range, int ns, int npe, TNtuple *nt)
{
  poi_upper = range;
  nsteps = ns;
  npes = npe;
  thismodel = m;

  bool verbose=true;

  for (int imu =0; imu<nsteps;imu++)
    {
      float delta = (imu*1.0)/(nsteps-1);
      float mu = pow(delta,3)*poi_upper;

      if (verbose) std::cout << "=== neyman imu = " << imu << " mu = " << mu << " === " << std::endl;

      // get nu for this mu in data
      nuis_params nu_hat_hat_obs;
      nu_hat_hat_obs.init(thismodel->nsamples,thismodel->nsyst);
      if (verbose) std::cout << " Calling fit to find observed nu " << std::endl;
      fit_histo( m->obs , m, mu, nu_hat_hat_obs, true,mu );
      
      std::vector<double> q_mu_dist;
      std::vector<double> mu_hat_dist;
      for (int ipe=0;ipe<npes;ipe++)
	{
	  //	  std::cout << "  == neyman toy mc = " << ipe << " == " << std::endl;
	  TH1F *toy_data =  m->gen_exp(mu, nu_hat_hat_obs );
	  
	  ///// randomly vary the constraints
	   m->randomize_priors();

	  // get best fit mu and nu
	  float mu_hat;
	  nuis_params nu_hat;
	  nu_hat.init(thismodel->nsamples,thismodel->nsyst);
	  float lh_best = fit_histo( toy_data , m, mu_hat, nu_hat,false);
	  if (mu_hat<0)
	    {
	      mu_hat = 0;
	      // fix mu to 0, maximize the nps, recalc lhood	    
	      nu_hat.init(thismodel->nsamples,thismodel->nsyst);
	      lh_best = fit_histo( toy_data , m, mu_hat, nu_hat,false, mu_hat);
	    }

	  // get best fit nu at this mu
	  nuis_params nu_hat_hat;
	  nu_hat_hat.init(thismodel->nsamples,thismodel->nsyst);
	  float lh_mu   = fit_histo( toy_data , m, mu, nu_hat_hat, false,mu);
	  
	  float lambda_mu = lh_mu / lh_best;

	  /*
	  if (lambda_mu>2.0)
	    {
	      std::cout << "Error: lambda should be < 1, but we find " << lambda_mu << " best = " << lh_best << " mu = " << lh_mu << std::endl;
	      std::cout << " ===== best fit letting mu,nu float ===== " << std::endl;

	      nu_hat.init(thismodel->nsamples,thismodel->nsyst);
	      float lh_best_conf = fit_histo( toy_data , m, mu_hat, nu_hat,true);
	      std::cout << " ===== best fit fixing mu to " << mu << " floating nu " << std::endl;
	      nu_hat_hat.init(thismodel->nsamples,thismodel->nsyst);
	      float lh_mu_conf   = fit_histo( toy_data , m, mu, nu_hat_hat, true,mu);
	      std::cout << " best = " << lh_best_conf << " true = " << lh_mu_conf << std::endl;
	    }
	  */




	  float q_mu = 0;
	  if (mu_hat > mu)
	    q_mu = 0;
	  else
	    q_mu = - 2 * log (lambda_mu );
	  q_mu_dist.push_back(q_mu);
	  mu_hat_dist.push_back(mu_hat);

	  int id=0;
	  float ntdata[100];
	  ntdata[id++] = imu;
	  ntdata[id++] = mu;
	  ntdata[id++] = toy_data->Integral();
	  ntdata[id++] = mu_hat;
	  ntdata[id++] = ipe;
	  ntdata[id++] = lh_best;
	  ntdata[id++] = lh_mu;
	  ntdata[id++] = lambda_mu;
	  ntdata[id++] = q_mu;
	  for (int z=0;z<m->nsamples;z++)
	    {
	      ntdata[id++] = nu_hat_hat_obs.overall[z];
	      ntdata[id++] = nu_hat_hat.overall[z];
	      ntdata[id++] = nu_hat.overall[z];
	    }
	  for (int z=0;z<m->nsyst;z++)
	    {
	      ntdata[id++] = nu_hat_hat_obs.shape[z];
	      ntdata[id++] = nu_hat_hat.shape[z];
	      ntdata[id++] = nu_hat.shape[z];
	    }
	  if (ipe%100==0) 
	    std::cout << "  PE " << ipe << " true = " << mu << " fit = " << mu_hat << " lh_best = " << lh_best << " lh_mu = " << lh_mu << " lambda_mu = " << lambda_mu << " q_mu = " << q_mu << std::endl;
	  //	  std::cout << " ntuple = " << nt << std::endl;
	  nt->Fill(ntdata);

	}
      
      sort(q_mu_dist.begin(),q_mu_dist.end());
      sort(mu_hat_dist.begin(),mu_hat_dist.end());
      _threshold_mu[imu] = q_mu_dist[(int)(q_mu_dist.size()*0.95)];
      _mu[imu] = mu;
      std::cout << " neynman imu= " << imu << " true mu = " << mu << " median measured mu = " << mu_hat_dist[(int)(mu_hat_dist.size()*0.5)] << " median q_mu = " << q_mu_dist[(int)(q_mu_dist.size()*0.5)] << " threshold = " << q_mu_dist[(int)(q_mu_dist.size()*0.95)] << std::endl;
    }
  m->reset_priors();
  _thresh = new TGraph(nsteps,_mu,_threshold_mu);
}

neyman::~neyman()
{
}

float neyman::threshold(float mu)
{
  return _thresh->Eval(mu);
}

float neyman::get_limits (TH1F *hexp, bool verbose)
{
  // get best fit mu and nu
  float mu_hat;
  nuis_params nu_hat;
  nu_hat.init(thismodel->nsamples,thismodel->nsyst);
  float lh_best = fit_histo( hexp , thismodel, mu_hat, nu_hat,true);
  if (mu_hat<0) 
    {
      mu_hat = 0;
      // fix mu to 0, maximize the nps, recalc lhood
      nu_hat.init(thismodel->nsamples,thismodel->nsyst);	      
      lh_best = fit_histo( hexp , thismodel, mu_hat, nu_hat,true, mu_hat );
    }
  std::cout << " fit poi = " << mu_hat << " lh_best = " << lh_best << std::endl;
  for (float mu = poi_upper; mu > 0 ; mu *= 0.98)
    {
      nuis_params nu_hat_hat;
      nu_hat_hat.init(thismodel->nsamples,thismodel->nsyst);
      float lh_mu = fit_histo( hexp , thismodel, mu, nu_hat_hat,false,mu);

      float lambda_mu = lh_mu / lh_best;
      float q_mu = 0;
      if (mu_hat > mu)
	q_mu = 0;
      else
	q_mu = - 2 * log (lambda_mu );

      std::cout << "  Scan mu = " << mu << " lh_mu = " << lh_mu << " q_mu = " << q_mu << " threshold = " << threshold(mu) << " ratio = " << q_mu/threshold(mu) << std::endl;

      float ratio = q_mu/threshold(mu);

      // speed up
      if (ratio > 2)
	mu = mu *0.75;

      if (q_mu <= threshold(mu))
	{
	  if (ratio < 0.5)
	    std::cout << " Your limit is likely truncated due to the upper range of your POI scan (currently " << poi_upper << "). You should increase and rerun. " << std::endl;
	  std::cout << " --> limit is " << mu << std::endl;
	  return mu;
	}
    }
  std::cout << " This should never happen.  Panic!" << std::endl;
  exit(-1);
  
}
