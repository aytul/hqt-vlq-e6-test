#ifndef NEYMANH
#define NEYMANH

#include "model.h"
#include "TGraph.h"
#include "TNtuple.h"

class neyman
{
public:
  float poi_upper;
  float _mu[1000];
  float _threshold_mu[1000];
  TGraph *_thresh;
  int nsteps;
  int npes;
  model *thismodel;
  neyman ( model *m, float range, int ns, int npe, TNtuple *nt=0);
  ~neyman();
  float threshold( float mu );
  float get_limits( TH1F *hexp , bool verbose=false);
};

#endif NEYMANH
