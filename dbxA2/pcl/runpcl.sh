#!/bin/bash
echo root is here: $ROOTSYS
export PATH=$PATH:$ROOTSYS/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ROOTSYS/lib:.
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$XRDSYS/lib
export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:$XRDSYS/lib/:$ROOTSYS/lib:.

numbg=$1

if [ -e fpcl.out ]; then
 echo output file exists. Will not run again, just retrieve results.
else
 echo running to get limits...
 ../pcl/pcl sibg.root $numbg 0 sibg.root > fpcl.out
 mv pcl_bg*.root pcl_out.root
fi


