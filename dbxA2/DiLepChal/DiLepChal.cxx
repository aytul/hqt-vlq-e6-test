#include "DiLepChal.h"
#include "TRandom3.h"
#include "TParameter.h"
#include "analysis_core.h"
#include "dbx_a.h"


////#include "WhichPeriod.h"

// #define __VERBOSE2__
/*

Cut 0 (C0): Trigger + Lar

 */

/*

elec: MET>35 GeV and MET+MtW>60 GeV,
muon: MET>20 GeV and MET+MtW>60 GeV, 
njet>=4, at least one btag MV1 w>0.601713 (70%)
 */

/////////////////////////
/////////////////////////
void DiLepChal::saveAsLHCO ( dbxParticle Lep, vector <dbxParticle> Jets, TVector2 Met, evt_data anevt ) {
	LHCOfile <<"  0"<<setw(16)<<anevt.event_no<<setw(6)<<"3587"<<endl;
	LHCOfile <<"  1    1    ";; Lep.dumpLHCO( LHCOfile);
	for (unsigned int i=0; i<Jets.size(); i++){
		LHCOfile <<"  "<<i<<"    4    ";;Jets.at(i).dumpLHCO( LHCOfile);
	}
	LHCOfile <<fixed<<"  6    6    0.000"<<setw(8)<<setprecision(1)<<Met.Phi()<<setw(8)<<setprecision(1)<<sqrt(Met.Mod2())<<"    0.0     0.0     0.0     0.0     0.0     0.0"<<endl;
}
int indexNoSelfParentParticle(int particle_index, vector<vector<int> > *mc_parent_index, vector<int> *mc_pdgId){

	if(mc_parent_index->at(particle_index).size()==0) return -1;
	const int PDG = fabs(mc_pdgId->at(particle_index));
	int parentindex = mc_parent_index->at(particle_index).at(0);
	int parentpdg = abs(mc_pdgId->at(parentindex ));
	while(parentpdg==PDG){
		if(mc_parent_index->at(parentindex).size()==0) return parentindex;
		parentindex = mc_parent_index->at(parentindex).at(0);
		parentpdg = fabs(mc_pdgId->at(parentindex ));
	}
	return parentindex;
}
int TrueLep(int id, vector<vector<int> > *mc_parent_index, vector<int> *mc_pdgId, vector<int> *mc_status)
{
	int truLep=0;
	for(int ii=0; ii<mc_pdgId->size(); ii++){   // Loop over all truth particles
		int pdg=abs(mc_pdgId->at(ii));
		int status=mc_status->at(ii);
		if(pdg!=id || status!=1) continue; // Check for final stat e or mu
		int parentindex=indexNoSelfParentParticle( ii,mc_parent_index, mc_pdgId);
		int parentpdg = parentindex>=0 ? abs(mc_pdgId->at(parentindex )) : 0;

		if (parentpdg==24 || parentpdg==23) truLep++; // Count l from W->lnu or Z->ll
		else if(parentpdg==15){
			parentindex=indexNoSelfParentParticle(parentindex,mc_parent_index, mc_pdgId);
			parentpdg = parentindex>=0 ? fabs(mc_pdgId->at(parentindex )) : 0;
			if (parentpdg==24 || parentpdg==23) truLep++; // Count l from Z,W->taunu->lnunu
		}

	}
	return truLep;
}
double deltaPhi(double phi1, double phi2) {
	double dPhi=std::fabs(phi1-phi2);
	if (dPhi>M_PI) dPhi=2*M_PI-dPhi;
	return dPhi;
}
bool isCosmicEvent(vector<dbxMuon> muons){
	bool isCosmicEvents=false;
	for (int i=0;i<(int)muons.size();i++) {
		for (int j=0;j<(int)muons.size();j++) {
			if (muons.at(i).d0()*muons.at(j).d0()>0) continue;
			if (fabs(muons.at(i).d0())<0.5 || fabs(muons.at(j).d0())<0.5) continue;
			//-delta(phi)>3.10
			if (deltaPhi(muons.at(i).lv().Phi(), muons.at(j).lv().Phi())<3.10) continue;
			isCosmicEvents=true;
			break;
		}
		if (isCosmicEvents) break;
	}
	return isCosmicEvents;
}
int DiLepChal::bookAdditionalHistos(){
	int retval=0;
	//additional histograms are defined here
	hNleps= new TH2F("hNleps" ,"N_{ele} vs. N_{#mu}", 6,-0.5,5.5, 6,-0.5,5.5);
	hvpt= new TH1F ("vpt", "pt of Z/W ", 100, 0.0, 500.0);
	hjpts= new TH1F ("jpts", "pt of soft jet", 50, 0.0, 200.0);
	hjpth= new TH1F ("jpth", "pt of hard jet", 50, 0.0, 200.0);
	hjjcosh= new TH1F("jjcosh" ,"Cos Angle between hard jets",50,-1.,1.);
	hjjcoss= new TH1F("jjcoss" ,"Cos Angle between soft jets",50,-1.,1.);
	hveta= new TH1F("veta" ,"eta of Z/W",50,-5.,5.);
	hvphi= new TH1F("vphi" ,"phi of Z/W",50,-5.,5.);
	hmV= new TH1F ("hmV", "m_{V}", 100, 0., 600.);
	hmW= new TH1F ("hmW", "m_{W}", 100, 0., 600.);
	hdphiW= new TH1F("hdphiW"  ,"#Delta#Phi_{l#nu}",32,-3.2,3.2);
	hdeltamin= new TH1F ("hdeltamin", "Qcand DeltaM", 50, 0.0, 500.0);
	hmet_phi= new TH1F ("hmet_phi", "MET #phi", 80, -5, 5.0);
	hmet_phio= new TH1F ("hmet_phio", "MET #phi", 80, -5, 5.0);
	hTRGs= new TH1F ("hTRGs", "Triggers", 10, 0.5, 10.5);

	hmetphi_qcd = new TH1F("hmetphi_qcd" ,"phi of Missing E_{T} qcd",80,-5.,5.);
	hmet_qcd = new TH1F("hmet_qcd "    ,"Missing E_{T} qcd",40,0.,200.);
	hjpt_qcd = new TH1F("jpt_qcd", "pt of qcd jets", 40, 0.0, 200.0);
	hept_qcd = new TH1F("ept_qcd", "pt of qcd electrons ", 80, 0.0, 400.0);
	hmpt_qcd = new TH1F("mpt_qcd", "pt of qcd muons ", 80, 0.0, 400.0);
	heeta_qcd= new TH1F("eeta_qcd" ,"eta of qcd electons",50,-5.,5.);
	hmeta_qcd= new TH1F("meta_qcd" ,"eta of qcd muons",50,-5.,5.);
	hjeta_qcd= new TH1F("jeta_qcd" ,"eta of qcd jets",50,-5.,5.);
	hephi_qcd= new TH1F("ephi_qcd" ,"phi of qcd electons",50,-5.,5.);
	hmphi_qcd= new TH1F("mphi_qcd" ,"phi of qcd muons",50,-5.,5.);
	hjphi_qcd= new TH1F("jphi_qcd" ,"phi of qcd jets",50,-5.,5.);

	int   mqhnbin = int((mqhxmax-mqhxmin)/2.+0.5);
	hmDb= new TH1F ("hmDb", "M_{D} both", mqhnbin, mqhxmin, mqhxmax);
	hmDa= new TH1F ("hmDa", "M_{D} average", mqhnbin, mqhxmin, mqhxmax);
	hmDh= new TH1F ("hmDh", "M_{D} hadronic", mqhnbin, mqhxmin, mqhxmax);
	hmDl= new TH1F ("hmDl", "M_{D} leptonic", mqhnbin, mqhxmin, mqhxmax);
	hEWl= new TH1F ("hEWl", "E_{W} leptonic", 100, 0, 1000);
	hmWt= new TH1F ("hmWt", "M_{W} transverse", 40, 0, 200);
	hmDD= new TH2F ("hmDD", "M_{D_{Wj1}} vs M_{D_{Wj2}}", mqhnbin, mqhxmin, mqhxmax,mqhnbin, mqhxmin, mqhxmax);

	icount=0;
	return retval;
}


// --------------------
int DiLepChal:: readAnalysisParams() {
	int retval=0;
	TString CardName=getDataCardName();
	minpte  = ReadCard(CardName,"minpte",20.0);
	minptm2011  = ReadCard(CardName,"minptm2011",10.0);
	minptm2012  = ReadCard(CardName,"minptm2012",10.0);
	maxetae = ReadCard(CardName,"maxetae",2.5);
	maxetam = ReadCard(CardName,"maxetam",2.5);
	minmete  = ReadCard(CardName,"minmete",35.0);
	minmetmu  = ReadCard(CardName,"minmetmu",20.0);
	minmwte = ReadCard(CardName,"minmwte",25.0);
	minmwtmu = ReadCard(CardName,"minmwtmu",60.0);
	minptj  = ReadCard(CardName,"minptj",5.0);
	maxetaj = ReadCard(CardName,"maxetaj",5.0);
	mindrjm = ReadCard(CardName,"mindrjm",0.4);
	mindrje = ReadCard(CardName,"mindrje",0.4);
	minptj1 = ReadCard(CardName,"minptj1",100);
	minptj2 = ReadCard(CardName,"minptj2",20);
	minEj2  = ReadCard(CardName,"minEj2",20);
	minetaj2 = ReadCard(CardName,"minetaj2",0.);
	mindrjj = ReadCard(CardName,"mindrjj",1.0);
	minetajj = ReadCard(CardName,"minetajj",1.0);
	maxdeltam = ReadCard(CardName,"maxdeltam",100.);
	mqhxmin = ReadCard(CardName,"mqhxmin",0.0);  // mHQ histo xmin
	mqhxmax = ReadCard(CardName,"mqhxmax",1000.0); // mHQ histo xmax
	maxMuPtCone = ReadCard(CardName,"maxMuPtCone",10.0);
	maxMuEtCone = ReadCard(CardName,"maxMuEtCone",10.0);
	maxElPtCone = ReadCard(CardName,"maxElPtCone",10.0);
	maxElEtCone = ReadCard(CardName,"maxElEtCone",10.0);
	TRGe = ReadCard(CardName,"TRGe",1);
	TRGm = ReadCard(CardName,"TRGm",1);
	jetVtxf2011 = ReadCard(CardName,"jetVtxf2011",0.75);
	jetVtxf2012 = ReadCard(CardName,"jetVtxf2012",0.50);


	////// PUT ANALYSIS PARAMETERS INTO .ROOT //////////////

	TParameter<double> *minpte_tmp=new TParameter<double> ("minpte", minpte);
	TParameter<double> *minptm2012_tmp=new TParameter<double> ("minptm2012", minptm2012);
	TParameter<double> *minptm2011_tmp=new TParameter<double> ("minptm2011", minptm2011);
	TParameter<double> *maxetae_tmp=new TParameter<double> ("maxetae", maxetae);
	TParameter<double> *maxetam_tmp=new TParameter<double> ("minpte", maxetam);
	TParameter<double> *minmete_tmp=new TParameter<double> ("minmete", minmete);
	TParameter<double> *minmetmu_tmp=new TParameter<double> ("minmetmu", minmetmu);
	TParameter<double> *minmwte_tmp=new TParameter<double> ("minmwte", minmwte);
	TParameter<double> *minmwtmu_tmp=new TParameter<double> ("minmwtmu", minmwtmu);
	TParameter<double> *minptj_tmp=new TParameter<double> ("minptj", minptj);
	TParameter<double> *maxetaj_tmp=new TParameter<double> ("maxetaj", maxetaj);
	TParameter<double> *mindrjm_tmp=new TParameter<double> ("mindrjm", mindrjm);
	TParameter<double> *mindrje_tmp=new TParameter<double> ("mindrje", mindrje);
	TParameter<double> *minptj1_tmp=new TParameter<double> ("minptj1", minptj1);
	TParameter<double> *minptj2_tmp=new TParameter<double> ("minptj2", minptj2);
	TParameter<double> *minEj2_tmp=new TParameter<double> ("minEj2", minptj2);
	TParameter<double> *minetaj2_tmp=new TParameter<double> ("minetaj2", minetaj2);
	TParameter<double> *mindrjj_tmp=new TParameter<double> ("mindrjj", mindrjj);
	TParameter<double> *minetajj_tmp=new TParameter<double> ("minetajj", minetajj);
	TParameter<double> *maxdeltam_tmp=new TParameter<double> ("maxdeltam", maxdeltam);
	TParameter<double> *mqhxmin_tmp=new TParameter<double> ("mqhxmin", mqhxmin);
	TParameter<double> *mqhxmax_tmp=new TParameter<double> ("mqhxmax", mqhxmax);
	TParameter<double> *maxMuPtCone_tmp=new TParameter<double> ("maxMuPtCone", maxMuPtCone);
	TParameter<double> *maxMuEtCone_tmp=new TParameter<double> ("maxMuEtCone", maxMuEtCone);
	TParameter<double> *maxElPtCone_tmp=new TParameter<double> ("maxElPtCone", maxElPtCone);
	TParameter<double> *maxElEtCone_tmp=new TParameter<double> ("maxElEtCone", maxElEtCone);
	TParameter<double> *TRGe_tmp=new TParameter<double> ("TRGe", TRGe);
	TParameter<double> *TRGm_tmp=new TParameter<double> ("TRGm", TRGe);
	TParameter<double> *jetVtxf2011_tmp=new TParameter<double> ("jetVtxf2011",jetVtxf2011);
	TParameter<double> *jetVtxf2012_tmp=new TParameter<double> ("jetVtxf2012",jetVtxf2012);


	minpte_tmp->Write("minpte");
	minptm2011_tmp->Write("minptm2011");
	minptm2012_tmp->Write("minptm2012");
	maxetae_tmp->Write("maxetae");
	maxetam_tmp->Write("maxetam");
	minmete_tmp->Write("minmete");
	minmetmu_tmp->Write("minmetmu");
	minmwte_tmp->Write("minmwte");
	minmwtmu_tmp->Write("minmwtmu");
	minptj_tmp->Write("minptj");
	maxetaj_tmp->Write("maxetaj");
	mindrjm_tmp->Write("mindrjm");
	mindrje_tmp->Write("mindrje");
	minptj1_tmp->Write("minptj1");
	minptj2_tmp->Write("minptj2");
	minEj2_tmp->Write("minEj2");
	minetaj2_tmp->Write("minetaj2");
	mindrjj_tmp->Write("mindrjj");
	minetajj_tmp->Write("minetajj");
	maxdeltam_tmp->Write("maxdeltam");
	mqhxmin_tmp->Write("mqhxmin");
	mqhxmax_tmp->Write("mqhxmax");
	maxMuPtCone_tmp->Write("maxMuPtCone");
	maxMuEtCone_tmp->Write("maxMuEtCone");
	maxElPtCone_tmp->Write("maxElPtCone");
	maxElEtCone_tmp->Write("maxElEtCone");
	TRGe_tmp->Write("TRGe");
	TRGm_tmp->Write("TRGm");
	jetVtxf2011_tmp->Write("jetVtxf2011");
	jetVtxf2012_tmp->Write("jetVtxf2012");

	return retval;
}

int DiLepChal:: printEfficiencies() {
	int retval=0;
	PrintEfficiencies(eff);
	return retval;
}

int DiLepChal:: initGRL() {
	grl_cut=true;
	return 0;
}



//////////////////////////////////
int DiLepChal::plotVariables(int sel){
	dbxA::plotVariables (sel);
	int retval=0;
	const Double_t small=1e-4;
	char tmp[128], idxn[128];
	sprintf(tmp,"c4-%s",cname);
	sprintf(idxn,"inv_mass-%s",cname);

	TCanvas *c4 = new TCanvas(idxn, tmp, 150, 10, 750, 500);
	c4->SetFillColor(0); c4->SetFrameFillStyle(0);
	c4->SetBorderMode(0); c4->Divide(1,3,small,small);
	c4->Draw();

	sprintf(tmp,"c5-%s",cname);
	sprintf(idxn,"aux_mass-%s",cname);
	TCanvas *c5 = new TCanvas(idxn, tmp, 40, 40, 450, 500);
	c5->SetFillStyle(0); c5->SetFrameFillStyle(0);
	c5->SetBorderMode(0); c5->Divide(1,2,small,small);
	c5->Draw();

	//
	// invariant mass  plots
	//
	c4->cd(1);
	// gPad->SetLogy(1);
	hmV->Draw();
	hmW->SetLineColor(2);
	hmW->Draw("same");
	c4->cd(2);
	hmDD->Draw();
	c4->cd(3);
	hmDb->Draw();
	//gPad->SetLogy(1);
	// 2nd set of AUXILIARY plots
	//
	c5->cd(1);
	gPad->SetLogy(1);
	hjpts->Draw();
	hjpth->SetLineColor(2);
	hjpth->Draw("same");
	c5->cd(2);
	hjjcoss->Draw();
	hjjcosh->SetLineColor(2);
	hjjcosh->Draw("same");

	/////////  SAVE /////////////////
	char aaa[64];
	if (sel & 1) {
		sprintf (aaa, "%s-4.eps",cname); c4->SaveAs(aaa);
		sprintf (aaa, "%s-5.eps",cname); c5->SaveAs(aaa);
	}
	if (sel & 2) {
		sprintf (aaa, "%s-4.png",cname); c4->SaveAs(aaa);
		sprintf (aaa, "%s-5.png",cname); c5->SaveAs(aaa);
	}

	return retval;
}
// Make Analysis for normal runs, this calls the montecarlo version with null.
int DiLepChal::makeAnalysis(vector<dbxMuon> muons, vector<dbxElectron> electrons,
		vector<dbxJet> jets, TVector2 met, evt_data anevt)
{

	if(TRGe==1 || TRGm==1)
	return DiLepChal::makeAnalysis(muons, electrons,jets, met, anevt,NULL, NULL,NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, NULL);
	else
		cout<<"Error Trying to run a Data Run in Monte Carlo Mode, Won't Make a Run"<<endl;
	return -1;

}

int DiLepChal::makeAnalysis(vector<dbxMuon> muons, vector<dbxElectron> electrons,
		vector<dbxJet> jets, TVector2 met, evt_data anevt,vector<vector<int> > *mc_child_index, vector<vector<int> > *mc_parent_index,
		vector<int> *mc_pdgId, vector<float> *mc_eta, vector<float> *mc_phi, vector<int> *mc_status,
		vector<float> *mc_pt, vector<float> *mc_E, vector<float> *mc_m, vector<float> *mc_charge)
{
	int cur_cut=1;
	TLorentzVector alepton(0,0,0,0), Whad(0,0,0,0), Wlep(0,0,0,0), Wlep2(0,0,0,0);
	dbxParticle theLepton(TLorentzVector(0,0,0,0));
	double theLeptonTrkEta = -9999;
	double theLeptonTrkPhi = -9999;
	int theLeptonIndx = -1;
	bool trgmatch = false;

	vector<dbxElectron>  goodElectrons;
	vector<dbxMuon>      goodMuons;
	vector<dbxJet>       goodJets, overlapJets;
	vector<dbxElectron>  emuoverlappedElectrons;


	if (anevt.year==2011) {
		minptm=minptm2011;
		jetVtxf=jetVtxf2011;
	} else {
		minptm=minptm2012;
		jetVtxf=jetVtxf2012;
	}

	// select electrons with a minimum set of requirements.
	// this will later be used in overlap removal etc.
	for (UInt_t i=0; i<electrons.size(); i++) {
		TLorentzVector ele4p=electrons.at(i).lv();
		if (
				(electrons.at(i).EtCone20() == 1 )  // EtCone20 @90%
				&& (electrons.at(i).PtCone30() == 1 )  // PtCone30 @90%
				&& (fabs(electrons.at(i).BestEt())  > minpte) // electron recommended Et
				&& (fabs(electrons.at(i).clusterEta() ) < maxetae )
				&& (electrons.at(i).isTight() == 1) // 1:tight, 0: not_tight -- We receive the mediums
				&& (fabs(electrons.at(i).clusterEta())>=1.52 || fabs(electrons.at(i).clusterEta())<=1.37)
				&& (fabs(electrons.at(i).Z0()) < 2)
		)   {
			goodElectrons.push_back( electrons.at(i) );
		}
	}

#ifdef __VERBOSE2__
	std::cout<<"electron cut "<<anevt.event_no<<std::endl;
#endif

	//https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/JVFUncertaintyTool
	// select jets  with a minimum set of requirements.
	for (UInt_t i=0; i<jets.size(); i++) {
		bool JVF_ok=true; // we let all jets pass by default
		TLorentzVector jet4p = jets.at(i).lv();
		if (  (fabs(jet4p.Pt())  > 25.0 ) // this corresponds to 25GeV cut
				//&& (fabs(jet4p.Eta())<= 2.5) //This can cause problems with muon overlap removal
				&& (jet4p.E() >= 0)
		)
		{
			if ( (jet4p.Pt() < 50) && fabs(jet4p.Eta())< 2.4) {
				if (fabs(jets.at(i).JetVtxF() )  <= jetVtxf) JVF_ok=false;
			}
			if (JVF_ok && (fabs(jet4p.Eta())<= 2.5) ) goodJets.push_back(jets.at(i) );
			if( JVF_ok) overlapJets.push_back(jets.at(i) );
		}
	}
#ifdef __VERBOSE2__
	std::cout<<"jet cut "<<anevt.event_no<<std::endl;
#endif

	// https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/TopCommonObjects#Muons
	// select muons with a minimum set of requirements.
	for (UInt_t i=0; i<muons.size(); i++) {
		TLorentzVector mu4p = muons.at(i).lv();
		if ( // (muons.at(i).EtCone() < maxMuEtCone) //2011
				// && (muons.at(i).PtCone() < maxMuPtCone) // 2011
				((muons.at(i).PtCone()/mu4p.Pt()) < 0.05)
				&& (mu4p.Pt()  > 25)
				&& (fabs(muons.at(i).Z0()) <= 2)
				&& (fabs(mu4p.Eta()) < 2.5)
		)  {
			goodMuons.push_back(muons.at(i));
		}
	}
#ifdef __VERBOSE2__
	std::cout<<"muon cut "<<anevt.event_no<<std::endl;
#endif
	//---------------------------
	// now the overlap removal BS.
	//---------------------------
	// overlap removal between Electron and muons
	for (UInt_t iele=0; iele<goodElectrons.size(); iele++) {
		bool el_mu_overlap=false;
		for (UInt_t imuon=0; imuon<goodMuons.size(); imuon++) {
			if (   std::fabs(goodElectrons.at(iele).TrkPhi() - goodMuons.at(imuon).MuIdPhi())<0.005
					&& std::fabs(goodElectrons.at(iele).TrkTheta() - goodMuons.at(imuon).MuIdTheta())<0.005 )
			{
				el_mu_overlap = true;
				emuoverlappedElectrons.push_back(goodElectrons.at(iele));
				//     break;
			}
		}
	}
#ifdef __VERBOSE2__
	std::cout<<"Muons: "<<muons.size()<<" goodMuons: "<<goodMuons.size()<<endl;
	std::cout<<"overlap ele_muon cut "<<anevt.event_no<<std::endl;
#endif
	// overlap removal between Muons and Jets , we remove Muons
	for (UInt_t imuon=0; imuon<goodMuons.size(); imuon++) {
		bool mu_jet_overlap=false;
		for (UInt_t ijet=0; ijet<overlapJets.size(); ijet++) {
			double dR = dbxA::deltaR(goodMuons.at(imuon).lv().Eta(), goodMuons.at(imuon).lv().Phi(),
					overlapJets.at(ijet).lv().Eta(),  overlapJets.at(ijet).lv().Phi() );
			if(dR < 0.4 && overlapJets.at(ijet).lv().Pt()>25) {mu_jet_overlap=true; break;}
		}
		if ( mu_jet_overlap ) {
			goodMuons.erase(goodMuons.begin()+imuon);
			imuon--;
		}
	}

#ifdef __VERBOSE2__
	std::cout<<"overlap muon_jet cut "<<anevt.event_no<<std::endl;
#endif
	/*
// now we add the jet eta cut, we remove Jets
   for (UInt_t ijet=0; ijet<goodJets.size(); ijet++) {
       if ( (fabs(goodJets.at(ijet).EmEta()+goodJets.at(ijet).EtaCorr()) >= maxetaj)) {
               goodJets.erase(goodJets.begin()+ijet);
               ijet--;
       }
   }
	 */
#ifdef __VERBOSE2__
	// std::cout<<"jet eta cut "<<anevt.event_no<<std::endl;
#endif
	 //overlap removal between Jets and Electrons, we remove Jets
//			for (UInt_t ijet=0; ijet<goodJets.size(); ijet++) {
//				bool jet_ele_overlap=false;
//				for (UInt_t iele=0; iele<goodElectrons.size(); iele++) {
//					double dR = dbxA::deltaR(goodElectrons.at(iele).TrkEta(), goodElectrons.at(iele).TrkPhi(),
//							goodJets.at(ijet).lv().Eta(),  goodJets.at(ijet).lv().Phi() );
//					if(dR < 0.2) {jet_ele_overlap=true; break;}
//				}
//				if ( jet_ele_overlap ) {
//					goodJets.erase(goodJets.begin()+ijet);
//					ijet--;
//				}
//			}// end of loop over jets
	 //Alternative algorithm for overlap removal. If you want to implement this. Comment out the part above.
	for (UInt_t iele=0; iele<goodElectrons.size(); iele++) {
		double dR=999;
		int loc=-1;
		for (UInt_t ijet=0; ijet<goodJets.size(); ijet++) {
			double dR2 = dbxA::deltaR(goodElectrons.at(iele).TrkEta(), goodElectrons.at(iele).TrkPhi(),
					goodJets.at(ijet).lv().Eta(),  goodJets.at(ijet).lv().Phi() );
			if(dR2<dR)
			{
				dR=dR2;
				loc=ijet;
			}

		}
		if ( dR<0.2 )
			goodJets.erase(goodJets.begin()+loc);
	}

#ifdef __VERBOSE2__
	std::cout<<"overlap jet_electron cut "<<anevt.event_no<<std::endl;
#endif
	// overlap removal between Electrons and Jets, we remove Electrons
	for (UInt_t iele=0; iele<goodElectrons.size(); iele++) {
		bool el_jet_overlap=false;
		for (UInt_t ijet=0; ijet<goodJets.size(); ijet++) {
			TLorentzVector ajet = goodJets.at(ijet).lv();
			double dR = dbxA::deltaR(goodElectrons.at(iele).TrkEta(), goodElectrons.at(iele).TrkPhi(),
					ajet.Eta(),  ajet.Phi() );
			if ((dR < 0.4) &&  (fabs(ajet.Pt())  > 25.)) { el_jet_overlap=true; break;}
		}
		if ( el_jet_overlap ) {
			goodElectrons.erase(goodElectrons.begin()+iele);
			iele--;
		}
	} // end of loop over electrons

#ifdef __VERBOSE2__
	std::cout<<"overlap Electron_jets cuts "<<anevt.event_no<<std::endl;
#endif
	double theLeptonWeight = 1;
	double theFourJetWeight = 1;
	unsigned int njets;

	double evt_weight = 1; //anevt.pileup_weight * anevt.z_vtx_weight;
#ifdef __VERBOSE2__
	std::cout<<"Cut Init"<<endl;
	std::cout<<"Event Weight "<<evt_weight<<std::endl;
#endif


	// --------- INITIAL  # events  ====> C0
	eff->Fill(cur_cut, evt_weight);
	eff->GetXaxis()->SetBinLabel(cur_cut,"all");
	cur_cut++;
	// --------- INITIAL  # events  ====> C0 //Same twice so that we can view them
	eff->Fill(cur_cut, evt_weight);
	eff->GetXaxis()->SetBinLabel(cur_cut,"all Events");
	cur_cut++;

#ifdef __VERBOSE2__
	std::cout<<"INITIAL event_c0 cuts "<<anevt.event_no<<std::endl;
#endif

	// --------- INITIAL  # events  ====> C0_1
	eff->Fill(cur_cut, evt_weight);
	eff->GetXaxis()->SetBinLabel(cur_cut,"all w");
	cur_cut++;

#ifdef __VERBOSE2__
	std::cout<<"INITIAL event_c0_1 cuts "<<anevt.event_no<<std::endl;
#endif
	// Corrupted Events ========> C18
	// LarError ========> C18_1

	eff->GetXaxis()->SetBinLabel(cur_cut,TString("LarError"));
	if (anevt.lar_Error > 1) return cur_cut;
	eff->Fill(cur_cut, evt_weight);
	cur_cut++;
#ifdef __VERBOSE2__
	std::cout<<"Truth Match ========> C18_1 "<<anevt.event_no<<std::endl;
#endif
	// Tile Error ========> C18_2
	eff->GetXaxis()->SetBinLabel(cur_cut,TString("Tile Error"));
	if (anevt.tile_Error==2) return cur_cut;
	eff->Fill(cur_cut, evt_weight);
	cur_cut++;
#ifdef __VERBOSE2__
	std::cout<<"Tile Error ========> C18_2 "<<anevt.event_no<<std::endl;
#endif
	// Incomplete Events ========> C18_3
	eff->GetXaxis()->SetBinLabel(cur_cut,TString("Incomplete Event"));
	// Closed because not everybody is using it.
	int kedi = anevt.core_Flags;
	if ( anevt.core_Flags != 0) cout<<"CoreFlags:"<<anevt.core_Flags<<endl;
	if ((kedi &0x40000)!=0){
	cout<<"CoreFlags:"<< anevt.core_Flags<<endl;
	return cur_cut;
	}
	eff->Fill(cur_cut, evt_weight);
	cur_cut++;
#ifdef __VERBOSE2__
	std::cout<<"Incomplete Events ========> C18_3 "<<anevt.event_no<<std::endl;
#endif
	// Pass Tile Trip ========> C18_4
	eff->GetXaxis()->SetBinLabel(cur_cut,TString("Tile Trip"));
	eff->Fill(cur_cut, evt_weight);
	cur_cut++;
#ifdef __VERBOSE2__
	std::cout<<"Pass Tile Trip ========> C18_4 "<<anevt.event_no<<std::endl;
#endif
	// GRL/TrueDilepton cuts =====> C1

	// ---------- GRL---------------
	// force no GRL for MC events
	if ((TRGm==1) || (TRGe==1)) {
		eff->GetXaxis()->SetBinLabel(cur_cut,"GRL");
		if ( grl_cut) {
			bool pass = isGoodRun(anevt.run_no,anevt.lumiblk_no);
			if ( !pass ) return cur_cut;
		}
		eff->Fill(cur_cut, evt_weight);
		cur_cut++;
	}

	// --------TrueDilepton Cuts-----------
	if((TRGm==2) || (TRGe==2) )
	{
		eff->GetXaxis()->SetBinLabel(cur_cut,TString("True Dilepton"));
		int countM=TrueLep(13,mc_parent_index,mc_pdgId,mc_status);
		int countE=TrueLep(11,mc_parent_index,mc_pdgId,mc_status);
		if(TRGm==2 && ( countM!=2 || countE!=0) ) return 0;
		if(TRGe==2 && ( countE!=2 || countM!=0) ) return 0;
		eff->Fill(cur_cut, evt_weight);
		cur_cut++;
	}
#ifdef __VERBOSE2__
	std::cout<<" GRL/TrueDilepton cuts ===> C1 "<<anevt.event_no<<std::endl;
#endif

	// TRIGGER cuts =====> C2
	bool trgpass = false;
	if ( TRGe>0 ) { if (anevt.TRG_e) trgpass=true; }
	if ( TRGm>0 ) { if (anevt.TRG_m) trgpass=true; }
	eff->GetXaxis()->SetBinLabel(cur_cut,TString("TRiGger"));
	if (!trgpass) return cur_cut;;
	eff->Fill(cur_cut, evt_weight);
	cur_cut++;

#ifdef __VERBOSE2__
	std::cout<<"TRIGGER cuts =====> C2 "<<anevt.event_no<<std::endl;
#endif

	// clean wrt the primary vertex number of tracks ======> C3
	eff->GetXaxis()->SetBinLabel(cur_cut,TString("Vtx Trks>=5"));
	if ((anevt.vxpType != 1 && anevt.vxpType != 3) || (anevt.vxp_maxtrk_no < 5)) return cur_cut;;
	eff->Fill(cur_cut, evt_weight);
	cur_cut++;

#ifdef __VERBOSE2__
	std::cout<<"vertex number of tracks ======> C3 "<<anevt.event_no<<std::endl;
#endif
	// Cosmic Event Removal ======> C4

	eff->GetXaxis()->SetBinLabel(cur_cut,TString("COSMICS"));
	if( isCosmicEvent(goodMuons)) return cur_cut;
	eff->Fill(cur_cut, evt_weight);
	cur_cut++;
#ifdef __VERBOSE2__
	std::cout<<"Cosmic Event Removal ======> C4 "<<anevt.event_no<<std::endl;
#endif
	// At least 1 Electron or 1 muon  ========> C5
	eff->GetXaxis()->SetBinLabel(cur_cut,TString(">=1 Lepton"));
	if ((TRGm>0) && goodMuons.size() == 0 ) return cur_cut;; //  1mu in muon channel
	if ((TRGe>0) && goodElectrons.size() == 0 ) return cur_cut;; // only 1e in e channel
	eff->Fill(cur_cut, evt_weight);
	cur_cut++;
#ifdef __VERBOSE2__
	std::cout<<"At least 1 Electron or 1 muon  ========> C5 "<<anevt.event_no<<std::endl;
#endif
	// At least 2 Electron or 2 muon  ========> C6
	eff->GetXaxis()->SetBinLabel(cur_cut,TString(">=2 Lepton"));
	if ((TRGm>0) && goodMuons.size()<2 ) return cur_cut;; //  1mu in muon channel
	if ((TRGe>0) && goodElectrons.size()<2 ) return cur_cut;; // only 1e in e channel
	eff->Fill(cur_cut, evt_weight);
	cur_cut++;

#ifdef __VERBOSE2__
	std::cout<<"At least 2 El or 2 muon  ========> C6 "<<anevt.event_no<<std::endl;
#endif
	// TRIGGER Match =====> C7

	trgmatch=false;
	if (TRGe>0) {
		for(int i=0; i<goodElectrons.size(); i++ )
			trgmatch = trgmatch|| goodElectrons.at(i).ElTriggerMatch();
	}
	if (TRGm>0) {
		for(int i=0; i<goodMuons.size(); i++ )
			trgmatch = trgmatch || goodMuons.at(i).MuTriggerMatch();
	}

	eff->GetXaxis()->SetBinLabel(cur_cut,TString("TRiGger match"));
	if (!trgmatch) return cur_cut;;
	eff->Fill(cur_cut, evt_weight);
	cur_cut++;

#ifdef __VERBOSE2__
	std::cout<<"TRIGGER Match =====> C7 "<<anevt.event_no<<std::endl;
#endif

	// electron muon overlap  =====> C8

	bool emu_overlap = false;
	if ( TRGe>0  && emuoverlappedElectrons.size()>0) emu_overlap=1;
	eff->GetXaxis()->SetBinLabel(cur_cut,TString("e-mu overlap"));
	if ( emu_overlap ) return cur_cut;
	eff->Fill(cur_cut, evt_weight);
	cur_cut++;

#ifdef __VERBOSE2__
	std::cout<<"electron muon overlap  =====> C8 "<<anevt.event_no<<std::endl;
#endif

	// BAD JET CUT ====> C9
	// clean wrt bad goodJets
	bool badjet=anevt.badjet;
	eff->GetXaxis()->SetBinLabel(cur_cut,TString("bad goodJets>0"));
	if (badjet) return cur_cut;
	eff->Fill(cur_cut, evt_weight);
	cur_cut++;

#ifdef __VERBOSE2__
	std::cout<<"BAD JET CUT ====> C9 "<<anevt.event_no<<std::endl;
#endif

	// MET CUT ====> C10
	TVector2 nmet=met;
	eff->GetXaxis()->SetBinLabel(cur_cut,TString("MET m/e >")+minmetmu+TString("/")+minmete);
	if (TRGm>0 && nmet.Mod() < 60.) return cur_cut;;
	if (TRGe>0 && nmet.Mod() < 60.) return cur_cut;;
	eff->Fill(cur_cut, evt_weight);
	cur_cut++;
#ifdef __VERBOSE2__
	std::cout<<"MET CUT ====> C10 "<<anevt.event_no<<std::endl;
#endif
	// at least 1 goodJets with tighter cuts ========> C11
	eff->GetXaxis()->SetBinLabel(cur_cut,"# Iso-goodJets>=1");
	if (goodJets.size()<1 ) return cur_cut;
	eff->Fill(cur_cut, evt_weight);
	cur_cut++;
#ifdef __VERBOSE2__
	std::cout<<"at least 1 goodJets cuts ========> C11 "<<anevt.event_no<<std::endl;
#endif
	// at least 2 goodJets with tighter cuts ========> C12
	eff->GetXaxis()->SetBinLabel(cur_cut,"# Iso-goodJets>=2");
	if (goodJets.size()<2 ) return cur_cut;
	eff->Fill(cur_cut, evt_weight);
	cur_cut++;
#ifdef __VERBOSE2__
	std::cout<<"at least 1 goodJets cuts ========> C12 "<<anevt.event_no<<std::endl;
#endif
	// Excatly 2 good leptons cut ========> C13
	eff->GetXaxis()->SetBinLabel(cur_cut,"# 2 Leptons");
	// This should be Electron + Muon while this cut also works as E or Mu
	if (TRGe>0 && ( ( goodElectrons.size() + goodMuons.size() ) !=2 ) ) return cur_cut;
	if (TRGm>0 && (goodMuons.size() + goodElectrons.size() ) !=2 ) return cur_cut;
	eff->Fill(cur_cut, evt_weight);
	cur_cut++;
#ifdef __VERBOSE2__
	std::cout<<"Excatly 2 good leptons cut ========> C13 "<<anevt.event_no<<std::endl;
#endif
	// The leptons have opposite sign CUT========> C14
	eff->GetXaxis()->SetBinLabel(cur_cut,"Opposite Sign");
	if (TRGe>0)
	{
		if(goodElectrons.at(0).q()*goodElectrons.at(1).q()!=-1) return cur_cut;
	}	
	if (TRGm>0) 	{
		if(goodMuons.at(0).q()*goodMuons.at(1).q()!=-1) return cur_cut;
	}
	eff->Fill(cur_cut, evt_weight);
	cur_cut++;
#ifdef __VERBOSE2__
	std::cout<<"Opposite Sign ========> C14 "<<anevt.event_no<<std::endl;
#endif
	// ZBoson Reconstruction ========> No Cut Here
	dbxParticle ZBos;
	if(TRGe>0)
	{
		ZBos.setTlv(goodElectrons.at(0).lv()+goodElectrons.at(1).lv());

	}
	else if(TRGm>0)
	{
		ZBos.setTlv(goodMuons.at(0).lv()+goodMuons.at(1).lv());
	}
#ifdef __VERBOSE2__
	std::cout<<"ZBoson Reconstruction ========> No Cut Here "<<anevt.event_no<<std::endl;
	std::cout<<"ZBoson Mass is: "<<ZBos.lv().M()<<endl;
#endif
	// Z Boson mass is > 15 Cut========> C15
	eff->GetXaxis()->SetBinLabel(cur_cut,"M>15");
	if(ZBos.lv().M()<15) return cur_cut;
	eff->Fill(cur_cut, evt_weight);
	cur_cut++;
#ifdef __VERBOSE2__
	std::cout<<"Z Boson mass is > 15 Cut========> C15 "<<anevt.event_no<<std::endl;
#endif
	// Z Mass Veto ========> C16

	eff->GetXaxis()->SetBinLabel(cur_cut,"Z Veto");
	if(TRGe>0 && fabs(ZBos.lv().M()-91)<10 ) return cur_cut;
	if(TRGm>0 && fabs(ZBos.lv().M()-91)<10 ) return cur_cut;
	eff->Fill(cur_cut, evt_weight);
	cur_cut++;
#ifdef __VERBOSE2__
	std::cout<<"Z Mass Veto ========> C16 "<<anevt.event_no<<std::endl;
#endif
	// Truth Match ========> C17
	eff->GetXaxis()->SetBinLabel(cur_cut,"Truth Match");
	// Currently Not implemented
	eff->Fill(cur_cut, evt_weight);
	cur_cut++;
#ifdef __VERBOSE2__
	std::cout<<"Truth Match ========> C17 "<<anevt.event_no<<std::endl;
#endif

	// b-tag on at least 1 jet ========> C19
	eff->GetXaxis()->SetBinLabel(cur_cut,"b-tag veto");
	int bj_found=0;
	for (UInt_t i=0; i<goodJets.size(); i++)
	{
		//if ((goodJets.at(i).Flavor()> 0.601713) ) bj_found++;
		if ((goodJets.at(i).Flavor()> 0.7892) ) bj_found++;
	}
	if (bj_found<1 )  return cur_cut;;
	eff->Fill(cur_cut, evt_weight);
	cur_cut++;
#ifdef __VERBOSE2__
	std::cout<<"b-tag on at least 1 jet ========> C19 "<<anevt.event_no<<std::endl;
#endif




#ifdef __VERBOSE2__
	std::cout<<"b-tag on at least 1 jet ==================> C16 "<<anevt.event_no<<std::endl;
	std::cout<<"MCH EventNumber : "<<anevt.event_no;
	std::cout << "WPileUp: " << anevt.pileup_weight << " WZwe:"<< anevt.z_vtx_weight<<endl;

	for (UInt_t i=0; i<goodElectrons.size(); i++) {
		std::cout<<"e :";
		goodElectrons.at(i).dump_b();
	}
	for (UInt_t i=0; i<goodMuons.size(); i++) {
		std::cout<<"mu :";
		goodMuons.at(i).dump_b();
	}

	for (UInt_t i=0; i<goodJets.size(); i++) {
		std::cout<<"Jets :";
		goodJets.at(i).dump_b();
	}

	std::cout<<"MET: "<<nmet.Mod()<<" MET_phi: "<<nmet.Phi()<<std::endl;
#endif
	// ............ do NOT remove .....
	dbxA::makeAnalysis (goodMuons, goodElectrons, goodJets, nmet, anevt);
	// ............ do NOT remove .....

	for (int k=cur_cut; k<=eff->GetNbinsX(); k++) eff->Fill(k, evt_weight);
	return cur_cut;;
}
