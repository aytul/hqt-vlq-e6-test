# analysis parameters card for FF wjwj semileptonic
# format is "variable = value"
minpte  = 25.0  # min pt of electrons 25
minptm2011  = 20.0  # min pt of muons
minptm2012  = 25.0  # min pt of muons
maxz0e=2           #max z0 wrt PV for electrons   //we are not using now
maxz0m=2           #max z0 wrt PV for muons
jetVtxf2011=0.75;  #jet vertex fraction of 2011
jetVtxf2012=0.50;  #jet vertex fraction of 2012
maxetae = 2.47   # max pseudorapidity of electrons
maxetam = 2.5   # max pseudorapidity of muons
minmete = 35.0
minmetmu = 20.0
minmwte = 25.0
minmwtmu = 60.0
minmet  = 20.0  # min MET
minmwtmet = 60.0  # min MWT+MET
minptj  = 25.0   # min pt of jets
maxetaj = 2.50   # max pseudorapidity of jets
mindrjm = 0.4   # min deltaR between jets and muons
mindrje = 0.2   # min deltaR between jets and electrons
minptj1 = 060.   # min pt of jet1 120=3s 160 is ok for 600
minptj2 = 020.   # min pt of jet2
minEj2 = 1.      # min E of jet2
minetaj2 = 0.0  # min pseudorapidity of jet2
mindrjj = 0.1
minetajj = 0.1  
maxdeltam = 100 # maximum mass difference between 2 candidates 
mqhxmin = 50    # for the final HQ mass histo, x-axis min
mqhxmax = 850 # for the final HQ mass histo, x-axis max
maxMuPtCone = 2.5 # Max muon Pt 
maxMuEtCone = 4.0 # Max muon Et
maxElPtCone = 4 # Max electron Pt
maxElEtCone = 3.5 # Max electron Pt
TRGe = 0 # electron Trigger Type: 0=dont trigger, 1=1st trigger (data) 2=2nd trigger (MC) etc
TRGm = 1 #     muon Trigger Type: 0=dont trigger, 1=1st trigger (data) 2=2nd trigger (MC) etc
