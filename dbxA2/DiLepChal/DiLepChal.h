#ifndef DILEPCHAL_H
#define DILEPCHAL_H

#include "dbx_a.h"
#include "ReadCard.h"
#include "LeptonicWReconstructor.h"
#include <TRandom.h>
#include <TSystem.h>
#include <fstream>
#include <iostream>
#include <iomanip>

class DiLepChal : public dbxA {
  public: 
      DiLepChal(char *aname) : dbxA ( aname)
      {
       int r=dbxA::setDir(aname);
       if (r) std::cout <<"Root Directory Set Failure in:"<<cname<<endl;
       grl_cut=false;
       char fname[256];
#ifdef __GRID__
       sprintf (fname,"MCHanalysis.lhco");
#else
       sprintf (fname,"%s.lhco",aname);
#endif
       LHCOfile.open(fname);
       LHCOfile << "#LHCO file - don't remove this line.\n";
      }

      int initGRL();
      int bookAdditionalHistos();
      int readAnalysisParams();
      int printEfficiencies();
      int plotVariables(int sel);
      int makeAnalysis(vector<dbxMuon> muons, vector<dbxElectron> electrons, 
                       vector<dbxJet> jets, TVector2 met, evt_data anevt ); 
      int makeAnalysis(vector<dbxMuon> muons, vector<dbxElectron> electrons,
      		vector<dbxJet> jets, TVector2 met, evt_data anevt,vector<vector<int> > *mc_child_index, vector<vector<int> > *mc_parent_index,
      		vector<int> *mc_pdgId, vector<float> *mc_eta, vector<float> *mc_phi, vector<int> *mc_status,
      		vector<float> *mc_pt, vector<float> *mc_E, vector<float> *mc_m, vector<float> *mc_charge);
      int saveHistos() {
        int r = dbxA::saveHistos();
        LHCOfile.close();
        return r;
      }
   private:
        void saveAsLHCO (dbxParticle, vector <dbxParticle>, TVector2, evt_data);
        bool grl_cut;
        int icount;
        ofstream LHCOfile;
        
        TH1F *hjjcosh, *hjjcoss, *he0fake, 
             *hveta, *hvphi, *hmet_phi, *hmet_phio,
             *hjpts, *hjpth, *hvpt,
             *hmV, *hmW, *hmDb, *hmDa, *hmDh, *hmDl,  
             *hEWl, *hmWt,
             *hdeltamin, *hdphiW,*hTRGs ;
        TH1F *hmet_qcd, *hjpt_qcd, *hept_qcd, *hmpt_qcd,
             *heeta_qcd, *hmeta_qcd, *hjeta_qcd,
             *hephi_qcd, *hmphi_qcd, *hjphi_qcd, *hmetphi_qcd;
        TH2F *hmDD, *hNleps;

};

#endif // DILEPCHAL_H
